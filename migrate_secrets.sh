#!/bin/bash

#You'll need to brew install python-yq or pip3 install yq
#Please see https://kislyuk.github.io/yq/
set -e
if [ -f "$1" ]; then
  sqitchTargetString=value=$(<"$1")
else
  sqitchTargetString="$1"
fi
sqitchTarget=$(echo $sqitchTargetString | jq .data.SPRING_DATASOURCE_URL | xargs echo | base64 -D | cut -c 6-)
sqitchTargetB64=$(echo $sqitchTarget | base64)
sqitchUsernameB64=$(echo $sqitchTargetString | jq .data.SPRING_DATASOURCE_USERNAME | xargs echo)
sqitchPasswordB64=$(echo $sqitchTargetString | jq .data.SPRING_DATASOURCE_PASSWORD | xargs echo)
sqitchTargetTuple=$(printf "\"%s\":\"%s\"" "SQITCH_TARGET" $sqitchTargetB64)
sqitchUsernameTuple=$(printf "\"%s\":\"%s\"" "SQITCH_USERNAME" $sqitchUsernameB64)
sqitchPasswordTuple=$(printf "\"%s\":\"%s\"" "SQITCH_PASSWORD" $sqitchPasswordB64)
sqitchMap="{$sqitchTargetTuple,$sqitchUsernameTuple, $sqitchPasswordTuple}"
updatedSecrets=$(echo $sqitchTargetString | jq . | jq --argjson v "$sqitchMap" '.data += $v' | jq .data )
echo $updatedSecrets
updatedData=$(echo "{ \"data\": {}}" | jq . | jq --argjson v "$updatedSecrets" '.data += $v')
ns=$(echo $sqitchTargetString | jq -r .metadata.namespace)
echo kubectl patch secret -n "$ns" jdbc-env --patch "'$updatedData'"
kubectl patch secret -n "$ns" jdbc-env --patch "$updatedData"
