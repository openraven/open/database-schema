#!/bin/bash
set -e
for i in $(ls sql);
do subname=$(echo $i | gawk 'match($0, /.*__(.*).sql/, array) {print array[1]}'); 
    cp sql/$i deploy/$subname.sql \
    && echo "copied $i" \
    && sqitch add "$subname" --note "'Adds the $subname migration'" \
    && echo "planned $i" \
    && $(git add sqitch.plan deploy/$subname.sql revert/$subname.sql verify/$subname.sql) \
    && echo "added $i" \
    && git ci -m "'Add $i migration to sqitch'" \
    && echo "commit $i";
done;
