-- Deploy database-schema:report-timeout to pg

BEGIN;

ALTER TABLE reportjobrun ADD COLUMN IF NOT EXISTS timedoutstates text[] DEFAULT '{}';

COMMIT;
