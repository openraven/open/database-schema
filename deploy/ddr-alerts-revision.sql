BEGIN;

ALTER TABLE ddr_alerts DROP COLUMN event_source;
ALTER TABLE ddr_alerts ADD COLUMN event_parameters jsonb;

COMMIT;