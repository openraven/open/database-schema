-- Deploy database-schema:cascade_on_delete_in_dmap to pg

BEGIN;

ALTER TABLE dmap DROP CONSTRAINT IF EXISTS dmap_assetid_fkey;

ALTER TABLE dmap ADD CONSTRAINT dmap_assetid_fkey FOREIGN KEY(arn) REFERENCES globalassets(assetid) ON DELETE CASCADE;

COMMIT;
