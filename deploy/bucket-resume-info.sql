-- Deploy database-schema:bucket-resume-info to pg

BEGIN;

CREATE TABLE IF NOT EXISTS bucketresumeinfo
(
    id           UUID NOT NULL,
    resumeobject JSONB,
    CONSTRAINT pk_bucketresumeinfo PRIMARY KEY (id)
);

insert into bucketresumeinfo (id) select id from scannerjobrunstatistics on conflict do nothing;

COMMIT;
