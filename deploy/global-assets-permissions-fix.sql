-- Deploy correct permissions to v_global*

BEGIN;
    GRANT SELECT ON v_globalassets to orvn_ro;
    GRANT SELECT ON v_globalassetscomplete to orvn_ro;
COMMIT;