-- Deploy database-schema:init-containingentity to pg

BEGIN;

UPDATE globalassets SET containingentity=coalesce(
            "locationblob" ->> 'accountLabel',
                                 "locationblob" ->> 'accountId',
                                 "locationblob" ->> 'projectName',
                                 "locationblob" ->> 'projectId'
                         );

COMMIT;
