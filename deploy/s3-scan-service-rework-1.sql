BEGIN;
CREATE TABLE IF NOT EXISTS scannerjob (
    id                          UUID PRIMARY KEY NOT NULL,
    name                        text,
    version                     text,
    owner                       text,
    managed                     boolean,
    description                 text,
    status                      text,
    datacollections             jsonb,
    sendprogressnotification    boolean,
    schedule                    text,
    assetgroup                  UUID,
    scanrestrictions            jsonb,
    runoncejob                  boolean,
    createdat                   timestamptz,
    updatedat                   timestamptz
);
COMMIT;
