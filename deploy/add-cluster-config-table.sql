-- Deploy database-schema:add-cluster-config-table to pg

BEGIN;

CREATE SCHEMA IF NOT EXISTS cluster;

CREATE TABLE IF NOT EXISTS cluster.global_configuration (
    key TEXT PRIMARY KEY,
    value JSONB
);

COMMIT;
