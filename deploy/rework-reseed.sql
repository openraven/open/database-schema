-- Deploy database-schema:rework-reseed to pg

BEGIN;

    DROP TABLE IF EXISTS scanfindingaggregatorreseedconfig;
    ALTER TABLE aggregatedassetfindings ADD COLUMN reseededat TIMESTAMP WITH TIME ZONE DEFAULT NULL;

COMMIT;
