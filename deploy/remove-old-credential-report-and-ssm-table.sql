-- Deploy database-schema:remove-old-credential-report-and-ssm-table to pg

BEGIN;

DROP TABLE IF EXISTS awscredentialreport;
DROP TABLE IF EXISTS awsusercredentialreport;
DROP TABLE IF EXISTS awsssminstance;

COMMIT;
