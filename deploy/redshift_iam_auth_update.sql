-- Deploy database-schema:redshift_iam_auth_update to pg

BEGIN;

INSERT INTO datastorecredentialmodels (id, name, assettypes, credentialmodel)
    VALUES ('IAM_REDSHIFT_ACCESS_PASSWORD', 'IAM temporary credentials for Redshift', '["AWS::Redshift::Cluster"]', '{"user": "The username to connect to (required)"}')
    ON CONFLICT (id)
    DO UPDATE SET credentialmodel = '{"user": "The username to connect to (required)"}';

COMMIT;
