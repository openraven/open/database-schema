-- Deploy database-schema:asset-workspace to pg

BEGIN;

CREATE TABLE IF NOT EXISTS assetworkspace
(
    workspaceid TEXT PRIMARY KEY NOT NULL,
    seq         BIGSERIAL        NOT NULL,
    name        TEXT             NOT NULL,
    UNIQUE (name),
    description TEXT,
    owner       TEXT,
    managed     boolean          NOT NULL,
    version     TEXT,
    filters     JSONB            NOT NULL
);
CREATE INDEX assetworkspace_seq ON assetworkspace (seq);
CREATE INDEX assetworkspace_name ON assetworkspace (name);
CREATE INDEX assetworkspace_description ON assetworkspace (description);
CREATE INDEX assetworkspace_version ON assetworkspace (version);

COMMIT;
