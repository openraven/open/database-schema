CREATE TABLE IF NOT EXISTS ddr_gcp_metadata (
    projectid   text,
    lastpolled  text,
    creatediso  timestamp with time zone,
    updatediso  timestamp with time zone,
    PRIMARY KEY (projectid)
);