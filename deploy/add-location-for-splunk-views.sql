-- Deploy database-schema:add-location-for-splunk-views to pg

BEGIN;

DROP VIEW IF EXISTS v_scanfinding;
create or replace view v_scanfinding
            (scannerjobid, scannerjobrunid, assetid, locationblob, scantarget, scantargetversion, dataclassid, findingscount,
             createdat, updatedat, scantargetchild, findingslocations, findingsdata)
as
SELECT s.scannerjobid,
       s.scannerjobrunid,
       s.assetid,
       ga.locationblob,
       s.scantarget,
       s.scantargetversion,
       s.dataclassid,
       s.findingscount,
       s.createdat,
       s.updatedat,
       s.scantargetchild,
       s.findingslocations,
       cs.findingsdata
FROM scanfinding s
         LEFT JOIN compositescanfinding cs ON s.assetid = cs.assetid AND s.scantarget = cs.scantarget AND
                                              s.scantargetchild = cs.scantargetchild AND s.dataclassid = cs.dataclassid
         LEFT JOIN globalassets ga on s.assetid = ga.assetid;




DROP VIEW IF EXISTS v_assetviolation;
create or replace view v_assetviolation
            (id, datecreated, updatedat, resourcename, policyid, ruleid, status, awsaccountid, awsregion,
             locationblob, objectcount) as
SELECT av.id,
       av.datecreated,
       av.updatedat,
       av.resourcename,
       av.policyid,
       av.ruleid,
       av.status,
       av.awsaccountid,
       av.awsregion,
       ga.locationblob,
       av.objectcount
FROM assetviolation av
         JOIN policies p ON av.policyid = p.id
         JOIN rules r ON av.ruleid = r.id
         LEFT JOIN globalassets ga on av.assetid = ga.assetid;

COMMIT;
