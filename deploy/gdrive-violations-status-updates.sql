-- Deploy database-schema:gdrive-violations-status-updates to pg

BEGIN;

-- Asset Violation changes to statuses
CREATE TABLE IF NOT EXISTS gdriveviolations_status_copy AS SELECT * FROM gdriveviolations;

UPDATE gdriveviolations
SET status = 'CLOSED'
WHERE status = 'CLOSED_AS_VERIFIED';

UPDATE gdriveviolations
SET status = 'RISK_ACCEPTED'
WHERE status = 'IGNORED';

ALTER TABLE gdriveviolations ADD CONSTRAINT gdrive_violations_status_types
    CHECK (status IN (
                      'OPEN',
                      'CLOSED',
                      'FALSE_POSITIVE',
                      'RISK_ACCEPTED',
                      'IN_PROGRESS'
        ));

COMMIT;
