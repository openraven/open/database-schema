-- Deploy database-schema:create-kong-database to pg

BEGIN;
    -- the password is set via a separate process
    -- DO NOT grant "rds_iam" to "kong", as it will cause PGPASSWORD auth to stop working
    CREATE ROLE kong WITH LOGIN;
    -- this is required for the "WITH OWNER" to work below
    GRANT kong TO root;
    GRANT kong TO orvn_superuser;
COMMIT;
-- ERROR:  CREATE DATABASE cannot run inside a transaction block
CREATE DATABASE kong WITH OWNER kong ENCODING 'utf-8';
REVOKE kong FROM root;
