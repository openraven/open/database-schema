-- Deploy database-schema:remove-info-from-v-assetviolation to pg

BEGIN;

drop view v_assetviolation;

create or replace view v_assetviolation as
SELECT av.id           as id,
       av.datecreated  as datecreated,
       av.managed      as managed,
       av.name         as name,
       av.updatedat    as updatedat,
       av.vanity_id    as vanity_id,
       av.version      as version,
       av.assetid      as assetid,
       av.resourcename as resourcename,
       av.policyid     as policyid,
       av.ruleid       as ruleid,
       av.status       as status,
       av.awsaccountid as awsaccountid,
       av.awsregion    as awsregion,
       av.size         as size,
       av.objectcount  as objectcount,
       fv.filename     as filename,
       fv.filesize     as filesize,
       fv.sourcechild  as sourcechild,
       fvdc.dataclassname as dataclassname
FROM assetviolation av RIGHT JOIN fileviolation fv on av.id = fv.assetviolationid
    RIGHT JOIN fileviolationdataclass fvdc on fv.id = fvdc.fileviolationid;

COMMIT;
