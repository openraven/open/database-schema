-- Deploy database-schema:add_objectnamepattern_to_metadataclass to pg

BEGIN;

ALTER TABLE metadataclass ADD COLUMN IF NOT EXISTS objectnamepattern text;
ALTER TABLE metadataclass DROP COLUMN IF EXISTS fileformats;

COMMIT;
