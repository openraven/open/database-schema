-- Deploy database-schema:scan-audit-log-events-finished-scans-backfill-duration to pg

BEGIN;

DO $$
    DECLARE
        finished_count integer;
        ids UUID[];
    BEGIN
        finished_count := (SELECT COUNT(*) FROM finishedscans where duration is null);

    EXECUTE 'SELECT ARRAY(SELECT finishedscans.scanjobrunid FROM finishedscans where duration is null)' INTO ids;

    RAISE NOTICE '% finished jobs will be updated, ids=[%], chaning duration from null to 0', finished_count, ids;

    UPDATE finishedscans SET duration=0 WHERE duration is null;

    alter table finishedscans alter COLUMN duration set not null;

    END;

$$;

COMMIT;
