-- Deploy database-schema:asset-details-v2 to pg

BEGIN;

CREATE TABLE IF NOT EXISTS assetdetails(
     assetid TEXT NOT NULL,
     cloudprovider TEXT NOT NULL,
     details JSONB NOT NULL,
     updatedat TIMESTAMP WITH TIME ZONE,
     PRIMARY KEY(assetid, cloudprovider)
);

COMMIT;
