-- Deploy database-schema:add_s3specializedscanfindings to pg

BEGIN;

CREATE TABLE IF NOT EXISTS s3specializeddatascanfinding () INHERITS (scanfinding);

ALTER TABLE s3specializeddatascanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);

CREATE INDEX IF NOT EXISTS s3specfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON s3specializeddatascanfinding(assetid,scantarget,scantargetchild,scannerjobid);

ALTER TABLE s3specializeddatascanfinding REPLICA IDENTITY FULL;

COMMIT;
