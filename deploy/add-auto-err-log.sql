-- Deploy database-schema:add-auto-err-log to pg

BEGIN;

CREATE TABLE IF NOT EXISTS automations_error_audit (
    id                UUID,
    event              JSONB,
    entity             JSONB,
    entity_type        TEXT,
    entity_version     UUID,
    exception          TEXT,
    exception_message  TEXT,
    exception_stack_trace JSONB,
    retry_count        INT,
    timestamp          TIMESTAMPTZ
);

COMMIT;
