-- Deploy database-schema:gdrive-access-analysis-splunk to pg

BEGIN;

CREATE VIEW v_suspiciousgoogledrivefilepermissions (
    assetid,
    fileid,
    driveid,
    filename,
    username,
    fileurl,
    suspiciousemails,
    groupemails,
    sharedbylink,
    updatedat
) AS
SELECT s.assetid,
       s.fileid,
       s.driveid,
       s.filename,
       s.username,
       s.fileurl,
       s.suspiciousemails,
       s.groupemails,
       s.sharedbylink,
       s.updatedat
FROM suspiciousgoogledrivefilepermissions s;

CREATE VIEW v_suspiciousgoogleshareddrivepermissions (
    assetid,
    driveid,
    drivename,
    suspiciousemails,
    groupemails,
    updatedat
) AS 
SELECT s.assetid,
       s.driveid,
       s.drivename,
       s.suspiciousemails,
       s.groupemails,
       s.updatedat
FROM suspiciousgoogleshareddrivepermissions s;

COMMIT;
