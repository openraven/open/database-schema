-- Deploy database-schema:rm-indexes-on-parent to pg

BEGIN;

DROP INDEX IF EXISTS scanfinding_assetid;
DROP INDEX IF EXISTS scanfinding_dataclassid;
DROP INDEX IF EXISTS scanfinding_scantarget;
DROP INDEX IF EXISTS scanfinding_scantargetchild;

COMMIT;
