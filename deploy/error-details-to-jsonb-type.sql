-- Deploy database-schema:error-details-to-jsonb-type to pg

BEGIN;

UPDATE scannerjobruns SET errordetails = json_build_array(json_build_object('errorMessage', regexp_replace(errordetails, E'[\\n\\r]+', ' ', 'g' ), 'assetid', '')) WHERE errordetails IS NOT NULL;
UPDATE scannerjobruns SET errordetails = json_build_array() WHERE errordetails IS NULL;

ALTER TABLE scannerjobruns ALTER COLUMN errordetails TYPE JSONB USING errordetails::jsonb;

COMMIT;
