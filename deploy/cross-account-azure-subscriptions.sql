-- Deploy database-schema:cross-account-azure-subscriptions to pg

BEGIN;

CREATE TABLE IF NOT EXISTS azure_subscriptions(
                                                  id uuid primary key not null,
                                                  tenant_id text not null,
                                                  client_id text not null,
                                                  subscription_id text not null
);
COMMIT;
