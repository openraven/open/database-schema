-- Deploy database-schema:add-startedat-and-finishedat-to-finished-scans to pg

ALTER TABLE finishedscans ADD COLUMN IF NOT EXISTS startedat timestamp with time zone;
ALTER TABLE finishedscans ADD COLUMN IF NOT EXISTS finishedat timestamp with time zone;