-- Deploy database-schema:support-ignore-finding to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS ignored boolean DEFAULT false;

CREATE TABLE IF NOT EXISTS auditlog(
    id              uuid NOT NULL PRIMARY KEY,
    eventtype       text NOT NULL,
    eventtarget     jsonb DEFAULT NULL,
    userdescriptor  text NOT NULL,
    message         text DEFAULT NULL,
    eventtimestamp  timestamp with time zone NOT NULL
);

CREATE INDEX auditlog_eventtype ON auditlog(eventtype);
CREATE INDEX auditlog_user ON auditlog(userdescriptor);
CREATE INDEX auditlog_eventtarget ON auditlog USING GIN(eventtarget jsonb_path_ops);
CREATE INDEX auditlog_eventtimestamp ON auditlog(eventtimestamp);

COMMIT;