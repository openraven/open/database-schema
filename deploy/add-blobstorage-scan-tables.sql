-- Deploy database-schema:add-blobstorage-scan-tables.sql to pg

BEGIN;

CREATE TABLE IF NOT EXISTS blobstoragescanfinding () INHERITS (scanfinding);
CREATE TABLE IF NOT EXISTS blobstoragemetadatascanfinding () INHERITS (scanfinding);

ALTER TABLE blobstoragescanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);
ALTER TABLE blobstoragemetadatascanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);

CREATE INDEX IF NOT EXISTS bscanfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON blobstoragescanfinding(assetid,scantarget,scantargetchild,scannerjobid);

CREATE INDEX IF NOT EXISTS bsmetadatascanfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON blobstoragemetadatascanfinding(assetid,scantarget,scantargetchild,scannerjobid);


COMMIT;