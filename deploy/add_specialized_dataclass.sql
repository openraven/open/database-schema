-- Deploy database-schema:add_specialized_dataclass to pg

BEGIN;

CREATE TABLE IF NOT EXISTS specializeddataclass
(
    id              UUID PRIMARY KEY NOT NULL,
    name            text,
    version         text,
    managed         boolean DEFAULT true,
    createdat       timestamptz,
    updatedat       timestamptz,
    owner           text,
    refid           text,
    cacheindex      INT CHECK ( cacheindex BETWEEN 0 AND 1024),
    description     text,
    status          text,
    category        text,
    processortype   text,
    parameters      jsonb,
    scope           jsonb
);

DROP TRIGGER IF EXISTS specializeddataclass_create ON specializeddataclass;
CREATE TRIGGER specializeddataclass_create
    AFTER INSERT
    ON specializeddataclass
    FOR EACH ROW
    EXECUTE PROCEDURE dataclass_populate_dataclassv2();

CREATE
OR REPLACE FUNCTION dataclassv2_update() RETURNS TRIGGER AS
$BODY$
BEGIN
    UPDATE dataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    UPDATE metadataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    UPDATE compositedataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    UPDATE specializeddataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    RETURN new;
END;
$BODY$
LANGUAGE plpgsql;

COMMIT;
