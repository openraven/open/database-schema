-- Deploy database-schema:update-rdb-fkey to pg

BEGIN;

ALTER TABLE aggregatedrelationaldatabasefindings DROP CONSTRAINT IF EXISTS aggregateddatabasefindings_assetid_fkey;
ALTER TABLE aggregatedrelationaldatabasefindings DROP CONSTRAINT IF EXISTS aggregateddatabasefindings_dataclassid_fkey;
ALTER TABLE aggregatedrelationaldatabasefindings ADD CONSTRAINT aggregateddatabasefindings_assetid_fkey FOREIGN KEY(assetid) REFERENCES globalassets(assetid) ON DELETE CASCADE;
ALTER TABLE aggregatedrelationaldatabasefindings ADD CONSTRAINT aggregateddatabasefindings_dataclassid_fkey FOREIGN KEY(dataclassid) REFERENCES dataclassv2(id) ON DELETE CASCADE;

COMMIT;
