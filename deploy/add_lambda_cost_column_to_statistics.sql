-- Deploy database-schema:add_lambda_cost_column_to_statistics to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS functioncost DOUBLE PRECISION DEFAULT 0;

COMMIT;
