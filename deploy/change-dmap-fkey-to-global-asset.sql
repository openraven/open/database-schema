-- Deploy database-schema:change-dmap-fkey-to-global-asset to pg

BEGIN;

ALTER TABLE dmap DROP CONSTRAINT dmap_arn_fkey;

ALTER TABLE dmap ADD CONSTRAINT dmap_assetid_fkey FOREIGN KEY(arn) REFERENCES globalassets(assetid);

COMMIT;
