-- Deploy database-schema:add-function-for-dcollect to pg

BEGIN;

DROP FUNCTION IF EXISTS calculate_word_frequencies;
CREATE FUNCTION calculate_word_frequencies(text[]) RETURNS jsonb AS $BODY$
    SELECT jsonb_object_agg(i,c)
    FROM (
        SELECT i, count(*) c
        FROM (
            SELECT unnest($1::text[]) i
        ) i
        GROUP BY i
        ORDER BY c DESC
    ) result_json;
$BODY$
LANGUAGE SQL;

COMMIT;
