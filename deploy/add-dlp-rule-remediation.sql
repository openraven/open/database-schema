-- Deploy database-schema:add-dlp-rule-remediation to pg

BEGIN;

ALTER TABLE gdriverules ADD COLUMN IF NOT EXISTS remediation TEXT;

COMMIT;