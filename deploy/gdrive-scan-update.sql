-- Deploy database-schema:gdrive-scan-update to pg

BEGIN;

DROP VIEW IF EXISTS v_gdrive_scanfinding_enriched;

CREATE VIEW v_gdrive_scanfinding_enriched AS SELECT g.assetid, d.name, g.scantarget, g.scantargetchild, g.findingscount, g.findingslocations, s.fileid, s.fileurl, s.filename
     FROM gdrivescanfinding g, suspiciousgoogledrivefilepermissions s, dataclassv2 d
     WHERE g.assetid = s.assetid
       AND g.scantarget = s.fileid
       AND g.assetid=s.assetid
       AND d.id = g.dataclassid;

COMMIT;
