-- Deploy database-schema:remove-drive-roots to pg

BEGIN;

-- Removes Shared Drive root nodes.
-- Identified by the fileId matching the driveId
delete from googledrivefiles where fileid in
(select externalidentifiers->>'resourceId' from globalassets where resourcetype='GOOGLEWORKSPACE::SharedDrive');

-- Removes My Drive root nodes.
delete from googledrivefiles where filename = 'My Drive' and mimetype='application/vnd.google-apps.folder';

COMMIT;
