-- Deploy database-schema:rds_iam_cred_model to pg

BEGIN;

INSERT INTO datastorecredentialmodels (id, name, assettypes, credentialmodel)
VALUES ('IAM_RDS_ACCESS_TOKEN', 'IAM access token for RDS', '["AWS::RDS::DBInstance"]', '{"user": "The username to connect to (required)"}');

COMMIT;
