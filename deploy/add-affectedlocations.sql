-- Deploy database-schema:add-affectedlocations to pg

BEGIN;

ALTER TABLE issues ADD COLUMN IF NOT EXISTS affectedlocationssummary JSONB DEFAULT json_build_array() ;

COMMIT;
