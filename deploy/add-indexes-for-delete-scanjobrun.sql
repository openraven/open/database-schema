-- Deploy database-schema:add-indexes-for-delete-scanjobrun to pg

BEGIN;

CREATE INDEX IF NOT EXISTS scannerjob_lastscannerjobrun_index
    ON scannerjob (lastscanjobrunid);

CREATE INDEX IF NOT EXISTS scannerjobpage_jobrunid_index
    ON scannerjobpage (jobrunid);

CREATE INDEX IF NOT EXISTS scanfinding_scannerjobrunid_index
    ON scanfinding (scannerjobrunid);

CREATE INDEX IF NOT EXISTS scanfinding_scannerjobid_index
    ON scanfinding (scannerjobid);

COMMIT;
