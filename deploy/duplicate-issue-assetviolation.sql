-- Deploy database-schema:duplicate-issue-assetviolation to pg

BEGIN;

CREATE TABLE IF NOT EXISTS issue_assetviolation_temp
(
    assetviolationid uuid NOT NULL,
    CONSTRAINT fk_assetviolation
        FOREIGN KEY (assetviolationid)
            REFERENCES assetviolation (id),
    issueid          uuid NOT NULL,
    CONSTRAINT fk_issue
        FOREIGN KEY (issueid)
            REFERENCES issues (id),
    PRIMARY KEY (assetviolationid, issueid)
);

INSERT INTO issue_assetviolation_temp(issueid, assetviolationid)
SELECT DISTINCT issueid, assetviolationid
FROM issue_assetviolation;

ALTER TABLE issue_assetviolation RENAME TO issue_assetviolation_dupe_copy;
ALTER TABLE issue_assetviolation_temp RENAME TO issue_assetviolation;

COMMIT;
