-- Deploy database-schema:gdrive-violations-changestream to pg

BEGIN;

ALTER TABLE orvn.public.gdriveviolations REPLICA IDENTITY FULL;

COMMIT;
