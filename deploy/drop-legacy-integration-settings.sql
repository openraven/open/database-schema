-- Deploy database-schema:drop-legacy-integration-settings to pg

BEGIN;

DROP TABLE IF EXISTS slack_integration_settings, webhook_integration_settings, jira_integration_settings;

COMMIT;
