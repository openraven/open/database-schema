-- Deploy database-schema:credential-manager to pg

BEGIN;

CREATE TABLE IF NOT EXISTS apikeyextension
(
    keyid TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL
);

COMMIT;
