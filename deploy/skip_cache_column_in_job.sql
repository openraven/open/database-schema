-- Deploy database-schema:skip_cache_column_in_job to pg

BEGIN;

ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS skipcache boolean not null default false;

COMMIT;
