-- Deploy database-schema:s3-scan-service-add-more-metrics-to-scannerjobrunstatistics to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS numberofbuckets INTEGER DEFAULT 0;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS currentbucketnumber INTEGER DEFAULT 0;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS currentbucketname TEXT;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS skippedfilecount BIGINT DEFAULT 0;

COMMIT;