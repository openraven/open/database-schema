-- Deploy database-schema:data-collections-data-class-v2 to pg

BEGIN;

ALTER TABLE IF EXISTS datacollection_dataclass RENAME TO datacollection_dataclass_old;
CREATE TABLE IF NOT EXISTS datacollection_dataclass
(
    datacollectionid   UUID NOT NULL REFERENCES datacollection(id) ON DELETE CASCADE ON UPDATE CASCADE,
    dataclassid        UUID NOT NULL REFERENCES dataclassv2(id) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO datacollection_dataclass(
                                     datacollectionid,
                                     dataclassid
) SELECT datacollectionid, dataclassid FROM datacollection_dataclass_old;

CREATE OR replace VIEW v_datacollection_dataclass AS
SELECT datacollectionid, dataclassid
FROM datacollection_dataclass;

DROP TABLE IF EXISTS datacollection_dataclass_old;

COMMIT;
