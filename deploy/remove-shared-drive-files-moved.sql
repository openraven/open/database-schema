-- Deploy database-schema:remove-shared-drive-files-moved to pg

BEGIN;

-- Removes shared drive files which have been moved to new shared drives.
-- Previously these files were not removed from the database when moved to a new shared drive
-- Leaving an old file reference to the old shared drive and the new row in the new (correct) shared drive.
delete from googledrivefiles where assetid like 'google:shared-drive:%' and assetid != concat('google:shared-drive:', driveid);

COMMIT;
