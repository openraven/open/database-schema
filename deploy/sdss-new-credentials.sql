BEGIN;

create table if not exists datastorecredentialmodels
(
    id              text not null primary key,
    name            text not null,
    assettypes      jsonb not null,
    credentialmodel jsonb not null
);

insert into datastorecredentialmodels
(id, name, assettypes, credentialmodel)
values
    ('SECRETS_MANAGER_PASSWORD', 'Secrets Manager password', '[ "AWS::RDS::DBInstance" ]', '{ "user": "the username to connect to", "arn": "ARN to Secrets Manager where the password is held" }');

insert into datastorecredentialmodels
(id, name, assettypes, credentialmodel)
values
    ('SECRETS_MANAGER_PASSWORD_IN_JSON', 'Secrets Manager password in JSON object', '[ "AWS::RDS::DBInstance" ]', '{ "user": "the username to connect to", "arn": "ARN to Secrets Manager where the password is held", "secretKey": "Name of JSON field where password is stored" }');

alter table datastoreaccesscredentials
    add column credentialname text,
    add column credentialmodeltype text,
    add column credentialmodel jsonb;

update datastoreaccesscredentials
set
    credentialname = id,
    credentialmodeltype = case
                              when secretlabel is not null then 'SECRETS_MANAGER_PASSWORD_IN_JSON'
                              else 'SECRETS_MANAGER_PASSWORD'
        end,
    credentialmodel = case
                          when secretlabel is not null then ('{ "secretKey" : "' || secretlabel || '", "arn" : "' || passwordsecretlocation || '", "user" : "' || username || '" }')::jsonb
                          else ('{ "arn" : "' || passwordsecretlocation || '", "user" : "' || username || '" }')::jsonb
        end;

alter table datastoreaccesscredentials
    add constraint datastoreaccesscredentials_credentialname_unique unique (credentialname),
    alter column credentialmodeltype set not null,
    alter column credentialmodel set not null,
    drop column secretlabel,
    drop column secretversion,
    drop column passwordsecretlocation,
    drop column username;

COMMIT;
