-- Deploy database-schema:update-dash-view to pg

BEGIN;

DROP VIEW IF EXISTS v_gdrive_dash;

CREATE VIEW v_gdrive_dash AS
SELECT main.assetid,
       main.fileid,
       main.fileurl,
       main.filename,
       ga.namelabel AS drivename,
       main.username,
       s1.sharedbylink AS anyonewithlink,
       s1.suspiciousemails,
       main.filecreatedat,
       main.fileupdatedat,
       main.fileaccessedat,
       ga.resourcetype,
       main.restrictions,
       main.orgunitpath,
       main.mimetype,
       main.updatedat,
       main.rulesviolated,
       main.lastsuccessfulscan,
       main.sharinguser,
       s2.emaildomains
FROM ((((( SELECT gf.assetid,
                  gf.fileid,
                  gf.fileurl,
                  gf.filename,
                  gf.username,
                  gf.lastsuccessfulscan,
                  gf.sharinguser,
                  (array_agg(gs.filecreatedat))[1] AS filecreatedat,
            (array_agg(gs.fileupdatedat))[1] AS fileupdatedat,
            (array_agg(gs.fileaccessedat))[1] AS fileaccessedat,
            ((array_agg(gb.configuration))[1] -> 'restrictions'::text) AS restrictions,
            gb.configuration->>'orgUnitPath' as orgunitpath,
            gf.mimetype,
            gf.updatedat,
            COALESCE(json_agg(DISTINCT gr.name) FILTER (WHERE (gr.name IS NOT NULL)), NULL::json) AS rulesviolated
           FROM ((((googledrivefiles gf
               JOIN globalassetsblob gb ON ((gb.assetid = gf.assetid)))
               LEFT JOIN googledrivestalefiles gs ON (((gf.assetid = gs.assetid) AND (gf.fileid = gs.fileid))))
               LEFT JOIN gdriveviolations gv ON ((gf.fileid = gv.nodeid)))
               LEFT JOIN gdriverules gr ON ((gv.gdriveruleid = gr.id)))
           GROUP BY gf.assetid, gf.fileid, gb.configuration) main
    JOIN globalassets ga ON ((main.assetid = ga.assetid))))
    LEFT JOIN LATERAL ( SELECT COALESCE(
                                       CASE
                                           WHEN (jsonb_array_length(gp.suspiciousemails) = 0) THEN NULL::jsonb
                                           ELSE gp.suspiciousemails
                                           END, NULL::jsonb) AS suspiciousemails,
                               gp.sharedbylink
                        FROM suspiciousgoogledrivefilepermissions gp
                        WHERE ((gp.assetid = main.assetid) AND (gp.fileid = main.fileid))) s1 ON (true))
    LEFT JOIN LATERAL ( SELECT array_to_json(COALESCE(array_agg(DISTINCT "substring"(emails.emails, '@(.+)$'::text)), '{}'::text[])) AS emaildomains
                        FROM ( SELECT jsonb_array_elements_text(gp.suspiciousemails) AS emails
                               FROM suspiciousgoogledrivefilepermissions gp
                               WHERE ((gp.assetid = main.assetid) AND (gp.fileid = main.fileid))) emails) s2 ON (true))
WHERE (ga.resourcetype = ANY (ARRAY['GOOGLEWORKSPACE::SharedDrive'::text, 'GOOGLEWORKSPACE::MyDrive'::text]));

COMMIT;
