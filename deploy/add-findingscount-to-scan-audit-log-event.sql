-- Deploy database-schema:add-findingscount-to-scan-audit-log-event to pg

ALTER TABLE scanauditlogevent ADD COLUMN IF NOT EXISTS findingscount INTEGER DEFAULT 0;

