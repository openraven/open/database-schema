-- Deploy database-schema:add_scanned_objects_stats to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS totalobjects BIGINT NOT NULL DEFAULT 0;
ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS scannedobjects BIGINT NOT NULL DEFAULT 0;
UPDATE scannerjobrunstatistics SET totalobjects = denominator;
UPDATE scannerjobrunstatistics SET scannedObjects = numerator;

COMMIT;
