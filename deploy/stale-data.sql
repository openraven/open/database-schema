BEGIN;
CREATE TABLE IF NOT EXISTS awsstaledatametrics
(
arn                        text primary key not null,
resourcetype               text,
staleness_score            text,
metric_data                jsonb not null default '{}'::jsonb,
calculation_metadata       jsonb not null default '{}'::jsonb,
creatediso                 timestamptz,
updatediso                 timestamptz
);

CREATE INDEX IF NOT EXISTS awsstaledatametrics_resource_type ON awsstaledatametrics (resourcetype);
COMMIT;