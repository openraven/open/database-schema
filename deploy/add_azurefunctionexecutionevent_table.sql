-- Deploy database-schema:add_azurefunctionexecutionevent_table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS azurefunctionexecutionevent
(
    executiondate       DATE NOT NULL,
    raminmb             INTEGER NOT NULL,
    region              TEXT NOT NULL,
    architecture        TEXT NOT NULL,
    duration            BIGINT NOT NULL DEFAULT 0,
    PRIMARY KEY (executiondate, raminmb, region, architecture)
    );

COMMIT;

COMMIT;
