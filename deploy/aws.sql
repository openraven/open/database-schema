BEGIN;
CREATE TABLE IF NOT EXISTS aws
(
    documentid                 text primary key not null,

    arn                        text,
    resourcename               text,
    resourceid                 text,
    resourcetype               text,
    awsregion                  text,
    awsaccountid               text,
    creatediso                 timestamptz,
    updatediso                 timestamptz,
    discoverysessionid         text,
    tags                       jsonb,
    configuration              jsonb,
    supplementaryConfiguration jsonb,
    discoverymeta              jsonb
);
CREATE INDEX IF NOT EXISTS aws_resource_type ON aws (resourcetype);
COMMIT;
