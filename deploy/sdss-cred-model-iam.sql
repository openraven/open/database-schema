-- Deploy database-schema:sdss-cred-model-iam to pg

BEGIN;

INSERT INTO datastorecredentialmodels
(id, name, assettypes, credentialmodel)
VALUES ('IAM_REDSHIFT_ACCESS_PASSWORD', 'IAM temporary credentials for Redshift', '["AWS::Redshift::Cluster"]', '{"user": "The username to connect to (required)", "region": "Region in which cluster is running (required)", "clusterId": "AWS cluster identifier (required)"}');

COMMIT;
