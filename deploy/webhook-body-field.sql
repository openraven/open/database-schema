-- Deploy database-schema:webhook-body-field to pg

BEGIN;

ALTER TABLE orvn.public.webhook_integration_settings ADD COLUMN IF NOT EXISTS body TEXT;

COMMIT;
