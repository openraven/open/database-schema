-- Deploy database-schema:add-sus-drive-file-perms-table to pg

BEGIN;

ALTER TABLE suspiciousgoogledrivepermissions RENAME TO suspiciousgoogledrivefilepermissions;
CREATE TABLE IF NOT EXISTS suspiciousgoogleshareddrivepermissions (
    assetid TEXT NOT NULL,
    driveid TEXT NOT NULL,
    drivename TEXT NOT NULL,
    suspiciousemails JSONB DEFAULT '[]'::jsonb,
    groupemails JSONB DEFAULT '[]'::jsonb,
    updatedat TIMESTAMPTZ,
    PRIMARY KEY(assetid)
);

COMMIT;
