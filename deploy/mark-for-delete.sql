-- Deploy database-schema:mark-for-delete to pg

BEGIN;

ALTER TABLE aws ADD COLUMN IF NOT EXISTS markedfordelete SMALLINT DEFAULT 0;

COMMIT;