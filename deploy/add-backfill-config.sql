-- Deploy database-schema:add-backfill-config to pg

BEGIN;

INSERT INTO cluster.global_configuration (key, value) VALUES ('dlp.backfill-enable', 'true'::jsonb) ON CONFLICT (key) DO NOTHING;

COMMIT;
