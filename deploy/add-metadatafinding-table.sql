-- Deploy database-schema:add-metadatafinding-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS metadatafinding () INHERITS (scanfinding);

ALTER TABLE metadatafinding ADD CONSTRAINT metadatafinding_assetid_fkey FOREIGN KEY(assetid) REFERENCES awss3bucket(arn);
ALTER TABLE metadatafinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);

CREATE INDEX IF NOT EXISTS metadatafinding_assetid_scantarget_scantargetchild_scannerjobid ON metadatafinding(assetid,scantarget,scantargetchild,scannerjobid);

COMMIT;