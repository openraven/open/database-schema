-- Deploy database-schema:policy_violation_perf_improvements to pg

BEGIN;

ALTER TABLE IF EXISTS issues ADD COLUMN affectedregions JSONB DEFAULT '[]'::jsonb;
ALTER TABLE IF EXISTS issues ADD COLUMN affectedaccounts JSONB DEFAULT '[]'::jsonb;
ALTER TABLE IF EXISTS issues ADD COLUMN violateddataclasses JSONB DEFAULT '[]'::jsonb;
ALTER TABLE IF EXISTS issues ADD COLUMN affectedassetcount bigint DEFAULT 0;


CREATE TABLE IF NOT EXISTS issuedataclasses (
                                                id uuid NOT NULL,
                                                issueid uuid NOT NULL,
                                                dataclassid uuid NOT NULL,
                                                dataclassname varchar(255),
                                                PRIMARY KEY (id),
                                                CONSTRAINT issues_fk FOREIGN KEY (issueid) REFERENCES issues (id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE INDEX dataclassname_idx ON issuedataclasses USING btree (dataclassname);

COMMIT;
