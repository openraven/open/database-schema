-- Deploy database-schema:add-containingentity-column to pg

BEGIN;

ALTER TABLE globalassets ADD COLUMN IF NOT EXISTS containingentity TEXT DEFAULT NULL;
CREATE INDEX IF NOT EXISTS globalassets_containingentity ON globalassets(containingentity);

COMMIT;
