-- Deploy database-schema:remove-files-incorrect-assetid to pg

BEGIN;

-- Remove files written with mismatched assetId issue.
-- Fixed by PROD-10009 in google-drive-files-service
-- Files in My Drives should never have a drive ID set (indicates a shared drive).
DELETE FROM googledrivefiles WHERE assetid like 'google:my-drive:%' AND driveid IS NOT NULL;

COMMIT;
