-- Deploy database-schema:dbx-views to pg

BEGIN;

create or replace view v_assetgroups as
SELECT id,
       managed,
       owner,
       description,
       name,
       type,
       createdat,
       updatedat,
       version,
       assets,
       custom,
       filtergroups,
       assettypes
FROM assetgroupsv2;

create or replace view v_policies as
SELECT id,
       datecreated,
       managed,
       name,
       updatedat,
       vanity_id,
       version,
       assetgroupid,
       description,
       enabled,
       exclusions,
       lastexecutedat,
       refid,
       schedule
FROM policies;

create or replace view v_rules as
SELECT id,
       datecreated,
       managed,
       name,
       updatedat,
       vanity_id,
       version,
       description,
       enabled,
       refid,
       severity,
       type
FROM rules;

create or replace view v_policy_rule as
SELECT ruleid, policyid
FROM policy_rule;

create or replace view v_dataclass as
SELECT id,
       name,
       version,
       owner,
       managed,
       refid,
       description,
       status,
       category,
       createdat,
       updatedat
FROM dataclass;

create or replace view v_datacollection as
SELECT id,
       name,
       version,
       managed,
       refid,
       description,
       status,
       owner,
       createdat,
       updatedat
FROM datacollection;

create or replace view v_datacollection_dataclass as
SELECT datacollectionid, dataclassid
FROM datacollection_dataclass;

create or replace view v_issues as
SELECT id,
       datecreated,
       managed,
       name,
       updatedat,
       vanity_id,
       version,
       last_closed,
       policyid,
       ruleid,
       status
FROM issues;

create or replace view v_assetviolation as
SELECT av.id           as id,
       av.datecreated  as datecreated,
       av.managed      as managed,
       av.name         as name,
       av.updatedat    as updatedat,
       av.vanity_id    as vanity_id,
       av.version      as version,
       av.assetid      as assetid,
       av.resourcename as resourcename,
       av.info         as info,
       av.policyid     as policyid,
       av.ruleid       as ruleid,
       av.status       as status,
       av.awsaccountid as awsaccountid,
       av.awsregion    as awsregion,
       av.size         as size,
       av.objectcount  as objectcount,
       fv.filename     as filename,
       fv.filesize     as filesize,
       fv.sourcechild  as sourcechild,
       fvdc.dataclassname as dataclassname
    FROM assetviolation av RIGHT JOIN fileviolation fv on av.id = fv.assetviolationid
        RIGHT JOIN fileviolationdataclass fvdc on fv.id = fvdc.fileviolationid;

create or replace view v_scannerjob as
SELECT id,
       name,
       version,
       owner,
       managed,
       description,
       status,
       datacollections,
       sendprogressnotification,
       schedule,
       assetgroup,
       scanrestrictions,
       runoncejob,
       createdat,
       updatedat,
       lastscanjobrunid
FROM scannerjob;

create or replace view v_scannerjobruns as
SELECT id, lastscannedobjectkey, lastscannedbucketarn, lastscanpagecount, lastscanstarttime, status
FROM scannerjobruns;

create or replace view v_scanfinding as
SELECT scannerjobid,
       scannerjobrunid,
       assetid,
       scantarget,
       scantargetversion,
       dataclassid,
       findingscount,
       createdat,
       updatedat,
       scantargetchild,
       findingslocations
FROM scanfinding;

COMMIT;
