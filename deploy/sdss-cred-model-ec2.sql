-- Deploy database-schema:sdss-cred-model-ec2 to pg

BEGIN;

INSERT INTO datastorecredentialmodels 
(id, name, assettypes, credentialmodel) 
VALUES ('EC2_SECRETS_MANAGER_PASSWORD', 'Secrets Manager password for EC2 instance', '["AWS::EC2::Instance"]', '{"arn": "ARN to Secrets Manager where the password is held", "user": "The username to connect to", "port": "Database port (required)", "databaseType": "Database type (one of POSTGRESQL, MARIADB, MYSQL, MSSQL or ORACLE) (required)"}');

INSERT INTO datastorecredentialmodels 
(id, name, assettypes, credentialmodel) 
VALUES ('EC2_SECRETS_MANAGER_PASSWORD_IN_JSON', 'Secrets Manager password in JSON object for EC2 instance', '["AWS::EC2::Instance"]', '{"arn": "ARN to Secrets Manager where the password is held", "user": "The username to connect to", "secretKey": "Name of JSON field where password is stored", "port": "Database port (required)", "databaseType": "Database type (one of POSTGRESQL, MARIADB, MYSQL, MSSQL or ORACLE) (required)"}');

INSERT INTO datastorecredentialmodels 
(id, name, assettypes, credentialmodel) 
VALUES ('EC2_PARAMETER_STORE_PASSWORD', 'Parameter Store password for EC2 instance', '["AWS::EC2::Instance"]', '{"user": "The username to connect to (required)", "region": "Region in which parameter is held", "parameterName": "Parameter name (required)", "port": "Database port (required)", "databaseType": "Database type (one of POSTGRESQL, MARIADB, MYSQL, MSSQL or ORACLE) (required)"}');

COMMIT;
