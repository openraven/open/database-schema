-- Deploy database-schema:unify-integration-settings to pg

BEGIN;

CREATE TABLE integrations
(
    id              uuid    NOT NULL PRIMARY KEY,
    enabled         BOOLEAN NOT NULL,
    type            TEXT    NOT NULL,
    status          TEXT    NOT NULL DEFAULT 'UNKNOWN',
    previous_status TEXT,
    exception       jsonb
);

CREATE TABLE slack_integration_settings_v2
(
    name TEXT,
    url  TEXT
) INHERITS (integrations);
ALTER TABLE slack_integration_settings_v2
    ADD PRIMARY KEY (id);

INSERT INTO slack_integration_settings_v2 (id, enabled, type, name, url)
SELECT id, enabled, 'SLACK', name, url
FROM slack_integration_settings;

CREATE TABLE webhook_integration_settings_v2
(
    name   TEXT,
    url    TEXT,
    method TEXT,
    params jsonb,
    body   TEXT
) INHERITS (integrations);
ALTER TABLE webhook_integration_settings_v2
    ADD PRIMARY KEY (id);

INSERT INTO webhook_integration_settings_v2 (id, enabled, type, name, url, method, params, body)
SELECT id, enabled, 'WEBHOOK', name, url, method, params, body
FROM webhook_integration_settings;

CREATE TABLE jira_integration_settings_v2
(
    client_id     TEXT,
    client_secret TEXT,
    site_name     TEXT,
    api_url       TEXT,
    refresh_token TEXT,
    access_token  TEXT,
    state_code    UUID
) INHERITS (integrations);
ALTER TABLE jira_integration_settings_v2
    ADD PRIMARY KEY (id);

INSERT INTO jira_integration_settings_v2 (id, enabled, type, client_id, client_secret, site_name, api_url,
                                          refresh_token, access_token, state_code)
SELECT id, enabled, 'JIRA', client_id, client_secret, site_name, api_url, refresh_token, access_token, state_code
from jira_integration_settings;

CREATE TABLE eventbridge_integration_settings_v2
(
    enable_cmdb      BOOLEAN,
    enable_seim      BOOLEAN,
    event_bus_name   TEXT,
    event_bus_region TEXT,
    role_arn         TEXT,
    external_id      TEXT
) INHERITS (integrations);
ALTER TABLE eventbridge_integration_settings_v2
    ADD PRIMARY KEY (id);

COMMIT;
