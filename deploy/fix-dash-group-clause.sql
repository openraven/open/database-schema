-- Deploy database-schema:fix-dash-group-clause to pg

BEGIN;

DROP VIEW v_gdrive_dash;

CREATE VIEW v_gdrive_dash AS SELECT gf.assetid, gf.fileid, gf.fileurl, gf.filename,
        ga.namelabel AS drivename, gf.username, gp.sharedbylink AS anyonewithlink,
        gp.suspiciousemails, gs.filecreatedat, gs.fileupdatedat, gs.fileaccessedat,
        gf.permissions as allpermissions, json_agg(gr.name) AS rulesviolated, ga.resourcetype,
        gb.configuration->'restrictions' AS restrictions, gf.mimetype,
        array_to_json(ARRAY(
            SELECT DISTINCT
            substring(email, '@(.+)$')
        FROM
            jsonb_array_elements_text(suspiciousemails) AS email
    )) AS emaildomains,
            array_to_json(ARRAY(
                SELECT DISTINCT
            substring(email, '(.+)@')
        FROM
            jsonb_array_elements_text(suspiciousemails) AS email
    )) AS emailusers
     FROM googledrivefiles gf
              INNER JOIN globalassets ga ON ga.assetid=gf.assetid
              INNER JOIN globalassetsblob gb ON gb.assetid=gf.assetid
              LEFT JOIN suspiciousgoogledrivefilepermissions gp ON gf.assetid=gp.assetid AND gf.fileid=gp.fileid
              LEFT JOIN googledrivestalefiles gs ON gf.assetid=gs.assetid AND gf.fileid=gs.fileid
              LEFT JOIN gdriveviolations gv ON gf.fileid=gv.nodeid
              LEFT JOIN gdriverules gr ON gv.gdriveruleid=gr.id
     WHERE ga.resourcetype IN ('GOOGLEWORKSPACE::SharedDrive','GOOGLEWORKSPACE::MyDrive')
     GROUP BY gf.assetid, gf.fileid, gf.fileurl, gf.filename, ga.namelabel, gf.username, gp.sharedbylink, gp.suspiciousemails, gs.filecreatedat,
              gs.fileupdatedat, gs.fileaccessedat, gf.permissions, ga.resourcetype, gb.configuration, gf.mimetype;

COMMIT;
