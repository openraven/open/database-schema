-- Deploy database-schema:set-mark-for-del-0-gdrive to pg

BEGIN;

UPDATE globalassets
SET markedfordelete=0
WHERE source = 'google-workspace';

COMMIT;
