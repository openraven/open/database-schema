-- Deploy database-schema:policy-eventing-reseed to pg

BEGIN;

CREATE TABLE IF NOT EXISTS reseed_job_run
(
    id                 BIGSERIAL PRIMARY KEY,
    operation_key      TEXT        NOT NULL,
    state              TEXT        NOT NULL,
    state_message      TEXT,
    target_topic       TEXT        NOT NULL,
    previous_state     TEXT,
    error              JSONB,
    date_created       TIMESTAMPTZ NOT NULL,
    date_started       TIMESTAMPTZ,
    date_state_changed TIMESTAMPTZ,
    pagination_args    JSONB,
    message_sequence   BIGINT
);
CREATE INDEX IF NOT EXISTS reseed_job_run_operation_key ON reseed_job_run (operation_key);
CREATE INDEX IF NOT EXISTS reseed_job_run_state ON reseed_job_run (state);

CREATE TABLE IF NOT EXISTS reseed_work_unit
(
    id             UUID        NOT NULL,
    job_run_id     BIGINT      NOT NULL,
    PRIMARY KEY (id, job_run_id),
    CONSTRAINT fk_jobrun_id
        FOREIGN KEY (job_run_id) REFERENCES reseed_job_run (id),
    description    TEXT,
    parameters JSONB,
    triggered_by   TEXT        NOT NULL,
    marked_redundant BOOLEAN   NOT NULL DEFAULT FALSE,
    time_requested TIMESTAMPTZ NOT NULL
);
CREATE INDEX IF NOT EXISTS reseed_work_unit_time_requested ON reseed_work_unit(time_requested);

CREATE TABLE IF NOT EXISTS gdriveviolations_eventing
(
    id                      uuid PRIMARY KEY NOT NULL,
    datecreated             timestamptz,
    updatedat               timestamptz,
    nodeid                  TEXT             NOT NULL,
    nodename                TEXT             NOT NULL,
    nodetype                TEXT             NOT NULL,
    gdriveruleid            uuid,
    status                  text,
    previousstatus          text,
    supplementalinformation JSONB,
    actionid                uuid,
    remediationinfo         JSONB,
    assetid                 TEXT,
    reseedid                BIGINT,
    CONSTRAINT gdrive_eventing_unique_violation UNIQUE (nodeid, nodetype, gdriveruleid)
);
CREATE INDEX IF NOT EXISTS gdriveviolations_eventing_assetid ON gdriveviolations_eventing (assetid);
CREATE INDEX IF NOT EXISTS gdriveviolations_eventing_status ON gdriveviolations_eventing (status);
CREATE INDEX IF NOT EXISTS gdriveviolations_eventing_assetid_nodeid ON gdriveviolations_eventing (assetid, nodeid);
CREATE INDEX IF NOT EXISTS gdriveviolations_eventing_reseedid ON gdriveviolations_eventing (reseedid);

COMMIT;
