-- Deploy database-schema:delete-dataClassId-from-logAuditScanEvent to pg

BEGIN;

ALTER TABLE scanauditlogevent DROP CONSTRAINT IF EXISTS scanauditlogevent_pkey;
SELECT * FROM scanauditlogevent WHERE dataclassid != '00000000-0000-0000-0000-000000000000';
DELETE FROM scanauditlogevent WHERE dataclassid != '00000000-0000-0000-0000-000000000000';
ALTER TABLE scanauditlogevent DROP COLUMN IF EXISTS dataclassid;
ALTER TABLE scanauditlogevent ADD PRIMARY KEY (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state);

COMMIT;
