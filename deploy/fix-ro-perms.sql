-- Deploy database-schema:fix-ro-perms to pg

BEGIN;

-- Basic usage privilege
GRANT USAGE ON SCHEMA cluster TO orvn_ro;
-- Read-only access to all existing tables
GRANT SELECT ON ALL TABLES IN SCHEMA cluster TO orvn_ro;
-- Read-only access to future new tables
ALTER DEFAULT PRIVILEGES IN SCHEMA cluster
    GRANT SELECT ON TABLES TO orvn_ro;

COMMIT;
