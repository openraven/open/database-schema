-- Deploy database-schema:PROD-4382 to pg

BEGIN;

create view v_active_scanfinding
            (scannerjobid, scannerjobrunid, assetid, scantarget, scantargetversion, dataclassid, findingscount,
             createdat, updatedat, scantargetchild, findingslocations)
as
SELECT scanfinding.scannerjobid,
       scanfinding.scannerjobrunid,
       scanfinding.assetid,
       scanfinding.scantarget,
       scanfinding.scantargetversion,
       scanfinding.dataclassid,
       scanfinding.findingscount,
       scanfinding.createdat,
       scanfinding.updatedat,
       scanfinding.scantargetchild,
       scanfinding.findingslocations
FROM scanfinding
WHERE alwaysfalsepositive = false AND falsepositive = false AND ignored = false;

alter table v_active_scanfinding
    owner to root;

grant select on v_active_scanfinding to orvn_ro;


create view v_active_s3scanfinding
            (scannerjobid, scannerjobrunid, assetid, scantarget, scantargetversion, dataclassid, findingscount,
             createdat, updatedat, scantargetchild, findingslocations)
as
SELECT s3scanfinding.scannerjobid,
       s3scanfinding.scannerjobrunid,
       s3scanfinding.assetid,
       s3scanfinding.scantarget,
       s3scanfinding.scantargetversion,
       s3scanfinding.dataclassid,
       s3scanfinding.findingscount,
       s3scanfinding.createdat,
       s3scanfinding.updatedat,
       s3scanfinding.scantargetchild,
       s3scanfinding.findingslocations
FROM s3scanfinding
WHERE alwaysfalsepositive = false AND falsepositive = false AND ignored = false;

alter table v_active_s3scanfinding
    owner to root;

grant select on v_active_s3scanfinding to orvn_ro;

COMMIT;
