-- Deploy database-schema:stale-data-view to pg

BEGIN;

CREATE VIEW v_googledrivestalefiles (
    assetid,
    fileid,
    driveid,
    filename,
    username,
    fileurl,
    filecreatedat,
    fileupdatedat,
    fileaccessedat,
    recordupdatedat,
    ignored
) AS
SELECT
    s.assetid,
    s.fileid,
    s.driveid,
    s.filename,
    s.username,
    s.fileurl,
    s.filecreatedat,
    s.fileupdatedat,
    s.fileaccessedat,
    s.updatedat,
    s.ignored
FROM googledrivestalefiles s;

COMMIT;
