-- Deploy database-schema:scanner-job-run-haserrors-column to pg

ALTER TABLE scannerjobruns ADD COLUMN IF NOT EXISTS haserrors BOOLEAN NOT NULL default false;
