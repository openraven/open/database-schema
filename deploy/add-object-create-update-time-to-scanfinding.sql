-- Deploy database-schema:add-object-create-update-time-to-scanfinding to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS objectcreated TIMESTAMP WITH TIME ZONE;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS objectlastupdated TIMESTAMP WITH TIME ZONE;

COMMIT;
