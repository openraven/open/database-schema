-- Deploy database-schema:scan-audit-log-events-target-length to pg

BEGIN;

alter table scanauditlogevent alter COLUMN scantarget type text;
alter table scanauditlogevent alter COLUMN childtarget type text;

COMMIT;
