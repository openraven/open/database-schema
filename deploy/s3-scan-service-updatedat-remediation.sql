-- Deploy database-schema:s3-scan-service-updatedat-remediation to pg
BEGIN;
-- Issue appeared from the September 12 on DEV.
-- We are fully OK if older jobs also updated since the NULLS cause ordering issues and such jobs sticks on top
-- log the rows that will be updated so that we can revert if necessary
 SELECT id from scannerjob where createdat IS NULL;
 SELECT id from scannerjob where updatedat IS NULL;
-- update nulls
 UPDATE scannerjob SET createdat = now() WHERE createdat IS NULL;
 UPDATE scannerjob SET updatedat = now() WHERE updatedat IS NULL;

COMMIT;
