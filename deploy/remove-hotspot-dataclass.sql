-- Deploy database-schema:remove-hotspot-dataclass to pg

BEGIN;

DELETE FROM s3scanfinding WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
DELETE FROM dataclass WHERE id = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
DELETE FROM datacollection WHERE id in ('02777843-e6ee-44bb-970a-d77cd5cdc1fa', 'ed920696-dccf-4946-9fa4-a2dd8fd97978');
DELETE FROM datacollection_dataclass WHERE datacollectionid in ('02777843-e6ee-44bb-970a-d77cd5cdc1fa', 'ed920696-dccf-4946-9fa4-a2dd8fd97978');
DELETE FROM datacollection_dataclass WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
DELETE FROM aggregatedassetfindings WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
DELETE FROM aggregatedrelationaldatabasefindings WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
DELETE FROM aggregatedscanfindings WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';

COMMIT;
