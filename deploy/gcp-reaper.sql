-- Deploy database-schema:gcp-reaper to pg

BEGIN;

ALTER TABLE globalassets DROP COLUMN IF EXISTS markedfordelete;
ALTER TABLE globalassets ADD COLUMN IF NOT EXISTS markedfordelete SMALLINT DEFAULT 0;
CREATE INDEX globalassets_markedfordelete ON globalassets(markedfordelete);

COMMIT;
