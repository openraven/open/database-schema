-- Deploy database-schema:add-sg-and-subnet-to-scanaccountandregion to pg

BEGIN;

alter table scanaccountandregion 
add column if not exists securitygroupids jsonb not null default '[]'::jsonb,
add column if not exists subnetids jsonb not null default '[]'::jsonb;

COMMIT;
