-- Deploy database-schema:assetviolation-withoutpolicyid-view to pg

BEGIN;

DROP VIEW IF EXISTS v_assetviolation_v2;
CREATE OR REPLACE VIEW v_assetviolation_v2(
                id,
                assetid,
                status,
                ruleid,
                policyids,
                datecreated,
                updatedat,
                namelabel,
                locationblob,
                externalidentifiers
) AS
SELECT av.id                  as id,
       av.assetid             as assetid,
       av.status              as status,
       av.ruleid              as ruleid,
       ARRAY_TO_JSON(policyids) as policyids,
       av.datecreated         as datecreated,
       av.updatedat           as updatedat,
       ga.namelabel           as namelabel,
       ga.locationblob        as locationblob,
       ga.externalidentifiers as externalidentifiers
FROM (
        SELECT av.id,
             ARRAY(
                     SELECT DISTINCT p.id
                     FROM policies p
                     INNER JOIN policy_rule pr ON p.id = pr.policyid
                     LEFT OUTER JOIN assetgrouptoassets ag
                          ON p.assetgroupid = ag.assetgroupid AND ag.assetid = av.assetid
                     WHERE pr.ruleid = r.id
                        AND (p.assetgroupid IS NULL OR ag.assetgroupid IS NOT NULL)
             ) as policyids
        FROM assetviolations_removepolicyid av
        INNER JOIN rules r ON av.ruleid = r.id
        INNER JOIN v_globalassets ga ON av.assetid = ga.assetid
      ) filtered
      INNER JOIN assetviolations_removepolicyid av ON filtered.id = av.id
      INNER JOIN v_globalassets ga ON av.assetid = ga.assetid;

COMMIT;
