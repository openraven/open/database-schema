-- Deploy database-schema:drop-file-count-view to pg

BEGIN;

drop materialized view if exists mat_v_gdrive_asset_file_count;

COMMIT;
