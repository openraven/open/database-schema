-- Deploy database-schema:add-report-engine-data-gen-fields to pg

BEGIN;

ALTER TABLE IF EXISTS reportjobrun ADD COLUMN IF NOT EXISTS operationkey TEXT;
ALTER TABLE IF EXISTS reportjobrun ADD COLUMN IF NOT EXISTS errorinfo JSONB;
ALTER TABLE IF EXISTS reportjobrun ADD COLUMN IF NOT EXISTS paused BOOLEAN DEFAULT FALSE;
ALTER TABLE IF EXISTS reportjobrun ADD COLUMN IF NOT EXISTS currentstatestartcount INTEGER DEFAULT 0;
ALTER TABLE IF EXISTS reportjobrun ADD COLUMN IF NOT EXISTS statechangeddate TIMESTAMPTZ;
CREATE SCHEMA IF NOT EXISTS reportengine_intermediate_tables;

COMMIT;
