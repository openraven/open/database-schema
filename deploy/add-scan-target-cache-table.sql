-- Deploy database-schema:add-scan-target-cache-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS cached.scantargetcache
(
  targethash BYTEA PRIMARY KEY,
  dataclassmask BIT(1024)
);

COMMIT;

