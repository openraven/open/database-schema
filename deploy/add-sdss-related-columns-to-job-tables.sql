-- Deploy database-schema:add-sdss-related-columns-to-job-tables to pg

BEGIN;

alter table scannerjobruns add column if not exists scantype text not null default 'UNSTRUCTURED',
                           add column if not exists errorcode text,
                           add column if not exists errordetails text;

insert into scannerjobruns (id, status, haserrors, scannerjobid, finishedat, errorcode, errordetails, scantype) 
                     select id, status, haserrors, scannerjobid, finishedat, errorcode, errordetails, 'STRUCTURED' from sdss_scannerjobruns
                     on conflict do nothing;

---

alter table scannerjob add column if not exists scantype text not null default 'UNSTRUCTURED',
                       add column if not exists scanrequest jsonb;

insert into scannerjob (id, name, version, owner, managed, description, status, sendprogressnotification, schedule, runoncejob, createdat, updatedat, maxlambdaduration, maxscancost, scanrequest, lastscanjobrunid, scantype) 
                 select id, name, version, owner, managed, description, status, sendprogressnotification, schedule, runoncejob, createdat, updatedat, maxlambdaduration, maxscancost, scanrequest, lastscanjobrunid, 'STRUCTURED' from sdss_scannerjob
                 on conflict do nothing;

---

alter table scannerjobpage add column if not exists scantype text default 'UNSTRUCTURED';

insert into scannerjobpage (id, jobrunid, jobid, pagetype, createdat, finishedat, lambdaduration, lambdamemory, lambdaarchitecture, lambdaregion, scantype) 
                     select id, jobrunid, jobid, pagetype, createdat, finishedat, lambdaduration, lambdamemory, lambdaarchitecture, lambdaregion, 'STRUCTURED' from sdss_scannerjobpage
                     on conflict do nothing;

update scannerjobpage set pagetype = case
                                        when pagetype = 'SCAN' then 'OBJECT'
                                        when pagetype = 'CANCEL' then 'FINISHED'
                                     end;

---

alter table scannerjobrunstatistics add column if not exists scantype text default 'UNSTRUCTURED';

insert into scannerjobrunstatistics (id, lastscannedobjectkey, lastscannedbucketarn, lastscanpagecount, lastscanstarttime, numerator, denominator, lastupdateat, lambdaduration, scantype) 
                              select id, lastscannedobjectkey, lastscannedbucketarn, lastscanpagecount, lastscanstarttime, numerator, denominator, lastupdateat, lambdaduration, 'STRUCTURED' from sdss_scannerjobrunstatistics
                              on conflict do nothing;


---

alter table datastoreaccessjobs drop constraint if exists datastoreaccessjobs_jobid_fkey;
alter table datastoreaccessjobs add constraint datastoreaccessjobs_jobid_fkey foreign key (jobid) references scannerjob(id);

COMMIT;
