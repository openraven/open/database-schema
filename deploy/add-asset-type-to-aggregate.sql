-- Deploy database-schema:add-asset-type-to-aggregate to pg

BEGIN;

ALTER TABLE aggregatedassetfindings ADD COLUMN IF NOT EXISTS assettype TEXT DEFAULT NULL;
ALTER TABLE aggregatedscanfindings ADD COLUMN IF NOT EXISTS assettype TEXT DEFAULT NULL;
CREATE INDEX IF NOT EXISTS aggregatedassetfindings_assettype ON aggregatedassetfindings(assettype);
CREATE INDEX IF NOT EXISTS aggregatedscanfindings_assettype ON aggregatedscanfindings(assettype);

COMMIT;
