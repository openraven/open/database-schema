-- Deploy database-schema:set-impl-type-on-findings to pg

BEGIN;

UPDATE compositescanfinding SET implementationtype='composite';

UPDATE sdss_scanfinding SET implementationtype='standard';
UPDATE s3scanfinding SET implementationtype='standard';
UPDATE cloudstoragescanfinding SET implementationtype='standard';

UPDATE s3metadatascanfinding SET implementationtype='metadata';
UPDATE cloudstoragemetadatascanfinding SET implementationtype='metadata';

COMMIT;
