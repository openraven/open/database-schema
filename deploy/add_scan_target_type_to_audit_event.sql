-- Deploy database-schema:add_scan_target_type_to_audit_event to pg

BEGIN;

ALTER TABLE scanauditlogevent ADD COLUMN IF NOT EXISTS scantargettype varchar(64) DEFAULT NULL;
ALTER TABLE sdss_scanauditlogevent ADD COLUMN IF NOT EXISTS scantargettype varchar(64) DEFAULT NULL;
ALTER TABLE finishedscans ADD COLUMN IF NOT EXISTS scantargettype varchar(64) DEFAULT NULL;
ALTER TABLE sdss_finishedscans ADD COLUMN IF NOT EXISTS scantargettype varchar(64) DEFAULT NULL;

COMMIT;
