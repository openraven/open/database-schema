-- Deploy database-schema:update-gdrive-sus-perms-table to pg

BEGIN;

-- Dev table, is empty currently. No danger from dropping.
DROP TABLE IF EXISTS suspiciousgoogledrivepermissions;
CREATE TABLE IF NOT EXISTS suspiciousgoogledrivepermissions (
    assetid TEXT NOT NULL,
    fileid TEXT NOT NULL,
    driveid TEXT,
    filename TEXT NOT NULL,
    username TEXT,
    fileurl TEXT NOT NULL,
    suspiciousemails JSONB DEFAULT '[]'::jsonb,
    groupemails JSONB DEFAULT '[]'::jsonb,
    sharedbylink BOOLEAN DEFAULT FALSE,
    updatedat TIMESTAMPTZ,
    PRIMARY KEY(assetid, fileid)
);
COMMIT;
