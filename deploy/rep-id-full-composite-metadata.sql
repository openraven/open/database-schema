-- Deploy database-schema:rep-id-full-composite-metadata to pg

BEGIN;

ALTER TABLE metadatafinding REPLICA IDENTITY FULL;
ALTER TABLE compositescanfinding REPLICA IDENTITY FULL;

COMMIT;
