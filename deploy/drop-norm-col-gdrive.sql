-- Deploy database-schema:drop-norm-col-gdrive to pg

BEGIN;

ALTER TABLE googledrivestalefiles DROP COLUMN IF EXISTS driveid;
ALTER TABLE googledrivestalefiles DROP COLUMN IF EXISTS filename;
ALTER TABLE googledrivestalefiles DROP COLUMN IF EXISTS username;
ALTER TABLE googledrivestalefiles DROP COLUMN IF EXISTS fileurl;
ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS driveid;
ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS filename;
ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS username;
ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS fileurl;
ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS permissions;

COMMIT;
