-- Deploy database-schema:metadataclass_create_tables to pg

BEGIN;

    CREATE TABLE IF NOT EXISTS metadataclass
    (
        -- OpenRavenCrudModel
        id              UUID PRIMARY KEY NOT NULL,
        name            text,
        version         text,
        managed         boolean,
        createdat       timestamptz,
        updatedat       timestamptz,
        -- MetaDataClass
        owner           text,
        refid           text,
        description     text,
        validatorfunction text,
        status          text,
        category        text,
        fileformats     jsonb,
        entries         jsonb
    );

    CREATE TABLE IF NOT EXISTS datacollection_metadataclass
    (
        datacollectionid   UUID NOT NULL REFERENCES datacollection(id) ON DELETE CASCADE ON UPDATE CASCADE,
        metadataclassid    UUID NOT NULL REFERENCES metadataclass(id) ON DELETE CASCADE ON UPDATE CASCADE
    );

COMMIT;
