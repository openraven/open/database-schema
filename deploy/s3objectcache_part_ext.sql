-- Deploy database-schema:s3objectcache_part_ext to pg

BEGIN;

CREATE SCHEMA IF NOT EXISTS partman;
CREATE EXTENSION IF NOT EXISTS pg_partman SCHEMA partman;

COMMIT;
