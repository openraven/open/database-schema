-- Deploy database-schema:add-columns-to-splunk-views to pg

BEGIN;

create or replace view v_scanfinding as
SELECT scannerjobid,
       scannerjobrunid,
       assetid,
       scantarget,
       scantargetversion,
       dataclassid,
       findingscount,
       createdat,
       updatedat,
       scantargetchild,
       findingslocations,
       falsepositive,
       alwaysfalsepositive,
       ignored
FROM scanfinding;


COMMIT;
