-- Deploy database-schema:create_lambdaexecutionevent_table to pg
BEGIN;

CREATE TABLE IF NOT EXISTS lambdaexecutionevent
(
    executiondate       DATE NOT NULL,
    lambdatype          TEXT NOT NULL,
    raminmb             INTEGER NOT NULL,
    region              TEXT NOT NULL,
    architecture        TEXT NOT NULL,
    duration            BIGINT NOT NULL DEFAULT 0,
    PRIMARY KEY (executiondate, lambdatype, raminmb, region, architecture)
);

COMMIT;
