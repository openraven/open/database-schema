-- Deploy database-schema:add_gdrive_metadata_findings_table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS gdrivemetadatascanfinding () INHERITS (scanfinding);

ALTER TABLE gdrivemetadatascanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);


CREATE INDEX IF NOT EXISTS gdrivemetadatascanfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON gdrivemetadatascanfinding(assetid,scantarget,scantargetchild,scannerjobid);

COMMIT;
