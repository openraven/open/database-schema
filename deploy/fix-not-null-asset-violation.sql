-- Deploy database-schema:fix-not-null-asset-violation to pg

BEGIN;

ALTER TABLE assetviolation ALTER COLUMN awsregion DROP NOT NULL;

COMMIT;
