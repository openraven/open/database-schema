-- Deploy database-schema:add-ext-dom-column to pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS suspiciousdomains JSONB;
CREATE INDEX IF NOT EXISTS suspiciousgoogledrivefilepermissions_suspiciousdomains ON suspiciousgoogledrivefilepermissions USING GIN (suspiciousdomains);

COMMIT;
