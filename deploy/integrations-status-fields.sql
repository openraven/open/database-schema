-- Deploy database-schema:integrations-status-fields to pg

BEGIN;

ALTER TABLE webhook_integration_settings ADD COLUMN IF NOT EXISTS status TEXT NOT NULL DEFAULT 'CONFIGURED';
ALTER TABLE webhook_integration_settings ADD COLUMN IF NOT EXISTS status_message TEXT;

ALTER TABLE slack_integration_settings ADD COLUMN IF NOT EXISTS status TEXT NOT NULL DEFAULT 'CONFIGURED';
ALTER TABLE slack_integration_settings ADD COLUMN IF NOT EXISTS status_message TEXT;

COMMIT;
