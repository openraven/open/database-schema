-- Deploy database-schema:add-match-redaction-to-dataclass to pg

BEGIN;

ALTER TABLE dataclass ADD COLUMN IF NOT EXISTS redactiontype TEXT DEFAULT 'DEFAULT';
ALTER TABLE dataclass ADD COLUMN IF NOT EXISTS redaction TEXT;

COMMIT;
