-- Deploy database-schema:issues-rule-index to pg

BEGIN;

CREATE INDEX IF NOT EXISTS issues_ruleid ON issues(ruleid);

COMMIT;
