BEGIN;

CREATE TABLE IF NOT EXISTS service_enablement (
    service_name text,
    enabled bool default false,
    PRIMARY KEY (service_name)
);

INSERT INTO service_enablement (service_name, enabled) VALUES ('AWS_DDR', false);
INSERT INTO service_enablement (service_name, enabled) VALUES ('GCP_DDR', false);

COMMIT;