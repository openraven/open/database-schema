-- Deploy database-schema:alter-table-aggregate-table to pg

BEGIN;

ALTER TABLE aggregateddatabasefindings RENAME TO aggregatedrelationaldatabasefindings;

COMMIT;
