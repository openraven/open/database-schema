-- Deploy database-schema:add-meta-finding-gdrive to pg

BEGIN;

DROP VIEW IF EXISTS v_gdrive_scanfinding_enriched;

CREATE VIEW v_gdrive_scanfinding_enriched AS
        SELECT g.assetid, d.name, g.scantarget, g.scantargetchild, g.findingscount, g.findingslocations, s.fileid, s.fileurl, s.filename
        FROM gdrivescanfinding g, googledrivefiles s, dataclassv2 d
        WHERE g.assetid = s.assetid
        AND g.scantarget = s.fileid
        AND g.assetid = s.assetid
        AND d.id = g.dataclassid
    UNION ALL
        SELECT g.assetid, d.name, g.scantarget, g.scantargetchild, g.findingscount, g.findingslocations, s.fileid, s.fileurl, s.filename
        FROM gdrivemetadatascanfinding g, googledrivefiles s, dataclassv2 d
        WHERE g.assetid = s.assetid
        AND g.scantarget = s.fileid
        AND g.assetid = s.assetid
        AND d.id = g.dataclassid;

COMMIT;
