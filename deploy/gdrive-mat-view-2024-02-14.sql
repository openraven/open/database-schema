-- Deploy database-schema:gdrive-mat-view-2024-02-14 to pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS mat_v_suspicious_user_aggregation;

CREATE MATERIALIZED VIEW mat_v_suspicious_user_aggregation AS
SELECT sp.assetid, suspiciousemail,
       jsonb_agg(jsonb_build_object('id', gf.dataclassid, 'count', gf.findingscount)) FILTER ( WHERE gf.dataclassid IS NOT NULL ) AS dataclasses,
       COUNT(DISTINCT sp.fileid) AS filecount,
       SUM(CASE WHEN gdf.lastsuccessfulscan IS NOT NULL THEN 1 ELSE 0 END) AS filescannedcount
FROM suspiciousgoogledrivefilepermissions sp
         LEFT JOIN gdrivescanfinding gf ON sp.assetid=gf.assetid AND sp.fileid=gf.scantarget
         INNER JOIN googledrivefiles gdf ON sp.assetid = gdf.assetid AND sp.fileid = gdf.fileid
         INNER JOIN globalassets assets ON sp.assetid=assets.assetid,
     jsonb_array_elements_text(suspiciousemails) AS suspiciousemail
GROUP BY sp.assetid, suspiciousemail ORDER BY filecount DESC WITH NO DATA;

CREATE UNIQUE INDEX suspicious_user_aggregation_unique
    ON mat_v_suspicious_user_aggregation(assetid,suspiciousemail);

CREATE INDEX mat_v_suspicious_user_aggregation_suspiciousemail ON mat_v_suspicious_user_aggregation(suspiciousemail);
REFRESH MATERIALIZED VIEW mat_v_suspicious_user_aggregation;

COMMIT;
