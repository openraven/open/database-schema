-- Deploy database-schema:add-analysisrun-col to pg

BEGIN;

ALTER TABLE googledriveactivitywatermark ADD COLUMN IF NOT EXISTS analysisscannerjobrunid UUID;

COMMIT;
