-- Deploy database-schema:add-scan-account-and-region to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scanaccountandregion
(
    id          UUID PRIMARY KEY NOT NULL,
    jobid       UUID NOT NULL REFERENCES scannerjob(id),
    accountid   text NOT NULL,
    region      text NOT NULL
);
CREATE INDEX IF NOT EXISTS scanaccountandregion_jobid ON scanaccountandregion(jobId);
COMMIT;
