-- Deploy database-schema:sdss-move-to-scannerjob-datacollection to pg

BEGIN;

update scannerjob
set datacollections = case
                         when scanrequest->'dataCollections' is null then '[]'::jsonb
                         else scanrequest->'dataCollections'
                      end 
where scantype = 'STRUCTURED';

COMMIT;
