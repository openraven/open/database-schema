
BEGIN;

CREATE INDEX IF NOT EXISTS scanfinding_composite_scan_idx ON scanfinding USING btree (
                                                                                      dataclassid,
                                                                                      falsepositive,
                                                                                      alwaysfalsepositive,
                                                                                      ignored
    );

CREATE INDEX IF NOT EXISTS cloudstoragemetadatascanfinding_composite_scan_idx ON cloudstoragemetadatascanfinding USING btree (
                                                                                                                              dataclassid,
                                                                                                                              falsepositive,
                                                                                                                              alwaysfalsepositive,
                                                                                                                              ignored
    );

CREATE INDEX IF NOT EXISTS cloudstoragescanfinding_composite_scan_idx ON cloudstoragescanfinding USING btree (
                                                                                                              dataclassid,
                                                                                                              falsepositive,
                                                                                                              alwaysfalsepositive,
                                                                                                              ignored
    );

CREATE INDEX IF NOT EXISTS compositescanfinding_composite_scan_idx ON compositescanfinding USING btree (
                                                                                                        dataclassid,
                                                                                                        falsepositive,
                                                                                                        alwaysfalsepositive,
                                                                                                        ignored
    );


CREATE INDEX IF NOT EXISTS s3metadatascanfinding_composite_scan_idx ON s3metadatascanfinding USING btree (
                                                                                                          dataclassid,
                                                                                                          falsepositive,
                                                                                                          alwaysfalsepositive,
                                                                                                          ignored
    );


CREATE INDEX IF NOT EXISTS s3scanfinding_composite_scan_idx ON s3scanfinding USING btree (
                                                                                          dataclassid,
                                                                                          falsepositive,
                                                                                          alwaysfalsepositive,
                                                                                          ignored
    );


CREATE INDEX IF NOT EXISTS sdss_scanfinding_composite_scan_idx ON sdss_scanfinding USING btree (
                                                                                                dataclassid,
                                                                                                falsepositive,
                                                                                                alwaysfalsepositive,
                                                                                                ignored
    );

COMMIT;
