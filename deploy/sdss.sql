begin;

create table if not exists sdss_scannerjobruns
(
    id           uuid not null primary key,
    status       text,
    haserrors    boolean default false not null,
    scannerjobid uuid,
    finishedat   timestamp with time zone
);

create table if not exists sdss_scannerjob
(
    id                       uuid not null primary key,
    name                     text,
    version                  text,
    owner                    text,
    managed                  boolean,
    description              text,
    status                   text,
    sendprogressnotification boolean,
    schedule                 text,
    runoncejob               boolean,
    createdat                timestamp with time zone,
    updatedat                timestamp with time zone,
    lambdaduration           bigint default 0 not null,
    maxlambdaduration        bigint default 0 not null,
    maxscancost              bigint default 0 not null,
    scanrequest              jsonb,
    lastscanjobrunid         uuid
        constraint lastscanjobrunid_fkey
            references sdss_scannerjobruns
);

create table if not exists sdss_scannerjobrunstatistics
(
    id                   uuid not null primary key,
    lastscannedobjectkey text,
    lastscannedbucketarn text,
    lastscanpagecount    bigint default 0,
    lastscanstarttime    timestamp,
    numerator            bigint default 0 not null,
    denominator          bigint default 0 not null,
    lastupdateat         timestamp with time zone,
    lambdaduration       bigint default 0 not null
);

create table if not exists sdss_lambdareferenceinfo
(
    id            serial primary key,
    lambdaref     text not null unique,
    createdat     timestamp with time zone not null,
    survivalcount integer,
    schedulerid   integer,
    jobrunid      text not null,
    region        text not null,
    accountid     text not null,
    functionname  text not null
);
create index if not exists sdss_lambdarefinfo_createdat on sdss_lambdareferenceinfo(createdat);

create table if not exists sdss_scannerjobpage
(
    id                 uuid not null primary key,
    jobrunid           uuid references sdss_scannerjobruns,
    jobid              uuid references sdss_scannerjob,
    pagetype           text,
    createdat          timestamp with time zone,
    finishedat         timestamp with time zone,
    lambdaduration     bigint,
    lambdamemory       integer default 0 not null,
    lambdaarchitecture text,
    lambdaregion       text
);

create table if not exists metadatadbs
(
    id                  uuid primary key not null,
    assetid             text not null,
    dbname              text not null,
    updatedat           timestamp with time zone
);

create table if not exists metadatatables
(
    id                  uuid primary key not null,
    assetid             text not null,
    dbid                uuid references metadatadbs(id),
    tablename           text not null,
    tableschema         jsonb not null,
    updatedat           timestamp with time zone
);

create table if not exists datastoreaccesscredentials
(
    id                              uuid primary key not null,
    username                        text not null,
    passwordsecretlocation          text not null,
    secretversion                   text default null,
    secretlabel                     text default null,
    updatedat                       timestamp with time zone,
    updatedby                       text
);

create table if not exists datastoreresourceaccessmapping
(
    id                  uuid primary key not null,
    assetid             text not null,
    credid              uuid not null references datastoreaccesscredentials(id),
    lastused            timestamp with time zone default null,
    success             boolean default null,
    errorcode           text default null,
    errordetails        text default null,
    createdby           text default null
);

create table if not exists datastoreaccessjobs
(
    id                  uuid primary key not null,
    mappingid           uuid references datastoreresourceaccessmapping(id),
    jobid               uuid references sdss_scannerjob(id)
);


DO $$
    BEGIN
        if not exists (select constraint_name
                       from information_schema.constraint_column_usage
                       where table_name = 'awsrdsdbinstance'  and constraint_name = 'awsrdsdbinstance_arn_unique')
        THEN
            ALTER TABLE awsrdsdbinstance ADD CONSTRAINT awsrdsdbinstance_arn_unique UNIQUE (arn);
        END IF;
    END
$$;


create table if not exists sdss_scanfinding
(
    primary key (assetid, dataclassid, scantarget, scantargetchild),
    foreign key (assetid) references awsrdsdbinstance (arn)
        on delete cascade
)
    inherits (scanfinding);

create index if not exists sdss_scanfinding_assetid_scantarget_scantargetchild_scannerjob
    on sdss_scanfinding (assetid, scantarget, scantargetchild, scannerjobid);


commit;
