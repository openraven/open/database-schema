-- Deploy database-schema:gcp-data-catalog-prep to pg

BEGIN;

ALTER TABLE cloudstoragescanfinding REPLICA IDENTITY FULL;
ALTER TABLE aggregatedassetfindings ADD COLUMN IF NOT EXISTS source TEXT DEFAULT 'aws';
ALTER TABLE aggregatedscanfindings ADD COLUMN IF NOT EXISTS source TEXT DEFAULT 'aws';
CREATE INDEX IF NOT EXISTS aggregatedassetfindings_source ON aggregatedassetfindings(source);
CREATE INDEX IF NOT EXISTS aggregatedscanfindings_source ON aggregatedscanfindings(source);

COMMIT;
