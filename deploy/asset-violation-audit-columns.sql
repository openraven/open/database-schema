-- Deploy database-schema:asset-violation-audit-columns to pg

BEGIN;

ALTER TABLE assetviolation ADD COLUMN statuschangedby TEXT;
ALTER TABLE assetviolation ADD COLUMN statuschangedbytimestamp BIGINT;

COMMIT;
