-- Deploy database-schema:ddr-change-stream to pg

BEGIN;

ALTER TABLE ddr_alerts REPLICA IDENTITY FULL;

COMMIT;
