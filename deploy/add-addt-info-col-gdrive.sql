-- Deploy database-schema:add-addt-info-col-gdrive to pg

BEGIN;

ALTER TABLE googledrivefiles ADD COLUMN IF NOT EXISTS trashed BOOLEAN DEFAULT FALSE;
ALTER TABLE googledrivefiles ADD COLUMN IF NOT EXISTS filesizebytes BIGINT DEFAULT NULL;

COMMIT;
