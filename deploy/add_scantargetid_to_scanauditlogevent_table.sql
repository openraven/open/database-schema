-- Deploy database-schema:add_scantargetid_to_scanauditlogevent_table to pg

BEGIN;

ALTER TABLE scanauditlogevent ADD COLUMN IF NOT EXISTS scantargetid TEXT;

COMMIT;
