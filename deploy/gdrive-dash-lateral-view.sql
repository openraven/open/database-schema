-- Deploy database-schema:gdrive-dash-lateral-view to pg

BEGIN;

DROP VIEW v_gdrive_dash;
CREATE VIEW v_gdrive_dash AS
SELECT main.assetid,
       main.fileid,
       main.fileurl,
       main.filename,
       ga.namelabel as drivename,
       main.username,
       sharedbylink as anyonewithlink,
       suspiciousemails,
       main.filecreatedat,
       main.fileupdatedat,
       main.fileaccessedat,
       ga.resourcetype,
       main.restrictions,
       main.mimetype,
       main.updatedat,
       main.rulesviolated,
       data_classes,
       data_collections,
       emaildomains,
       emailusers
FROM (SELECT gf.assetid,
             gf.fileid,
             gf.fileurl,
             gf.filename,
             gf.username,
             (ARRAY_AGG(gs.filecreatedat))[1]                                                    as filecreatedat,
             (ARRAY_AGG(gs.fileupdatedat))[1]                                                    as fileupdatedat,
             (ARRAY_AGG(gs.fileaccessedat))[1]                                                   as fileaccessedat,
             (ARRAY_AGG(gb.configuration))[1] -> 'restrictions'::text                            AS restrictions,
             gf.mimetype,
             gf.updatedat,
             COALESCE(json_agg(DISTINCT gr.name) FILTER (WHERE gr.name IS NOT NULL), NULL::json) AS rulesviolated
      FROM googledrivefiles gf
               JOIN globalassetsblob gb ON gb.assetid = gf.assetid
               LEFT JOIN googledrivestalefiles gs ON gf.assetid = gs.assetid AND gf.fileid = gs.fileid
               LEFT JOIN gdriveviolations gv ON gf.fileid = gv.nodeid
               LEFT JOIN gdriverules gr ON gv.gdriveruleid = gr.id
      GROUP BY gf.assetid,
               gf.fileid
) main
INNER JOIN globalassets ga ON main.assetid = ga.assetid
LEFT JOIN LATERAL (
    SELECT sf.assetid,
           sf.scantarget,
           COALESCE(json_agg(DISTINCT dc.name) FILTER (WHERE dc.name IS NOT NULL), NULL::json) AS data_classes,
           COALESCE(json_agg(DISTINCT dx.name) FILTER (WHERE dx.name IS NOT NULL), NULL::json) AS data_collections
    FROM gdrivescanfinding sf
    INNER JOIN dataclassv2 dc ON dc.id = sf.dataclassid
    INNER JOIN datacollection_dataclass dj ON dj.dataclassid = dc.id
    INNER JOIN datacollection dx ON dx.id = dj.datacollectionid
    WHERE sf.scantarget = main.fileid
          AND sf.assetid = main.assetid
    GROUP BY sf.assetid, sf.scantarget
) dclasses ON TRUE
LEFT JOIN LATERAL (
    SELECT COALESCE(
                   CASE
                       WHEN jsonb_array_length(gp.suspiciousemails) = 0 THEN NULL::jsonb
                       ELSE gp.suspiciousemails
                       END, NULL::jsonb) AS suspiciousemails,
           sharedbylink
    FROM suspiciousgoogledrivefilepermissions gp
    WHERE gp.assetid = main.assetid
      AND gp.fileid = main.fileid
) s1 ON TRUE
LEFT JOIN LATERAL (
    SELECT array_to_json(
                   COALESCE(
                           ARRAY_AGG(DISTINCT (substring(emails, '@(.+)$'::text))),
                           '{}'
                   )
           ) as emaildomains,
           array_to_json(
                   COALESCE(
                           ARRAY_AGG(DISTINCT (substring(emails, '(.+)@'::text))),
                           '{}'
                   )
           ) as emailusers
    FROM (SELECT jsonb_array_elements_text(gp.suspiciousemails) emails
          FROM suspiciousgoogledrivefilepermissions gp
          WHERE gp.assetid = main.assetid
            AND gp.fileid = main.fileid) emails
) s2 ON TRUE
WHERE ga.resourcetype IN ('GOOGLEWORKSPACE::SharedDrive', 'GOOGLEWORKSPACE::MyDrive');

COMMIT;
