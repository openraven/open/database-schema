BEGIN;

INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.allowedDomains', jsonb_build_array()) ON CONFLICT (key) DO NOTHING;
INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.deniedDomains', jsonb_build_array()) ON CONFLICT (key) DO NOTHING;

COMMIT;
