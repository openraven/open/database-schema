-- Deploy database-schema:index-sdss-scanfinding to pg

BEGIN;

CREATE INDEX IF NOT EXISTS sdss_scanfinding_tablename ON sdss_scanfinding(tablename);
CREATE INDEX IF NOT EXISTS sdss_scanfinding_schemaname ON sdss_scanfinding(schemaname);
CREATE INDEX IF NOT EXISTS sdss_scanfinding_columnname ON sdss_scanfinding(columnname);
CREATE INDEX IF NOT EXISTS sdss_scanfinding_scannerjobrunid ON sdss_scanfinding(scannerjobrunid);

COMMIT;
