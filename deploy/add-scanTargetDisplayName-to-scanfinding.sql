-- Deploy database-schema:add-scanTargetDisplayName-to-scanfinding to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS scantargetdisplayname TEXT;

COMMIT;
