-- Deploy database-schema:remediation-action-log to pg

BEGIN;

CREATE TABLE IF NOT EXISTS remediationactionlog (
    id uuid NOT NULL PRIMARY KEY,
    remediationaction TEXT NOT NULL, -- the remediation that was performed (always `UNSHARE` for now)
    success BOOLEAN NOT NULL, -- whether the modification succeeded (API calls returned successfully)

    assetid TEXT NOT NULL,  -- globalassets table id of the asset containing the modified resource
    nodetype TEXT NOT NULL, -- Type of cloud resource that was modified (always file for now)
    nodeid TEXT NOT NULL,   -- id of entity modified (the file id for now)

    timestamp TIMESTAMP WITH TIME ZONE NOT NULL, -- timestamp at which this modification occurred
    securityprincipal TEXT NOT NULL,     -- "credential" used to preform the modification; for now, always a user's email
    securityprincipaltype TEXT NOT NULL, -- type of "credential" above; for now always `googleImpersonatedUser`

    violationid UUID NOT NULL, -- id from source table of violation being remediated
    violationtable TEXT NOT NULL, -- name of source table, always `gdriveviolations` for now

    remediationTrigger TEXT NOT NULL, -- what invoked the remediation action; either `automation` or `api`
    -- JSON-serialized snapshots of the rule and event to which we're responding;
    -- Only present when `remediationtrigger` is `automation`
    triggeringautomationrule JSONB,
    triggeringevent JSONB,
    -- Oauth user id of the logged-in user invoking the remediation in the UI
    -- only present when remediationtrigger is `api`
    oauthuser TEXT,

    -- exception details, only present when success != true
    exceptionmessage TEXT,
    exceptionstack_trace TEXT
);

COMMIT;
