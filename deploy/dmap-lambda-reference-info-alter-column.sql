-- Deploy database-schema:dmap-lambda-reference-info-alter-column to pg

BEGIN;

alter table dmaplambdareferenceinfo alter column jobrunid drop not null;

COMMIT;
