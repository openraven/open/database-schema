-- Deploy database-schema:recreate_caching_table to pg

BEGIN;

DROP TABLE IF EXISTS cached.s3objects CASCADE;
DELETE FROM partman.part_config WHERE parent_table = 'cached.s3objects';
DROP TABLE IF EXISTS partman.template_cached_s3objects;

CREATE TABLE cached.s3objects(
                                 id BIGSERIAL PRIMARY KEY,
                                 hashed_id BYTEA

) PARTITION BY RANGE (id);
CREATE INDEX cached_s3objects_hashed_long_index ON cached.s3objects(hashed_id);
-- https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/PostgreSQL_Partitions.html#PostgreSQL_Partitions.create_parent
SELECT partman.create_parent(p_parent_table =>'cached.s3objects', p_control => 'id', p_type => 'native', p_interval => '10000000000', p_premake => '100');

COMMIT;
