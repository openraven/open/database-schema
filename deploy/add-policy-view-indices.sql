-- Deploy database-schema:add-policy-view-indices to pg

BEGIN;

CREATE INDEX IF NOT EXISTS suspiciousgoogledrivefilepermissions_sharedbylink ON suspiciousgoogledrivefilepermissions(sharedbylink);
CREATE INDEX IF NOT EXISTS googledrivestalefiles_fileaccessedat ON googledrivestalefiles(fileaccessedat);
CREATE INDEX IF NOT EXISTS googledrivestalefiles_filecreatedat ON googledrivestalefiles(filecreatedat);
CREATE INDEX IF NOT EXISTS googledrivestalefiles_fileupdatedat ON googledrivestalefiles(fileupdatedat);

COMMIT;