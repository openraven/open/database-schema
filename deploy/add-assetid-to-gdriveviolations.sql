-- Deploy database-schema:add-assetid-to-gdriveviolations to pg

BEGIN;

ALTER TABLE IF EXISTS gdriveviolations
ADD assetid TEXT;

CREATE INDEX IF NOT EXISTS gdriveviolations_assetid ON gdriveviolations(assetid);
CREATE INDEX IF NOT EXISTS gdriveviolations_status ON gdriveviolations(status);
CREATE INDEX IF NOT EXISTS gdriveviolations_assetid_nodeid ON gdriveviolations(assetid, nodeid);

DROP VIEW v_gdriveviolations;

CREATE VIEW v_gdriveviolations AS SELECT v.id,
     v.nodeid,
     v.nodename,
     v.nodetype,
     v.updatedat,
     v.datecreated,
     v.gdriveruleid,
     r.name AS rulename,
     r.severity,
     v.status,
     v.previousstatus,
     v.supplementalinformation,
     gf.mimetype,
     v.assetid     
FROM gdriveviolations v
       INNER JOIN gdriverules r ON v.gdriveruleid = r.id
       INNER JOIN googledrivefiles gf ON gf.fileid=v.nodeid;

COMMIT;
