-- Deploy database-schema:metadataclass-fileformats-column to pg

BEGIN;

ALTER TABLE metadataclass ADD COLUMN IF NOT EXISTS fileformats jsonb;

COMMIT;
