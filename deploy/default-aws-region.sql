-- Deploy database-schema:default-aws-region to pg

BEGIN;

-- Here we will only touch violations because there is a locking problem with aws.
-- In an ideal world, we would fix the problem in aws too.
-- We do a DELETE instead of an UPDATE here because we'd like to see the data
-- recreate correctly.
DELETE FROM assetviolation WHERE awsregion IS NULL;
ALTER TABLE assetviolation ALTER COLUMN awsregion SET DEFAULT 'aws-global';
-- A not null condition is needed because defaults only work when users do not
-- explicitly set a value for the column.
-- Without it this would create a row with a null value:
--  UPDATE assetviolation SET awsregion = null WHERE id='some-id'
ALTER TABLE assetviolation ALTER COLUMN awsregion SET NOT NULL;

COMMIT;
