-- Deploy database-schema:gdrive-policy-event-debezium-change-streams to pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions REPLICA IDENTITY FULL;
ALTER TABLE googledrivefiles REPLICA IDENTITY FULL;
ALTER TABLE googledrivestalefiles REPLICA IDENTITY FULL;

COMMIT;
