-- Deploy database-schema:add-gin-index-drive-perm to pg

BEGIN;

CREATE INDEX IF NOT EXISTS suspiciousgoogleshareddrivepermissions_permissions_gin ON suspiciousgoogleshareddrivepermissions USING GIN(permissions);

COMMIT;
