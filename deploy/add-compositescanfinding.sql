-- Deploy database-schema:add-compositescanfinding to pg

BEGIN;

CREATE TABLE IF NOT EXISTS compositescanfinding (
    findingsdata JSONB NOT NULL DEFAULT '{}'::jsonb,
    PRIMARY KEY (assetid, dataclassid, scantarget, scantargetchild)
) INHERITS (scanfinding);

CREATE INDEX IF NOT EXISTS compositescanfinding_dataclass ON compositescanfinding(dataclassid);
CREATE INDEX IF NOT EXISTS compositescanfinding_scantarget_scantargetchild ON compositescanfinding(scantarget, scantargetchild);
CREATE INDEX IF NOT EXISTS compositescanfinding_assetid ON compositescanfinding(assetid);
CREATE INDEX IF NOT EXISTS compositescanfinding_assetid_scantarget_scantargetch_scannerjob
    ON compositescanfinding (assetid, scantarget, scantargetchild, scannerjobid);

COMMIT;
