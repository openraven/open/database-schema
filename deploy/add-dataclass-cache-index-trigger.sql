-- Deploy: add-dataclass-cache-index-trigger.sql

DROP SEQUENCE IF EXISTS custom_dataclasses_global_seq;

CREATE OR REPLACE FUNCTION populate_cache_index_func() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  NEW.cacheindex := (SELECT max(val) + 1 FROM (
    SELECT COALESCE(MAX(cacheindex), 0) as val FROM dataclass
      UNION
    SELECT COALESCE(MAX(cacheindex), 0) as val FROM metadataclass
) a);
RETURN NEW;
end
$$;

DROP TRIGGER IF EXISTS dataclass_cache_index_trg ON dataclass;
CREATE TRIGGER dataclass_cache_index_trg
    BEFORE INSERT
    ON dataclass
    FOR EACH ROW
    EXECUTE PROCEDURE populate_cache_index_func();

DROP TRIGGER IF EXISTS metadataclass_cache_index_trg ON metadataclass;
CREATE TRIGGER metadataclass_cache_index_trg
    BEFORE INSERT
    ON metadataclass
    FOR EACH ROW
    EXECUTE PROCEDURE populate_cache_index_func();

