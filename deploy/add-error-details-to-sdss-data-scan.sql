
BEGIN;

alter table sdss_scannerjobruns add column if not exists errorcode text default null;
alter table sdss_scannerjobruns add column if not exists errordetails text default null;

COMMIT;