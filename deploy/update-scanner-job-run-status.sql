DO $$
    DECLARE
        sj_count integer;
        ids UUID[];
    BEGIN
        sj_count := (SELECT COUNT(*) FROM scannerjob sj
                    LEFT JOIN scannerjobruns sjr ON sj.lastscanjobrunid = sjr.id
                     WHERE sj.status = 'ENABLED'
                       AND sj.runoncejob = true
                       AND sj.lastscanjobrunid = sjr.id
                       AND sjr.status = 'COMPLETED');

    EXECUTE 'SELECT ARRAY(SELECT sj.id FROM scannerjob sj
        LEFT JOIN scannerjobruns sjr ON sj.lastscanjobrunid = sjr.id
        WHERE sj.status = ''ENABLED''
          AND sj.runoncejob = true
          AND sj.lastscanjobrunid = sjr.id
          AND sjr.status = ''COMPLETED'')' INTO ids;

    RAISE NOTICE '% records impacted, ids=[%], changed status from ENABLED to DISABLED', sj_count, ids;

    UPDATE scannerjob sj SET status = 'DISABLED' FROM scannerjobruns sjr
        WHERE sj.runoncejob= true
          AND sj.status = 'ENABLED'
          AND sj.lastscanjobrunid = sjr.id
          AND sjr.status = 'COMPLETED';
    END;
$$