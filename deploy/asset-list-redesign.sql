-- Deploy database-schema:asset-list-redesign to pg

BEGIN;
-- We use trigrams as a solution to allow faster searching of substrings for certain columns
CREATE EXTENSION IF NOT EXISTS "pg_trgm";

CREATE TABLE IF NOT EXISTS assetsview
(
    seq               bigserial not null,
    assetid           text PRIMARY KEY NOT NULL,
    namelabel         text,
    resourcename      text,
    resourceid        text,
    resourcetype      text                  not null,
    resourcetypelabel text                  not null,
    category          text,
    creatediso        timestamptz,
    updatediso        timestamptz,
    discoverymeta     jsonb,
    deriveddatablob   jsonb,
    storageinbytes    bigint,
    resourcetypesize  text
);

CREATE INDEX assetsview_seq ON assetsview(seq);
CREATE INDEX assetsview_namelabel_gin_trgm_idx  ON assetsview USING gin  (namelabel gin_trgm_ops);
CREATE INDEX assetsview_resourceid ON assetsview(resourceid);
CREATE INDEX assetsview_resourcetype ON assetsview(resourcetype);
CREATE INDEX assetsview_resourcetypelabel ON assetsview(resourcetypelabel);
CREATE INDEX assetsview_category ON assetsview(category);
CREATE INDEX assetsview_storageinbytes ON assetsview(storageinbytes);
CREATE INDEX assetsview_resourcetypesize ON assetsview(resourcetypesize);
CREATE INDEX assetsview_updatediso ON assetsview(updatediso);

CREATE TABLE IF NOT EXISTS awsassetsview
(
    awsregion       text,
    awsregionlabel  text,
    awsaccountid    text,
    awsaccountlabel text,
    tags            jsonb
) INHERITS (assetsview);

-- We explicitly create these instead of inheriting them because this gives us control over
-- the index names
CREATE INDEX awsassetsview_seq ON awsassetsview(seq);
CREATE INDEX awsassetsview_namelabel_gin_trgm_idx  ON awsassetsview USING gin(namelabel gin_trgm_ops);
CREATE INDEX awsassetsview_resourceid ON awsassetsview(resourceid);
CREATE INDEX awsassetsview_resourcetype ON awsassetsview(resourcetype);
CREATE INDEX awsassetsview_resourcetypelabel ON awsassetsview(resourcetypelabel);
CREATE INDEX awsassetsview_category ON awsassetsview(category);
CREATE INDEX awsassetsview_storageinbytes ON awsassetsview(storageinbytes);
CREATE INDEX awsassetsview_resourcetypesize ON awsassetsview(resourcetypesize);
CREATE INDEX awsassetsview_updatediso ON awsassetsview(updatediso);

CREATE INDEX awsassetsview_awsregionlabel ON awsassetsview(awsregionlabel);
CREATE INDEX awsassetsview_awsregion ON awsassetsview(awsregion);
CREATE INDEX awsassetsview_awsaccountid_gin_trgm_idx ON awsassetsview USING gin(awsaccountid gin_trgm_ops);
CREATE INDEX awsassetsview_awsaccountlabel_gin_trgm_idx  ON awsassetsview USING gin(awsaccountlabel gin_trgm_ops);

CREATE TABLE IF NOT EXISTS awsbackupplantags (
    backupplanarn TEXT NOT NULL,
    tagkey TEXT NOT NULL,
    tagvalue TEXT NOT NULL,
    awsregion TEXT NOT NULL,
    awsaccountid TEXT NOT NULL,
    PRIMARY KEY (backupplanarn, tagkey, tagvalue)
);
CREATE INDEX awsbackupplantags_tagkey ON awsbackupplantags(tagkey);
CREATE INDEX awsbackupplantags_tagvalue ON awsbackupplantags(tagvalue);

CREATE TABLE IF NOT EXISTS awsbackupjobsassetsview (
    seq bigserial NOT NULL,
    awsaccountid TEXT NOT NULL,
    backupjobid TEXT NOT NULL,
    targetassetid TEXT NOT NULL,
    configuration JSONB,
    PRIMARY KEY (awsaccountid, backupjobid)
);
CREATE INDEX awsbackupjobs_seqkey ON awsbackupjobsassetsview(seq);
CREATE INDEX awsbackupjobs_awsaccountidkey ON awsbackupjobsassetsview(awsaccountid);
CREATE INDEX awsbackupjobs_backupjobid ON awsbackupjobsassetsview(backupjobid);
CREATE INDEX awsbackupjobs_targetassetid ON awsbackupjobsassetsview(targetassetid);

COMMIT;
