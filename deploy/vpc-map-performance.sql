-- Deploy database-schema:vpc-map-performance to pg

BEGIN;

CREATE INDEX IF NOT EXISTS awsassetsview_deriveddatablob_vpcid ON awsassetsview((deriveddatablob->>'vpcId'));

COMMIT;
