-- Deploy database-schema:scan-audit-log-events-primary-key-update to pg

BEGIN;

alter table scanauditlogevent drop constraint pk_scanauditlogevent ;
update scanauditlogevent set dataclassid = '00000000-0000-0000-0000-000000000000' where dataclassid is null;
alter table scanauditlogevent add primary key (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state, dataclassid);

COMMIT;
