-- Deploy database-schema:orvn_ro_add_read_access to pg

BEGIN;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO orvn_ro;
ALTER DEFAULT PRIVILEGES IN SCHEMA PUBLIC GRANT SELECT ON TABLES TO orvn_ro;

COMMIT;
