-- Deploy database-schema:gdrive-violations-eventing-switchover to pg

BEGIN;

ALTER TABLE gdriveviolations ADD COLUMN IF NOT EXISTS reseedid BIGINT;
CREATE INDEX IF NOT EXISTS gdriveviolations_reseedid ON gdriveviolations(reseedid);

COMMIT;
