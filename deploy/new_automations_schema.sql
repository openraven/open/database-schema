-- Deploy database-schema:new_automations_schema to pg

BEGIN;

CREATE TABLE IF NOT EXISTS automation_rule
(
    id            UUID                     NOT NULL PRIMARY KEY,
    name          TEXT                     NOT NULL,
    description   TEXT,
    filter_inputs JSONB,
    event_type    TEXT                     NOT NULL,
    enabled       BOOLEAN DEFAULT TRUE     NOT NULL,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    created_by    TEXT                     NOT NULL,
    updated_by    TEXT                     NOT NULL
);

CREATE TABLE IF NOT EXISTS automation_consequence
(
    id                 UUID  DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    automation_rule_id UUID REFERENCES automation_rule (id) ON DELETE CASCADE,
    entity_version     UUID  DEFAULT uuid_generate_v4() NOT NULL,
    consequence_type   TEXT                             NOT NULL,
    integration_id     UUID  DEFAULT NULL,
    configuration      JSONB DEFAULT NULL,
    status             TEXT  DEFAULT 'UNKNOWN'          NOT NULL,
    exception          JSONB DEFAULT NULL
);

INSERT INTO automation_rule (id, enabled, name, description, filter_inputs, event_type, created_at, updated_at, created_by,
                             updated_by)
SELECT id,
       enabled,
       name,
       description,
       constraints,
       event_type,
       created_at,
       updated_at,
       created_by,
       updated_by
FROM rule_configuration;

INSERT INTO automation_consequence (automation_rule_id, consequence_type)
SELECT rule.id, 'IGNORE_EVENT_TRIGGER'
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'ignoreEventTrigger';

INSERT INTO automation_consequence (automation_rule_id, consequence_type)
SELECT rule.id, 'UNSHARE'
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'unshareFile';

INSERT INTO automation_consequence (automation_rule_id, consequence_type, configuration)
SELECT rule.id, 'TAG_ASSET', consequence - '@type'
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'tagAsset';

INSERT INTO automation_consequence (automation_rule_id, consequence_type, integration_id)
SELECT rule.id, 'EVENTBRIDGE', (SELECT id from eventbridge_integration_settings_v2 LIMIT 1)
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'firehose';

INSERT INTO automation_consequence (automation_rule_id, consequence_type, integration_id)
SELECT rule.id, 'WEBHOOK', (consequence ->> 'integrationId')::uuid
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'webhook';

INSERT INTO automation_consequence (automation_rule_id, consequence_type, configuration)
SELECT rule.id, 'EMAIL', consequence - '@type' AS configuration
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'email';

INSERT INTO automation_consequence (automation_rule_id, consequence_type, integration_id, configuration)
SELECT rule.id, 'SLACK', (consequence ->> 'uuid')::uuid, consequence - '@type' - 'uuid'
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'slackMessage';

INSERT INTO automation_consequence (automation_rule_id, consequence_type, integration_id, configuration)
SELECT rule.id, 'JIRA', (SELECT id from jira_integration_settings_v2 WHERE enabled LIMIT 1), consequence - '@type'
FROM rule_configuration rule
         CROSS JOIN LATERAL jsonb_array_elements(consequences) AS consequence
WHERE consequence ->> '@type' = 'jira';

UPDATE automation_consequence
SET status = 'MISSING_INTEGRATION'
WHERE integration_id IS NULL
  AND consequence_type IN ('EVENTBRIDGE', 'WEBHOOK', 'SLACK');

UPDATE automation_rule rule
SET enabled = FALSE
FROM automation_consequence consequence
WHERE rule.enabled
  AND rule.id = consequence.automation_rule_id
  AND consequence.status = 'MISSING_INTEGRATION';

COMMIT;
