-- Deploy database-schema:add-resource-name-to-sg-connections to pg

BEGIN;

-- Drop and recreate table with new "resourcename" column and updated
-- indices. We do this instead of an ALTER statement because the cardinality of the table is high
-- which could provide larger service disruption.
-- This table is populated off of discovery data so nothing of note is lost
-- after discovery runs at least once.
DROP TABLE IF EXISTS awssecuritygroupconnections;
CREATE TABLE IF NOT EXISTS awssecuritygroupconnections
(
    seq               bigserial NOT NULL,
    sgarn             text NOT NULL,
    awsregion         text NOT NULL,
    cidr              text NOT NULL,
    cidrowner         text,
    cidrownernetrange text,
    cidrlabel         text,
    isbadcidr         boolean NOT NULL,
    description       text,
    fromport          bigint,
    toport            bigint,
    ipprotocol        text,
    egress            boolean NOT NULL,
    isexternal        boolean NOT NULL,
    updatediso        timestamptz,
    resourcename      text
);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_seq ON awssecuritygroupconnections(seq);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_sgarn ON awssecuritygroupconnections(sgarn);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_awsregion ON awssecuritygroupconnections(awsregion);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidr ON awssecuritygroupconnections(cidr);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrowner ON awssecuritygroupconnections(cidrowner);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrlabel ON awssecuritygroupconnections(cidrlabel);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrownernetrange ON awssecuritygroupconnections(cidrownernetrange);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_egress ON awssecuritygroupconnections(egress);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_updatediso ON awssecuritygroupconnections(updatediso);

-- Add trigram indices to help with search queries over these columns
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_sgarn_gin_trgm_idx ON awssecuritygroupconnections USING gin(sgarn gin_trgm_ops);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrlabel_gin_trgm_idx ON awssecuritygroupconnections USING gin(cidrlabel gin_trgm_ops);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_resourcename_gin_trgm_idx  ON awssecuritygroupconnections USING gin(resourcename gin_trgm_ops);



COMMIT;
