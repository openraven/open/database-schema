BEGIN;

alter table public.sdss_scanfinding
    add if not exists schemaname text,
    add if not exists  tablename text,
    add if not exists  columnname text;

update public.sdss_scanfinding set
    schemaname = '' where schemaname is null;

update public.sdss_scanfinding set
    tablename = '' where tablename is null;

update public.sdss_scanfinding set
    columnname = '' where columnname is null;

alter table public.sdss_scanfinding
    alter schemaname set not null,
    alter tablename set not null,
    alter columnname set not null;


alter table public.metadatatables
    add if not exists schemaname text;

update public.metadatatables set
    schemaname = '' where schemaname is null;

alter table public.metadatatables
    alter schemaname set not null;

COMMIT;
