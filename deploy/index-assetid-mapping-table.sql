-- Deploy database-schema:index-assetid-mapping-table to pg

BEGIN;

CREATE UNIQUE INDEX IF NOT EXISTS datastoreresourceaccessmapping_assetid ON datastoreresourceaccessmapping(assetid);

COMMIT;
