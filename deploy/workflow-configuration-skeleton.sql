-- Deploy database-schema:workflow-configuration-skeleton to pg

BEGIN;

CREATE TABLE IF NOT EXISTS workflowconfiguration
(
    id                 UUID NOT NULL PRIMARY KEY,
    name               text,
    description        text,
    awsaccountnumber   text,
    dataclassid        UUID,
    stopprocessing     boolean,
    previewmatch       text,
    regionid           text
);

COMMIT;
