-- Deploy database-schema:policy-opa-removal to pg

BEGIN;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
-- Rename old tables to save them before remaking new ones
ALTER TABLE policies
    RENAME TO policies_opa;
ALTER TABLE rules
    RENAME TO rules_opa;
ALTER TABLE policy_rule
    RENAME TO policy_rule_opa;
ALTER TABLE issues
    RENAME TO issues_opa;
ALTER TABLE issueremediation
    RENAME TO issueremediation_opa;
ALTER TABLE policyaudit
    RENAME TO policyaudit_opa;
ALTER TABLE assetviolation
    RENAME TO assetviolation_opa;
ALTER TABLE fileviolation
    RENAME TO fileviolation_opa;
ALTER TABLE fileviolationdataclass
    RENAME TO fileviolationdataclass_opa;
ALTER TABLE issue_assetviolation
    RENAME TO issue_assetviolation_opa;

-- Create new versions of tables
CREATE TABLE IF NOT EXISTS policies
(
    id             uuid PRIMARY KEY NOT NULL,
    datecreated    int8,
    managed        boolean,
    name           text,
    updatedat      int8,
    version        text,
    -- TODO: Fkey for this?
    assetgroupid   uuid,
    description    text,
    enabled        boolean          NOT NULL DEFAULT FALSE,
    exclusions     text,
    lastexecutedat int8,
    refid          text,
    schedule       text,
    archived       boolean          NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS rules
(
    id                 uuid PRIMARY KEY NOT NULL,
    datecreated        int8,
    managed            boolean,
    name               text,
    updatedat          int8,
    version            text,
    description        text,
    enabled            boolean          NOT NULL DEFAULT FALSE,
    refid              text,
    sql                text,
    severity           text,
    type               text,
    ruleversion        text,
    remediation        text,
    remediationdocurls text[],
    archived       boolean          NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS policy_rule
(
    ruleid   uuid NOT NULL,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id)
            ON DELETE CASCADE,
    policyid uuid NOT NULL,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id)
            ON DELETE CASCADE,
    PRIMARY KEY (ruleid, policyid)
);

CREATE TABLE IF NOT EXISTS issues
(
    id          uuid PRIMARY KEY NOT NULL,
    datecreated int8,
    updatedat   int8,
    lastclosed  int8,
    policyid    uuid,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id)
            ON DELETE CASCADE,
    ruleid      uuid,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id)
            ON DELETE CASCADE,
    status      text
);

CREATE TABLE IF NOT EXISTS issueremediation
(
    id            uuid PRIMARY KEY NOT NULL,
    datecreated   int8,
    managed       boolean,
    name          text,
    updatedat     int8,
    version       text,
    integrationid uuid,
    misc          jsonb,
    timesent      int8,
    type          text,
    issueid       uuid,
    CONSTRAINT fk_issue
        FOREIGN KEY (issueid)
            REFERENCES issues (id)
            ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS assetviolation
(
    id             uuid PRIMARY KEY NOT NULL,
    datecreated    int8,
    updatedat      int8,
    assetid        text,
    policyid       uuid,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id)
            ON DELETE CASCADE,
    ruleid         uuid,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id)
            ON DELETE CASCADE,
    status         text,
    previousstatus text,
    awsaccountid   text,
    awsregion      text,
    resourcename   text,
    objectcount    bigint,
    CONSTRAINT unique_policy_rule_assetid_sql UNIQUE (assetid, policyid, ruleid)
);

CREATE TABLE IF NOT EXISTS fileviolation
(
    id               uuid PRIMARY KEY NOT NULL,
    assetviolationid uuid,
    CONSTRAINT fk_assetviolation
        FOREIGN KEY (assetviolationid)
            REFERENCES assetviolation (id)
            ON DELETE CASCADE,
    filename         text,
    sourcechild      text
);

CREATE TABLE IF NOT EXISTS fileviolationdataclass
(
    id              uuid PRIMARY KEY NOT NULL,
    -- this will one day sort of be a foreign key. We don't allow class name
    -- changes at the moment
    dataclassid     uuid,
    CONSTRAINT fk_dataclass
        FOREIGN KEY (dataclassid)
            REFERENCES dataclass (id)
            ON DELETE CASCADE,
    -- To keep things "simple" we keep this property for now. S3-Scan-Service does not expose it's models to other microservices
    dataclassname   text,
    count           bigint,
    fileviolationid uuid,
    CONSTRAINT fk_fileviolation
        FOREIGN KEY (fileviolationid)
            REFERENCES fileviolation (id)
            ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS issue_assetviolation
(
    assetviolationid uuid NOT NULL,
    CONSTRAINT fk_assetviolation
        FOREIGN KEY (assetviolationid)
            REFERENCES assetviolation (id)
            ON DELETE CASCADE,
    issueid          uuid NOT NULL,
    CONSTRAINT fk_issue
        FOREIGN KEY (issueid)
            REFERENCES issues (id)
            ON DELETE CASCADE,
    PRIMARY KEY (assetviolationid, issueid)
);

-- Migrate Data between OPA tables and New ones
INSERT INTO policies(id,
                     datecreated,
                     managed,
                     name,
                     updatedat,
                     version,
                     assetgroupid,
                     description,
                     enabled,
                     exclusions,
                     lastexecutedat,
                     refid,
                     schedule)
    (
        SELECT policies_opa.id,
               policies_opa.datecreated,
               policies_opa.managed,
               policies_opa.name,
               policies_opa.updatedat,
               policies_opa.version,
               policies_opa.assetgroupid,
               policies_opa.description,
               policies_opa.enabled,
               policies_opa.exclusions,
               policies_opa.lastexecutedat,
               policies_opa.refid,
               policies_opa.schedule
        FROM policies_opa
    );
UPDATE policies
set enabled = FALSE
WHERE managed = FALSE;

INSERT INTO rules(id,
                  datecreated,
                  managed,
                  name,
                  updatedat,
                  version,
                  description,
                  enabled,
                  refid,
                  severity,
                  type)
    (
        SELECT rules_opa.id,
               rules_opa.datecreated,
               rules_opa.managed,
               rules_opa.name,
               rules_opa.updatedat,
               rules_opa.version,
               rules_opa.description,
               rules_opa.enabled,
               rules_opa.refid,
               rules_opa.severity,
               rules_opa.type
        FROM rules_opa
    );

UPDATE rules
set enabled = FALSE
WHERE managed = FALSE;

INSERT INTO policy_rule(ruleid,
                        policyid)
    (
        SELECT policy_rule_opa.ruleid,
               policy_rule_opa.policyid
        FROM policy_rule_opa
    )
ON CONFLICT(ruleid, policyid) DO NOTHING;

INSERT INTO issues(id,
                   datecreated,
                   updatedat,
                   lastclosed,
                   policyid,
                   ruleid,
                   status)
    (
        SELECT issues_opa.id,
               issues_opa.datecreated,
               issues_opa.updatedat,
               issues_opa.last_closed,
               issues_opa.policyid,
               issues_opa.ruleid,
               issues_opa.status
        FROM issues_opa
    );
INSERT INTO assetviolation(id,
                           datecreated,
                           updatedat,
                           assetid,
                           policyid,
                           ruleid,
                           status,
                           awsaccountid,
                           awsregion,
                           resourcename,
                           objectcount)
    (
        SELECT assetviolation_opa.id,
               assetviolation_opa.datecreated,
               assetviolation_opa.updatedat,
               assetviolation_opa.assetid,
               assetviolation_opa.policyid,
               assetviolation_opa.ruleid,
               assetviolation_opa.status,
               assetviolation_opa.awsaccountid,
               assetviolation_opa.awsregion,
               assetviolation_opa.resourcename,
               assetviolation_opa.objectcount
        FROM assetviolation_opa
    );
INSERT INTO fileviolation(id,
                          assetviolationid,
                          filename,
                          sourcechild)
    (
        SELECT fileviolation_opa.id,
               fileviolation_opa.assetviolationid,
               fileviolation_opa.filename,
               fileviolation_opa.sourcechild
        FROM fileviolation_opa
    );
INSERT INTO fileviolationdataclass(id,
                                   dataclassid,
                                   dataclassname,
                                   count,
                                   fileviolationid)(
    SELECT fileviolationdataclass_opa.id,
           dataclass.id,
           dataclass.name,
           fileviolationdataclass_opa.count,
           fileviolationdataclass_opa.fileviolationid
    FROM fileviolationdataclass_opa
             INNER JOIN dataclass ON dataclass.name = fileviolationdataclass_opa.dataclassname
);
INSERT INTO issue_assetviolation(assetviolationid,
                                 issueid)
    (
        SELECT issue_assetviolation_opa.assetviolationid,
               issue_assetviolation_opa.issueid
        FROM issue_assetviolation_opa
    )
ON CONFLICT(assetviolationid, issueid) DO NOTHING;
COMMIT;
