-- Deploy database-schema:workflow-configuration-schema to pg

BEGIN;

CREATE TABLE IF NOT EXISTS workflow_configuration
(
    id          UUID NOT NULL PRIMARY KEY,
    name        TEXT NOT NULL,
    description TEXT
);

CREATE TABLE IF NOT EXISTS rule_configuration
(
    id           UUID NOT NULL PRIMARY KEY,
    workflow_id  UUID NOT NULL,
    name         TEXT NOT NULL,
    description  TEXT,
    salience     INT,
    constraints  JSONB,
    consequences JSONB,
    CONSTRAINT fk_workflow
        FOREIGN KEY (workflow_id)
            REFERENCES workflow_configuration (id)
            ON DELETE CASCADE
);

INSERT INTO workflow_configuration
SELECT id, name, description
    FROM workflowconfiguration;

INSERT INTO rule_configuration
SELECT
    uuid_generate_v4() AS id,
    id AS workflow_id,
    name,
    description,
    0 AS salience,
    array_to_json(
            array_remove(
                    ARRAY[
                        CASE WHEN awsaccountnumber IS NOT NULL THEN
                                 jsonb_build_object('type', 'AWS_ACCOUNT_ID', 'parameter', awsaccountnumber)
                            END,
                        CASE WHEN dataclassid IS NOT NULL THEN
                                 jsonb_build_object('type', 'DATA_CLASS_ID', 'parameter', dataclassid)
                            END,
                        CASE WHEN previewmatch IS NOT NULL THEN
                                 jsonb_build_object('type', 'DATA_PREVIEW_CONTAINS', 'parameter', previewmatch)
                            END,
                        CASE WHEN regionid IS NOT NULL THEN
                                 jsonb_build_object('type', 'REGION', 'parameter', regionid)
                            END
                        ], NULL)
        ) AS constraints,
    array_to_json(
            array_remove(
                    ARRAY[
                        jsonb_build_object('type', 'IGNORE_SCAN_FINDING'),
                        CASE WHEN stopprocessing THEN
                                 jsonb_build_object('type', 'STOP_PROCESSING')
                            END
                        ], NULL
                )
        ) AS consequences
FROM workflowconfiguration;

COMMIT;
