-- Deploy database-schema:sdss-cloudsql-iam-cred-model to pg

BEGIN;

INSERT INTO datastorecredentialmodels
    (id, name, assettypes, credentialmodel)
VALUES ('GC_IAM_CLOUD_SQL_ACCESS_TOKEN', 'IAM access token for Cloud Sql',
        '["GCP::SQL::DBInstance"]',
        '{"user": "The username to connect to (required)"}') ON CONFLICT DO NOTHING;
COMMIT;