-- Deploy database-schema:add-lastsuccessfullyused-column to pg

BEGIN;

ALTER TABLE datastoreresourceaccessmapping ADD COLUMN IF NOT EXISTS lastsuccessfullyused TIMESTAMP WITH TIME ZONE;

COMMIT;
