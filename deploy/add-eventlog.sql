-- Deploy database-schema:add-eventslog to pg

BEGIN;

create table if not exists eventlog
(
    id            uuid                     not null
        primary key,
    timestamp     timestamp not null,
    actor         text,
    remoteip      text,
    eventcategory text,
    eventtype     text,
    data          jsonb,
    message       text
);

create index if not exists eventlogindex_actor
    on eventlog (actor);

create index if not exists eventlogindex_eventcategory
    on eventlog (eventcategory);

create index if not exists eventlogindex_eventtype
    on eventlog (eventtype);

create index if not exists eventlogindex_message
    on eventlog (message);

create index if not exists eventlogindex_timestamp
    on eventlog (timestamp);

COMMIT;
