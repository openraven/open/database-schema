-- Deploy database-schema:set-columns-to-be-dropped-nullable to pg

BEGIN;

ALTER TABLE aggregatedassetfindings ALTER COLUMN region DROP NOT NULL;
ALTER TABLE aggregatedassetfindings ALTER COLUMN account DROP NOT NULL;

ALTER TABLE aggregatedscanfindings ALTER COLUMN region DROP NOT NULL;
ALTER TABLE aggregatedscanfindings ALTER COLUMN account DROP NOT NULL;

COMMIT;
