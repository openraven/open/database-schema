-- Deploy database-schema:gdrive-stale-data-init to pg

BEGIN;

CREATE TABLE IF NOT EXISTS googledrivestalefiles (
     assetid TEXT NOT NULL,
     fileid TEXT NOT NULL,
     driveid TEXT,
     filename TEXT NOT NULL,
     username TEXT,
     fileurl TEXT NOT NULL,
     filecreatedat TIMESTAMPTZ, -- When the file was created in gdrive.
     fileupdatedat TIMESTAMPTZ, -- When the file was last updated in gdrive
     fileaccessedat TIMESTAMPTZ, -- When the file was last accessed in gdrive
     updatedat TIMESTAMPTZ, -- When the row was updated in the db. Internal use only.
     ignored BOOLEAN DEFAULT false,
     PRIMARY KEY(assetid, fileid)
);

CREATE TABLE IF NOT EXISTS googledrivestalenesssettings (
    id SERIAL NOT NULL PRIMARY KEY,
    config JSONB NOT NULL
);

CREATE INDEX IF NOT EXISTS googledrivestalefiles_fileid on googledrivestalefiles(fileid);
CREATE INDEX IF NOT EXISTS googledrivestalefiles_assetid on googledrivestalefiles(assetid);

INSERT INTO
    googledrivestalenesssettings (id, config)
VALUES (
    1,
    jsonb_build_object('staleAccessThresholdDays', 60, 'staleAgeThresholdDays', 120, 'staleEditThresholdDays', 120)
);

COMMIT;
