-- Deploy database-schema:v-globalassets-add-seq to pg

BEGIN;

CREATE OR REPLACE VIEW v_globalassets AS
SELECT
    globalassets.assetid as assetid,
    locationstring,
    namelabel,
    externalidentifiers,
    resourcetype,
    resourcetypelabel,
    category,
    creatediso,
    updatediso,
    discoverymeta,
    deriveddatablob,
    storageinbytes,
    resourcetypesize,
    source,
    locationblob,
    extendedattributes,
    resourcemetadata,
    highestruleseverityviolated,
    dataclassmap,
    datacollectionmap,
    scannedasset.assetid IS NOT NULL as scanned,
    seq
FROM globalassets
         LEFT JOIN scannedasset ON scannedasset.assetid = globalassets.assetid
WHERE markedfordelete = 0;

COMMIT;
