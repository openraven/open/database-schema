-- Deploy database-schema:add-compute-engine-info-tables to pg

BEGIN;

CREATE TABLE IF NOT EXISTS ec2priceinfo
(
    instancetype        TEXT NOT NULL,
    regioncode          TEXT NOT NULL,
    memory              TEXT NOT NULL,
    vcpu                INTEGER NOT NULL,
    capacitystatus      TEXT NOT NULL,
    locationtype        TEXT NOT NULL,
    storage             TEXT NOT NULL,
    operatingsystem     TEXT NOT NULL,
    unit                TEXT NOT NULL,
    priceperunit        DOUBLE PRECISION DEFAULT 0,
    publicationdate     TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY(instancetype, regioncode)
    );

CREATE TABLE IF NOT EXISTS ebspriceinfo
(
    volumeapiname                TEXT NOT NULL,
    regioncode                   TEXT NOT NULL,
    unit                         TEXT NOT NULL,
    priceperunit                 DOUBLE PRECISION DEFAULT 0,
    storagemedia                 TEXT NOT NULL,
    usagetype                    TEXT NOT NULL,
    publicationdate              TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY(volumeapiname, regioncode)
    );

CREATE TABLE IF NOT EXISTS computeengineinfo
(
    instanceid               TEXT NOT NULL PRIMARY KEY,
    instancetype             TEXT NOT NULL,
    regioncode               TEXT NOT NULL,
    jobid                    UUID NOT NULL,
    jobrunid                 UUID NOT NULL,
    createdat                TIMESTAMP WITH TIME ZONE,
    terminatedat             TIMESTAMP WITH TIME ZONE,
    lastcostcalculatedat     TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS ebsvolumeinfo
(
    volumeid                 TEXT NOT NULL PRIMARY KEY,
    instanceid               TEXT NOT NULL,
    volumesize               INTEGER NOT NULL,
    volumetype               TEXT NOT NULL,
    CONSTRAINT fk_instance_id
    FOREIGN KEY(instanceid) REFERENCES computeengineinfo(instanceid)
    ON DELETE CASCADE
    );

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS computeengineduration BIGINT NOT NULL DEFAULT 0;
ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS computeenginecost DOUBLE PRECISION DEFAULT 0;

COMMIT;