-- Deploy database-schema:remove-workflow-tables to pg

BEGIN;

-- removes obsolete prototype workflow table
DROP TABLE IF EXISTS workflowconfiguration;

-- migrates workflow_type from workflow_configuration to event_type on rule_configuration
ALTER TABLE rule_configuration ADD COLUMN event_type TEXT;

UPDATE rule_configuration rc SET event_type = wc.workflow_type
FROM workflow_configuration wc WHERE rc.workflow_id = wc.id;

ALTER TABLE rule_configuration ALTER COLUMN event_type SET NOT NULL;

-- removes foreign key from rule_configuration
ALTER TABLE rule_configuration DROP CONSTRAINT IF EXISTS fk_workflow;
ALTER TABLE rule_configuration DROP COLUMN IF EXISTS workflow_id;

-- removes workflow_configuration table
DROP TABLE IF EXISTS workflow_configuration;

COMMIT;
