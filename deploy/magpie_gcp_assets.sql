-- Deploy database-schema:magpie_gcp_assets to pg

BEGIN;

CREATE TABLE IF NOT EXISTS gcp (
                                          documentid TEXT primary key not null,
                                          assetid TEXT,
                                          resourcename TEXT,
                                          resourceid TEXT,
                                          resourcetype TEXT,
                                          region TEXT,
                                          gcpaccountid TEXT,
                                          projectid TEXT,
                                          creatediso TIMESTAMPTZ,
                                          updatediso TIMESTAMPTZ,
                                          discoverysessionid TEXT,
                                          tags JSONB,
                                          configuration JSONB,
                                          supplementaryconfiguration JSONB,
                                          discoverymeta JSONB
);
CREATE INDEX IF NOT EXISTS gcp_resource_type ON gcp (resourcetype);

COMMIT;
