-- Deploy database-schema:update-audit-log-sequence to pg

BEGIN;

ALTER SEQUENCE IF EXISTS audit_log_event_sequence INCREMENT BY 100;

COMMIT;
