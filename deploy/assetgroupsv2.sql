BEGIN;
CREATE TABLE IF NOT EXISTS assetgroupsv2
(
    id           uuid primary key not null,
    managed      boolean,
    owner        text,
    description  text,
    name         text,
    -- Enums.GroupType
    type         text,
    createdat    timestamptz,
    updatedat    timestamptz,
    version      text,

-- StaticAssetGroup
    -- Collection<AssetProviderAssetIds>
    assets       jsonb,

-- DynamicAssetGroup
    custom       boolean,
    -- Collection<FilterGroup>
    filtergroups jsonb,
    -- Collection<AssetType>
    assettypes   jsonb
);
COMMIT;
