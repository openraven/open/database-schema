-- Deploy database-schema:metric-tables to pg

BEGIN;

CREATE TABLE resourcetypeaggregate (
    id uuid PRIMARY KEY NOT NULL,
    resourcetype text NOT NULL,
    source text NOT NULL,
    region text,
    accountid text,
    typecount bigint NOT NULL,
    dateevaluated timestamptz NOT NULL
);
CREATE TABLE violationaggregate (
     id uuid PRIMARY KEY NOT NULL,
     accountid text,
     source text NOT NULL,
     region text,
     ruleid text NOT NULL,
     severity text NOT NULL,
     status text NOT NULL,
     violationcount bigint NOT NULL,
     dateevaluated timestamptz NOT NULL
);
CREATE TABLE s3aggregate (
  id uuid PRIMARY KEY NOT NULL,
  region text,
  acccountid text,
  bucketsizebytes bigint,
  numberofobjects bigint,
  storagetypesizeinbytes jsonb,
  dateevaluated timestamptz NOT NULL
);


COMMIT;
