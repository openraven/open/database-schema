-- Deploy database-schema:add-asset-fkey-drives to pg

BEGIN;

DELETE FROM suspiciousgoogleshareddrivepermissions WHERE assetid NOT IN (SELECT assetid FROM globalassets);

ALTER TABLE suspiciousgoogleshareddrivepermissions
    ADD CONSTRAINT fk_suspiciousgoogleshareddrivepermissions_asset_id
    FOREIGN KEY (assetid) REFERENCES globalassets(assetid) ON DELETE CASCADE;

COMMIT;
