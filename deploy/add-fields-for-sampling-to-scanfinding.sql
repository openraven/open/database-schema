-- Deploy database-schema:add-fields-for-sampling-to-scanfinding to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS samplesize BIGINT DEFAULT 0;
ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS findingsfromsamplecount INTEGER DEFAULT 0;
ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS matchpercentage DOUBLE PRECISION DEFAULT 0;
ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS estimated BOOLEAN NOT NULL DEFAULT false;

COMMIT;
