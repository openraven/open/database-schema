-- Deploy database-schema:add-pub-access-column to pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS publiclyaccessible BOOLEAN;
UPDATE suspiciousgoogledrivefilepermissions SET publiclyaccessible=sharedbylink;
ALTER TABLE suspiciousgoogledrivefilepermissions ALTER COLUMN publiclyaccessible SET DEFAULT FALSE;
CREATE INDEX IF NOT EXISTS suspiciousgoogledrivefilepermissions_publiclyaccessible ON suspiciousgoogledrivefilepermissions(publiclyaccessible);

COMMIT;
