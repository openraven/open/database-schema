-- Deploy database-schema:add-deriveddatablob-index to pg

BEGIN;

ALTER TABLE globalassets ADD COLUMN IF NOT EXISTS seq BIGSERIAL;
CREATE INDEX IF NOT EXISTS globalassets_deriveddatablob_severitycount ON globalassets (
    ((deriveddatablob->'violationCountsBySeverity'->>'HIGH')::BIGINT),
    ((deriveddatablob->'violationCountsBySeverity'->>'MEDIUM')::BIGINT),
    ((deriveddatablob->'violationCountsBySeverity'->>'LOW')::BIGINT),
    seq
);
CREATE INDEX IF NOT EXISTS globalassets_seq ON globalassets (seq);

COMMIT;
