-- Deploy database-schema:add-asset-to-file-count to pg

BEGIN;

CREATE MATERIALIZED VIEW mat_v_gdrive_asset_file_count AS
SELECT gf.assetid, COUNT(gf.fileid) AS filecount FROM googledrivefiles gf GROUP BY gf.assetid;
CREATE UNIQUE INDEX mat_v_gdrive_asset_file_count_unique ON mat_v_gdrive_asset_file_count(assetid);

CREATE INDEX mat_v_gdrive_file_aggregate_data_assetid ON mat_v_gdrive_file_aggregate_data(assetid);

COMMIT;
