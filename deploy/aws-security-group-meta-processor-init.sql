-- Deploy database-schema:aws-security-group-meta-processor-init to pg

BEGIN;

CREATE TABLE awsec2securitygroupconnectionmetadata (
    arn TEXT PRIMARY KEY,
    hasexternalconnections BOOLEAN,
    haspeering BOOLEAN,
    region TEXT,
    lastupdated TIMESTAMPTZ
);

CREATE INDEX IF NOT EXISTS awsec2securitygroupconnectionmetadata_region ON awsec2securitygroupconnectionmetadata(region);
CREATE INDEX IF NOT EXISTS awsec2securitygroupconnectionmetadata_lastupdated ON awsec2securitygroupconnectionmetadata(lastupdated);
CREATE INDEX IF NOT EXISTS awsec2securitygroupconnectionmetadata_haspeering ON awsec2securitygroupconnectionmetadata(haspeering);
CREATE INDEX IF NOT EXISTS awsec2securitygroupconnectionmetadata_hasexternalconnections ON awsec2securitygroupconnectionmetadata(hasexternalconnections);

COMMIT;
