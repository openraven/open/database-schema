-- Deploy database-schema:add-mime-column to pg

BEGIN;

ALTER TABLE googledrivefiles ADD COLUMN mimetype TEXT;

COMMIT;
