-- Deploy database-schema:sdss-privatekey-cred-model to pg

BEGIN;

insert into datastorecredentialmodels
(id, name, assettypes, credentialmodel)
values
    ('SECRETS_MANAGER_PRIVATE_KEY', 'Secrets Manager Private Key', '[ "Snowflake" ]', '{"arn": "ARN to Secrets Manager where the private key is held (required)", "user": "The username to connect to (required)"}');

insert into datastorecredentialmodels
(id, name, assettypes, credentialmodel)
values
    ('PARAMETER_STORE_PASSWORD', 'Parameter Store password', '[ "AWS::RDS::DBInstance" ]', '{"parameterName": "Parameter name (required)", "user": "The username to connect to (required)", "region": "Region in which parameter is held"}');

COMMIT;
