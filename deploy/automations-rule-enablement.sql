-- Deploy database-schema:automations-rule-enablement to pg

BEGIN;

ALTER TABLE rule_configuration ADD COLUMN IF NOT EXISTS enabled BOOLEAN DEFAULT TRUE;
ALTER TABLE rule_configuration ALTER COLUMN enabled SET NOT NULL;

COMMIT;
