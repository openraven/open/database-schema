-- Deploy database-schema:asset-group-filter to pg

BEGIN;

CREATE TABLE IF NOT EXISTS assetgrouptoassets(
                                                   assetid TEXT,
                                                   assetgroupid UUID,
                                                   reseedid TEXT,
                                                   PRIMARY KEY(assetid, assetgroupid)
);

CREATE INDEX IF NOT EXISTS assetgrouptoassets_reseedid ON assetgrouptoassets(reseedid);

COMMIT;
