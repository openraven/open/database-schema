-- Deploy database-schema:add-cloudsql-vpc-connector-info-table to pg
BEGIN;

CREATE TABLE IF NOT EXISTS cloudsqlvpcconnectorinfo
(
    cloudfunctionname       TEXT NOT NULL,
    vpcconnectorname        TEXT NOT NULL,
    jobrunid                TEXT NOT NULL,
    PRIMARY KEY (cloudfunctionname)
    );

COMMIT;