-- Deploy database-schema:add-implementation-type-column-scanfinding to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS implementationtype TEXT;

COMMIT;
