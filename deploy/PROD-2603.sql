-- Deploy database-schema:PROD-2603 to pg

BEGIN;

ALTER ROLE orvn_superuser WITH CREATEROLE CREATEDB;

COMMIT;
