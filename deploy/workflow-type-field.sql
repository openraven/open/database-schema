-- Deploy database-schema:workflow-type-field to pg

BEGIN;

ALTER TABLE workflow_configuration ADD COLUMN IF NOT EXISTS workflow_type TEXT DEFAULT 'SCANFINDING';
ALTER TABLE workflow_configuration ALTER COLUMN workflow_type SET NOT NULL;

COMMIT;
