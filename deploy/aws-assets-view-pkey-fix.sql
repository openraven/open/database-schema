-- Deploy database-schema:aws-assets-view-pkey-fix to pg

BEGIN;

DO
$$
    BEGIN
        if not exists(select constraint_name
                      from information_schema.constraint_column_usage
                      where table_name = 'awsassetsview'
                        and constraint_name = 'awsassetsview_pkey')
        THEN
            -- We have no use for the duplicate rows deleted from this.
            -- Delete them and don't back them up
            DELETE
            FROM awsassetsview a USING (
                SELECT MIN(ctid) as ctid, assetid
                FROM awsassetsview
                GROUP BY assetid
                HAVING COUNT(*) > 1
            ) b
            WHERE a.assetid = b.assetid
              AND a.ctid <> b.ctid;

            ALTER TABLE awsassetsview
                ADD PRIMARY KEY (assetid);
        END IF;
    END
$$;

COMMIT;
