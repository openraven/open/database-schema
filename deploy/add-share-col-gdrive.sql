-- Deploy database-schema:add-share-col-gdrive to pg

BEGIN;

ALTER TABLE googledrivefiles ADD COLUMN IF NOT EXISTS fileissharedtouser BOOLEAN DEFAULT NULL;
ALTER TABLE googledrivefiles ADD COLUMN IF NOT EXISTS sharinguser TEXT DEFAULT NULL;

COMMIT;
