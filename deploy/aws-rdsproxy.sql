-- Deploy database-schema:aws-rdsproxy to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awsrdsproxy () INHERITS (aws);
CREATE INDEX IF NOT EXISTS "idx_awsrdsproxy_documentId" ON "public"."awsrdsproxy" USING btree ("documentid");

INSERT INTO awsrdsproxy SELECT * FROM aws WHERE resourcetype = 'AWS::RDS::DBProxy' AND NOT EXISTS (SELECT * FROM awsrdsproxy);

COMMIT;
