-- Deploy database-schema:dlp_violations_jira_key to pg

BEGIN;

ALTER TABLE gdriveviolations ADD COLUMN IF NOT EXISTS external_key TEXT;

COMMIT;
