-- Deploy database-schema:change-btree-to-gin to pg

BEGIN;

-- Btree indices perform better for our query patterns than the GIN indices.
-- Unfortunately, the btree indices encountered errors during creation on certain customers related to index size.
DROP INDEX IF EXISTS mat_v_gdrive_file_aggregate_data_dataclasses_btree;
DROP INDEX IF EXISTS mat_v_gdrive_file_aggregate_data_rulesviolated_btree;
CREATE INDEX IF NOT EXISTS mat_v_gdrive_file_aggregate_data_dataclasses_gin ON mat_v_gdrive_file_aggregate_data USING gin(dataclasses);
CREATE INDEX IF NOT EXISTS mat_v_gdrive_file_aggregate_data_rulesviolated_gin ON mat_v_gdrive_file_aggregate_data USING gin(rulesviolated);

COMMIT;
