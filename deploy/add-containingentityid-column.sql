-- Deploy database-schema:add-containingentityid-column to pg

BEGIN;

ALTER TABLE globalassets ADD COLUMN IF NOT EXISTS containingentityid TEXT DEFAULT NULL;
CREATE INDEX IF NOT EXISTS globalassets_containingentityid ON globalassets(containingentityid);

COMMIT;
