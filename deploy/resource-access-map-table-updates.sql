-- Deploy database-schema:resource-access-map-table-updates to pg

BEGIN;

ALTER TABLE datastoreresourceaccessmapping ADD COLUMN IF NOT EXISTS status TEXT DEFAULT NULL;
ALTER TABLE datastoreresourceaccessmapping REPLICA IDENTITY FULL;

COMMIT;
