-- Deploy database-schema:add-col-last-scan-ext to pg

BEGIN;

ALTER TABLE googledrivefiles ADD COLUMN IF NOT EXISTS lastsuccessfulscan TIMESTAMPTZ;
ALTER TABLE googledrivefiles ADD COLUMN IF NOT EXISTS fileisexternal BOOLEAN;
CREATE INDEX IF NOT EXISTS googledrivefiles_fileisexternal on googledrivefiles(fileisexternal);

COMMIT;
