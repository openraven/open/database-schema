-- Deploy database-schema:use-gdrive-files to pg

BEGIN;

DROP VIEW v_gdrive_scanfinding_enriched;
DROP VIEW v_gdrive_dash;
DROP VIEW v_gdriveviolations;

CREATE VIEW v_gdrive_dash AS SELECT gf.assetid, gf.fileid, gf.fileurl, gf.filename,
       ga.namelabel AS drivename, gf.username, gp.sharedbylink AS anyonewithlink,
       gp.suspiciousemails, gs.filecreatedat, gs.fileupdatedat, gs.fileaccessedat,
       gf.permissions as allpermissions, json_agg(gr.name) AS rulesviolated, ga.resourcetype,
       gb.configuration->'restrictions' AS restrictions, gf.mimetype,
       array_to_json(ARRAY(
           SELECT DISTINCT
            substring(email, '@(.+)$')
        FROM
            jsonb_array_elements_text(suspiciousemails) AS email
    )) AS emaildomains,
       array_to_json(ARRAY(
           SELECT DISTINCT
            substring(email, '(.+)@')
        FROM
            jsonb_array_elements_text(suspiciousemails) AS email
    )) AS emailusers
FROM googledrivefiles gf
         INNER JOIN globalassets ga ON ga.assetid=gf.assetid
         INNER JOIN globalassetsblob gb ON gb.assetid=gf.assetid
         LEFT JOIN suspiciousgoogledrivefilepermissions gp ON gf.assetid=gp.assetid AND gf.fileid=gp.fileid
         LEFT JOIN googledrivestalefiles gs ON gf.assetid=gs.assetid AND gf.fileid=gs.fileid
         LEFT JOIN gdriveviolations gv ON gf.fileid=gv.nodeid
         LEFT JOIN gdriverules gr ON gv.gdriveruleid=gr.id
WHERE ga.resourcetype IN ('GOOGLEWORKSPACE::SharedDrive','GOOGLEWORKSPACE::MyDrive')
GROUP BY gf.assetid, gf.fileid, gf.fileurl, gf.filename, ga.namelabel, gp.username, gp.sharedbylink, gp.suspiciousemails, gs.filecreatedat,
         gs.fileupdatedat, gs.fileaccessedat, gf.permissions, ga.resourcetype, gb.configuration, gf.mimetype;

CREATE VIEW v_gdriveviolations AS SELECT v.id,
       v.nodeid,
       v.nodename,
       v.nodetype,
       v.updatedat,
       v.datecreated,
       v.gdriveruleid,
       r.name,
       r.severity,
       v.status,
       v.previousstatus,
       v.supplementalinformation,
       gf.mimetype
FROM gdriveviolations v
         INNER JOIN gdriverules r ON v.gdriveruleid = r.id
         INNER JOIN googledrivefiles gf ON gf.fileid=v.nodeid;



CREATE VIEW v_gdrive_scanfinding_enriched AS SELECT g.assetid, d.name, g.scantarget, g.scantargetchild, g.findingscount, g.findingslocations, s.fileid, s.fileurl, s.filename
FROM gdrivescanfinding g, googledrivefiles s, dataclassv2 d
WHERE g.assetid = s.assetid
  AND g.scantarget = s.fileid
  AND g.assetid=s.assetid
  AND d.id = g.dataclassid;

COMMIT;
