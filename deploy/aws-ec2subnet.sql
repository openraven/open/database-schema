-- Deploy database-schema:aws-ec2subnet to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awsec2subnet () INHERITS (aws);
CREATE INDEX IF NOT EXISTS "idx_awsec2subnet_documentId" ON "public"."awsec2subnet" USING btree ("documentid");

INSERT INTO awsec2subnet SELECT * FROM aws WHERE resourcetype = 'AWS::EC2::Subnet' AND NOT EXISTS (SELECT * FROM awsec2subnet);

COMMIT;
