-- Deploy database-schema:scanner-job-run-statistics-errorscount-and-findingscount-columns to pg

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS errorscount bigint NOT NULL default 0;
ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS findingscount bigint NOT NULL default 0;
