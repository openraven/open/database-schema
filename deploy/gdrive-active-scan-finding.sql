BEGIN;

create view v_active_gdrivescanfinding
            (scannerjobid, scannerjobrunid, assetid, scantarget, scantargetversion, dataclassid, findingscount,
             createdat, updatedat, scantargetchild, findingslocations)
as
SELECT gdrivescanfinding.scannerjobid,
       gdrivescanfinding.scannerjobrunid,
       gdrivescanfinding.assetid,
       gdrivescanfinding.scantarget,
       gdrivescanfinding.scantargetversion,
       gdrivescanfinding.dataclassid,
       gdrivescanfinding.findingscount,
       gdrivescanfinding.createdat,
       gdrivescanfinding.updatedat,
       gdrivescanfinding.scantargetchild,
       gdrivescanfinding.findingslocations
FROM gdrivescanfinding
WHERE alwaysfalsepositive = false AND falsepositive = false AND ignored = false;

alter table v_active_gdrivescanfinding
    owner to root;

grant select on v_active_gdrivescanfinding to orvn_ro;

COMMIT;