-- Deploy database-schema:reseed-upgrade to pg

BEGIN;

CREATE TABLE IF NOT EXISTS reseed_provider_upgrade_version
(
    operation_key   TEXT        NOT NULL,
    upgrade_version BIGINT      NOT NULL,
    PRIMARY KEY (operation_key, upgrade_version),
    time_upgraded TIMESTAMPTZ NOT NULL
);

COMMIT;
