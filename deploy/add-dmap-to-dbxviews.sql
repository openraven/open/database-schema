-- Deploy database-schema:add-dmap-to-dbxviews to pg

BEGIN;

create or replace view v_dmap as
SELECT dmapnode,
       dmapmeta,
       hasapplications,
       lastqueuerequesttime,
       lastscancompletedtime,
       arn
FROM dmap;

COMMIT;
