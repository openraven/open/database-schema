-- Deploy database-schema:add-last-update-at-to-dmaplambda-ref-info to pg

BEGIN;

ALTER TABLE dmaplambdareferenceinfo ADD COLUMN IF NOT EXISTS lastupdateat timestamp with time zone NOT NULL default NOW();
DROP INDEX IF EXISTS dmaplambdarefinfo_createdat;
CREATE INDEX IF NOT EXISTS dmaplambdarefinfo_lastupdateat ON dmaplambdareferenceinfo(lastupdateat);

COMMIT;
