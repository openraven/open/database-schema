-- Deploy database-schema:set-cont-ent to pg

BEGIN;

UPDATE globalassets SET containingentity=accounttoaccountalias.accountalias
    FROM accounttoaccountalias
WHERE source='aws' AND locationblob->>'accountId'=accounttoaccountalias.accountid;

COMMIT;
