-- Deploy database-schema:scanner-job-page-lambda-metadata-columns.sql to pg

ALTER TABLE scannerjobpage ADD COLUMN IF NOT EXISTS lambdaregion TEXT;
ALTER TABLE scannerjobpage ADD COLUMN IF NOT EXISTS lambdaarchitecture TEXT;
ALTER TABLE scannerjobpage ADD COLUMN IF NOT EXISTS lambdamemory integer NOT NULL default 0;