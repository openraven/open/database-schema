-- Deploy database-schema:recreate-awsusercredentialreport to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awsusercredentialreport () INHERITS (aws);

COMMIT;
