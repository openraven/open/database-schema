-- Deploy database-schema:violation-status-migration to pg

BEGIN;

-- Create table to hold affected data classes for rules
CREATE TABLE IF NOT EXISTS rule_dataclasses (
  rule_id UUID NOT NULL,
  dataclass_id UUID NOT NULL,
  PRIMARY KEY (rule_id, dataclass_id),
  FOREIGN KEY (rule_id) REFERENCES rules(id) ON DELETE CASCADE,
  FOREIGN KEY (dataclass_id) REFERENCES dataclassv2(id) ON DELETE CASCADE
);
-- populate rule data classes
WITH issue_dcs AS (
    SELECT DISTINCT ON(i.ruleid, idc.dataclassid)
        i.ruleid as rule_id,
        idc.dataclassid as dataclass_id
    FROM issuedataclasses idc
    INNER JOIN issues i ON i.id = idc.issueid
    INNER JOIN dataclassv2 dc ON dc.id = idc.dataclassid
)
INSERT INTO rule_dataclasses
SELECT * FROM issue_dcs;

-- Asset Violation changes to statuses
CREATE TABLE IF NOT EXISTS assetviolations_removepolicyid_status_copy AS  SELECT * FROM assetviolations_removepolicyid;
-- Maintain legacy table for now too
CREATE TABLE IF NOT EXISTS assetviolation_status_copy AS  SELECT * FROM assetviolation;

ALTER TABLE assetviolations_removepolicyid ADD COLUMN IF NOT EXISTS statuschangedbytype TEXT;
CREATE INDEX IF NOT EXISTS assetviolations_removepolicyid_statuschangedbytype ON assetviolations_removepolicyid(statuschangedbytype);
-- Maintain legacy table for now too
ALTER TABLE assetviolation ADD COLUMN IF NOT EXISTS statuschangedbytype TEXT;
CREATE INDEX IF NOT EXISTS assetviolation_statuschangedbytype ON assetviolation(statuschangedbytype);

UPDATE assetviolations_removepolicyid
SET status = 'CLOSED',
    statuschangedbytype = 'RULE_EVALUATOR'
WHERE status = 'CLOSED_AS_VERIFIED';

UPDATE assetviolations_removepolicyid
SET status = 'CLOSED',
    statuschangedbytype = 'USER'
WHERE status IN ('CLOSED_PENDING_VERIFICATION', 'CLOSED_BY_USER');

UPDATE assetviolations_removepolicyid
SET status = 'CLOSED',
    statuschangedbytype = 'RULE_EVALUATOR'
WHERE status = 'CLOSED_AS_ASSET_DELETED';

UPDATE assetviolations_removepolicyid
SET status = 'RISK_ACCEPTED',
    statuschangedbytype = 'USER'
WHERE status = 'IGNORED';

-- Duplicate operations for legacy table
UPDATE assetviolation
SET status = 'CLOSED',
    statuschangedbytype = 'RULE_EVALUATOR'
WHERE status = 'CLOSED_AS_VERIFIED';

UPDATE assetviolation
SET status = 'CLOSED',
    statuschangedbytype = 'USER'
WHERE status IN ('CLOSED_PENDING_VERIFICATION', 'CLOSED_BY_USER');

UPDATE assetviolation
SET status = 'CLOSED',
    statuschangedbytype = 'RULE_EVALUATOR'
WHERE status = 'CLOSED_AS_ASSET_DELETED';

UPDATE assetviolation
SET status = 'RISK_ACCEPTED',
    statuschangedbytype = 'USER'
WHERE status = 'IGNORED';

ALTER TABLE assetviolations_removepolicyid ADD CONSTRAINT av_status_types
CHECK (status IN (
                  'OPEN',
                  'CLOSED',
                  'FALSE_POSITIVE',
                  'RISK_ACCEPTED',
                  'IN_PROGRESS'
));

COMMIT;
