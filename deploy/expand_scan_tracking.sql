-- Deploy database-schema:expand_scan_tracking to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scannerjobpage
(
    id              UUID PRIMARY KEY NOT NULL,
    jobRunId        UUID REFERENCES scannerjobruns(id),
    jobId           UUID REFERENCES scannerjob(id),
    -- The type of scan (resumption, archive, object)
    pagetype       text,
    -- Per page number of objects to be scanned
    objectCount     int,
    -- Per page size of objects to be scanned
    objectSize      int,
    -- When we created the scan
    createdat       timestamptz,
    -- When this scan page was reported complete
    finishedat       timestamptz
);
COMMIT;
