-- Deploy database-schema:scanner-job-run-progress-columns to pg

ALTER TABLE scannerjobruns ADD COLUMN numerator bigint NOT NULL default 0;
ALTER TABLE scannerjobruns ADD COLUMN denominator bigint NOT NULL default 0;
ALTER TABLE scannerjobruns ADD COLUMN scannerjobid UUID default NULL;
