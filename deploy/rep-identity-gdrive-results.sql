-- Deploy database-schema:rep-identity-gdrive-results to pg

BEGIN;

ALTER TABLE gdrivescanfinding REPLICA IDENTITY FULL;

COMMIT;
