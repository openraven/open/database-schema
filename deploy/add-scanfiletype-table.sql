-- Deploy database-schema:add-scanfiletype-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scanfiletype
(
    mimetype        TEXT NOT NULL PRIMARY KEY,
    extensions      jsonb,
    isarchive       boolean NOT NULL,
    enabled         boolean NOT NULL DEFAULT true,
    maxFileSize     INT,
    createdat       timestamptz DEFAULT CURRENT_TIMESTAMP,
    updatedat       timestamptz
);

INSERT INTO scanfiletype(mimetype, extensions, isarchive, maxFileSize)
VALUES
    ('text/plain', '["txt", "log", "sh", "bash", "conf", "config", "cmd", "cs", "go"]', false, 50000),
    ('text/csv', '["csv"]', false, 50000),
    ('text/html', '["html", "htm"]', false, 500),
    ('application/x-yaml', '["yml", "yaml"]', false, 1000),
    ('text/yaml', null, false, 1000),
    ('application/xhtml+xml', '["xhtml"]', false, 500),
    ('application/xml', '["xml"]', false, 500),
    ('text/xml', null, false, 500),
    ('application/json', '["json"]', false, 8000),
    ('application/msword', '["doc"]', false, 100),
    ('application/vnd.openxmlformats-officedocument.wordprocessingml.document', '["docx"]', false, 100),
    ('application/pdf', '["pdf"]', false, 120),
    ('application/vnd.cups-pdf', null, false, 120),
    ('application/vnd.ms-powerpoint', '["ppt"]', false, 1000),
    ('application/vnd.openxmlformats-officedocument.presentationml.presentation', '["pptx"]', false, 100),
    ('application/vnd.visio', '["vsd"]', false, 100),
    ('application/vnd.ms-excel', '["xls"]', false, 100),
    ('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '["xlsx"]', false, 100),
    ('application/vnd.oasis.opendocument.text', '["odt"]', false, 1000),
    ('application/x-vnd.oasis.opendocument.text', null, false, 1000),
    ('application/vnd.oasis.opendocument.spreadsheet', '["odc"]', false, 1000),
    ('application/x-vnd.oasis.opendocument.spreadsheet', null, false, 1000),
    ('application/vnd.oasis.opendocument.presentation', '["odp"]', false, 1000),
    ('application/x-vnd.oasis.opendocument.presentation', null, false, 1000),
    ('application/x-parquet', '["parquet", "parq"]', false, 300),
    ('application/avro', '["avro"]', false, 8000),
    ('application/javascript', '["js"]', false, 1000),
    ('application/vnd.ms-outlook-pst', '["pst"]', false, 500),
    ('message/rfc822', '["eml"]', false, 1000),
    ('application/vnd.google-apps.document', null, false, 50),
    ('application/vnd.google-apps.spreadsheet', null, false, 50),
    ('application/vnd.google-apps.presentation', null, false, 100),

    ('application/x-archive', '["ar"]', true, 8000),
    ('application/x-arj', '["arj"]', true, 1000),
    ('application/x-brotli', '["br"]', true, 8000),
    ('application/x-bzip', '["bz"]', true, 8000),
    ('application/x-bzip2', '["bz2"]', true, 8000),
    ('application/x-cpio', '["cpio"]', true, 8000),
    ('application/zlib', '["deflate"]', true, 8000),
    ('application/x-gtar', '["gtar"]', true, 8000),
    ('application/gzip', '["gz", "gzip"]', true, 8000),
    ('application/x-gzip', null, true, 8000),
    ('application/x-lz4', '["lz4"]', true, 8000),
    ('application/x-lzma', '["lzma"]', true, 8000),
    ('application/x-java-pack200', '["pack200"]', true, 1000),
    ('application/vnd.rar', '["rar"]', true, 8000),
    ('application/x-tar', '["tar"]', true, 8000),
    ('application/x-xz', '["xz"]', true, 8000),
    ('application/x-compress', '["z"]', true, 8000),
    ('application/zip', '["zip"]', true, 8000),
    ('application/x-zip', null, true, 8000);

INSERT INTO scanfiletype(mimetype, extensions, isarchive, enabled)
VALUES
    ('excluded', null, false, false);

COMMIT;


