-- Deploy database-schema:update-gdrive-views to pg

BEGIN;

DROP VIEW IF EXISTS v_suspiciousgoogledrivefilepermissions;
DROP VIEW IF EXISTS v_suspiciousgoogleshareddrivepermissions;
DROP VIEW IF EXISTS v_googledrivestalefiles;

CREATE VIEW v_googledrivestalefiles (
    assetid,
    fileid,
    driveid,
    filename,
    username,
    fileurl,
    filecreatedat,
    fileupdatedat,
    fileaccessedat,
    recordupdatedat,
    ignored,
    drivename,
    restrictions
    ) AS
SELECT
    s.assetid,
    s.fileid,
    s.driveid,
    s.filename,
    s.username,
    s.fileurl,
    s.filecreatedat,
    s.fileupdatedat,
    s.fileaccessedat,
    s.updatedat,
    s.ignored,
    ga.namelabel,
    ga.configuration->>'restrictions'
FROM googledrivestalefiles s INNER JOIN v_globalassetscomplete ga ON s.assetid=ga.assetid;

CREATE VIEW v_suspiciousgoogledrivefilepermissions (
    assetid,
    fileid,
    driveid,
    filename,
    username,
    fileurl,
    suspiciousemails,
    groupemails,
    anyonewithlink,
    updatedat,
    drivename,
    restrictions
    ) AS
SELECT s.assetid,
       s.fileid,
       s.driveid,
       s.filename,
       s.username,
       s.fileurl,
       s.suspiciousemails,
       s.groupemails,
       s.sharedbylink,
       s.updatedat,
       ga.namelabel,
       ga.configuration->>'restrictions'
FROM suspiciousgoogledrivefilepermissions s INNER JOIN v_globalassetscomplete ga ON s.assetid=ga.assetid;

CREATE VIEW v_suspiciousgoogleshareddrivepermissions (
    assetid,
    driveid,
    drivename,
    suspiciousemails,
    groupemails,
    updatedat,
    restrictions
    ) AS
SELECT s.assetid,
       s.driveid,
       s.drivename,
       s.suspiciousemails,
       s.groupemails,
       s.updatedat,
       ga.configuration->>'restrictions'
FROM suspiciousgoogleshareddrivepermissions s INNER JOIN v_globalassetscomplete ga ON s.assetid=ga.assetid;

COMMIT;
