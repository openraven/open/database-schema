-- Deploy database-schema:add-pre-aggregated-file-view to pg

BEGIN;

CREATE MATERIALIZED VIEW mat_v_gdrive_file_aggregate_data AS
SELECT gf.assetid,
       gf.fileid,
       JSONB_AGG(DISTINCT gscan.dataclassid) AS dataclasses,
       COALESCE(JSONB_AGG(distinct gvio.gdriveruleid)
                FILTER (WHERE gvio.gdriveruleid IS NOT NULL and gvio.status = 'OPEN'), '[]') AS rulesviolated,
       JSONB_AGG(DISTINCT jsonb_build_object('id', gscan.dataclassid, 'count', gscan.findingscount)) AS dataclasscounts
FROM googledrivefiles gf
         INNER JOIN scanfinding gscan ON gf.assetid = gscan.assetid AND gscan.scantarget = gf.fileid
         LEFT JOIN gdriveviolations gvio ON gf.assetid=gvio.assetid AND gf.fileid = gvio.nodeid
WHERE gscan.ignored = false
  AND gscan.falsepositive = false
GROUP BY gf.assetid, gf.fileid;

CREATE UNIQUE INDEX mat_v_gdrive_file_aggregate_data_unique ON mat_v_gdrive_file_aggregate_data(assetid,fileid);
CREATE INDEX mat_v_gdrive_file_aggregate_data_dataclasses_btree ON mat_v_gdrive_file_aggregate_data USING btree(dataclasses);
CREATE INDEX mat_v_gdrive_file_aggregate_data_rulesviolated_btree ON mat_v_gdrive_file_aggregate_data USING btree(rulesviolated);

COMMIT;
