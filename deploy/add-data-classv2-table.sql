-- Deploy database-schema:add-data-classv2-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS dataclassv2
(
    id UUID NOT NULL PRIMARY KEY,
    name TEXT,
    description TEXT,
    category TEXT,
    dataclasssource TEXT,
    refid TEXT,
    status TEXT
);

INSERT INTO dataclassv2(id,
                        name,
                        description,
                        category,
                        dataclasssource,
                        refid,
                        status)
SELECT id, name, description, category, 'dataclass', refid, status
FROM dataclass;

INSERT INTO dataclassv2(id,
                        name,
                        description,
                        category,
                        dataclasssource,
                        refid,
                        status)
SELECT id, name, description, category, 'metadataclass', refid, status
FROM metadataclass ON CONFLICT DO NOTHING;

INSERT INTO dataclassv2(id,
                        name,
                        description,
                        category,
                        dataclasssource,
                        refid,
                        status)
SELECT id, name, description, category, 'compositedataclass', refid, status
FROM compositedataclass ON CONFLICT DO NOTHING;

CREATE
OR REPLACE FUNCTION dataclass_populate_dataclassv2() RETURNS TRIGGER AS
$BODY$
BEGIN
INSERT INTO dataclassv2(id,
                        name,
                        description,
                        category,
                        dataclasssource,
                        refid,
                        status)
VALUES (new.id, new.name, new.description, new.category, TG_TABLE_NAME::regclass::text, new.refid, new.status) ON CONFLICT DO NOTHING;

RETURN new;
END;
$BODY$
LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS dataclass_create ON dataclass;
CREATE TRIGGER dataclass_create
    AFTER INSERT
    ON dataclass
    FOR EACH ROW
    EXECUTE PROCEDURE dataclass_populate_dataclassv2();

DROP TRIGGER IF EXISTS metadataclass_create ON metadataclass;
CREATE TRIGGER metadataclass_create
    AFTER INSERT
    ON metadataclass
    FOR EACH ROW
    EXECUTE PROCEDURE dataclass_populate_dataclassv2();

DROP TRIGGER IF EXISTS compositedataclass_create ON compositedataclass;
CREATE TRIGGER compositedataclass_create
    AFTER INSERT
    ON compositedataclass
    FOR EACH ROW
    EXECUTE PROCEDURE dataclass_populate_dataclassv2();

CREATE
OR REPLACE FUNCTION dataclassv2_update() RETURNS TRIGGER AS
$BODY$
BEGIN
UPDATE dataclass
SET name=new.name,
    description=new.description,
    category=new.category,
    refid=new.refid,
    status=new.status
WHERE id = new.id;

UPDATE metadataclass
SET name=new.name,
    description=new.description,
    category=new.category,
    refid=new.refid,
    status=new.status
WHERE id = new.id;

UPDATE compositedataclass
SET name=new.name,
    description=new.description,
    category=new.category,
    refid=new.refid,
    status=new.status
WHERE id = new.id;

RETURN new;
END;
$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS dataclassv2_update ON dataclassv2;
CREATE TRIGGER dataclassv2_update
    AFTER UPDATE
    ON dataclassv2
    FOR EACH ROW
    EXECUTE PROCEDURE dataclassv2_update();

ALTER TABLE compositedataclass NO INHERIT dataclass;

COMMIT;

