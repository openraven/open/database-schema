-- Deploy database-schema:add-onprem-to-scannerjob to pg

BEGIN;

ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS onprem boolean DEFAULT FALSE;

COMMIT;