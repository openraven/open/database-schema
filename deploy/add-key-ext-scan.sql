-- Deploy database-schema:add-key-ext-scan to pg

BEGIN;

INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.scan-externally-owned-enabled', 'true'::jsonb) ON CONFLICT (key) DO NOTHING;

COMMIT;
