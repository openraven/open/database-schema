-- Deploy database-schema:prepare-sdss-change-cap to pg

BEGIN;

ALTER TABLE sdss_scanfinding REPLICA IDENTITY FULL;

COMMIT;
