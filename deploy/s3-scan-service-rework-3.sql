BEGIN;
DO $$
    BEGIN
        if not exists (select constraint_name
                       from information_schema.constraint_column_usage
                       where table_name = 'awss3bucket'  and constraint_name = 'awss3bucket_arn_unique')
        THEN
            ALTER TABLE awss3bucket ADD CONSTRAINT awss3bucket_arn_unique UNIQUE (arn);
        END IF;
    END
$$;

ALTER TABLE scanfinding DROP CONSTRAINT IF EXISTS scanfinding_pkey;
ALTER TABLE scanfinding DROP CONSTRAINT IF EXISTS scanfinding_assetid_fkey;

CREATE TABLE IF NOT EXISTS s3scanfinding () INHERITS (scanfinding);
ALTER TABLE s3scanfinding DROP CONSTRAINT IF EXISTS s3scanfinding_assetid_fkey;
ALTER TABLE s3scanfinding DROP CONSTRAINT IF EXISTS s3scanfinding_pkey;
ALTER TABLE s3scanfinding ADD CONSTRAINT s3scanfinding_assetid_fkey FOREIGN KEY(assetid) REFERENCES awss3bucket(arn);
ALTER TABLE s3scanfinding ADD PRIMARY KEY (assetid, dataclassid, scantarget);
COMMIT;
