-- Deploy database-schema:update-change-cap-pub to pg

BEGIN;

-- Noop. Debezium manages it's own publication in version >2.
-- We do not want to manage this on new clusters.

COMMIT;
