-- Deploy database-schema:scan-audit-log-events-varchar-text to pg

BEGIN;

alter table scanauditlogevent alter column statusreason type text;
alter table scanauditlogevent alter column reportedmimetype type text;
alter table scanauditlogevent alter column detectedmimetype type text;
alter table scanauditlogevent alter column etag type text;
alter table scanauditlogevent alter column resourceid type text;

COMMIT;
