-- Deploy database-schema:add_schema_to_scanauditlogevent to pg

BEGIN;

ALTER TABLE scanauditlogevent ADD COLUMN IF NOT EXISTS schema TEXT DEFAULT NULL;
ALTER TABLE sdss_scanauditlogevent ADD COLUMN IF NOT EXISTS schema TEXT DEFAULT NULL;

COMMIT;
