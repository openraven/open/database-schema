-- Deploy database-schema:hackathon-dashboard to pg

BEGIN;

CREATE TABLE IF NOT EXISTS dashboardwidgets (
                                  key TEXT,
                                  evaluationnumber INTEGER,
                                  data JSONB NOT NULL,
                                  widgettypeversion INTEGER NOT NULL,
                                  dateevaluated timestamptz NOT NULL,
                                  PRIMARY KEY(key, evaluationnumber)
);

CREATE TABLE IF NOT EXISTS discoveredassets (
    assetid TEXT PRIMARY KEY NOT NULL,
    discovereddate TIMESTAMPTZ NOT NULL
);

INSERT INTO discoveredassets (assetid, discovereddate)
SELECT assetid, CURRENT_TIMESTAMP - INTERVAL '8 days'
FROM globalassets;

COMMIT;
