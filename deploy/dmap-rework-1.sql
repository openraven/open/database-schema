BEGIN;
ALTER TABLE dmap DROP CONSTRAINT IF EXISTS dmap_resourcename_fkey;
ALTER TABLE dmap DROP COLUMN IF EXISTS resourcename;
ALTER TABLE awsec2instance DROP CONSTRAINT IF EXISTS unique_resourcename CASCADE;
DO $$
    BEGIN
        if not exists (select constraint_name
                       from information_schema.constraint_column_usage
                       where table_name = 'awsec2instance'  and constraint_name = 'awsec2instance_arn_unique')
        THEN
            ALTER TABLE awsec2instance ADD CONSTRAINT awsec2instance_arn_unique UNIQUE (arn) ;
        END IF;
    END
$$;
ALTER TABLE dmap ADD COLUMN IF NOT EXISTS arn text REFERENCES awsec2instance(arn);
COMMIT;
