BEGIN;

create view v_active_compositescanfinding
            (scannerjobid, scannerjobrunid, assetid, scantarget, scantargetversion, dataclassid, findingscount,
             createdat, updatedat, scantargetchild, findingslocations)
as
SELECT compositescanfinding.scannerjobid,
       compositescanfinding.scannerjobrunid,
       compositescanfinding.assetid,
       compositescanfinding.scantarget,
       compositescanfinding.scantargetversion,
       compositescanfinding.dataclassid,
       compositescanfinding.findingscount,
       compositescanfinding.createdat,
       compositescanfinding.updatedat,
       compositescanfinding.scantargetchild,
       compositescanfinding.findingslocations
FROM compositescanfinding
WHERE alwaysfalsepositive = false AND falsepositive = false AND ignored = false;

alter table v_active_compositescanfinding
    owner to root;

grant select on v_active_compositescanfinding to orvn_ro;

COMMIT;