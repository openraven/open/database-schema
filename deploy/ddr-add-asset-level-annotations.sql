BEGIN;

CREATE TABLE IF NOT EXISTS ddr_asset_coverage (
    arn        text,
    coverage   jsonb not null,
    creatediso timestamp with time zone,
    updatediso timestamp with time zone,
    PRIMARY KEY (arn)
);

COMMIT;