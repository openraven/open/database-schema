BEGIN;

ALTER TABLE gdriverules ADD COLUMN ruletype text DEFAULT '';

INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.customFindingLimit', '10'::jsonb) ON CONFLICT (key) DO NOTHING;
INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.personalFindingLimit', '100'::jsonb) ON CONFLICT (key) DO NOTHING;
INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.healthFindingLimit', '10'::jsonb) ON CONFLICT (key) DO NOTHING;
INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.financialFindingLimit', '10'::jsonb) ON CONFLICT (key) DO NOTHING;
INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.devSecretFindingLimit', '10'::jsonb) ON CONFLICT (key) DO NOTHING;
INSERT INTO cluster.global_configuration (key, value) VALUES ('google-workspace.genericFindingLimit', '10'::jsonb) ON CONFLICT (key) DO NOTHING;

COMMIT;