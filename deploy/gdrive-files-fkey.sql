-- Deploy database-schema:gdrive-files-fkey to pg

BEGIN;

-- Remove all rows that do not adhere to the new foreign key constraint
DELETE FROM suspiciousgoogledrivefilepermissions
WHERE NOT EXISTS (
    SELECT 1
    FROM googledrivefiles
    WHERE googledrivefiles.assetid = suspiciousgoogledrivefilepermissions.assetid
      AND googledrivefiles.fileid = suspiciousgoogledrivefilepermissions.fileid
);

DELETE FROM googledrivestalefiles
WHERE NOT EXISTS (
    SELECT 1
    FROM googledrivefiles
    WHERE googledrivefiles.assetid = googledrivestalefiles.assetid
      AND googledrivefiles.fileid = googledrivestalefiles.fileid
);

-- Create fkeys
ALTER TABLE suspiciousgoogledrivefilepermissions
    ADD CONSTRAINT fk_suspiciousgoogledrivefilepermissions_assetid_fileid
        FOREIGN KEY (assetid, fileid)
            REFERENCES googledrivefiles(assetid, fileid)
            ON DELETE CASCADE;

ALTER TABLE googledrivestalefiles
    ADD CONSTRAINT fk_googledrivestalefiles_assetid_fileid
        FOREIGN KEY (assetid, fileid)
            REFERENCES googledrivefiles(assetid, fileid)
            ON DELETE CASCADE;

COMMIT;
