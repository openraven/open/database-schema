-- Deploy database-schema:drop-s3objects-cache to pg
BEGIN;

DROP TABLE IF EXISTS cached.s3objects CASCADE;
DELETE FROM partman.part_config WHERE parent_table = 'cached.s3objects';
DROP TABLE IF EXISTS partman.template_cached_s3objects;

COMMIT;