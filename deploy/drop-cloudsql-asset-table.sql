-- Deploy database-schema:drop-cloudsql-asset-table to pg

BEGIN;

DROP TABLE IF EXISTS cloudsqlasset;

COMMIT;