-- Deploy database-schema:prepare-data-table-normalization to pg

BEGIN;

-- Unused views
DROP VIEW IF EXISTS v_googledrivestalefiles;
DROP VIEW IF EXISTS v_suspiciousgoogledrivefilepermissions;

-- Prepare columns to be dropped
ALTER TABLE suspiciousgoogledrivefilepermissions ALTER COLUMN filename DROP NOT NULL;
ALTER TABLE suspiciousgoogledrivefilepermissions ALTER COLUMN fileurl DROP NOT NULL;
ALTER TABLE googledrivestalefiles ALTER COLUMN filename DROP NOT NULL;
ALTER TABLE googledrivestalefiles ALTER COLUMN fileurl DROP NOT NULL;

COMMIT;
