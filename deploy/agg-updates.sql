-- Deploy database-schema:agg-updates to pg

BEGIN;

ALTER TABLE s3aggregate add column if not exists assetid text;
ALTER TABLE s3aggregate RENAME COLUMN acccountid TO accountid;

COMMIT;
