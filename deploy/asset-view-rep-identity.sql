-- Deploy database-schema:asset-view-rep-identity to pg

BEGIN;

ALTER TABLE awsassetsview REPLICA IDENTITY FULL;

COMMIT;
