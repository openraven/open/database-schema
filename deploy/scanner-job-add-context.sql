-- Deploy database-schema:scanner-job-add-context.sql

ALTER table scannerjob ADD COLUMN IF NOT EXISTS context jsonb;