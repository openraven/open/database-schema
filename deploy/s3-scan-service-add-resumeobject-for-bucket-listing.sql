-- Deploy database-schema:s3-scan-service-add-resumeobject-for-bucket-listing to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS resumeobject JSONB;

COMMIT;
