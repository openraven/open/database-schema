-- Deploy database-schema:dropping-fileviolation-et-al-tables to pg

BEGIN;

DROP VIEW IF EXISTS v_assetviolation;
create or replace view v_assetviolation as
SELECT av.id           as id,
       av.datecreated  as datecreated,
       av.updatedat    as updatedat,
       av.resourcename as resourcename,
       av.policyid     as policyid,
       av.ruleid       as ruleid,
       av.status       as status,
       av.awsaccountid as awsaccountid,
       av.awsregion    as awsregion,
       av.objectcount  as objectcount
FROM assetviolation av
         JOIN policies p on av.policyid = p.id
         JOIN rules r on av.ruleid = r.id;

DROP TABLE IF EXISTS fileviolationdataclass;
DROP TABLE IF EXISTS fileviolationdataclass_opa;
DROP TABLE IF EXISTS fileviolation;
DROP TABLE IF EXISTS fileviolation_opa;

COMMIT;
