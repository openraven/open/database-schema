-- Deploy database-schema:scanner-job-run-lastupdateat-column to pg

ALTER TABLE scannerjobruns ADD COLUMN IF NOT EXISTS lastupdateat timestamp with time zone;
