
BEGIN;

create table if not exists sdss_finishedscans
(
    scanjobrunid         uuid not null,
    scanjobid            uuid not null,
    scancompletiontime   timestamp with time zone,
    lastsendtime         timestamp with time zone,
    eventssent           boolean,
    jvmid                bigint,
    lastprocessedeventid bigint,
    duration             bigint not null,
    reason               text,
    startedat            timestamp with time zone,
    finishedat           timestamp with time zone,
    constraint pk_sdss_finishedscans primary key (scanjobrunid)
);

create table if not exists sdss_scanauditlogevent
(
    resourceid       text not null,
    scantarget       text not null,
    childtarget      text not null,
    scanjobrunid     uuid not null,
    scanjobid        uuid not null,
    state            text not null,
    lastid           bigint,
    scaniso          timestamp with time zone,
    statusreason     text,
    schemaname text,
    tablename text,
    findingscount    integer default 0,
    primary key (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state)
);

create index if not exists sdss_scanauditlogevent_jobrunid_lastid_idx
    on sdss_scanauditlogevent (scanjobrunid, lastid);

COMMIT;
