BEGIN;

CREATE TABLE IF NOT EXISTS sdss_scanaccountandregion
(
    id          UUID PRIMARY KEY NOT NULL,
    jobid       UUID NOT NULL REFERENCES sdss_scannerjob(id),
    accountid   text NOT NULL,
    region      text NOT NULL
);
CREATE INDEX IF NOT EXISTS sdss_scanaccountandregion_jobid ON sdss_scanaccountandregion(jobId);
COMMIT;
