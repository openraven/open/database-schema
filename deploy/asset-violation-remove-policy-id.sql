-- Deploy database-schema:asset-violation-remove-policy-id to pg

BEGIN;

CREATE TABLE IF NOT EXISTS assetviolations_removepolicyid
(
    id                       uuid PRIMARY KEY NOT NULL,
    datecreated              bigint,
    updatedat                bigint,
    assetid                  text,
    ruleid                   uuid,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id)
            ON DELETE CASCADE,
    status                   text,
    previousstatus           text,
    awsaccountid             text,
    awsregion                text,
    resourcename             text,
    objectcount              bigint,
    external_link            text,
    external_key             text,
    statuschangedby          text,
    statuschangedbytimestamp bigint,
    CONSTRAINT unique_rule_assetid_sql UNIQUE (assetid, ruleid)
);
CREATE INDEX IF NOT EXISTS assetviolation2_assetid ON assetviolations_removepolicyid (assetid);
CREATE INDEX IF NOT EXISTS assetviolation2_ruleid ON assetviolations_removepolicyid (ruleid);
CREATE INDEX IF NOT EXISTS assetviolation2_status ON assetviolations_removepolicyid (status);

INSERT INTO assetviolations_removepolicyid(id,
                                datecreated,
                                updatedat,
                                assetid,
                                ruleid,
                                status,
                                previousstatus,
                                awsaccountid,
                                awsregion,
                                resourcename,
                                objectcount,
                                external_link,
                                external_key,
                                statuschangedby,
                                statuschangedbytimestamp)
SELECT id,
       datecreated,
       updatedat,
       assetid,
       ruleid,
       status,
       previousstatus,
       awsaccountid,
       awsregion,
       resourcename,
       objectcount,
       external_link,
       external_key,
       statuschangedby,
       statuschangedbytimestamp
FROM (SELECT id,
             datecreated,
             updatedat,
             assetid,
             ruleid,
             status,
             previousstatus,
             awsaccountid,
             awsregion,
             resourcename,
             objectcount,
             external_link,
             external_key,
             statuschangedby,
             statuschangedbytimestamp,
             ROW_NUMBER() OVER (
                 PARTITION BY assetid, ruleid
                 order by
                     case
                         when status = 'CLOSED_BY_USER' then 0
                         when status = 'CLOSED_AS_VERIFIED' then 1
                         when status = 'CLOSED_PENDING_VERIFICATION' then 2
                         when status = 'CLOSED_AS_ASSET_DELETED' then 3
                         when status = 'RISK_ACCEPTED' then 4
                         when status = 'FALSE_POSITIVE' then 5
                         when status = 'IGNORED' then 6
                         when status = 'IN_PROGRESS' then 7
                         when status = 'OPEN' then 8
                         END
                     DESC) as row_num
      from assetviolation
     ) t
WHERE t.row_num = 1
ON CONFLICT DO NOTHING;


COMMIT;
