-- Deploy database-schema:add_arn_index_on_awsec2securitygroup to pg

BEGIN;

CREATE INDEX IF NOT EXISTS awsec2securitygroup_arn ON awsec2securitygroup (arn);

COMMIT;
