-- Deploy database-schema:add-account-to-alias-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS accounttoaccountalias (
    accountid TEXT PRIMARY KEY,
    accountalias TEXT
);

COMMIT;
