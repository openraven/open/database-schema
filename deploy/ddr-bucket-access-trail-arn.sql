BEGIN;

ALTER TABLE ddr_bucket_access ADD COLUMN IF NOT EXISTS trailarn TEXT DEFAULT '';
ALTER TABLE ddr_bucket_access DROP CONSTRAINT IF EXISTS ddr_bucket_access_pkey;
ALTER TABLE ddr_bucket_access ADD CONSTRAINT ddr_bucket_access_pkey PRIMARY KEY (trailarn, bucketname);

COMMIT;