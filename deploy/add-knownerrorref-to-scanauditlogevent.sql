-- Deploy database-schema:add-knownerrorref-to-scanauditlogevent to pg

BEGIN;

ALTER TABLE scanauditlogevent ADD COLUMN IF NOT EXISTS knownerrorref varchar DEFAULT NULL;

COMMIT;
