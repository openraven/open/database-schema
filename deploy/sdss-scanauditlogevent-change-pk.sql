-- Deploy database-schema:sdss-scanauditlogevent-change-pk to pg

BEGIN;

ALTER TABLE sdss_scanauditlogevent DROP CONSTRAINT IF EXISTS sdss_scanauditlogevent_pkey;

UPDATE sdss_scanauditlogevent SET schemaname = '' WHERE schemaname IS null;

UPDATE sdss_scanauditlogevent SET tablename = '' WHERE tablename IS null;

ALTER TABLE sdss_scanauditlogevent ALTER COLUMN schemaname SET NOT null;

ALTER TABLE sdss_scanauditlogevent ALTER COLUMN tablename SET NOT null;

ALTER TABLE sdss_scanauditlogevent ADD CONSTRAINT sdss_scanauditlogevent_pkey PRIMARY KEY (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state, schemaname, tablename);

COMMIT;
