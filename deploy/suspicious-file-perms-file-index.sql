-- Deploy database-schema:suspicious-file-perms-file-index to pg

BEGIN;

CREATE INDEX IF NOT EXISTS suspiciousgoogledrivefilepermissions_fileid ON suspiciousgoogledrivefilepermissions(fileid);

COMMIT;
