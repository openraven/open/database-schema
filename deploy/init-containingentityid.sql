-- Deploy database-schema:init-containingentityid to pg

BEGIN;

UPDATE globalassets SET containingentityid=coalesce(
            "locationblob" ->> 'accountId',
            "locationblob" ->> 'projectId'
    );

COMMIT;