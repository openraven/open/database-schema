-- Deploy database-schema:file-list-indices to pg

BEGIN;

CREATE INDEX IF NOT EXISTS googledrivefiles_mimetype ON googledrivefiles(mimetype);
CREATE INDEX IF NOT EXISTS gdrivescanfinding_dataclassid ON gdrivescanfinding(dataclassid);
CREATE INDEX IF NOT EXISTS suspiciousemails_fts ON suspiciousgoogledrivefilepermissions USING gin (to_tsvector('english', suspiciousemails::text));

-- Truncate very long file names.
-- Otherwise index creation fails due to long index key length.
-- Due to customers putting file contents in file names (which gdrive allows you to do)
-- Only an issue in older clusters. Newly discovered files are automatically truncated
UPDATE googledrivefiles
SET filename = LEFT(filename, 200)
WHERE length(filename) > 200;

CREATE INDEX IF NOT EXISTS googledrivefiles_filename ON googledrivefiles(filename);

COMMIT;
