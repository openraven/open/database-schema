-- Deploy database-schema:add-perm-col-shared-drive to pg

BEGIN;

ALTER TABLE suspiciousgoogleshareddrivepermissions ADD COLUMN IF NOT EXISTS permissions JSONB DEFAULT json_build_array();

COMMIT;
