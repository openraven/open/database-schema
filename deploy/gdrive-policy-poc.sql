-- Deploy database-schema:gdrive-policy-poc to pg

BEGIN;
CREATE TABLE IF NOT EXISTS gdriveviolations
(
    id             uuid PRIMARY KEY NOT NULL,
    datecreated    timestamptz,
    updatedat      timestamptz,
    nodeid         TEXT             NOT NULL,
    nodename       TEXT             NOT NULL,
    nodetype       TEXT             NOT NULL,
    gdriveruleid   uuid,
    status         text,
    previousstatus text,
    supplementalinformation JSONB,
    CONSTRAINT gdrive_unique_violation UNIQUE (nodeid, nodetype, gdriveruleid)
);

CREATE TABLE IF NOT EXISTS gdriverules
(
    id          uuid PRIMARY KEY NOT NULL,
    name        text             NOT NULL,
    description text,
    sql         text             NOT NULL,
    enabled     boolean,
    managed     boolean,
    datecreated timestamptz,
    updatedat   timestamptz,
    nodetype    text NOT NULL,
    severity    text,
    schedule    text NOT NULL
);

-- CONFIG values for gDrive violations
-- ex. key: 'ALLOWED_EXTERNAL_DOMAINS', values: ['openraven.com', 'gmail.com']
CREATE TABLE IF NOT EXISTS gdriveviolationsconfig
(
    key    TEXT PRIMARY KEY NOT NULL,
    values JSONB            NOT NULL
);


CREATE VIEW v_gdriveviolations
            (
             id,
             nodeid,
             nodename,
             nodetype,
             updatedat,
             datecreated,
             gdriveruleid,
             gdriverulename,
             severity,
             status,
             previousstatus,
             supplementalinformation
                )
AS
SELECT v.id,
       v.nodeid,
       v.nodename,
       v.nodetype,
       v.updatedat,
       v.datecreated,
       v.gdriveruleid,
       r.name,
       r.severity,
       v.status,
       v.previousstatus,
       v.supplementalinformation
FROM gdriveviolations v
INNER JOIN gdriverules r ON v.gdriveruleid = r.id;

COMMIT;
