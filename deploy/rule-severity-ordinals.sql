-- Deploy database-schema:rule-severity-ordinals to pg

BEGIN;

ALTER TABLE rules ADD COLUMN IF NOT EXISTS severityordinal smallint;
CREATE INDEX IF NOT EXISTS rules_severityordinal
    ON rules(severityordinal);
UPDATE rules SET severityordinal=0 WHERE severity='LOW';
UPDATE rules SET severityordinal=1 WHERE severity='MEDIUM';
UPDATE rules SET severityordinal=2 WHERE severity='HIGH';

COMMIT;
