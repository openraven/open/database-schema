-- Deploy database-schema:add-latest-report-job-run to pg

BEGIN;

ALTER TABLE reportjob ADD COLUMN IF NOT EXISTS latestrunid UUID;

WITH subquery AS (
    SELECT DISTINCT ON(run.reportjobid)
        run.reportjobid as reportjobid,
        run.id AS latestrunid
    FROM reportjobrun run
    ORDER BY run.reportjobid, run.datestarted DESC
)
UPDATE reportjob
SET latestrunid = subquery.latestrunid
FROM subquery
WHERE reportjob.id = subquery.reportjobid;

COMMIT;
