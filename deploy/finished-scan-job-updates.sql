-- Deploy database-schema:finished-scan-job-updates to pg

BEGIN;

ALTER TABLE scannerjobpage ADD COLUMN IF NOT EXISTS lambdaduration BIGINT;

ALTER TABLE finishedscans ADD COLUMN IF NOT EXISTS duration BIGINT;

ALTER TABLE finishedscans ADD COLUMN IF NOT EXISTS reason TEXT;

COMMIT;
