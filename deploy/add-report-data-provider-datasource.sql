-- Deploy database-schema:add-report-data-provider-datasource to pg

BEGIN;

CREATE TABLE IF NOT EXISTS report_operation_key_metadata (
    operation_key TEXT PRIMARY KEY NOT NULL,
    data_source_label TEXT NOT NULL
);

COMMIT;
