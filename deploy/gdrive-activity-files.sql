-- Deploy database-schema:gdrive-activity-files to pg

BEGIN;

-- Activity

CREATE TABLE IF NOT EXISTS googledriveactivitywatermark (
    assetid TEXT PRIMARY KEY,
    watermarktimestamp TIMESTAMPTZ,
    enabled BOOLEAN DEFAULT TRUE
);

CREATE INDEX IF NOT EXISTS googledriveactivitywatermark_enabled ON googledriveactivitywatermark(enabled);
ALTER TABLE googledriveactivitywatermark ADD CONSTRAINT googledriveactivitywatermark_assetid_fkey FOREIGN KEY(assetid) REFERENCES globalassets(assetid) ON DELETE CASCADE;

-- Files

CREATE TABLE IF NOT EXISTS googledrivefiles (
    assetid TEXT,
    fileid TEXT,
    filename TEXT NOT NULL,
    driveid TEXT,
    username TEXT NOT NULL,
    fileurl TEXT NOT NULL,
    parent TEXT,
    permissions JSONB,
    updatedat TIMESTAMPTZ,
    PRIMARY KEY(assetid, fileid)
);

CREATE INDEX IF NOT EXISTS googledrivefiles_parent ON googledrivefiles(parent);
CREATE INDEX IF NOT EXISTS googledrivefiles_fileid ON googledrivefiles(fileid);
ALTER TABLE googledrivefiles ADD CONSTRAINT googledrivefiles_assetid_fkey FOREIGN KEY(assetid) REFERENCES globalassets(assetid) ON DELETE CASCADE;

COMMIT;
