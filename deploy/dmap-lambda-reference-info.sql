-- Deploy database-schema:dmap-lambda-reference-info to pg
BEGIN;

CREATE TABLE IF NOT EXISTS dmaplambdareferenceinfo
(
    id  SERIAL PRIMARY KEY,
    lambdaref   text NOT NULL UNIQUE,
    createdat   timestamp with time zone NOT NULL,
    scantype    text NOT NULL,
    survivalcount int,
    schedulerid int,
    jobrunid    text NOT NULL,
    region      text NOT NULL,
    accountid   text NOT NULL,
    functionname text NOT NULL
);
CREATE INDEX IF NOT EXISTS dmaplambdarefinfo_createdat ON dmaplambdareferenceinfo(createdat);

COMMIT;