-- Deploy database-schema:migrate-gdrive-view-left-join to pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS permissions JSONB;

DROP VIEW v_gdrive_dash;

CREATE VIEW v_gdrive_dash AS SELECT gs.assetid, gs.fileid, gs.fileurl, gs.filename,
                                    ga.namelabel AS drivename, gp.username, gp.sharedbylink AS anyonewithlink,
                                    gp.suspiciousemails, gs.filecreatedat, gs.fileupdatedat, gs.fileaccessedat,
                                    gp.permissions as allpermissions, json_agg(gr.name) AS rulesviolated,
                                    gb.configuration->'restrictions' AS restrictions
FROM googledrivestalefiles gs
    INNER JOIN globalassets ga ON ga.assetid=gs.assetid
    INNER JOIN globalassetsblob gb ON gb.assetid=gs.assetid
    LEFT JOIN suspiciousgoogledrivefilepermissions gp ON gp.fileid=gs.fileid AND gp.username=gs.username
    LEFT JOIN gdriveviolations gv ON gs.fileid=gv.nodeid
    LEFT JOIN gdriverules gr ON gv.gdriveruleid=gr.id
WHERE ga.resourcetype IN ('GOOGLEWORKSPACE::SharedDrive','GOOGLEWORKSPACE::MyDrive')
GROUP BY gs.assetid, gs.fileid, gs.fileurl, gs.filename, ga.namelabel, gp.username, gp.sharedbylink, gp.suspiciousemails, gs.filecreatedat,
    gs.fileupdatedat, gs.fileaccessedat, gp.permissions, gb.configuration;

COMMIT;
