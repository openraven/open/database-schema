-- Deploy database-schema:jira_integration_track_jira_link to pg

BEGIN;

ALTER TABLE assetviolation ADD COLUMN IF NOT EXISTS external_link text;
ALTER TABLE assetviolation ADD COLUMN IF NOT EXISTS external_key text;

COMMIT;
