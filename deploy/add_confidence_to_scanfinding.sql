-- Deploy database-schema:add_confidence_to_scanfinding to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS confidencesum BIGINT;

COMMIT;
