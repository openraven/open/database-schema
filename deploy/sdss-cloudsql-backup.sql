-- Deploy database-schema:sdss-cloudsql-backup to pg

BEGIN;

create table if not exists sdss_cloudsql_env
(
    id                  uuid primary key not null,
    projectid           text not null,
    masterinstanceid    text not null
);

COMMIT;
