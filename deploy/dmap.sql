BEGIN;
DO $$
    BEGIN
        if not exists (select constraint_name
                       from information_schema.constraint_column_usage
                       where table_name = 'awsec2instance'  and constraint_name = 'unique_resourcename')
        THEN
            ALTER TABLE awsec2instance ADD CONSTRAINT unique_resourcename UNIQUE (resourcename);
        END IF;
    END
$$;

CREATE TABLE IF NOT EXISTS dmap (

    resourcename                text REFERENCES awsec2instance(resourcename),
    dmapnode                    jsonb,
    dmapmeta                    jsonb,
    hasapplications             boolean default false,
    lastqueuerequesttime        timestamptz,
    lastscancompletedtime       timestamptz
);
COMMIT;
