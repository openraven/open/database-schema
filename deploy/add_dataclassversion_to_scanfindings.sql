-- Deploy database-schema:add_dataclassversion_to_scanfindings to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN IF NOT EXISTS dataclassversion text;

COMMIT;
