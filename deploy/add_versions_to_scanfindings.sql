-- Deploy database-schema:add_versions_to_scanfindings to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN serviceversion bigint;

ALTER TABLE scanfinding ADD COLUMN scannerversion bigint;

ALTER TABLE scanfinding ADD COLUMN falsepositive boolean NOT NULL default false;

ALTER TABLE scanfinding ADD COLUMN alwaysfalsepositive boolean NOT NULL default false;

COMMIT;
