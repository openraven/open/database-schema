-- Deploy database-schema:add-trashed-update-index to pg

BEGIN;

CREATE INDEX IF NOT EXISTS googledrivefiles_updatedat ON googledrivefiles(updatedat);
CREATE INDEX IF NOT EXISTS googledrivefiles_trashed ON googledrivefiles(trashed);

COMMIT;
