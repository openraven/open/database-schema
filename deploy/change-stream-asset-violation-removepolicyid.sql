-- Deploy database-schema:change-stream-asset-violation-removepolicyid to pg

BEGIN;

ALTER TABLE assetviolations_removepolicyid REPLICA IDENTITY FULL;

COMMIT;
