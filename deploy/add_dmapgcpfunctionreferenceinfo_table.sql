-- Deploy database-schema:add_dmapgcpfunctionreferenceinfo_table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS gcpfunctionreferenceinfo
(
    id  SERIAL PRIMARY KEY,
    functionref   text NOT NULL UNIQUE,
    createdat   timestamp with time zone NOT NULL,
    lastupdateat timestamp with time zone NOT NULL,
    scantype    text NOT NULL,
    survivalcount int,
    schedulerid int,
    jobrunid    text NOT NULL,
    region      text NOT NULL,
    projectid   text NOT NULL,
    functionname text NOT NULL
);
CREATE INDEX IF NOT EXISTS gcpfunctionreferenceinfo_lastupdateat ON gcpfunctionreferenceinfo(lastupdateat);

COMMIT;
