-- Deploy database-schema:snowflake-reaper to pg

BEGIN;

ALTER TABLE globalassets ADD COLUMN manuallydiscovered BOOLEAN;
UPDATE globalassets SET manuallydiscovered = FALSE WHERE resourcetype != 'SNOWFLAKE::Account';
UPDATE globalassets SET manuallydiscovered = TRUE WHERE resourcetype = 'SNOWFLAKE::Account';
ALTER TABLE globalassets ALTER COLUMN manuallydiscovered SET NOT NULL;
ALTER TABLE globalassets ALTER COLUMN manuallydiscovered SET DEFAULT FALSE;
CREATE INDEX IF NOT EXISTS globalassets_manuallydiscovered ON globalassets(manuallydiscovered);

COMMIT;
