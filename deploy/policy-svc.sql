BEGIN;
CREATE TABLE IF NOT EXISTS policies
(
    id             uuid PRIMARY KEY NOT NULL,
    datecreated    int8,
    managed        boolean,
    name           text,
    updatedat      int8,
    vanity_id      bigint           NOT NULL,
    version        text,
    assetgroupid   uuid,
    description    text,
    enabled        boolean          NOT NULL,
    exclusions     text,
    lastexecutedat int8,
    refid          text,
    schedule       text
);

CREATE TABLE IF NOT EXISTS rules
(
    id          uuid PRIMARY KEY NOT NULL,
    datecreated int8,
    managed     boolean,
    name        text,
    updatedat   int8,
    vanity_id   bigint           NOT NULL,
    version     text,
    description text,
    enabled     boolean          NOT NULL,
    refid       text,
    rego        text,
    severity    text,
    type        text
);

CREATE TABLE IF NOT EXISTS policy_rule
(
    ruleid   uuid NOT NULL,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id),
    policyid uuid NOT NULL,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id)
);

CREATE TABLE IF NOT EXISTS issues
(
    id          uuid PRIMARY KEY NOT NULL,
    datecreated int8,
    managed     boolean,
    name        text,
    updatedat   int8,
    vanity_id   bigint           NOT NULL,
    version     text,
    last_closed int8,
    policyid    uuid,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id),
    ruleid      uuid,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id),
    status      text
);

CREATE TABLE IF NOT EXISTS issueremediation
(
    id            uuid PRIMARY KEY NOT NULL,
    datecreated   int8,
    managed       boolean,
    name          text,
    updatedat     int8,
    vanity_id     bigint           NOT NULL,
    version       text,
    integrationid uuid,
    misc          jsonb,
    timesent      int8,
    type          text,
    issueid       uuid,
    CONSTRAINT fk_issue
        FOREIGN KEY (issueid)
            REFERENCES issues (id)
);

CREATE TABLE IF NOT EXISTS policyaudit
(
    id          uuid PRIMARY KEY NOT NULL,
    datecreated int8,
    managed     boolean,
    name        text,
    updatedat   int8,
    vanity_id   bigint           NOT NULL,
    version     text,
    type        text,
    policyid    uuid,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id),
    assetviolationid uuid,
    evaluationstart int8,
    evaluationend int8,
    assetsevaluated int4,
    numberofviolations int4,
    initiatedbyuser text,
    oldstatus text,
    newstatus text,
    message text
);

CREATE TABLE IF NOT EXISTS assetviolation
(
    id          uuid PRIMARY KEY NOT NULL,
    datecreated int8,
    managed     boolean,
    name        text,
    updatedat   int8,
    vanity_id   bigint           NOT NULL,
    version     text,
    assetid     text,
    resourcename text,
    info        text,
    policyid    uuid,
    CONSTRAINT fk_policy
        FOREIGN KEY (policyid)
            REFERENCES policies (id),
    ruleid      uuid,
    CONSTRAINT fk_rule
        FOREIGN KEY (ruleid)
            REFERENCES rules (id),
    status      text,
    awsaccountid text,
    awsregion text,
    size bigint,
    objectcount bigint
);

CREATE TABLE IF NOT EXISTS fileviolation
(
    id uuid PRIMARY KEY NOT NULL,
    assetviolationid uuid,
    CONSTRAINT  fk_assetviolation
        FOREIGN KEY (assetviolationid)
            REFERENCES assetviolation (id),
    filename text,
    filesize bigint
);

CREATE TABLE IF NOT EXISTS fileviolationdataclass
(
    id uuid PRIMARY KEY NOT NULL,
    -- this will one day sort of be a foreign key. We don't allow class name
    -- changes at the moment
    dataclassname text,
    count bigint,
    fileviolationid uuid,
    CONSTRAINT fk_fileviolation
        FOREIGN KEY (fileviolationid)
            REFERENCES fileviolation (id)
);

CREATE TABLE IF NOT EXISTS issue_assetviolation
(
    assetviolationid uuid NOT NULL,
    CONSTRAINT fk_assetviolation
        FOREIGN KEY (assetviolationid)
            REFERENCES assetviolation (id),
    issueid          uuid NOT NULL,
    CONSTRAINT fk_issue
        FOREIGN KEY (issueid)
            REFERENCES issues (id)
);
COMMIT;
