-- Deploy database-schema:rds-aggregate to pg

BEGIN;

CREATE TABLE IF NOT EXISTS rdsaggregate (
  id uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  assetid text NOT NULL,
  region text,
  accountid text,
  sizeinbytes bigint,
  engine text,
  dateevaluated timestamptz NOT NULL
);

COMMIT;
