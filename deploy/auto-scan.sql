-- Deploy database-schema:auto-scan to pg

BEGIN;

CREATE TABLE IF NOT EXISTS autoscan
(
    id serial primary key not null,
    scannerjobid uuid not null,
    CONSTRAINT fk_scannerjob
        FOREIGN KEY (scannerjobid)
            REFERENCES scannerjob (id),
    durationtype text not null
);

COMMIT;
