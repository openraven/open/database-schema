BEGIN;

ALTER TABLE ddr_bucket_access ADD COLUMN IF NOT EXISTS noaccessreason TEXT;
ALTER TABLE ddr_cloudtrail_coverage ADD COLUMN IF NOT EXISTS noaccessreason TEXT;

COMMIT;
