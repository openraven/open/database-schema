-- Deploy database-schema:s3-scan-service-rework-4 to pg

BEGIN;

ALTER TABLE scanfinding ADD COLUMN scantargetchild text NOT NULL DEFAUlT '';

ALTER TABLE scanfinding ADD COLUMN findingslocations jsonb;

ALTER TABLE s3scanfinding DROP CONSTRAINT s3scanfinding_pkey;

ALTER TABLE s3scanfinding ADD PRIMARY KEY (assetid, dataclassid, scantarget, scantargetchild);

COMMIT;
