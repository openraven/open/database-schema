-- Deploy database-schema:sdss-cloudsql-asset-table to pg
BEGIN;

CREATE TABLE IF NOT EXISTS cloudsqlasset
(
    id                  TEXT NOT NULL,
    host                TEXT NOT NULL,
    port                INTEGER NOT NULL,
    databasetype        TEXT NOT NULL,
    dbname              TEXT NOT NULL,
    vpcnetwork          TEXT NOT NULL,
    PRIMARY KEY (id)
    );

COMMIT;