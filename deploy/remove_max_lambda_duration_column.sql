-- Deploy database-schema:remove_max_lambda_duration_column to pg

BEGIN;

ALTER TABLE scannerjob DROP COLUMN IF EXISTS maxlambdaduration;

COMMIT;
