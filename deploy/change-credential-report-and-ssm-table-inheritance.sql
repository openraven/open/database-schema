-- Deploy database-schema:change-credential-report-and-ssm-table-inheritance to pg

BEGIN;

CREATE TABLE IF NOT EXISTS extendawsiamusercredentialreport
(
    documentid                 text primary key not null,
    arn                        text,
    resourcename               text,
    resourceid                 text,
    resourcetype               text,
    awsregion                  text,
    awsaccountid               text,
    creatediso                 timestamptz,
    updatediso                 timestamptz,
    discoverysessionid         text,
    tags                       jsonb,
    configuration              jsonb,
    supplementaryConfiguration jsonb,
    discoverymeta              jsonb
);

CREATE TABLE IF NOT EXISTS extendawsssminstance
(
    documentid                 text primary key not null,
    arn                        text,
    resourcename               text,
    resourceid                 text,
    resourcetype               text,
    awsregion                  text,
    awsaccountid               text,
    creatediso                 timestamptz,
    updatediso                 timestamptz,
    discoverysessionid         text,
    tags                       jsonb,
    configuration              jsonb,
    supplementaryConfiguration jsonb,
    discoverymeta              jsonb
);

COMMIT;
