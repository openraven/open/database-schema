-- Deploy database-schema:asset-finding-aggregator-reseed-config to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scanfindingaggregatorreseedconfig (
    id SERIAL PRIMARY KEY NOT NULL,
    reseedtimestamp timestamp with time zone
);

INSERT INTO scanfindingaggregatorreseedconfig (id, reseedtimestamp) VALUES (1, NULL);

COMMIT;
