-- Deploy database-schema:gdrive-violations-rule-index to pg

BEGIN;

CREATE INDEX IF NOT EXISTS gdriveviolations_gdriveruleid ON gdriveviolations(gdriveruleid);
CREATE INDEX IF NOT EXISTS gdriverules_name_lower ON gdriverules(lower(name));

COMMIT;
