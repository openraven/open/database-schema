-- Deploy database-schema:scanned-assets to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scannedasset (
  assetid TEXT PRIMARY KEY NOT NULL
);
-- seed data with all assets we know have findings
INSERT INTO scannedasset (assetid) SELECT DISTINCT(assetid) FROM aggregatedassetfindings;
CREATE OR REPLACE VIEW v_globalassets AS
SELECT
    globalassets.assetid as assetid,
    locationstring,
    namelabel,
    externalidentifiers,
    resourcetype,
    resourcetypelabel,
    category,
    creatediso,
    updatediso,
    discoverymeta,
    deriveddatablob,
    storageinbytes,
    resourcetypesize,
    source,
    locationblob,
    extendedattributes,
    resourcemetadata,
    highestruleseverityviolated,
    dataclassmap,
    datacollectionmap,
    scannedasset.assetid IS NOT NULL as scanned
FROM globalassets
LEFT JOIN scannedasset ON scannedasset.assetid = globalassets.assetid
WHERE markedfordelete = 0;

CREATE OR REPLACE VIEW v_globalassetscomplete AS
SELECT
    globalassets.assetid,
    locationstring,
    namelabel,
    externalidentifiers,
    resourcetype,
    resourcetypelabel,
    category,
    creatediso,
    updatediso,
    discoverymeta,
    deriveddatablob,
    storageinbytes,
    resourcetypesize,
    source,
    locationblob,
    extendedattributes,
    resourcemetadata,
    highestruleseverityviolated,
    dataclassmap,
    datacollectionmap,
    configuration,
    supplementaryconfiguration,
    scannedasset.assetid IS NOT NULL as scanned
FROM globalassets
INNER JOIN globalassetsblob blobs ON blobs.assetid = globalassets.assetid
LEFT JOIN scannedasset ON scannedasset.assetid = globalassets.assetid
WHERE markedfordelete = 0;

COMMIT;
