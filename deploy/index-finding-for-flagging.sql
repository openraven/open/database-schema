-- Deploy database-schema:index-finding-for-flagging to pg

BEGIN;

CREATE INDEX IF NOT EXISTS scanfinding_scantarget ON scanfinding(scantarget);
CREATE INDEX IF NOT EXISTS scanfinding_scantargetchild ON scanfinding(scantargetchild);

COMMIT;
