-- Deploy database-schema:add-previously-scanned-files-to-scan-statistics.sql to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS previouslyscannedfilescount BIGINT DEFAULT 0;

COMMIT;
