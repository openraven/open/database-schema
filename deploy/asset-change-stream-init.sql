-- Deploy database-schema:asset-change-stream-init to pg

BEGIN;

CREATE TABLE IF NOT EXISTS aggregatedassetmetrics (
      assetid                 text,
      dataclassid             UUID NOT NULL,
      findingscount           bigint,
      region                  text NOT NULL,
      account                 text NOT NULL,
      updatedat               timestamp with time zone,
      PRIMARY KEY(assetid, dataclassid)
);

CREATE TABLE IF NOT EXISTS aggregatedscanmetrics (
     assetid                 text,
     dataclassid             UUID NOT NULL,
     findingscount           bigint,
     region                  text NOT NULL,
     account                 text NOT NULL,
     updatedat               timestamp with time zone,
     scannerjobid            UUID NOT NULL,
     PRIMARY KEY(assetid, scannerjobid, dataclassid)
);

CREATE INDEX IF NOT EXISTS aggregatedassetmetrics_region ON aggregatedassetmetrics(region);
CREATE INDEX IF NOT EXISTS aggregatedassetmetrics_dataclass ON aggregatedassetmetrics(dataclassid);
CREATE INDEX IF NOT EXISTS aggregatedassetmetrics_account ON aggregatedassetmetrics(account);

CREATE INDEX IF NOT EXISTS aggregatedscanmetrics_updateat ON aggregatedscanmetrics(updatedat);
CREATE INDEX IF NOT EXISTS aggregatedscanmetrics_region ON aggregatedscanmetrics(region);
CREATE INDEX IF NOT EXISTS aggregatedscanmetrics_dataclass ON aggregatedscanmetrics(dataclassid);
CREATE INDEX IF NOT EXISTS aggregatedscanmetrics_account ON aggregatedscanmetrics(account);

-- Necessary for debezium to capture all columns in changes.
ALTER TABLE s3scanfinding REPLICA IDENTITY FULL;

COMMIT;
