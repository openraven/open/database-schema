-- Deploy database-schema:add-gdrive-scan-tables.sql to pg

BEGIN;

CREATE TABLE IF NOT EXISTS gdrivescanfinding () INHERITS (scanfinding);

ALTER TABLE gdrivescanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);

CREATE INDEX IF NOT EXISTS gdcanfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON gdrivescanfinding(assetid,scantarget,scantargetchild,scannerjobid);

-- see file : add-composite-scan-indexes.sql
CREATE INDEX IF NOT EXISTS gdrivescanfinding_composite_scan_idx ON gdrivescanfinding
    USING btree (dataclassid, falsepositive, alwaysfalsepositive, ignored);

COMMIT;