-- Deploy database-schema:add-scantime-gdrivedash to pg

BEGIN;

DROP VIEW IF EXISTS v_gdrive_dash;

CREATE VIEW v_gdrive_dash AS
SELECT main.assetid,
       main.fileid,
       main.fileurl,
       main.filename,
       ga.namelabel AS drivename,
       main.username,
       s1.sharedbylink AS anyonewithlink,
       s1.suspiciousemails,
       main.filecreatedat,
       main.fileupdatedat,
       main.fileaccessedat,
       ga.resourcetype,
       main.restrictions,
       main.mimetype,
       main.updatedat,
       main.rulesviolated,
       main.lastsuccessfulscan,
       dclasses.data_classes,
       dclasses.data_collections,
       s2.emaildomains,
       s2.emailusers
FROM ((((( SELECT gf.assetid,
                  gf.fileid,
                  gf.fileurl,
                  gf.filename,
                  gf.username,
                  gf.lastsuccessfulscan,
                  (array_agg(gs.filecreatedat))[1] AS filecreatedat,
            (array_agg(gs.fileupdatedat))[1] AS fileupdatedat,
            (array_agg(gs.fileaccessedat))[1] AS fileaccessedat,
            ((array_agg(gb.configuration))[1] -> 'restrictions'::text) AS restrictions,
            gf.mimetype,
            gf.updatedat,
            COALESCE(json_agg(DISTINCT gr.name) FILTER (WHERE (gr.name IS NOT NULL)), NULL::json) AS rulesviolated
           FROM ((((googledrivefiles gf
               JOIN globalassetsblob gb ON ((gb.assetid = gf.assetid)))
               LEFT JOIN googledrivestalefiles gs ON (((gf.assetid = gs.assetid) AND (gf.fileid = gs.fileid))))
               LEFT JOIN gdriveviolations gv ON ((gf.fileid = gv.nodeid)))
               LEFT JOIN gdriverules gr ON ((gv.gdriveruleid = gr.id)))
           GROUP BY gf.assetid, gf.fileid) main
    JOIN globalassets ga ON ((main.assetid = ga.assetid)))
    LEFT JOIN LATERAL ( SELECT sf.assetid,
                               sf.scantarget,
                               COALESCE(json_agg(DISTINCT dc.name) FILTER (WHERE (dc.name IS NOT NULL)), NULL::json) AS data_classes,
                               COALESCE(json_agg(DISTINCT dx.name) FILTER (WHERE (dx.name IS NOT NULL)), NULL::json) AS data_collections
                        FROM (((gdrivescanfinding sf
                            JOIN dataclassv2 dc ON ((dc.id = sf.dataclassid)))
                            JOIN datacollection_dataclass dj ON ((dj.dataclassid = dc.id)))
                            JOIN datacollection dx ON ((dx.id = dj.datacollectionid)))
                        WHERE ((sf.scantarget = main.fileid) AND (sf.assetid = main.assetid))
                        GROUP BY sf.assetid, sf.scantarget) dclasses ON (true))
    LEFT JOIN LATERAL ( SELECT COALESCE(
                                       CASE
                                           WHEN (jsonb_array_length(gp.suspiciousemails) = 0) THEN NULL::jsonb
                                           ELSE gp.suspiciousemails
                                           END, NULL::jsonb) AS suspiciousemails,
                               gp.sharedbylink
                        FROM suspiciousgoogledrivefilepermissions gp
                        WHERE ((gp.assetid = main.assetid) AND (gp.fileid = main.fileid))) s1 ON (true))
    LEFT JOIN LATERAL ( SELECT array_to_json(COALESCE(array_agg(DISTINCT "substring"(emails.emails, '@(.+)$'::text)), '{}'::text[])) AS emaildomains,
                               array_to_json(COALESCE(array_agg(DISTINCT "substring"(emails.emails, '(.+)@'::text)), '{}'::text[])) AS emailusers
                        FROM ( SELECT jsonb_array_elements_text(gp.suspiciousemails) AS emails
                               FROM suspiciousgoogledrivefilepermissions gp
                               WHERE ((gp.assetid = main.assetid) AND (gp.fileid = main.fileid))) emails) s2 ON (true))
WHERE (ga.resourcetype = ANY (ARRAY['GOOGLEWORKSPACE::SharedDrive'::text, 'GOOGLEWORKSPACE::MyDrive'::text]));

COMMIT;
