BEGIN;

CREATE TABLE IF NOT EXISTS ddr_rules (
    id integer,
    rule_name text,
    description text,
    is_enabled boolean,
    alert_level varchar,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS ddr_state_store (
   event_source varchar,
   state_store_name varchar,
   key varchar,
   value text,
   PRIMARY KEY (event_source, state_store_name, key)
);

CREATE TABLE IF NOT EXISTS ddr_metadata (
    folder       text,
    datasourceid text,
    lastpolled   text,
    creatediso   timestamp with time zone,
    updatediso   timestamp with time zone,
    PRIMARY KEY (folder)
);


CREATE INDEX IF NOT EXISTS ddr_metadata_datasourceid ON ddr_metadata (datasourceid);

CREATE TABLE IF NOT EXISTS ddr_alerts (
    id                  serial primary key not null,
    source_log          varchar not null default '',
    alert_rule_id       integer not null,
    alert_rule_name     varchar not null,
    alert_rule_desc     varchar not null,
    alert_level         varchar not null,
    alert_generation_ts timestamptz not null,
    alert_text          varchar not null,
    event_source        jsonb,
    events              jsonb
);

COMMIT;    