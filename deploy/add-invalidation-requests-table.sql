-- Deploy database-schema:add-invalidationrequests-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS cached.invalidationrequests
(
  id            UUID NOT NULL PRIMARY KEY,
  cacheindex    INT,
  createdat     TIMESTAMP,
  executedat    TIMESTAMP
);

COMMIT;

