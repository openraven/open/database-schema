-- Deploy database-schema:add-gin-filename-index to pg

BEGIN;

CREATE INDEX IF NOT EXISTS googledrivefiles_filename_gin ON googledrivefiles USING GIN(filename gin_trgm_ops);

COMMIT;
