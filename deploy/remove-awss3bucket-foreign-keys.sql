-- Deploy database-schema:remove-awss3bucket-foreign-keys to pg

BEGIN;

ALTER TABLE s3scanfinding DROP CONSTRAINT IF EXISTS s3scanfinding_assetid_fkey;
ALTER TABLE s3metadatascanfinding DROP CONSTRAINT IF EXISTS metadatafinding_assetid_fkey;

COMMIT;
