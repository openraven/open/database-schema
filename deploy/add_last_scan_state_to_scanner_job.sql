-- Deploy database-schema:add_last_scan_state_to_scanner_job to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scannerjobruns
(
    id                          UUID PRIMARY KEY NOT NULL,
    lastscannedobjectkey        text,
    lastscannedbucketarn        text,
    lastscanpagecount           bigint DEFAULT (0),
    lastscanstarttime           timestamp,
    status                      text
);
ALTER TABLE scannerjob ADD COLUMN lastscanjobrunid uuid;
ALTER TABLE scannerjob ADD CONSTRAINT lastscanjobrunid_fkey FOREIGN KEY (lastscanjobrunid) REFERENCES scannerjobruns(id);
COMMIT;
