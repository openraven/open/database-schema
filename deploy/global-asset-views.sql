-- Deploy database-schema:global-asset-views to pg

BEGIN;

ALTER TABLE globalassets DROP COLUMN IF EXISTS configurationblob;

CREATE OR REPLACE VIEW v_globalassets AS
SELECT
    assetid,
    locationstring,
    namelabel,
    externalidentifiers,
    resourcetype,
    resourcetypelabel,
    category,
    creatediso,
    updatediso,
    discoverymeta,
    deriveddatablob,
    storageinbytes,
    resourcetypesize,
    source,
    locationblob,
    extendedattributes,
    resourcemetadata,
    highestruleseverityviolated,
    dataclassmap,
    datacollectionmap
FROM globalassets WHERE markedfordelete = 0;

CREATE OR REPLACE VIEW v_globalassetscomplete AS
SELECT
    globalassets.assetid,
    locationstring,
    namelabel,
    externalidentifiers,
    resourcetype,
    resourcetypelabel,
    category,
    creatediso,
    updatediso,
    discoverymeta,
    deriveddatablob,
    storageinbytes,
    resourcetypesize,
    source,
    locationblob,
    extendedattributes,
    resourcemetadata,
    highestruleseverityviolated,
    dataclassmap,
    datacollectionmap,
    configuration,
    supplementaryconfiguration
FROM globalassets
INNER JOIN globalassetsblob blobs ON blobs.assetid = globalassets.assetid
WHERE markedfordelete = 0;

COMMIT;
