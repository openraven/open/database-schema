-- Deploy database-schema:remediation-action-log-fixes to pg

BEGIN;

ALTER TABLE remediationactionlog DROP COLUMN IF EXISTS violationid;
ALTER TABLE remediationactionlog DROP COLUMN IF EXISTS violationtable;
ALTER TABLE remediationactionlog RENAME COLUMN exceptionstack_trace TO exceptionstacktrace;

COMMIT;
