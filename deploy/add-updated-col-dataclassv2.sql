-- Deploy database-schema:add-updated-col-dataclassv2 to pg

BEGIN;

ALTER TABLE dataclassv2 ADD COLUMN IF NOT EXISTS updatedat timestamp with time zone;
CREATE INDEX IF NOT EXISTS dataclassv2_name ON dataclassv2(name);

COMMIT;
