-- Deploy database-schema:sdss-drop-scanfinding-assetid-constraint to pg

BEGIN;

alter table sdss_scanfinding drop constraint if exists sdss_scanfinding_assetid_fkey;

COMMIT;
