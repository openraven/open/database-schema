-- Deploy database-schema:recreate-views-for-dbx to pg

BEGIN;
-- have to drop the view first, then create
DROP VIEW IF EXISTS v_assetviolation;
create or replace view v_assetviolation as
SELECT av.id           as id,
       av.datecreated  as datecreated,
       av.updatedat    as updatedat,
       av.resourcename as resourcename,
       av.policyid     as policyid,
       av.ruleid       as ruleid,
       av.status       as status,
       av.awsaccountid as awsaccountid,
       av.awsregion    as awsregion,
       av.objectcount  as objectcount,
       fv.filename     as filename,
       fv.sourcechild  as sourcechild,
       fvdc.dataclassname as dataclassname
FROM assetviolation av
         JOIN policies p on av.policyid = p.id
         JOIN rules r on av.ruleid = r.id
         LEFT JOIN fileviolation fv on av.id = fv.assetviolationid
         LEFT JOIN fileviolationdataclass fvdc on fv.id = fvdc.fileviolationid;

DROP VIEW IF EXISTS v_issues;
create or replace view v_issues as
SELECT id,
       datecreated,
       updatedat,
       lastclosed,
       policyid,
       ruleid,
       status
FROM issues;

DROP VIEW IF EXISTS v_policies;
create or replace view v_policies as
SELECT id,
       datecreated,
       managed,
       name,
       updatedat,
       version,
       assetgroupid,
       description,
       enabled,
       exclusions,
       lastexecutedat,
       refid,
       schedule
FROM policies;

DROP VIEW IF EXISTS v_policy_rule;
create or replace view v_policy_rule as
SELECT ruleid,
       policyid
FROM policy_rule;

DROP VIEW IF EXISTS v_rules;
create or replace view v_rules as
SELECT id,
       datecreated,
       managed,
       name,
       updatedat,
       version,
       description,
       enabled,
       refid,
       severity,
       type,
       remediation,
       remediationdocurls,
       archived
FROM rules;



COMMIT;