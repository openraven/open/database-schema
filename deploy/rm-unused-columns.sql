-- Deploy database-schema:rm-unused-columns to pg

BEGIN;

ALTER TABLE aggregatedassetfindings DROP COLUMN IF EXISTS region;
ALTER TABLE aggregatedassetfindings DROP COLUMN IF EXISTS account;
ALTER TABLE aggregatedassetfindings DROP COLUMN IF EXISTS assettype;
ALTER TABLE aggregatedassetfindings DROP COLUMN IF EXISTS source;

ALTER TABLE aggregatedscanfindings DROP COLUMN IF EXISTS region;
ALTER TABLE aggregatedscanfindings DROP COLUMN IF EXISTS account;
ALTER TABLE aggregatedscanfindings DROP COLUMN IF EXISTS assettype;
ALTER TABLE aggregatedscanfindings DROP COLUMN IF EXISTS source;

COMMIT;
