-- Deploy database-schema:add_s3scanfinding_composite_index to pg

BEGIN;

CREATE INDEX IF NOT EXISTS s3scanfinding_assetid_scantarget_scantargetchild_scannerjobid ON s3scanfinding(assetid,scantarget,scantargetchild,scannerjobid);

COMMIT;
