-- Deploy database-schema:add-sampling-enabled-to-scanner-job to pg

ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS samplingenabled BOOLEAN NOT NULL default false;
