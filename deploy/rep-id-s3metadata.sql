-- Deploy database-schema:rep-id-s3metadata to pg

BEGIN;

ALTER TABLE s3metadatascanfinding REPLICA IDENTITY FULL;

COMMIT;
