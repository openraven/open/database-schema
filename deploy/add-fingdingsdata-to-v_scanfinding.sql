-- Deploy database-schema:add-fingdingsdata-to-v_scanfinding to pg

BEGIN;

DROP VIEW v_scanfinding;

CREATE OR REPLACE VIEW v_scanfinding AS
SELECT s.scannerjobid,
       s.scannerjobrunid,
       s.assetid,
       s.scantarget,
       s.scantargetversion,
       s.dataclassid,
       s.findingscount,
       s.createdat,
       s.updatedat,
       s.scantargetchild,
       s.findingslocations,
       cs.findingsdata

FROM scanfinding s
         LEFT JOIN compositescanfinding cs ON (s.assetid = cs.assetid
                                                   AND s.scantarget = cs.scantarget
                                                   AND s.scantargetchild = cs.scantargetchild
                                                   AND s.dataclassid = cs.dataclassid);


COMMIT;
