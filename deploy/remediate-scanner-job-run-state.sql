-- Deploy database-schema:remediate-scanner-job-run-state to pg

BEGIN;

UPDATE scannerjobruns
SET status = 'CREATED'
WHERE status = 'DISABLED';

COMMIT;
