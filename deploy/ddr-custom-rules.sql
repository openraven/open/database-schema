DELETE FROM ddr_state_store;

DROP TABLE ddr_rules;
CREATE TABLE IF NOT EXISTS ddr_rules (
   id UUID,
   version varchar,
   last_updated timestamptz,
   user_created boolean,
   name text,
   description text,
   enabled boolean,
   alert_level varchar,
   rule_type varchar,
   parameters jsonb,
   criteria jsonb,
   message_template text,
   PRIMARY KEY (id)
);

DROP TABLE ddr_alerts;
CREATE TABLE IF NOT EXISTS ddr_alerts (
    id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    source_log varchar not null default '',
    alert_rule_id uuid not null,
    alert_rule_name varchar not null,
    alert_rule_desc varchar not null,
    alert_level varchar not null,
    alert_generation_ts timestamptz not null,
    alert_text varchar not null,
    events jsonb,
    alert_parameters jsonb
);
ALTER TABLE ddr_alerts REPLICA IDENTITY FULL;
