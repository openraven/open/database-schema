-- Deploy database-schema:add-scan-run-id-col to pg

BEGIN;

ALTER TABLE googledriveactivitywatermark ADD COLUMN IF NOT EXISTS scannerjobrunid UUID;

COMMIT;
