-- Deploy database-schema:add-table-aggregate-table to pg

BEGIN;

CREATE TABLE aggregateddatabasefindings(
      assetid TEXT NOT NULL,
      databasename TEXT NOT NULL,
      databaseschema TEXT NOT NULL DEFAULT '',
      databasetable TEXT NOT NULL,
      dataclassid UUID NOT NULL,
      findingscount BIGINT,
      updatedat TIMESTAMP WITH TIME ZONE,
      reseededat TIMESTAMP WITH TIME ZONE DEFAULT NULL,
      PRIMARY KEY(assetid, databasename, databaseschema, databasetable, dataclassid),
      FOREIGN KEY(assetid) REFERENCES awsrdsdbinstance (arn) ON DELETE CASCADE,
      FOREIGN KEY(dataclassid) REFERENCES dataclass (id) ON DELETE CASCADE
);

COMMIT;
