-- Deploy database-schema:fix-cloud-sql-resource-type to pg

BEGIN;

UPDATE datastorecredentialmodels
SET assettypes = '["GCP::SQL::Instance"]'
WHERE id IN ('GC_SECRETS_MANAGER_PASSWORD', 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN');

UPDATE datastorecredentialmodels
SET name = 'IAM access token for GCP Cloud SQL'
WHERE id = 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN';

COMMIT;
