-- Deploy database-schema:add-index-column-to-dataclasses.sql to pg

BEGIN;

-- constraint to limit incremental cacheindex growth over max cache bitmask size - 1024 bits
ALTER TABLE dataclass ADD COLUMN IF NOT EXISTS cacheindex INT CHECK ( cacheindex BETWEEN 0 AND 1024 );
ALTER TABLE metadataclass ADD COLUMN IF NOT EXISTS cacheindex INT CHECK ( cacheindex BETWEEN 0 AND 1024);

-- Initial value is half from 1024 bit mask
CREATE SEQUENCE custom_dataclasses_global_seq START 512 MAXVALUE 1024;

-- OpenRaven Managed Dataclasses
-- DC
UPDATE dataclass
   SET cacheindex = replace(refid, 'opnrvn-class-', '')::numeric
 WHERE managed = true;
-- MDC
UPDATE metadataclass
SET cacheindex = replace(refid, 'opnrvn-class-', '')::numeric
WHERE managed = true;

-- Customer Dataclasses
-- DC
UPDATE dataclass
SET cacheindex = nextval('custom_dataclasses_global_seq')
WHERE managed = false;

UPDATE metadataclass
SET cacheindex = nextval('custom_dataclasses_global_seq')
WHERE managed = false;

COMMIT;