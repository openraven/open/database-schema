-- Deploy database-schema:security-groups-optimizations to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awssecuritygroupconnections
(
    seq               bigserial NOT NULL,
    sgarn             text NOT NULL,
    awsregion         text NOT NULL,
    cidr              text NOT NULL,
    cidrowner         text,
    cidrownernetrange text,
    cidrlabel         text,
    isbadcidr         boolean NOT NULL,
    description       text,
    fromport          bigint,
    toport            bigint,
    ipprotocol        text,
    egress            boolean NOT NULL,
    isexternal        boolean NOT NULL,
    updatediso        timestamptz
);
CREATE INDEX awssecuritygroupconnections_seq ON awssecuritygroupconnections(seq);
CREATE INDEX awssecuritygroupconnections_sgarn ON awssecuritygroupconnections(sgarn);
CREATE INDEX awssecuritygroupconnections_awsregion ON awssecuritygroupconnections(awsregion);
CREATE INDEX awssecuritygroupconnections_cidr ON awssecuritygroupconnections(cidr);
CREATE INDEX awssecuritygroupconnections_cidrowner ON awssecuritygroupconnections(cidrowner);
CREATE INDEX awssecuritygroupconnections_cidrlabel ON awssecuritygroupconnections(cidrlabel);
CREATE INDEX awssecuritygroupconnections_cidrownernetrange ON awssecuritygroupconnections(cidrownernetrange);
CREATE INDEX awssecuritygroupconnections_egress ON awssecuritygroupconnections(egress);
CREATE INDEX awssecuritygroupconnections_updatediso ON awssecuritygroupconnections(updatediso);


CREATE TABLE awssecuritygrouppeering
(
    seq bigserial not null,
    parentsgarn text NOT NULL,
    parentsgarnawsregion text NOT NULL,
    connectedtosgarn text NOT NULL,
    peeringstatus text,
    updatediso timestamptz,
    PRIMARY KEY(parentsgarn, connectedtosgarn)
);
CREATE INDEX awssecuritygrouppeering_seq ON awssecuritygrouppeering(seq);
CREATE INDEX awssecuritygrouppeering_parentsgarnawsregion ON awssecuritygrouppeering(parentsgarnawsregion);
CREATE INDEX awssecuritygrouppeering_updatediso ON awssecuritygrouppeering(updatediso);

COMMIT;
