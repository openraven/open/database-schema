-- Deploy database-schema:gdrive_violation_remediation_fields to pg

BEGIN;

ALTER TABLE gdriveviolations ADD COLUMN IF NOT EXISTS actionid uuid;
ALTER TABLE gdriveviolations ADD COLUMN IF NOT EXISTS remediationinfo jsonb;

COMMIT;
