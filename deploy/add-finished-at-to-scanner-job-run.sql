-- Deploy database-schema:add-finished-at-to-scanner-job-run to pg

ALTER TABLE scannerjobruns ADD COLUMN finishedat timestamp with time zone default now();
ALTER TABLE scannerjobruns ALTER COLUMN finishedat drop default;
