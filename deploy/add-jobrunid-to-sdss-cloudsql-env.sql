-- Deploy database-schema:add-jobrunid-to-sdss-cloudsql-env to pg

BEGIN;

ALTER TABLE sdss_cloudsql_env ADD COLUMN IF NOT EXISTS jobrunid uuid;

COMMIT;