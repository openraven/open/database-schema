-- Deploy database-schema:redshift-credentialsmodel to pg

BEGIN;

UPDATE datastorecredentialmodels
    SET assettypes = assettypes || '"AWS::Redshift::Cluster"'::jsonb
    WHERE id = 'SECRETS_MANAGER_PASSWORD' OR
          id = 'SECRETS_MANAGER_PASSWORD_IN_JSON' OR
          id = 'PARAMETER_STORE_PASSWORD';

COMMIT;
