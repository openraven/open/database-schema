-- Deploy database-schema:add-fkey-perm-aggregate to pg

BEGIN;

ALTER TABLE assetfilepermissionaggregate
    ADD CONSTRAINT fk_assetfilepermissionaggregate_assetid
        FOREIGN KEY (assetid)
            REFERENCES globalassets(assetid)
            ON DELETE CASCADE;

COMMIT;
