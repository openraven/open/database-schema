BEGIN;

ALTER TABLE ddr_alerts DROP COLUMN event_parameters;
ALTER TABLE ddr_alerts ADD COLUMN alert_parameters jsonb;

COMMIT;