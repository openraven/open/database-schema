-- Deploy database-schema:support-scanning-sql-files to pg

BEGIN;

UPDATE scanfiletype SET extensions = COALESCE(extensions, '[]'::jsonb) || '["sql"]'::jsonb
                    WHERE mimetype='text/plain' AND NOT extensions::jsonb ? 'sql';

INSERT INTO scanfiletype (mimetype, extensions, isarchive, enabled, maxfilesize, createdat, updatedat)
    VALUES ('text/x-sql', '["sql"]'::jsonb, FALSE, TRUE, 1000, now(), now()) ON CONFLICT DO NOTHING;

COMMIT;
