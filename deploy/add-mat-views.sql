-- Deploy database-schema:add-mat-views to pg

BEGIN;

create materialized view mat_v_external_files_by_asset as
select assetid, sharinguser, count(*) as filesharedexternalcount from googledrivefiles
where fileisexternal=true
group by assetid, sharinguser with no data;

create materialized view mat_v_suspicious_user_aggregation as
select sp.assetid, suspiciousemail,
       jsonb_agg(jsonb_build_object('id', gf.dataclassid, 'count', gf.findingscount)) FILTER ( WHERE gf.dataclassid IS NOT NULL ) as dataclasses,
        count(distinct sp.fileid) as filecount
from suspiciousgoogledrivefilepermissions sp
    left join gdrivescanfinding gf on sp.assetid=gf.assetid and sp.fileid=gf.scantarget
    inner join googledrivefiles gdf on sp.assetid = gdf.assetid and sp.fileid = gdf.fileid
    inner join globalassets assets on sp.assetid=assets.assetid,
    jsonb_array_elements_text(suspiciousemails) AS suspiciousemail
group by sp.assetid, suspiciousemail order by filecount desc with no data;

CREATE UNIQUE INDEX suspicious_user_aggregation_unique
    ON mat_v_suspicious_user_aggregation(assetid,suspiciousemail);

CREATE UNIQUE INDEX external_files_by_asset_unique
    ON mat_v_external_files_by_asset(assetid,sharinguser);

create index mat_v_suspicious_user_aggregation_suspiciousemail on mat_v_suspicious_user_aggregation(suspiciousemail);

refresh materialized view mat_v_external_files_by_asset;
refresh materialized view mat_v_suspicious_user_aggregation;

COMMIT;
