BEGIN;
CREATE TABLE IF NOT EXISTS dataclass
(
    -- OpenRavenCrudModel
    id              UUID PRIMARY KEY NOT NULL,
    name            text,
    version         text,
    owner           text,
    managed         boolean,
    refid           text,
    description     text,
    validatorfunction text,
    matchpatterns   jsonb,
    status          text,
    excludes        jsonb,
    keywords        jsonb,    
    keyworddistance int,
    category        text,    
    createdat       timestamptz,
    updatedat       timestamptz
);

CREATE TABLE IF NOT EXISTS datacollection
(
    id              UUID PRIMARY KEY NOT NULL,
    name            text,
    version         text,
    managed         boolean,
    refid           text,
    description     text,
    status          text,
    owner           text,
    createdat       timestamptz,
    updatedat       timestamptz
);

CREATE TABLE IF NOT EXISTS datacollection_dataclass
(
    datacollectionid   UUID NOT NULL REFERENCES datacollection(id) ON DELETE CASCADE ON UPDATE CASCADE,
    dataclassid        UUID NOT NULL REFERENCES dataclass(id) ON DELETE CASCADE ON UPDATE CASCADE 
);
COMMIT;
