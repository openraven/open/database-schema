-- Deploy database-schema:add-namespace-to-asset-group to pg

BEGIN;

ALTER TABLE assetworkspace
    ADD COLUMN IF NOT EXISTS namespace TEXT DEFAULT 'DEFAULT';
ALTER TABLE assetworkspace DROP CONSTRAINT IF EXISTS assetworkspace_name_key;

ALTER TABLE assetworkspace DROP CONSTRAINT IF EXISTS assetworkspace_name_namespace_unique;
ALTER TABLE assetworkspace
    ADD CONSTRAINT assetworkspace_name_namespace_unique UNIQUE (name, namespace);

CREATE INDEX IF NOT EXISTS assetworkspace_namespace ON assetworkspace(namespace);

UPDATE assetworkspace
SET namespace = 'SCAN'
WHERE managed IS FALSE OR managed IS NULL;

UPDATE assetworkspace
SET namespace = 'DEFAULT_MANAGED'
WHERE managed is TRUE;

-- Save Workspaces specified by customer success to be set as USER_CREATED
UPDATE assetworkspace
SET namespace = 'USER_CREATED'
WHERE managed is FALSE
  and workspaceid IN (
                      '6830d877-477a-44e1-a8c1-51955764d4d9',
                      '33a8eaa3-b5ec-4de9-ae07-db96c85cc2d4',
                      'a7798b77-ef3b-4202-a95a-b8ea87e17811',
                      '83df41b1-bdd1-4b46-b091-65d79c096631',
                      '824ac177-3ea3-46c7-90e6-31d1172fd762',
                      '17468e87-ee6e-401a-a47a-4aff195bb852',
                      '00a3bb00-e87f-4550-9754-2defde4fec72',
                      '0cbd22f3-5dbe-4d7a-9faa-f766d3cf44e3',
                      '4917ed50-a751-4a7c-b8a8-2b3286c8799a',
                      '0ca3e9b9-a25d-409c-a006-1bd7ac882af7',
                      '1f83c643-a033-4f59-81ac-646217767017',
                      '5dea2bfe-c458-4b51-b0e5-5608348ccaff',
                      '30170a1e-607d-4c2a-b21e-e783e7e564f6',
                      'e737faa2-171f-424f-86fa-0e564e61c987',
                      '37b646fb-7c2f-44ab-8b1b-dda36f7cd770',
                      '71b415e8-11de-40ff-932f-f4b26a45eecb',
                      '1916f9bd-a8f1-4b5a-897f-c528475ac61c',
                      '908cef5d-1c9b-4343-954b-abf01f5e3887',
                      '6c02e6fd-9789-428d-9272-ab60b02f559e',
                      '8839c0f5-c8a4-4c95-8e28-60196bd1c0b0',
                      'b159959d-be11-4c39-94bc-1f776c82bd25',
                      '04763ae9-d223-4ecc-a705-6a09d8116e4b',
                      'be566eb5-447a-4557-9040-1d3e35bf66c7',
                      'b7f6e2fa-30b4-4a95-a381-da681f3f32cb',
                      '3ae4cc6a-29a8-43e1-80d0-e4c4eb290ef2',
                      'bf4cfb22-8184-44b2-aa54-f0ede469de7b',
                      'f5d0d316-9552-4df2-b426-357116d4e704',
                      '58208ed9-b440-4a4c-91a8-ac99ff6eded7',
                      'b0db375c-20e6-451e-bef2-e08f82710be7',
                      '669b5a13-8ae7-4544-9c9d-864ec6768fc2',
                      '1d3c4204-44bd-45a6-9b3d-b65df58a122b',
                      '35ef23a3-57b4-4bf1-b79c-b6b13019d0d3',
                      'c5f77d05-c8a6-4fbc-8f24-33d47ad73093',
                      '36f94e91-7038-4b1c-8eaa-31797f12bc37',
                      '20f4d51f-2edb-4840-b583-dfc062becfa5',
                      'c4567aec-0806-4aae-855e-0f072ea47877',
                      'f00fa426-6d9f-4638-8d9f-daf76e988061',
                      '8402bd11-f6a2-4bfa-823b-69ff3de916b8',
                      '77cc7d3a-fbc7-4e4a-890d-07919ac9df30',
                      'b0a55f0c-c4dd-4cba-bdcb-7110888be61f',
                      'f836fd0a-deda-44f5-a906-e1fb11ad4ccd',
                      'f4f7e76a-ab6f-4113-b055-2b033a354e33',
                      '5c10c0e1-3943-4c64-be4e-893f0549cff5',
                      '4d3f74ef-fc50-480c-bf73-113c6ffe822c',
                      '6a560fc9-1107-41ff-bd0c-38a17522fe6a',
                      'a5575a81-0a15-4d34-8bae-72b93428659d',
                      '8d4f8067-64aa-466c-ac8a-b8a4c0356067',
                      '99a8301d-ba38-4816-89e1-df9cca133b9d',
                      '45bf051f-ebb5-45be-8535-73e1cf2eed63',
                      'be5ca0f6-3fb9-484d-9ab0-a647f0c666bf'
    );

COMMIT;
