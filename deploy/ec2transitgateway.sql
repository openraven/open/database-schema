-- Deploy database-schema:ec2transitgateway to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awsec2transitgateway () INHERITS (aws);
CREATE INDEX IF NOT EXISTS "idx_awsec2transit_documentId" ON "public"."awsec2transitgateway" USING btree ("documentid");

INSERT INTO awsec2transitgateway SELECT * FROM aws WHERE resourcetype = 'AWS::EC2::TransitGateway' AND NOT EXISTS (SELECT * FROM awsec2transitgateway);

COMMIT;

