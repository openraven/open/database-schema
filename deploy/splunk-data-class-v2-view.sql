-- Deploy database-schema:splunk-data-class-v2-view to pg

BEGIN;

DROP VIEW IF EXISTS v_dataclass_old;
ALTER VIEW v_dataclass RENAME TO v_dataclass_old;
CREATE OR replace VIEW v_dataclass AS
SELECT id,
       name,
       refid,
       description,
       status,
       category
FROM dataclassv2;

COMMIT;
