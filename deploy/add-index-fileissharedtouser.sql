-- Deploy database-schema:add-index-fileissharedtouser to pg

BEGIN;

CREATE INDEX IF NOT EXISTS googledrivefiles_fileissharedtouser ON googledrivefiles(fileissharedtouser);

COMMIT;
