-- Deploy database-schema:rename-gdrive-access-view-col to pg

BEGIN;

ALTER VIEW v_suspiciousgoogledrivefilepermissions
RENAME COLUMN sharedbylink TO anyonewithlink;

COMMIT;
