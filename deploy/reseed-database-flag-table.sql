-- Deploy database-schema:reseed-database-flag-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scanfindingaggregatordatabaseversionflag (
     id SERIAL PRIMARY KEY NOT NULL,
     flagversion BIGINT
);

COMMIT;
