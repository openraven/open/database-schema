-- Deploy database-schema:fix-report-dates to pg

BEGIN;

-- Correct timestamps in reportjob that are incorrect due to a bug
-- Use the earliest reportjobrun datestarted for each reportjob as a default.
WITH subquery AS (
    SELECT DISTINCT ON(run.reportjobid)
        run.reportjobid as reportjobid,
        run.id AS latestrunid,
        run.datestarted AS latestrundate
    FROM reportjobrun run
    ORDER BY run.reportjobid, run.datestarted ASC
)
UPDATE reportjob
SET datecreated = subquery.latestrundate
FROM subquery
WHERE reportjob.id = subquery.reportjobid;

-- If a reportjob has never been run, default datecreated to current date
UPDATE reportjob
SET datecreated = now()
WHERE latestrunid IS NULL;

COMMIT;
