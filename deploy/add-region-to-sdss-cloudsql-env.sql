-- Deploy database-schema:add-region-to-sdss-cloudsql-env to pg

BEGIN;

ALTER TABLE sdss_cloudsql_env ADD COLUMN IF NOT EXISTS region TEXT;

COMMIT;


