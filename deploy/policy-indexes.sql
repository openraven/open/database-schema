-- Deploy database-schema:policy-indexes to pg

BEGIN;

CREATE INDEX IF NOT EXISTS assetviolation_assetid ON assetviolation(assetid);
CREATE INDEX IF NOT EXISTS assetviolation_policyid ON assetviolation(policyid);
CREATE INDEX IF NOT EXISTS assetviolation_ruleid ON assetviolation(ruleid);
CREATE INDEX IF NOT EXISTS assetviolation_status ON assetviolation(status);

COMMIT;
