-- Deploy database-schema:global-tags to pg

BEGIN;

ALTER TABLE IF EXISTS awstags RENAME TO globalassettags;

COMMIT;
