-- Deploy database-schema:sdss-snapshot-vpc to pg

BEGIN;

CREATE TABLE IF NOT EXISTS sdss_snapshot_vpc
(
    id              UUID PRIMARY KEY NOT NULL,
    vpc_name        TEXT,
    vpc_account     TEXT,
    vpc_region      TEXT
);
COMMIT;
