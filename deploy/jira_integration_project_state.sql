-- Deploy database-schema:jira_integration_project_state to pg

BEGIN;

CREATE TABLE IF NOT EXISTS jira_label
(
    label character varying
);

CREATE TABLE IF NOT EXISTS jira_priority
(
    priority_id bigint NOT NULL,
    name character varying,
    description character varying,
    CONSTRAINT jira_priority_pkey PRIMARY KEY (priority_id)
);

CREATE TABLE IF NOT EXISTS jira_project
(
    project_id bigint NOT NULL,
    name character varying,
    project_key character varying,
    CONSTRAINT jira_project_pkey PRIMARY KEY (project_id)
);

CREATE TABLE IF NOT EXISTS jira_user
(
    account_id character varying NOT NULL,
    name character varying,
    account_type character varying,
    CONSTRAINT jira_users_pkey PRIMARY KEY (account_id)
);

CREATE TABLE IF NOT EXISTS jira_field
(
    required boolean,
    name varchar,
    key varchar,
    custom_field_id int,
    custom_field_schema jsonb,
    default_value varchar,
    containing_project_id bigint NOT NULL REFERENCES jira_project(project_id),
    issue_type_id bigint not null,

    CONSTRAINT jira_field_pkey PRIMARY KEY (key, issue_type_id, containing_project_id)
);

COMMIT;
