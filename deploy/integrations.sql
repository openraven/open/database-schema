BEGIN;
CREATE TABLE IF NOT EXISTS email_integration_settings
(
    id uuid PRIMARY KEY NOT NULL,
    enabled boolean NOT NULL,
    enable_high_risk_violations boolean,
    enable_low_risk_violation boolean,
    enable_medium_risk_violations boolean,
    email_address text,
    email_subject text
);

CREATE TABLE IF NOT EXISTS fourme_integration_settings
(
    id uuid PRIMARY KEY NOT NULL,
    enabled boolean NOT NULL,
    enable_high_risk_violations boolean,
    enable_low_risk_violation boolean,
    enable_medium_risk_violations boolean,
    category text,
    default_categeory text,
    four_me_account_id text,
    impact text,
    member text,
    name text,
    note text,
    o_auth_token text,
    requested_for text,
    service_instance_id text,
    status text,
    subject text,
    url text
);

CREATE TABLE IF NOT EXISTS slack_integration_settings
(
    id uuid PRIMARY KEY NOT NULL,
    enabled boolean NOT NULL,
    enable_high_risk_violations boolean,
    enable_low_risk_violation boolean,
    enable_medium_risk_violations boolean,
    name text,
    url text
);

CREATE TABLE IF NOT EXISTS webhook_integration_settings
(
    id uuid PRIMARY KEY NOT NULL,
    enabled boolean NOT NULL,
    enable_high_risk_violations boolean,
    enable_low_risk_violation boolean,
    enable_medium_risk_violations boolean,
    method text,
    name text,
    url text,
    params jsonb
);
COMMIT;
