-- Deploy database-schema:add-cloudstorage-scan-tables.sql to pg

BEGIN;

CREATE TABLE IF NOT EXISTS cloudstoragescanfinding () INHERITS (scanfinding);
CREATE TABLE IF NOT EXISTS cloudstoragemetadatascanfinding () INHERITS (scanfinding);

ALTER TABLE cloudstoragescanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);
ALTER TABLE cloudstoragemetadatascanfinding ADD PRIMARY KEY (assetid, scantarget, dataclassid, scantargetchild);

CREATE INDEX IF NOT EXISTS cscanfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON cloudstoragescanfinding(assetid,scantarget,scantargetchild,scannerjobid);

CREATE INDEX IF NOT EXISTS csmetadatascanfinding_assetid_scantarget_scantargetchild_scannerjobid
    ON cloudstoragemetadatascanfinding(assetid,scantarget,scantargetchild,scannerjobid);

-- refactor existing table name
ALTER TABLE metadatafinding RENAME TO s3metadatascanfinding;
CREATE TABLE metadatafinding AS SELECT * FROM s3metadatascanfinding; -- Backup to be removed with next successful release

COMMIT;