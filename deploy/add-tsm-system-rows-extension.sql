-- Deploy database-schema:add-tsm-system-rows-extension to pg

BEGIN;

CREATE EXTENSION IF NOT EXISTS "tsm_system_rows";

COMMIT;
