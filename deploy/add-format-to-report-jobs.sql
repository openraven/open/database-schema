-- Deploy database-schema:add-format-to-report-jobs to pg

BEGIN;

ALTER TABLE reportjob ADD COLUMN IF NOT EXISTS reportformat TEXT DEFAULT 'json';
ALTER TABLE reportjobrun ADD COLUMN IF NOT EXISTS reportformat TEXT DEFAULT 'json';

COMMIT;
