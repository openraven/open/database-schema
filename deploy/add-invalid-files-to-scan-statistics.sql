-- Deploy database-schema:add-invalid-files-to-scan-statistics to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS invalidfilecount BIGINT DEFAULT 0;

COMMIT;
