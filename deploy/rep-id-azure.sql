-- Deploy database-schema:rep-id-azure to pg

BEGIN;

ALTER TABLE blobstoragescanfinding REPLICA IDENTITY FULL;
ALTER TABLE blobstoragemetadatascanfinding REPLICA IDENTITY FULL;

COMMIT;
