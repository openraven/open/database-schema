-- Deploy database-schema:add-7zip-scanfiletype to pg

BEGIN;

INSERT INTO scanfiletype(mimetype, extensions, isarchive, maxFileSize)
VALUES ('application/x-7z-compressed', '["7z"]', true, 8000)
ON CONFLICT DO NOTHING;

COMMIT;
