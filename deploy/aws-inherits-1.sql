-- Deploy database-schema:aws-inherits-1 to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awsec2snapshot () INHERITS (aws);
CREATE TABLE IF NOT EXISTS awscloudtrailtrail () INHERITS (aws);
CREATE TABLE IF NOT EXISTS awsssminstance () INHERITS (aws);

INSERT INTO awsec2snapshot SELECT * FROM aws WHERE resourcetype = 'AWS::EC2::Snapshot' AND NOT EXISTS (SELECT * FROM awsec2snapshot);
INSERT INTO awscloudtrailtrail SELECT * FROM aws WHERE resourcetype = 'AWS::CloudTrail::Trail' AND NOT EXISTS (SELECT * FROM awscloudtrailtrail);
INSERT INTO awsssminstance SELECT * FROM aws WHERE resourcetype = 'AWS::SSM::Instance' AND NOT EXISTS (SELECT * FROM awsssminstance);

COMMIT;
