-- Deploy database-schema:report-generation to pg

BEGIN;

CREATE TABLE IF NOT EXISTS reportjob
(
    id                    UUID PRIMARY KEY,
    name                  TEXT,
    operationkey          TEXT,
    createdby             TEXT,
    datecreated           TIMESTAMPTZ,
    parameters            JSONB,
    initialpaginationargs JSONB,
    schedulemode          TEXT,
    cronschedule          TEXT,
    nextschedulabledate   TIMESTAMPTZ,
    enabled               BOOLEAN
);

CREATE TABLE IF NOT EXISTS reportjobrun
(
    id              UUID PRIMARY KEY,
    state           TEXT,
    previousstate   TEXT,
    statemessage    TEXT,
    messagesequence BIGINT,
    datestarted     TIMESTAMPTZ,
    reportjobid     UUID,
    FOREIGN KEY (reportjobid) REFERENCES reportjob (id) ON DELETE CASCADE,
    name            TEXT,
    parameters      JSONB,
    paginationargs  JSONB,
    reportlink      TEXT,
    reportlocation  JSONB
);

COMMIT;
