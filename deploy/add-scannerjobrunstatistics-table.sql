-- Deploy database-schema:add-scannerjobrunstatistics-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS scannerjobrunstatistics
(
    id                          UUID NOT NULL PRIMARY KEY,
    lastscannedobjectkey        TEXT,
    lastscannedbucketarn        TEXT,
    lastscanpagecount           BIGINT DEFAULT 0,
    lastscanstarttime           TIMESTAMP,
    numerator                   BIGINT NOT NULL DEFAULT 0,
    denominator                 BIGINT NOT NULL DEFAULT 0,
    lastupdateat                TIMESTAMP WITH TIME ZONE
);

INSERT INTO scannerjobrunstatistics (id, lastscannedobjectkey, lastscannedbucketarn, lastscanpagecount, lastscanstarttime, numerator, denominator, lastupdateat)
SELECT r.id, r.lastscannedobjectkey, r.lastscannedbucketarn, r.lastscanpagecount, r.lastscanstarttime, r.numerator, r.denominator, r.lastupdateat FROM scannerjobruns r, scannerjob j WHERE j.lastscanjobrunid = r.id;

COMMIT;
