-- Deploy database-schema:s3objectcache to pg

BEGIN;


CREATE TABLE cached.s3objects(
    id bigint primary key
) PARTITION BY RANGE (id);
-- https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/PostgreSQL_Partitions.html#PostgreSQL_Partitions.create_parent
SELECT partman.create_parent(p_parent_table =>'cached.s3objects', p_control => 'id', p_type => 'native', p_interval => '1000000000000');

COMMIT;
