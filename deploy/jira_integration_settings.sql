-- Deploy database-schema:jira_integration_settings to pg

BEGIN;

CREATE TABLE IF NOT EXISTS jira_integration_settings
(
    id uuid PRIMARY KEY NOT NULL,
    enabled boolean NOT NULL,
    client_id text NOT NULL,
    client_secret text NOT NULL,
    site_name text NOT NULL,
    api_url text,
    refresh_token text,
    access_token text,
    state_code uuid NOT NULL
);

COMMIT;
