-- Deploy database-schema:scan-audit-log-events-finalize-index-jobrunid-lastid to pg

BEGIN;

create index if not exists scanauditlogevent_jobrunid_lastid_idx on scanauditlogevent(scanjobrunid, lastid);

COMMIT;
