BEGIN;
DO $$
    BEGIN
        if not exists (select constraint_name
                       from information_schema.constraint_column_usage
                       where table_name = 'awss3bucket'  and constraint_name = 'unique_s3_bucketname')
        THEN
            ALTER TABLE awss3bucket ADD CONSTRAINT unique_s3_bucketname UNIQUE (resourcename);
        END IF;
    END
$$;


CREATE TABLE IF NOT EXISTS scanfinding (
    scannerjobid            UUID NOT NULL,
    scannerjobrunid         UUID NOT NULL,
    assetid                 text references awss3bucket(resourcename),
    scantarget              text NOT NULL,
    scantargetversion       text,
    dataclassid             UUID NOT NULL,
    findingscount           int,
    createdat               timestamp with time zone,
    updatedat               timestamp with time zone,
    PRIMARY KEY (assetid, dataclassid, scantarget)
);
COMMIT;
