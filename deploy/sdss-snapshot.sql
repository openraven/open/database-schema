-- Deploy database-schema:sdss-snapshot to pg

BEGIN;

CREATE TABLE IF NOT EXISTS sdss_snapshot
(
    id              UUID PRIMARY KEY NOT NULL,
    jobRunId        UUID NOT NULL,
    snapshot_info   JSONB
);
COMMIT;
