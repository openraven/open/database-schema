-- Deploy database-schema:purge-hotspot-dataclass to pg
DO $$
DECLARE var_hotspotid uuid;

BEGIN
var_hotspotid := '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';

delete from issuedataclasses where dataclassid=var_hotspotid;
delete from scanfinding where dataclassid=var_hotspotid;
delete from aggregatedassetfindings where dataclassid=var_hotspotid;
delete from aggregatedrelationaldatabasefindings where dataclassid=var_hotspotid;
delete from aggregatedscanfindings where dataclassid=var_hotspotid;
delete from datacollection_dataclass where dataclassid=var_hotspotid;
delete from dataclass where id=var_hotspotid;
delete from dataclassv2 where id=var_hotspotid;

COMMIT;

END $$;