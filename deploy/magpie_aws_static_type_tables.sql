-- Deploy database-schema:magpie_aws_static_type_tables to pg

BEGIN;

create table IF NOT EXISTS awsconfigurationrecorder () INHERITS (aws);
create table IF NOT EXISTS awsguarddutydetector () INHERITS (aws);
create table IF NOT EXISTS awsiamaccount () INHERITS (aws);
create table IF NOT EXISTS awsiamcredentialsreport () INHERITS (aws);
create table IF NOT EXISTS awslocationgeofencecollection () INHERITS (aws);
create table IF NOT EXISTS awslocationmap () INHERITS (aws);
create table IF NOT EXISTS awslocationplaceindex () INHERITS (aws);
create table IF NOT EXISTS awslocationroutecalculator () INHERITS (aws);
create table IF NOT EXISTS awslocationtracker () INHERITS (aws);
create table IF NOT EXISTS awsrdsdbsnapshot () INHERITS (aws);
create table IF NOT EXISTS awssecurityhubstandardsubscription () INHERITS (aws);
create table IF NOT EXISTS awssnssubscription () INHERITS (aws);
create table IF NOT EXISTS awssnstopic () INHERITS (aws);
create table IF NOT EXISTS awswatchalarm () INHERITS (aws);
create table IF NOT EXISTS awswatchdashboard () INHERITS (aws);
create table IF NOT EXISTS awswatchlogsmetricfilter () INHERITS (aws);
create table IF NOT EXISTS awswatchloggroup () INHERITS (aws);
create table IF NOT EXISTS awsssminstance () INHERITS (aws);
COMMIT;
