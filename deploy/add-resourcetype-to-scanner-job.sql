-- Deploy database-schema:add-resourcetype-to-scanner-job to pg

BEGIN;

ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS resourcetype text not null default 'AWS::S3::Bucket';

UPDATE scannerjob SET resourcetype = case
                                             when scantype = 'STRUCTURED' then 'AWS::RDS::DBInstance'
                                             when scantype = 'UNSTRUCTURED' then 'AWS::S3::Bucket'
    end;

COMMIT;
