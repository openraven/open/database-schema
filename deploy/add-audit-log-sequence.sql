-- Deploy database-schema:add-audit-log-sequence to pg

BEGIN;

CREATE SEQUENCE IF NOT EXISTS audit_log_event_sequence INCREMENT BY 1;

COMMIT;
