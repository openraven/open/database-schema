-- Deploy database-schema:scan-audit-log-entity to pg

BEGIN;

CREATE TABLE scanauditlogevent
(
    resourceid       VARCHAR(255) NOT NULL,
    scantarget       VARCHAR(255) NOT NULL,
    childtarget      VARCHAR(255) NOT NULL,
    scanjobrunid     UUID         NOT NULL,
    scanjobid        UUID         NOT NULL,
    state            TEXT         NOT NULL,
    dataclassid      UUID,
    lastid           BIGINT,
    scaniso          TIMESTAMP WITH TIME ZONE,
    etag             VARCHAR(255),
    statusreason     VARCHAR(255),
    reportedmimetype VARCHAR(255),
    detectedmimetype VARCHAR(255),
    sizeinbytes      BIGINT,
    CONSTRAINT pk_scanauditlogevent PRIMARY KEY (resourceid, scantarget, childtarget, scanjobrunid,
                                                 scanjobid, state)
);

COMMIT;
