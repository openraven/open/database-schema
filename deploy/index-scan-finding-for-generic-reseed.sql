-- Deploy database-schema:index-scan-finding-for-generic-reseed to pg

BEGIN;

CREATE INDEX IF NOT EXISTS scanfinding_assetid ON scanfinding(assetid);
CREATE INDEX IF NOT EXISTS scanfinding_dataclassid ON scanfinding(dataclassid);

COMMIT;
