CREATE TABLE IF NOT EXISTS ddr_bucket_access (
    bucketname   text,
    rolearn      text,
    canaccess    boolean NOT NULL default false,
    creatediso   timestamp with time zone,
    updatediso   timestamp with time zone,
    PRIMARY KEY (bucketname)
);


CREATE INDEX IF NOT EXISTS ddr_bucket_access_bucketname ON ddr_bucket_access (bucketname);


CREATE TABLE IF NOT EXISTS ddr_cloudtrail_coverage (
    trailarn        text,
    accountid       text,
    islogging       boolean NOT NULL default false,
    isorg           boolean NOT NULL default false,
    bucketname      text,
    canaccessbucket boolean NOT NULL default false,
    coverage        jsonb not null default '{}'::jsonb,
    configuration   jsonb not null default '{}'::jsonb,
    creatediso      timestamp with time zone,
    updatediso      timestamp with time zone,
    PRIMARY KEY (trailarn)
);

CREATE INDEX IF NOT EXISTS ddr_cloudtrail_coverage_accountid ON ddr_cloudtrail_coverage (accountid);
