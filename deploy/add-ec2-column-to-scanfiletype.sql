-- Deploy database-schema:add-ec2-column-to-scanfiletype to pg

BEGIN;

ALTER TABLE scanfiletype ADD COLUMN IF NOT EXISTS maxec2filesize INT;

UPDATE scanfiletype SET maxec2filesize = 8000 WHERE mimetype = 'application/x-parquet';

COMMIT;
