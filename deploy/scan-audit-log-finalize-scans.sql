-- Deploy database-schema:scan-audit-log-finalize-scans to pg

BEGIN;

CREATE TABLE IF NOT EXISTS finishedscans
(
    scanjobrunid       UUID NOT NULL,
    scanjobid          UUID NOT NULL,
    scancompletiontime timestamptz,
    lastsendtime       timestamptz,
    eventssent         BOOLEAN,
    jvmid              BIGINT,
    lastprocessedeventid BIGINT,
    CONSTRAINT pk_finishedscans PRIMARY KEY (scanjobrunid)
);

COMMIT;
