-- Deploy.

BEGIN;

-- Limiting size of index only to Enabled records drastically reduces cardinality of data
-- and improving speed of retrieval ~ 30% based related data experiments

CREATE INDEX scan_type_target_type_status_idx
    ON scannerjob (scantype, scantargettype, status, updatedat)
    WHERE status = 'ENABLED';

COMMIT;
