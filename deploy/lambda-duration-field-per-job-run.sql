-- Deploy database-schema:lambda-duration-field-per-job-run to pg

BEGIN;

ALTER TABLE scannerjobrunstatistics ADD COLUMN IF NOT EXISTS lambdaduration BIGINT NOT NULL DEFAULT 0;
ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS maxscancost BIGINT NOT NULL DEFAULT 0;
ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS maxlambdaduration BIGINT NOT NULL DEFAULT 0;

COMMIT;
