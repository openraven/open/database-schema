-- Deploy database-schema:email-user-dom-list to pg

BEGIN;

DROP VIEW v_gdrive_dash;
CREATE VIEW v_gdrive_dash AS SELECT gs.assetid, gs.fileid, gs.fileurl, gs.filename,
       ga.namelabel AS drivename, gp.username, gp.sharedbylink AS anyonewithlink,
       gp.suspiciousemails, gs.filecreatedat, gs.fileupdatedat, gs.fileaccessedat,
       gp.permissions as allpermissions, json_agg(gr.name) AS rulesviolated, ga.resourcetype,
       gb.configuration->'restrictions' AS restrictions,
       array_to_json(ARRAY(
           SELECT DISTINCT
            substring(email, '@(.+)$')
        FROM
            jsonb_array_elements_text(suspiciousemails) AS email
    )) AS emaildomains,
       array_to_json(ARRAY(
           SELECT DISTINCT
            substring(email, '(.+)@')
        FROM
            jsonb_array_elements_text(suspiciousemails) AS email
    )) AS emailusers
FROM googledrivestalefiles gs
         INNER JOIN globalassets ga ON ga.assetid=gs.assetid
         INNER JOIN globalassetsblob gb ON gb.assetid=gs.assetid
         LEFT JOIN suspiciousgoogledrivefilepermissions gp ON gp.fileid=gs.fileid AND gp.username=gs.username
         LEFT JOIN gdriveviolations gv ON gs.fileid=gv.nodeid
         LEFT JOIN gdriverules gr ON gv.gdriveruleid=gr.id
WHERE ga.resourcetype IN ('GOOGLEWORKSPACE::SharedDrive','GOOGLEWORKSPACE::MyDrive')
GROUP BY gs.assetid, gs.fileid, gs.fileurl, gs.filename, ga.namelabel, gp.username, gp.sharedbylink, gp.suspiciousemails, gs.filecreatedat,
         gs.fileupdatedat, gs.fileaccessedat, gp.permissions, ga.resourcetype, gb.configuration;

DROP VIEW v_suspiciousgoogleshareddrivepermissions;
CREATE VIEW v_suspiciousgoogleshareddrivepermissions AS
SELECT s.assetid,
       s.driveid,
       s.drivename,
       s.suspiciousemails,
       s.groupemails,
       s.updatedat,
       ga.configuration->>'restrictions' as restrictions,
        array_to_json(ARRAY(
            SELECT DISTINCT
            substring(email, '@(.+)$')
            FROM
            jsonb_array_elements_text(s.suspiciousemails) AS email
        )) AS emaildomains,
            array_to_json(ARRAY(
            SELECT DISTINCT
            substring(email, '(.+)@')
            FROM
            jsonb_array_elements_text(s.suspiciousemails) AS email
        )) AS emailusers
FROM suspiciousgoogleshareddrivepermissions s INNER JOIN v_globalassetscomplete ga ON s.assetid=ga.assetid;

DROP VIEW v_suspiciousgoogledrivefilepermissions;
CREATE VIEW v_suspiciousgoogledrivefilepermissions AS
SELECT  s.assetid,
        s.fileid,
        s.driveid,
        s.filename,
        s.username,
        s.fileurl,
        s.suspiciousemails,
        s.groupemails,
        s.sharedbylink AS anyonewithlink,
        s.updatedat,
        ga.namelabel,
        ga.configuration->>'restrictions',
        array_to_json(ARRAY(
            SELECT DISTINCT
            substring(email, '@(.+)$')
            FROM
            jsonb_array_elements_text(s.suspiciousemails) AS email
        )) AS emaildomains,
            array_to_json(ARRAY(
            SELECT DISTINCT
            substring(email, '(.+)@')
            FROM
            jsonb_array_elements_text(s.suspiciousemails) AS email
        )) AS emailusers
FROM suspiciousgoogledrivefilepermissions s INNER JOIN v_globalassetscomplete ga ON s.assetid=ga.assetid;

COMMIT;
