-- Deploy database-schema:automations_forensics to pg

BEGIN;

ALTER TABLE rule_configuration
    ADD COLUMN IF NOT EXISTS created_at TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS updated_at TIMESTAMP WITH TIME ZONE,
    ADD COLUMN IF NOT EXISTS created_by TEXT,
    ADD COLUMN IF NOT EXISTS updated_by TEXT;

UPDATE rule_configuration
SET created_at = make_timestamptz(1970, 1, 1, 0, 0, 0),
    updated_at = make_timestamptz(1970, 1, 1, 0, 0, 0),
    created_by = 'legacy rule',
    updated_by = 'legacy rule';

ALTER TABLE rule_configuration
    ALTER COLUMN updated_at SET NOT NULL,
    ALTER COLUMN created_at SET NOT NULL,
    ALTER COLUMN created_by SET NOT NULL,
    ALTER COLUMN updated_by SET NOT NULL;

COMMIT;
