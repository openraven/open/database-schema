-- Deploy database-schema:asset-change-stream-rename to pg

BEGIN;

ALTER TABLE aggregatedassetmetrics RENAME TO aggregatedassetfindings;
ALTER TABLE aggregatedscanmetrics RENAME TO aggregatedscanfindings;

ALTER INDEX IF EXISTS aggregatedassetmetrics_region RENAME TO aggregatedassetfindings_region;
ALTER INDEX IF EXISTS aggregatedassetmetrics_dataclass RENAME TO aggregatedassetfindings_dataclass;
ALTER INDEX IF EXISTS aggregatedassetmetrics_account RENAME TO aggregatedassetfindings_account;

ALTER INDEX IF EXISTS aggregatedscanmetrics_updateat RENAME TO aggregatedscanfindings_updateat;
ALTER INDEX IF EXISTS aggregatedscanmetrics_region RENAME TO aggregatedscanfindings_region;
ALTER INDEX IF EXISTS aggregatedscanmetrics_dataclass RENAME TO aggregatedscanfindings_dataclass;
ALTER INDEX IF EXISTS aggregatedscanmetrics_account RENAME TO aggregatedscanfindings_account;

COMMIT;
