-- Deploy database-schema:drop-unused-integration-settings to pg

BEGIN;

DROP TABLE IF EXISTS email_integration_settings, fourme_integration_settings;

COMMIT;
