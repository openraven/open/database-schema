-- Deploy database-schema:gcp-asset-consumer to pg

BEGIN;

CREATE TABLE IF NOT EXISTS globalassetsblob
(
    assetid                    text PRIMARY KEY NOT NULL,
    configuration              jsonb,
    supplementaryConfiguration jsonb,
    FOREIGN KEY(assetid) REFERENCES globalassets(assetid) ON DELETE CASCADE
);

COMMIT;
