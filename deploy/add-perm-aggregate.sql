-- Deploy database-schema:add-perm-aggregate to pg

BEGIN;

CREATE TABLE IF NOT EXISTS assetfilepermissionaggregate (
    assetid text PRIMARY KEY,
    permissions jsonb DEFAULT jsonb_build_array(),
    lastupdated timestamptz
);

CREATE INDEX permissions_gin_idx ON assetfilepermissionaggregate USING gin(permissions);

COMMIT;
