-- Deploy database-schema:fix-vio-view-col-name to pg

BEGIN;

DROP VIEW v_gdriveviolations;

CREATE VIEW v_gdriveviolations AS SELECT v.id,
     v.nodeid,
     v.nodename,
     v.nodetype,
     v.updatedat,
     v.datecreated,
     v.gdriveruleid,
     r.name AS rulename,
     r.severity,
     v.status,
     v.previousstatus,
     v.supplementalinformation,
     gf.mimetype
FROM gdriveviolations v
       INNER JOIN gdriverules r ON v.gdriveruleid = r.id
       INNER JOIN googledrivefiles gf ON gf.fileid=v.nodeid;

COMMIT;
