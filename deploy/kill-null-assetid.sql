-- Deploy database-schema:kill-null-assetid to pg

BEGIN;

DELETE FROM gdriveviolations WHERE assetid IS NULL;

COMMIT;
