-- Deploy database-schema:add-scanfinding-view-gdrive to pg

BEGIN;

CREATE VIEW v_gdrive_scanfinding_enriched AS SELECT g.assetid, d.name, g.scantarget, g.scantargetchild, g.findingscount, g.findingslocations, s.fileid, s.fileurl
                FROM gdrivescanfinding g, suspiciousgoogledrivefilepermissions s, dataclassv2 d
                WHERE g.assetid = s.assetid
                  AND g.scantarget = s.filename
                  AND g.assetid=s.assetid
                  AND d.id = g.dataclassid;

COMMIT;
