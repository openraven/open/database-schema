-- Deploy database-schema:assest-list-tags-filtering to pg

BEGIN;

CREATE TABLE IF NOT EXISTS awstags
(
    tagkey text NOT NULL,
    tagvalue text NOT NULL,
    PRIMARY KEY (tagkey, tagvalue)
);
CREATE INDEX awstags_tagkey ON awstags(tagkey);
CREATE INDEX awstags_tagvalue ON awstags(tagvalue);

COMMIT;
