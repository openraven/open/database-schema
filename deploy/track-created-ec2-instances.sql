-- Deploy database-schema:track-created-ec2-instances to pg

BEGIN;

alter table if exists sdss_snapshot_vpc
    add column if not exists has_created_db_resources boolean not null default false;

update sdss_snapshot_vpc set has_created_db_resources = true;

create table if not exists scannerjobruns_with_ec2 (
    id       uuid primary key not null,
    jobrunid uuid not null,
    environment_id uuid,
    constraint fk_environment_id
      foreign key(environment_id) references sdss_snapshot_vpc(id)
      on delete cascade
);

COMMIT;
