-- Deploy database-schema:sdss-cloudsql-cred-model to pg

BEGIN;

INSERT INTO datastorecredentialmodels
    (id, name, assettypes, credentialmodel)
VALUES ('GC_SECRETS_MANAGER_PASSWORD', 'Google Cloud Secrets Manager password',
        '["GCP::SQL::DBInstance"]',
        '{"resourceId" : "Path to Secrets Manager where the password is held (required)",
        "user": "The username to connect to (required)"}') ON CONFLICT DO NOTHING;

COMMIT;