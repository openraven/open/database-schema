-- Deploy database-schema:new_automations_schema_fixes to pg

BEGIN;

-- Initial creation of these tables was missing some columns and also migrated the data far too early.
-- Dropping the tables and recreating them (with the data migration at a future time) is therefore simpler and harmless.

DROP TABLE IF EXISTS automation_rule, automation_consequence;

CREATE TABLE automation_rule
(
    id               UUID    DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name             TEXT                               NOT NULL,
    description      TEXT    DEFAULT NULL,
    filter_inputs    JSONB                              NOT NULL,
    filter_status    TEXT    DEFAULT 'UNKNOWN'          NOT NULL,
    filter_exception JSONB   DEFAULT NULL,
    event_type       TEXT                               NOT NULL,
    enabled          BOOLEAN DEFAULT TRUE               NOT NULL,
    created_at       TIMESTAMP WITH TIME ZONE           NOT NULL,
    updated_at       TIMESTAMP WITH TIME ZONE           NOT NULL,
    created_by       TEXT                               NOT NULL,
    updated_by       TEXT                               NOT NULL
);

CREATE TABLE automation_consequence
(
    id                 UUID  DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    automation_rule_id UUID                             NOT NULL REFERENCES automation_rule (id) ON DELETE CASCADE,
    entity_version     UUID  DEFAULT uuid_generate_v4() NOT NULL,
    consequence_type   TEXT                             NOT NULL,
    integration_id     UUID  DEFAULT NULL,
    configuration      JSONB DEFAULT NULL,
    status             TEXT  DEFAULT 'UNKNOWN'          NOT NULL,
    exception          JSONB DEFAULT NULL,
    created_at         TIMESTAMP WITH TIME ZONE         NOT NULL,
    updated_at         TIMESTAMP WITH TIME ZONE         NOT NULL,
    created_by         TEXT                             NOT NULL,
    updated_by         TEXT                             NOT NULL
);


COMMIT;
