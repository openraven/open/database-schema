-- Deploy database-schema:refactor-compositedataclass to pg

BEGIN;

DROP TABLE IF EXISTS compositedataclass;

CREATE TABLE compositedataclass (
                                    criterion JSONB NOT NULL DEFAULT '{}'::jsonb,
                                    dirty boolean DEFAULT 't'
) INHERITS (dataclass);

CREATE INDEX IF NOT EXISTS compositedataclass_name ON compositedataclass(name);
CREATE INDEX IF NOT EXISTS compositedataclass_status ON compositedataclass(status);
CREATE INDEX IF NOT EXISTS compositedataclass_dirty ON compositedataclass(dirty);
CREATE INDEX IF NOT EXISTS compositedataclass_managed ON compositedataclass(managed);

COMMIT;

