-- Deploy database-schema:rename_sdss_scan_target_type to pg

BEGIN;

UPDATE scannerjob SET scantargettype = 'Structured Database' WHERE scantargettype = 'AWS Structured SQL databases';

COMMIT;
