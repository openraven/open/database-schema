-- Deploy: drop-dataclass-cache-index-trigger.sql

DROP TRIGGER IF EXISTS dataclass_cache_index_trg ON dataclass;
DROP TRIGGER IF EXISTS metadataclass_cache_index_trg ON metadataclass;
DROP FUNCTION IF EXISTS populate_cache_index_func;
