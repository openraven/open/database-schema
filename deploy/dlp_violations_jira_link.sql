-- Deploy database-schema:dlp_violations_jira_link to pg

BEGIN;

ALTER TABLE gdriveviolations ADD COLUMN IF NOT EXISTS external_link TEXT;

COMMIT;
