-- Deploy database-schema:add-new-asset-tables to pg

BEGIN;

CREATE TABLE IF NOT EXISTS globalassets(
    assetid           text PRIMARY KEY NOT NULL,
    locationstring   text NOT NULL,
    namelabel         text,
    externalidentifiers       jsonb,
    resourcetype      text                  NOT NULL,
    resourcetypelabel text                  NOT NULL,
    category          text,
    creatediso        timestamptz,
    updatediso        timestamptz,
    discoverymeta     jsonb,
    deriveddatablob   jsonb,
    storageinbytes    bigint,
    resourcetypesize  text,
    source            text,
    locationblob      jsonb,
    extendedattributes jsonb,
    resourcemetadata jsonb,
    configurationblob jsonb,
    highestruleseverityviolated text,
    dataclassmap jsonb,
    datacollectionmap jsonb,
    markedfordelete boolean DEFAULT false
);

CREATE INDEX globalassets_resourcetype ON globalassets(resourcetype);
CREATE INDEX globalassets_resourcetypelabel ON globalassets(resourcetypelabel);
CREATE INDEX globalassets_category ON globalassets(category);
CREATE INDEX globalassets_storageinbytes ON globalassets(storageinbytes);
CREATE INDEX globalassets_resourcetypesize ON globalassets(resourcetypesize);
CREATE INDEX globalassets_updatediso ON globalassets(updatediso);
CREATE INDEX globalassets_source ON globalassets(source);
CREATE INDEX globalassets_markedfordelete ON globalassets(markedfordelete);

-- JSON indices

CREATE INDEX globalassets_namelabel_gin_trgm_idx ON globalassets USING gin(namelabel gin_trgm_ops);
CREATE INDEX globalassets_locationblob ON globalassets USING GIN (locationblob jsonb_path_ops);
CREATE INDEX globalassets_externalidentifiers ON globalassets USING GIN (externalidentifiers jsonb_path_ops);
CREATE INDEX globalassets_dataclassmap ON globalassets USING GIN (dataclassmap jsonb_path_ops);
CREATE INDEX globalassets_datacollectionmap ON globalassets USING GIN (datacollectionmap jsonb_path_ops);

ALTER TABLE globalassets REPLICA IDENTITY FULL;

CREATE TABLE IF NOT EXISTS globalassetslist(
    seq               bigserial not null,
    assetid           TEXT PRIMARY KEY NOT NULL
);

CREATE INDEX globalassetslist_seq ON globalassetslist(seq);

COMMIT;
