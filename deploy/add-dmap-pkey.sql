-- Deploy database-schema:add-dmap-pkey to pg

BEGIN;

DELETE FROM dmap WHERE arn IN (
    SELECT arn FROM dmap GROUP BY arn HAVING COUNT(*) > 1
);

ALTER TABLE dmap ADD PRIMARY KEY (arn);

COMMIT;
