-- Deploy database-schema:remove-sdss-cloudsql-cred-model to pg

BEGIN;

DELETE FROM datastoreaccessjobs WHERE mappingid in
                                      (SELECT id FROM datastoreresourceaccessmapping WHERE credid in
                                      (SELECT id FROM datastoreaccesscredentials WHERE credentialmodeltype in ('GC_SECRETS_MANAGER_PASSWORD', 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN')));

DELETE FROM datastoreresourceaccessmapping WHERE credid in (SELECT id FROM datastoreaccesscredentials WHERE credentialmodeltype in ('GC_SECRETS_MANAGER_PASSWORD', 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN'));

DELETE FROM datastoreaccesscredentials WHERE credentialmodeltype in ('GC_SECRETS_MANAGER_PASSWORD', 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN');

DELETE FROM datastorecredentialmodels WHERE id in ('GC_SECRETS_MANAGER_PASSWORD', 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN');

COMMIT;