BEGIN;

ALTER TABLE ddr_state_store RENAME COLUMN event_source TO source_log;

COMMIT;