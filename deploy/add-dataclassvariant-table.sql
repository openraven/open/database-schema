-- Deploy database-schema:add-dataclassvariant-table to pg

BEGIN;

CREATE TABLE IF NOT EXISTS dataclassvariant
(
    id              UUID PRIMARY KEY NOT NULL,
    name            text,
    version         text,
    managed         boolean,
    validatorfunction text,
    matchpatterns   jsonb,
    status          text,
    excludes        jsonb,
    keywords        jsonb,
    keyworddistance int,
    redactiontype   text DEFAULT 'DEFAULT',
    redaction       text,
    createdat       timestamptz,
    updatedat       timestamptz,
    cacheindex      int CHECK ( cacheindex BETWEEN 0 AND 1024 ),
    dataclassid     UUID NOT NULL,
    priority        int DEFAULT 0,
    scope           jsonb DEFAULT '{}'::jsonb
);

COMMIT;