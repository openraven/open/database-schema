-- Deploy database-schema:add_scan_target_type to pg

BEGIN;

ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS scantargettype text not null default 'S3 Bucket';

UPDATE scannerjob SET scantargettype = case
                                           when resourcetype = 'AWS::S3::Bucket' then 'S3 Bucket'
                                           when resourcetype = 'GCP::Storage::Bucket' then 'Cloud Storage'
                                           when resourcetype = 'AWS::RDS::DBInstance' then 'AWS Structured SQL databases'
                                       end;

COMMIT;
