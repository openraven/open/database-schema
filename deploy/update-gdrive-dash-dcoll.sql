-- Deploy database-schema:update-gdrive-dash-dcoll to pg

BEGIN;

DROP VIEW IF EXISTS v_gdrive_dash;
CREATE VIEW v_gdrive_dash AS SELECT gf.assetid,
       gf.fileid,
       gf.fileurl,
       gf.filename,
       ga.namelabel                                                                           AS drivename,
       gf.username,
       gp.sharedbylink                                                                        AS anyonewithlink,
       COALESCE(gp.suspiciousemails, NULL)                                                    AS suspiciousemails,
       gs.filecreatedat,
       gs.fileupdatedat,
       gs.fileaccessedat,
       ga.resourcetype,
       gb.configuration -> 'restrictions'::text                                               AS restrictions,
        gf.mimetype,
       gf.updatedat,
       COALESCE(json_agg(DISTINCT gr.name) FILTER (WHERE gr.name IS NOT NULL), NULL)          AS rulesviolated,
       COALESCE(json_agg(DISTINCT dc.name) FILTER (WHERE dc.name IS NOT NULL), NULL)          AS data_classes,
       COALESCE(json_agg(DISTINCT dx.name) FILTER (WHERE dx.name IS NOT NULL), NULL)          AS data_collections,

       array_to_json(ARRAY(SELECT DISTINCT "substring"(email.value, '@(.+)$'::text) AS "substring"
                           FROM jsonb_array_elements_text(gp.suspiciousemails) email(value))) AS emaildomains,
       array_to_json(ARRAY(SELECT DISTINCT "substring"(email.value, '(.+)@'::text) AS "substring"
                           FROM jsonb_array_elements_text(gp.suspiciousemails) email(value))) AS emailusers
FROM googledrivefiles gf
         JOIN globalassets ga ON ga.assetid = gf.assetid
         JOIN globalassetsblob gb ON gb.assetid = gf.assetid
         LEFT JOIN suspiciousgoogledrivefilepermissions gp ON gf.assetid = gp.assetid AND gf.fileid = gp.fileid
         LEFT JOIN googledrivestalefiles gs ON gf.assetid = gs.assetid AND gf.fileid = gs.fileid
         LEFT JOIN gdriveviolations gv ON gf.fileid = gv.nodeid
         LEFT JOIN gdriverules gr ON gv.gdriveruleid = gr.id
         LEFT JOIN gdrivescanfinding sf ON gf.fileid = sf.scantarget
         LEFT JOIN dataclassv2 dc ON dc.id = sf.dataclassid
         LEFT JOIN datacollection_dataclass dj ON dj.dataclassid = dc.id
         LEFT JOIN datacollection dx ON dx.id = dj.datacollectionid

WHERE ga.resourcetype = ANY (ARRAY ['GOOGLEWORKSPACE::SharedDrive'::text, 'GOOGLEWORKSPACE::MyDrive'::text])
GROUP BY gf.assetid, gf.fileid, gf.fileurl, gf.filename, ga.namelabel, gf.username, gp.sharedbylink,
         gp.suspiciousemails, gs.filecreatedat, gs.fileupdatedat, gs.fileaccessedat, ga.resourcetype,
         gb.configuration, gf.mimetype, gf.updatedat, sf.scantarget, gr.name;

COMMIT;
