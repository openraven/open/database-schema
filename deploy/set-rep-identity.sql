-- Deploy database-schema:set-rep-identity to pg

BEGIN;

ALTER TABLE orvn.public.gdrivemetadatascanfinding REPLICA IDENTITY FULL;


COMMIT;
