-- Deploy database-schema:PROD-427 to pg

BEGIN;

-- this is a concession to docker-compose testing
-- and is verbose because there is no "CREATE ROLE IF NOT EXISTS"
DO $sql$
    BEGIN
        -- this is just a marker role
        CREATE ROLE rds_iam;
        CREATE ROLE rds_superuser WITH SUPERUSER;
    EXCEPTION
        -- this is expected inside RDS
        WHEN OTHERS THEN NULL;
    END;
$sql$;

-- these purposefully don't set a password since they are designed to be IAM-ed into
-- for **local** development, you can << ALTER USER foo WITH ENCRYPTED PASSWORD 'bar' >>
-- in order to login as that user, or << SET ROLE = orvn_ro; >> to just switch to the role from "postgres"
CREATE USER orvn_ro IN ROLE rds_iam;
CREATE USER orvn_superuser IN ROLE rds_iam, rds_superuser;

CREATE USER orvn_account_management IN ROLE rds_iam;
CREATE USER orvn_asset_groups IN ROLE rds_iam;
CREATE USER orvn_aws_postgres_consumer IN ROLE rds_iam;
CREATE USER orvn_cluster_upgrade IN ROLE rds_iam;
CREATE USER orvn_cross_account IN ROLE rds_iam;
CREATE USER orvn_data_catalog IN ROLE rds_iam;
CREATE USER orvn_dmap_scheduler IN ROLE rds_iam;
CREATE USER orvn_dmap IN ROLE rds_iam;
CREATE USER orvn_integrations IN ROLE rds_iam;
CREATE USER orvn_policy_v2 IN ROLE rds_iam;
CREATE USER orvn_s3_scan_service IN ROLE rds_iam;
CREATE USER orvn_user_notifications IN ROLE rds_iam;

GRANT SELECT ON ALL TABLES IN SCHEMA public, sqitch TO orvn_ro;
-- we are purposefully omitting any perm grants to those per-app users
-- pending further thinking about what perms they need

COMMIT;
