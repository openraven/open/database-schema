-- Verify database-schema:sdss-cred-model-iam on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'IAM_REDSHIFT_ACCESS_PASSWORD') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id IAM_REDSHIFT_ACCESS_PASSWORD should exist';
END IF;

END;
$$;

ROLLBACK;
