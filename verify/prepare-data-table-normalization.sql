-- Verify database-schema:prepare-data-table-normalization on pg

BEGIN;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_views
        WHERE
                pg_views.viewname = 'v_googledrivestalefiles';
        IF FOUND THEN
            RAISE EXCEPTION 'View v_googledrivestalefiles exists and should not.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_views
        WHERE
                pg_views.viewname = 'v_suspiciousgoogledrivefilepermissions';
        IF FOUND THEN
            RAISE EXCEPTION 'View v_suspiciousgoogledrivefilepermissions exists and should not.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_attribute
        WHERE
                attrelid = 'suspiciousgoogledrivefilepermissions'::regclass and attname in ('filename','fileurl') and attnotnull=true;
        IF FOUND THEN
            RAISE EXCEPTION 'Not null constraints exist on suspiciousgoogledrivefilepermissions.filename and suspiciousgoogledrivefilepermissions.fileurl. It should not exist.';
END IF;
END;
$$;


    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_attribute
        WHERE
                attrelid = 'googledrivestalefiles'::regclass and attname in ('filename','fileurl') and attnotnull=true;
        IF FOUND THEN
            RAISE EXCEPTION 'Not null constraints exist on googledrivestalefiles.filename and googledrivestalefiles.fileurl. It should not exist.';
END IF;
END;
$$;


ROLLBACK;
