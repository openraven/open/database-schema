-- Verify database-schema:add-namespace-to-asset-group on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetworkspace'
          AND column_name = 'namespace';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetworkspace.namespace does not exist';
END IF;
END;
$$;

ROLLBACK;
