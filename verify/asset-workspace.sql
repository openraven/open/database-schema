-- Verify database-schema:asset-workspace on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetworkspace'
          AND column_name = 'workspaceid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetworkspace.workspaceid does not exist';
        END IF;
    END;
$$;
ROLLBACK;
