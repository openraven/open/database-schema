-- verify database-schema:sdss-cloudsql-backup on pg

begin;

do $$
    begin
        perform true
        from   information_schema.tables
        where  table_schema  = 'public'
          and table_name = 'sdss_cloudsql_env';
        if not found then
            raise exception 'sdss_cloudsql_env table does not exist';
        end if;
    end;
$$;

rollback;
