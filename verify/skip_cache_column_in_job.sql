-- Verify database-schema:skip_cache_column_in_job on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjob'
	  AND column_name = 'skipcache';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjob.skipcache does not exist';
END IF;

END;
$$;

ROLLBACK;
