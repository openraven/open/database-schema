-- Verify database-schema:sdss-privatekey-cred-model on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'SECRETS_MANAGER_PRIVATE_KEY') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id SECRETS_MANAGER_PRIVATE_KEY should exist';
END IF;

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'PARAMETER_STORE_PASSWORD') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id PARAMETER_STORE_PASSWORD should exist';
END IF;

END;
$$;

ROLLBACK;
