-- Verify database-schema:add-match-redaction-to-dataclass on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'dataclass'
	  AND column_name = 'redactiontype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'dataclass.redactiontype does not exist';
END IF;
END;
$$;
DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'dataclass'
	  AND column_name = 'redaction';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'dataclass.redaction does not exist';
END IF;
END;
$$;

ROLLBACK;
