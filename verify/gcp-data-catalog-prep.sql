-- Verify database-schema:gcp-data-catalog-prep on pg

BEGIN;

DO $sql$
        DECLARE
replica_id TEXT;
BEGIN
SELECT CASE relreplident
           WHEN 'd' THEN 'default'
           WHEN 'n' THEN 'nothing'
           WHEN 'f' THEN 'full'
           WHEN 'i' THEN 'index'
           END AS replica_identity
FROM pg_class
    INTO replica_id
WHERE oid = 'cloudstoragescanfinding'::regclass;
ASSERT replica_id = 'full', 'Replication identity for cloudstoragescanfinding should be full';
END;
$sql$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'source';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.source does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'source';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.source does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetfindings'
          AND
                indexname = 'aggregatedassetfindings_source';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings_source does not exist';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanfindings'
          AND
                indexname = 'aggregatedscanfindings_source';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings_source does not exist';
END IF;
END;
$$;

ROLLBACK;
