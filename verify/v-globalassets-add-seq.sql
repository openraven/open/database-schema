-- Verify database-schema:v-globalassets-add-seq on pg

BEGIN;

SELECT seq FROM v_globalassets LIMIT 1;

ROLLBACK;
