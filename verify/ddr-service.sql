BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_rules'
	  AND column_name = 'id';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_rules.id should exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_state_store'
	  AND column_name = 'key';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_state_store.key should exist';
END IF;
END;
$$;


DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_metadata'
	  AND column_name = 'folder';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_metadata.folder should exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_alerts'
	  AND column_name = 'alert_rule_name';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_alerts.folder should exist';
END IF;
END
$$;
ROLLBACK;
