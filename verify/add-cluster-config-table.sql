-- Verify database-schema:add-cluster-config-table on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.schemata
        WHERE  schema_name  = 'cluster';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cluster schema does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema = 'cluster'
          AND  table_name  = 'global_configuration';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cluster.global_configuration table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'cluster'
          AND  table_name  = 'global_configuration'
          AND  column_name = 'key';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cluster.global_configuration.key column does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'cluster'
          AND  table_name  = 'global_configuration'
          AND  column_name = 'value';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cluster.global_configuration.value column does not exist';
        END IF;
    END;
$$;

ROLLBACK;
