-- Verify database-schema:add-sdss-audit-log on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_finishedscans';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_finishedscans table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scanauditlogevent';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scanauditlogevent table does not exist';
        END IF;
    END;
$$;


ROLLBACK;
