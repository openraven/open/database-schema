-- Verify database-schema:scanned-assets on pg

BEGIN;

SELECT assetid, scanned FROM v_globalassetscomplete LIMIT 1;
SELECT assetid, scanned FROM v_globalassets LIMIT 1;
SELECT assetid FROM scannedasset LIMIT 1;

ROLLBACK;
