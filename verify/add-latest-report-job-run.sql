-- Verify database-schema:add-latest-report-job-run on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'reportjob'
          AND column_name = 'latestrunid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'reportjob.latestrunid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
