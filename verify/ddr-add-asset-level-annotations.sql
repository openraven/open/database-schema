BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_asset_coverage'
	  AND column_name = 'arn';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_asset_coverage.arn should exist';
END IF;
END;
$$;

ROLLBACK;

