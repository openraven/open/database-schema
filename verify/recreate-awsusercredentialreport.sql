-- Verify database-schema:recreate-awsusercredentialreport on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'awsusercredentialreport';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsusercredentialreport table does not exist';
END IF;
END;
$$;


ROLLBACK;
