DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'dmap'
          AND column_name = 'arn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dmap.arn does not exist';
        END IF;
    END;
$$;
