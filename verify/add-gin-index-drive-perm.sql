-- Verify database-schema:add-gin-index-drive-perm on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'suspiciousgoogleshareddrivepermissions'
          AND
                indexname = 'suspiciousgoogleshareddrivepermissions_permissions_gin';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogleshareddrivepermissions.suspiciousgoogleshareddrivepermissions_permissions_gin does not exist';
END IF;
END;
$$;

ROLLBACK;
