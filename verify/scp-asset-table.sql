-- Verify database-schema:add-service-control-policy-table on pg

BEGIN;

INSERT INTO awsorganizationscp(documentId, arn, resourcename, resourceid, resourcetype, awsregion, awsaccountid, creatediso, updatediso, discoverysessionid, tags, configuration, supplementaryconfiguration, discoverymeta)
VALUES ('docId', 'arn', 'resourceName', 'resourceId', 'resourceType', 'region', 'account', now(), now(),'session', '{}', '{}', '{}', '{}');

ROLLBACK;