-- Verify database-schema:sdss-cloudsql-cred-models on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'GC_SECRETS_MANAGER_PASSWORD') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id GC_SECRETS_MANAGER_PASSWORD should exist';
END IF;

END;
$$;

ROLLBACK;