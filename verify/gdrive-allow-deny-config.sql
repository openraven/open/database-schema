BEGIN;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.allowedDomains') THEN
            RAISE EXCEPTION 'No records found in the table.';
    END IF;
END $$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.deniedDomains') THEN
            RAISE EXCEPTION 'No records found in the table.';
    END IF;
END $$;

ROLLBACK;
