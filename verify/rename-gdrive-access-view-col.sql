-- Verify database-schema:rename-gdrive-access-view-col on pg

BEGIN;

SELECT anyonewithlink FROM v_suspiciousgoogledrivefilepermissions;

ROLLBACK;
