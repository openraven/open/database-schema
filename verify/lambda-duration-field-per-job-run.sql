-- Verify database-schema:lambda-duration-field-per-job-run on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'lambdaduration';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.lambdaduration does not exist';
    END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjob'
          AND column_name = 'maxscancost';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scannerjob.maxscancost does not exist';
    END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjob'
          AND column_name = 'maxlambdaduration';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scannerjob.maxlambdaduration does not exist';
    END IF;
END;
$$;

ROLLBACK;
