-- Verify database-schema:add-asset-to-file-count on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_matviews
        WHERE
                pg_matviews.matviewname = 'mat_v_gdrive_asset_file_count';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Mat view mat_v_gdrive_asset_file_count does not exist.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_asset_file_count'
          AND
                indexname = 'mat_v_gdrive_asset_file_count_unique';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_gdrive_asset_file_count.mat_v_gdrive_asset_file_count_unique does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_gdrive_file_aggregate_data.mat_v_gdrive_file_aggregate_data_assetid does not exist';
END IF;
END;
$$;

ROLLBACK;
