-- Verify database-schema:add-scantime-gdrivedash on pg

BEGIN;

SELECT lastsuccessfulscan FROM v_gdrive_dash LIMIT 1;

ROLLBACK;
