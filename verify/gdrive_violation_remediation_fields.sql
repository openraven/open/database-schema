-- Verify database-schema:gdrive_violation_remediation_fields on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriveviolations'
          AND column_name = 'actionid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations.actionid does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriveviolations'
          AND column_name = 'remediationinfo';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations.remediationinfo does not exist';
        END IF;
    END;
$$;

ROLLBACK;
