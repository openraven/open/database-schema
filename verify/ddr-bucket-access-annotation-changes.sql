BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_bucket_access'
	  AND column_name = 'noaccessreason';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_bucket_access.noaccessreason should exist';
END IF;
END;
$$;


DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_cloudtrail_coverage'
	  AND column_name = 'noaccessreason';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_cloudtrail_coverage.noaccessreason should exist';
END IF;
END;
$$;

ROLLBACK;