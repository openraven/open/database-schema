-- Verify database-schema:add-gdrive-dash-poc-view on pg

BEGIN;

SELECT * FROM v_gdrive_dash LIMIT 1;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'gdriveviolations'
          AND
                indexname = 'gdriveviolations_nodeid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations.gdriveviolations_nodeid does not exist.';
END IF;
END;
$$;

ROLLBACK;
