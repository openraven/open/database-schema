BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_alerts'
	  AND column_name = 'alert_parameters';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_alerts.alert_parameters should exist';
END IF;
END;
$$;
ROLLBACK;