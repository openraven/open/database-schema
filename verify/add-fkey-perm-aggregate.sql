-- Verify database-schema:add-fkey-perm-aggregate on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'fk_assetfilepermissionaggregate_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'fk_assetfilepermissionaggregate_assetid does not exist';
END IF;
END;
$$;

ROLLBACK;
