-- Verify database-schema:bucket-resume-info on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'bucketresumeinfo'
          AND column_name = 'id';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'bucketresumeinfo.id does not exist';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'bucketresumeinfo'
          AND column_name = 'resumeobject';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'bucketresumeinfo.resumeobject does not exist';
    END IF;
END;
$$;

ROLLBACK;
