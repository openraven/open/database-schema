-- Verify database-schema:sdss-drop-scanfinding-assetid-constraint on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.table_constraints
        WHERE  table_name  = 'sdss_scanfinding'
          AND constraint_name = 'sdss_scanfinding_assetid_fkey';
        IF FOUND THEN
            RAISE EXCEPTION 'sdss_scanfinding.sdss_scanfinding_assetid_fkey still exists';
        END IF;
    END;
$$;

ROLLBACK;
