-- Verify database-schema:jira_integration_settings on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'jira_integration_settings';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_integration_settings table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
