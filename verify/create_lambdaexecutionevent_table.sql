-- Verify database-schema:create_lambdaexecutionevent_table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'lambdaexecutionevent';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'lambdaexecutionevent table does not exist';
END IF;
END;
$$;

ROLLBACK;
