-- Verify database-schema:migrate-gdrive-view-left-join on pg

BEGIN;

SELECT COUNT(*) FROM v_gdrive_dash LIMIT 1;
SELECT COUNT(permissions) FROM suspiciousgoogledrivefilepermissions LIMIT 1;

ROLLBACK;
