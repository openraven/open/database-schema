BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_gcp_metadata'
	  AND column_name = 'projectid';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_gcp_metadata.projectid should exist';
END IF;
END;
$$;

ROLLBACK;

