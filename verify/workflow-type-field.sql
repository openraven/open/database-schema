-- Verify database-schema:workflow-type-field on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflow_configuration'
          AND column_name = 'workflow_type';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflow_configuration.workflow_type does not exist';
        END IF;
    END;
$$;

ROLLBACK;
