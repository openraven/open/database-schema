DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'fileviolation'
          AND column_name = 'sourcechild';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'fileviolation.sourcechild does not exist';
        END IF;
    END;
$$;
