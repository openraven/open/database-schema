-- Verify database-schema:prepare-sdss-change-cap on pg

BEGIN;

DO $sql$
        DECLARE
        replica_id TEXT;
        BEGIN
        SELECT CASE relreplident
                   WHEN 'd' THEN 'default'
                   WHEN 'n' THEN 'nothing'
                   WHEN 'f' THEN 'full'
                   WHEN 'i' THEN 'index'
                   END AS replica_identity
        FROM pg_class
            INTO replica_id
        WHERE oid = 'sdss_scanfinding'::regclass;
        ASSERT replica_id = 'full', 'Replication identity for sdss_scanfinding should be full';
        END;
$sql$;

ROLLBACK;
