-- Verify database-schema:gdrive-access-analysis-splunk on pg
BEGIN;

SELECT COUNT(*) FROM v_suspiciousgoogledrivefilepermissions;
SELECT COUNT(*) FROM v_suspiciousgoogleshareddrivepermissions;

ROLLBACK;
