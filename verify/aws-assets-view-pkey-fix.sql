-- Verify database-schema:aws-assets-view-pkey-fix on pg

BEGIN;

DO
$$
    BEGIN
        if not exists(select constraint_name
                      from information_schema.constraint_column_usage
                      where table_name = 'awsassetsview'
                        and constraint_name = 'awsassetsview_pkey')
        THEN
            RAISE EXCEPTION 'awsassetsview_pkey does not exist';
        END IF;
    END
$$;

ROLLBACK;
