-- Verify database-schema:add-eventslog on pg

BEGIN;

DO $$
    BEGIN
            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 'eventlog';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'eventlog table does not exist';
            END IF;
    END
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'eventlog'
          AND
            indexname = 'eventlogindex_actor';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index eventlogindex_actor does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'eventlog'
          AND
            indexname = 'eventlogindex_eventcategory';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index eventlogindex_eventcategory does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'eventlog'
          AND
            indexname = 'eventlogindex_eventtype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index eventlogindex_eventtype does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'eventlog'
          AND
            indexname = 'eventlogindex_message';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index eventlogindex_message does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'eventlog'
          AND
            indexname = 'eventlogindex_timestamp';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index eventlogindex_timestamp does not exist';
END IF;
END;
$$;

ROLLBACK;
