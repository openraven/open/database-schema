-- Verify database-schema:add_dataclassversion_to_scanfindings on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'dataclassversion'
          AND data_type   = 'text';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.dataclassversion does not exist with type text';
END IF;
END;
$$;

ROLLBACK;
