-- Verify database-schema:sdss-cloudsql-iam-cred-models on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id GC_IAM_CLOUD_SQL_ACCESS_TOKEN should exist';
END IF;

END;
$$;

ROLLBACK;

