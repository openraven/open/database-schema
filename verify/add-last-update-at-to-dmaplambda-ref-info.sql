-- Verify database-schema:add-last-update-at-to-dmaplambda-ref-info on pg

BEGIN;

    DO $$
        BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'dmaplambdareferenceinfo'
              AND column_name = 'lastupdateat';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'dmaplambdareferenceinfo.lastupdateat does not exist';
            END IF;

            PERFORM TRUE
            FROM
                pg_indexes
            WHERE
                schemaname = 'public'
              AND
                tablename = 'dmaplambdareferenceinfo'
              AND
                indexname = 'dmaplambdarefinfo_lastupdateat';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'Index dmaplambdareferenceinfo.dmaplambdarefinfo_lastupdateat does not exist';
            END IF;
        END;
    $$;

ROLLBACK;
