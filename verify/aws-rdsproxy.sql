-- Verify database-schema:aws-rdsproxy on pg

BEGIN;


DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsrdsproxy'
          AND column_name = 'documentid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsrdsproxy.documentid does not exist';
        END IF;

        PERFORM TRUE
        FROM   pg_index i
        JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
        WHERE  i.indrelid = 'awsrdsproxy'::regclass
        AND    i.indisprimary = false and a.attname = 'documentid';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'awsrdsproxy.documentid should be of part of an index for awsrdsproxy';
        END IF;

    END;
$$;
ROLLBACK;
