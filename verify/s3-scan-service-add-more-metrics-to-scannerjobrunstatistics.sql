-- Verify database-schema:s3-scan-service-add-more-metrics-to-scannerjobrunstatistics on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'numberofbuckets';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.numberofbuckets does not exist';
    END IF;
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'currentbucketnumber';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.currentbucketnumber does not exist';
    END IF;
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'currentbucketname';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.currentbucketname does not exist';
    END IF;
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'skippedfilecount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.skippedfilecount does not exist';
    END IF;
END;
$$;
