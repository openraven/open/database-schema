BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'sdss_lambdareferenceinfo'
          AND
                indexname = 'sdss_lambdarefinfo_createdat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_lambdareferenceinfo.sdss_lambdarefinfo_createdat does not exist';
END IF;
END;
$$;
DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'sdss_scanfinding'
          AND
                indexname = 'sdss_scanfinding_assetid_scantarget_scantargetchild_scannerjob';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scanfinding.sdss_scanfinding_assetid_scantarget_scantargetchild_scannerjob does not exist';
END IF;
END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.table_constraints
        WHERE  table_name  = 'awsrdsdbinstance'
          AND constraint_name = 'awsrdsdbinstance_arn_unique'
          AND constraint_type = 'UNIQUE';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsrdsdbinstance.awsrdsdbinstance_arn_unique does not exists with type uuid';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scannerjob';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scannerjob table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scannerjobruns';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scannerjobruns table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scannerjobrunstatistics';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scannerjobrunstatistics table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_lambdareferenceinfo';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_lambdareferenceinfo table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scannerjobpage';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scannerjobpage table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'metadatadbs';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadatadbs table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'metadatatables';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadatatables table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'datastoreaccesscredentials';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datastoreaccesscredentials table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'datastoreresourceaccessmapping';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datastoreresourceaccessmapping table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'datastoreaccessjobs';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datastoreaccessjobs table does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scanfinding table does not exist';
        END IF;
    END;
$$;

ROLLBACK;