BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'ddr_bucket_access'
          AND column_name = 'trailarn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'ddr_bucket_access.trailarn should exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.table_constraints
        WHERE  table_name  = 'ddr_bucket_access'
          AND constraint_name = 'ddr_bucket_access_pkey'
          AND constraint_type = 'PRIMARY KEY';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'ddr_bucket_access_pkey does not exist.';
        END IF;
    END;
$$;

ROLLBACK;