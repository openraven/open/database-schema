-- Verify database-schema:orvn_ro_add_read_access on pg

BEGIN;

DO
$sql$
    DECLARE
        ct BIGINT;
    BEGIN
        SET ROLE orvn_ro;
        SELECT COUNT(*) INTO ct FROM awsassetsview;
        ASSERT ct >= 0, 'access should be allowed after changes';
    END;
$sql$;

ROLLBACK;
