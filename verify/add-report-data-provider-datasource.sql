-- Verify database-schema:add-report-data-provider-datasource on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'report_operation_key_metadata'
          AND column_name = 'operation_key';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'report_operation_key_metadata.operation_key should exist';
        END IF;
    END;
$$;

ROLLBACK;
