-- Verify database-schema:rep-id-full-composite-metadata on pg

BEGIN;

DO $sql$
        DECLARE
replica_id TEXT;
BEGIN
SELECT CASE relreplident
           WHEN 'd' THEN 'default'
           WHEN 'n' THEN 'nothing'
           WHEN 'f' THEN 'full'
           WHEN 'i' THEN 'index'
           END AS replica_identity
FROM pg_class
    INTO replica_id
WHERE oid = 'metadatafinding'::regclass;
ASSERT replica_id = 'full', 'Replication identity for metadatafinding should be full';
END;
$sql$;

DO $sql$
        DECLARE
replica_id TEXT;
BEGIN
SELECT CASE relreplident
           WHEN 'd' THEN 'default'
           WHEN 'n' THEN 'nothing'
           WHEN 'f' THEN 'full'
           WHEN 'i' THEN 'index'
           END AS replica_identity
FROM pg_class
    INTO replica_id
WHERE oid = 'compositescanfinding'::regclass;
ASSERT replica_id = 'full', 'Replication identity for compositescanfinding should be full';
END;
$sql$;

ROLLBACK;
