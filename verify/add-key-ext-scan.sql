-- Verify database-schema:add-key-ext-scan on pg

BEGIN;

DO $$
BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.scan-externally-owned-enabled') THEN
            RAISE EXCEPTION 'No records found in the table.';
END IF;
END;
$$;


ROLLBACK;
