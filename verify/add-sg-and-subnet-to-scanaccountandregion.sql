-- Verify database-schema:add-sg-and-subnet-to-scanaccountandregion on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanaccountandregion'
	  AND column_name = 'securitygroupids';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanaccountandregion.securitygroupids does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanaccountandregion'
	  AND column_name = 'subnetids';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanaccountandregion.subnetids does not exist';
    END IF;
END;
$$;

ROLLBACK;
