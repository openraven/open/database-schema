-- Verify database-schema:asset-violation-audit-columns on pg

BEGIN;

SElECT statuschangedby FROM assetviolation LIMIT 1;
SElECT statuschangedbytimestamp FROM assetviolation LIMIT 1;

ROLLBACK;
