-- Verify database-schema:s3objectcache_part_ext on pg

BEGIN;

CREATE schema partman_test;
CREATE TABLE partman_test.id_taptest_table (
                                               col1 bigint
    , col2 text not null
    , col3 timestamptz DEFAULT now()
    , col4 text) PARTITION BY RANGE (col1);

CREATE INDEX ON partman_test.id_taptest_table (col1);
SELECT partman.create_parent('partman_test.id_taptest_table', 'col1', 'native', '10');

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            partman.part_config
        WHERE
                parent_table = 'partman_test.id_taptest_table';
        IF NOT FOUND THEN
            DROP SCHEMA partman_test CASCADE ;
            RAISE EXCEPTION 'partman failed to partition';
        END IF;
    END;
$$;
ROLLBACK;
