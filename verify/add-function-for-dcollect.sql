-- Verify database-schema:add-function-for-dcollect on pg

BEGIN;

DO $$
BEGIN
    BEGIN
        perform pg_get_functiondef('calculate_word_frequencies(text[])'::regprocedure);
    EXCEPTION WHEN undefined_function THEN
        RAISE EXCEPTION 'calculate_word_frequencies does not exist';
    END;
END $$;

ROLLBACK;
