-- Verify database-schema:add-index-fileissharedtouser on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_fileissharedtouser';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivefiles.googledrivefiles_fileissharedtouser does not exist';
END IF;
END;
$$;

ROLLBACK;
