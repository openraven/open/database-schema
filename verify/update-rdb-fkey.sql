-- Verify database-schema:update-rdb-fkey on pg

BEGIN;

    DO $sql$
    DECLARE
        keycount BIGINT;
    BEGIN
    select count(*) INTO keycount FROM information_schema.table_constraints where table_name='aggregatedrelationaldatabasefindings' and constraint_type='FOREIGN KEY';
    ASSERT keycount = 2, 'Total foreign keys on aggregatedrelationaldatabasefindings was not 2.';
    END;
$sql$;



ROLLBACK;
