-- Verify database-schema:reseed-upgrade on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'reseed_provider_upgrade_version'
          AND column_name = 'operation_key';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'reseed_provider_upgrade_version.operation_key does not exist';
        END IF;
    END;
$$;

ROLLBACK;
