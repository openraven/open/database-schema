-- Verify database-schema:unify-integration-settings on pg

BEGIN;

DO
$$
    BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'integrations';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.integrations does not exist';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'eventbridge_integration_settings_v2';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.eventbridge_integration_settings_v2 does not exist';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'jira_integration_settings_v2';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.jira_integration_settings_v2 does not exist';
        END IF;

        PERFORM TRUE
        WHERE (SELECT count(1) FROM jira_integration_settings) = (SELECT count(1) FROM jira_integration_settings_v2);
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_integration_settings_v2 count mismatch';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'slack_integration_settings_v2';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.slack_integration_settings_v2 does not exist';
        END IF;

        PERFORM TRUE
        WHERE (SELECT count(1) FROM slack_integration_settings) = (SELECT count(1) FROM slack_integration_settings_v2);
        IF NOT FOUND THEN
            RAISE EXCEPTION 'slack_integration_settings_v2 count mismatch';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'webhook_integration_settings_v2';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.webhook_integration_settings_v2 does not exist';
        END IF;

        PERFORM TRUE
        WHERE (SELECT count(1) FROM webhook_integration_settings) = (SELECT count(1) FROM webhook_integration_settings_v2);
        IF NOT FOUND THEN
            RAISE EXCEPTION 'webhook_integration_settings_v2 count mismatch';
        END IF;
    END;
$$;

ROLLBACK;
