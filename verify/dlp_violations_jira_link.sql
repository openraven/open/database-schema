-- Verify database-schema:dlp_violations_jira_link on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriveviolations'
          AND column_name = 'external_link';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations.external_link does not exist';
        END IF;
    END;
$$;
ROLLBACK;
