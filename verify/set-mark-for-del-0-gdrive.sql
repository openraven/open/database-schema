-- Verify database-schema:set-mark-for-del-0-gdrive on pg

BEGIN;

DO $sql$
    DECLARE
        countMarkDelete BIGINT;
    BEGIN
        SELECT COUNT(*) INTO countMarkDelete FROM globalassets WHERE source = 'google-workspace' AND markedfordelete=1;
        ASSERT countMarkDelete = 0, 'Should be no marked for delete assets in google-workspace';
    END;
$sql$;

ROLLBACK;
