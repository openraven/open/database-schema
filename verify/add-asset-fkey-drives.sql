-- Verify database-schema:add-asset-fkey-drives on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'fk_suspiciousgoogleshareddrivepermissions_asset_id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'fk_suspiciousgoogleshareddrivepermissions_asset_id does not exist';
END IF;
END;
$$;

ROLLBACK;
