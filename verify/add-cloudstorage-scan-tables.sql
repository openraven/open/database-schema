-- Verify database-schema:add-cloudstoragefinding-table on pg

BEGIN;

DO $$
    BEGIN
            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 'cloudstoragescanfinding';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'cloudstoragescanfinding table does not exist';
            END IF;

            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 'cloudstoragemetadatascanfinding';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'cloudstoragemetadatafinding table does not exist';
            END IF;

            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 's3metadatascanfinding';
            IF NOT FOUND THEN
                RAISE EXCEPTION 's3metadatascanfinding table does not exist';
            END IF;

    END;
$$;

ROLLBACK;