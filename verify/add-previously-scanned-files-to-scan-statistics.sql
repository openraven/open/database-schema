-- Verify database-schema:add-previously-scanned-files-to-scan-statistics.sql on pg

BEGIN;

DO $$
BEGIN

PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'previouslyscannedfilescount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.previouslyscannedfilescount does not exist';
    END IF;
END;
$$;

ROLLBACK;

