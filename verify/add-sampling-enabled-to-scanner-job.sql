-- Verify database-schema:add-sampling-enabled-to-scanner-job in pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjob'
	  AND column_name = 'samplingenabled';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjob.samplingenabled does not exist';
END IF;
END;
$$;
