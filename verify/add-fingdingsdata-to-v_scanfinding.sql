-- Verify database-schema:add-fingdingsdata-to-v_scanfinding on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.views
        WHERE  table_name = 'v_scanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_scanfinding does not exist';
        END IF;
    END
$$;

ROLLBACK;
