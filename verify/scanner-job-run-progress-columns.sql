-- Verify database-schema:scanner-job-run-progress-columns in pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'numerator';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.numerator does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'denominator';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.denominator does not exist';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjobruns'
          AND column_name = 'scannerjobid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjobruns.scannerjobid does not exist';
    END IF;
END;
$$;