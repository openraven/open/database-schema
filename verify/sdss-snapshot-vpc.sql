-- Verify database-schema:sdss-snapshot-vpc on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_snapshot_vpc';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_snapshot_vpc table does not exist';
END IF;
END;
$$;

ROLLBACK;
