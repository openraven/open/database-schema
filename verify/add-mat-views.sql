-- Verify database-schema:add-mat-views on pg

BEGIN;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_matviews
        WHERE
                pg_matviews.matviewname = 'mat_v_external_files_by_asset';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Mat view mat_v_external_files_by_asset does not exist.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_matviews
        WHERE
                pg_matviews.matviewname = 'mat_v_suspicious_user_aggregation';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Mat view mat_v_suspicious_user_aggregation does not exist.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_suspicious_user_aggregation'
          AND
                indexname = 'suspicious_user_aggregation_unique';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_suspicious_user_aggregation.suspicious_user_aggregation_unique does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_suspicious_user_aggregation'
          AND
                indexname = 'mat_v_suspicious_user_aggregation_suspiciousemail';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_suspicious_user_aggregation.mat_v_suspicious_user_aggregation_suspiciousemail does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_external_files_by_asset'
          AND
                indexname = 'external_files_by_asset_unique';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_external_files_by_asset.external_files_by_asset_unique does not exist';
END IF;
END;
$$;

ROLLBACK;
