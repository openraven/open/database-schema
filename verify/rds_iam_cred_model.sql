-- Verify database-schema:rds_iam_cred_model on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'IAM_RDS_ACCESS_TOKEN') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id IAM_RDS_ACCESS_TOKEN should exists';
END IF;

END;
$$;

ROLLBACK;
