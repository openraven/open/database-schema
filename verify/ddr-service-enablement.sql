BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'service_enablement'
          AND column_name = 'service_name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'service_enablement.service_name should exist';
        END IF;
    END;
$$;

ROLLBACK;