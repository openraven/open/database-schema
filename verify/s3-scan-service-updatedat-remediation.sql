-- Verify database-schema:s3-scan-service-updatedat-remediation to pg

DO $$
BEGIN
        PERFORM TRUE
        FROM   scannerjob
        WHERE  createdat IS NULL OR updatedat is NULL;
        IF FOUND THEN
            RAISE EXCEPTION 'scannerjob remain records with createdat / updatedat = null';
END IF;
END;
$$;
