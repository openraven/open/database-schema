-- Verify database-schema:add-index-column-to-dataclasses.sql to pg

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'dataclass'
          AND column_name = 'cacheindex';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclass.cacheindex does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'metadataclass'
          AND column_name = 'cacheindex';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadataclass.cacheindex does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.sequences
        WHERE  sequence_name  = 'custom_dataclasses_global_seq';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'custom_dataclasses_global_seq does not exist';
        END IF;
    END;
$$;