-- Verify database-schema:add-ext-dom-column on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'suspiciousgoogledrivefilepermissions'
          AND
                indexname = 'suspiciousgoogledrivefilepermissions_suspiciousdomains';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.suspiciousgoogledrivefilepermissions_suspiciousdomains does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'suspiciousgoogledrivefilepermissions'
          AND column_name = 'suspiciousdomains';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.suspiciousdomains does not exist';
END IF;
END;
$$;

ROLLBACK;
