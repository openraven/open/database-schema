-- Verify database-schema:add-sus-drive-file-perms-table on pg

BEGIN;

SELECT * FROM suspiciousgoogledrivefilepermissions;
SELECT * FROM suspiciousgoogleshareddrivepermissions;

ROLLBACK;
