-- Verify database-schema:drop-cloudsql-asset-table on pg

BEGIN;

DO
$$
BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'cloudsqlasset';
        IF FOUND THEN
            RAISE EXCEPTION 'cloudsqlasset still exists';
END IF;
END;
$$;

ROLLBACK;
