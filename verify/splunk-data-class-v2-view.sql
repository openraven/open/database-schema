-- Verify database-schema:splunk-data-class-v2-view on pg

BEGIN;

DO $sql$
    DECLARE
sourcecount BIGINT;
    viewcount BIGINT;
BEGIN
SELECT COUNT(*) INTO sourcecount FROM dataclassv2;
SELECT COUNT(*) INTO viewcount FROM v_dataclass;
ASSERT sourcecount = viewcount, 'dataclass view does not show the same data as the raw table';
END;
$sql$;

ROLLBACK;
