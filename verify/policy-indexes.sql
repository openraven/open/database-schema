-- Verify database-schema:policy-indexes on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'assetviolation'
          AND
                indexname = 'assetviolation_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetviolation_assetid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'assetviolation'
          AND
                indexname = 'assetviolation_policyid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetviolation_policyid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'assetviolation'
          AND
                indexname = 'assetviolation_ruleid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetviolation_ruleid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'assetviolation'
          AND
                indexname = 'assetviolation_status';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetviolation_status does not exist';
END IF;
END;
$$;

ROLLBACK;

