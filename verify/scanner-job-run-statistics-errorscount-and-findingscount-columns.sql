-- Verify database-schema:scanner-job-run-statistics-errorscount-and-findingscount-columns on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'errorscount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.errorscount does not exist';
    END IF;

    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'findingscount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.findingscount does not exist';
    END IF;
END;
$$;
