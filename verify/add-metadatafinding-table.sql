-- Verify database-schema:add-metadatafinding-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'metadatafinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadatafinding table does not exist';
END IF;
END;
$$;

ROLLBACK;