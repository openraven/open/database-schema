-- Verify database-schema:add-scan-target-cache-table

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'cached'
          AND table_name = 'scantargetcache';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cached.scantargetcache table does not exist';
END IF;
END;
$$;

ROLLBACK;
