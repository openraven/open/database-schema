-- Verify database-schema:violations-change-stream on pg

BEGIN;

DO $sql$
    DECLARE
        replica_id TEXT;
    BEGIN
        SELECT CASE relreplident
                   WHEN 'd' THEN 'default'
                   WHEN 'n' THEN 'nothing'
                   WHEN 'f' THEN 'full'
                   WHEN 'i' THEN 'index'
                   END AS replica_identity
        FROM pg_class
        INTO replica_id
        WHERE oid = 'assetviolation'::regclass;
        ASSERT replica_id = 'full', 'Replication identity should be full';
    END;
$sql$;

ROLLBACK;
