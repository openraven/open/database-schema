-- Verify database-schema:policy_violation_perf_improvements on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'issuedataclasses';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'issuedataclasses table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema  = 'public'
          AND table_name = 'issues'
          AND column_name = 'violateddataclasses';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'issues is missing the violateddataclasses column';
        END IF;
        PERFORM TRUE
                       FROM   information_schema.columns
                       WHERE  table_schema  = 'public'
                         AND table_name = 'issues'
                         AND column_name = 'affectedaccounts';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'issues is missing the affectedaccounts column';
        END IF;
        PERFORM TRUE
                       FROM   information_schema.columns
                       WHERE  table_schema  = 'public'
                         AND table_name = 'issues'
                         AND column_name = 'affectedregions';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'issues is missing the affectedregions column';
        END IF;
        PERFORM TRUE
                       FROM   information_schema.columns
                       WHERE  table_schema  = 'public'
                         AND table_name = 'issues'
                         AND column_name = 'affectedassetcount';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'issues is missing the affectedassetcoun column';
        END IF;
    END;
$$;

ROLLBACK;
