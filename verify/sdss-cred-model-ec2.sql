-- Verify database-schema:sdss-cred-model-ec2 on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'EC2_SECRETS_MANAGER_PASSWORD') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id EC2_SECRETS_MANAGER_PASSWORD should exist';
END IF;

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'EC2_SECRETS_MANAGER_PASSWORD_IN_JSON') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id EC2_SECRETS_MANAGER_PASSWORD_IN_JSON should exist';
END IF;

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'EC2_PARAMETER_STORE_PASSWORD') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id EC2_PARAMETER_STORE_PASSWORD should exist';
END IF;

END;
$$;

ROLLBACK;
