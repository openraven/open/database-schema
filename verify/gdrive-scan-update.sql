-- Verify database-schema:gdrive-scan-update on pg

BEGIN;

SELECT COUNT(*) FROM v_gdrive_scanfinding_enriched LIMIT 1;

ROLLBACK;
