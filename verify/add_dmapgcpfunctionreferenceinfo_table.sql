-- Verify database-schema:add_dmapgcpfunctionreferenceinfo_table on pg

BEGIN;

    DO $$
        BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'gcpfunctionreferenceinfo';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'gcpfunctionreferenceinfo does not exist';
            END IF;
        END;
    $$;

ROLLBACK;
