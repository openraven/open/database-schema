-- Verify database-schema:delete-dataClassId-from-logAuditScanEvent on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanauditlogevent'
	  AND column_name = 'dataclassid';
	IF FOUND THEN
		RAISE EXCEPTION 'scanauditlogevent.dataclassid should not exist';
END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM   pg_index i
	JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
	WHERE  i.indrelid = 'scanauditlogevent'::regclass
	    AND i.indisprimary and a.attname = 'dataclassid';
    IF FOUND THEN
        RAISE EXCEPTION 'scanauditlogevent.dataclassid should not be of part of the primary key for scanauditlogevent';
END IF;
END;
$$;

DO $$
    DECLARE
primarykeycolumnscount BIGINT;
BEGIN
    SELECT COUNT(*) INTO primarykeycolumnscount FROM pg_index i
    JOIN   pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
    WHERE  i.indrelid = 'scanauditlogevent'::regclass AND i.indisprimary;
    ASSERT primarykeycolumnscount = 6, 'scanauditlogevent has incorrect primary key columns, should be ' ||
    'resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state';
END;
$$;

ROLLBACK;