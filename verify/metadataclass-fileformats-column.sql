-- Verify database-schema:metadataclass-fileformats-column on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'metadataclass'
          AND column_name = 'fileformats'
          AND data_type   = 'jsonb';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadataclass.fileformats does not exist with type jsonb';
END IF;
END;
$$;

ROLLBACK;
