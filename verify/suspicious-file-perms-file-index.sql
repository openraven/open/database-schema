-- Verify database-schema:suspicious-file-perms-file-index on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'suspiciousgoogledrivefilepermissions'
          AND
            indexname = 'suspiciousgoogledrivefilepermissions_fileid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions_fileid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
