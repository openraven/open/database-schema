BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'scantype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.scantype does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'errorcode';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.errorcode does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'errordetails';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.errordetails does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjob'
	  AND column_name = 'scantype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjob.scantype does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjob'
	  AND column_name = 'scanrequest';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjob.scantype does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobpage'
	  AND column_name = 'scantype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobpage.scantype does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'scantype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.scantype does not exist';
    END IF;
END;
$$;


ROLLBACK;
