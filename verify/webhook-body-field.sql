-- Verify database-schema:webhook-body-field on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'webhook_integration_settings'
          AND column_name = 'body';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'webhook_integration_settings.body does not exist';
        END IF;
    END;
$$;

ROLLBACK;
