-- Verify database-schema:dbx-views on pg
-- we're just going to walk though the views, making sure that the counts in the view vs the raw table match up

BEGIN;

-- assets groups
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM assetgroupsv2;
        SELECT COUNT(*) INTO viewcount FROM v_assetgroups;
        ASSERT sourcecount = viewcount, 'asset groups view does not show the same data as the raw table';
    END;
$sql$;


-- policies
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM policies;
        SELECT COUNT(*) INTO viewcount FROM v_policies;
        ASSERT sourcecount = viewcount, 'policies view does not show the same data as the raw table';
    END;
$sql$;

-- rules
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM rules;
        SELECT COUNT(*) INTO viewcount FROM v_rules;
        ASSERT sourcecount = viewcount, 'rules view does not show the same data as the raw table';
    END;
$sql$;

-- policy-rules
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM policy_rule;
        SELECT COUNT(*) INTO viewcount FROM v_policy_rule;
        ASSERT sourcecount = viewcount, 'policy_rule view does not show the same data as the raw table';
    END;
$sql$;

-- data collections
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM datacollection;
        SELECT COUNT(*) INTO viewcount FROM v_datacollection;
        ASSERT sourcecount = viewcount, 'datacollection view does not show the same data as the raw table';
    END;
$sql$;

-- data classes
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM dataclass;
        SELECT COUNT(*) INTO viewcount FROM v_dataclass;
        ASSERT sourcecount = viewcount, 'dataclass view does not show the same data as the raw table';
    END;
$sql$;

-- datacollection_dataclasses
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM datacollection_dataclass;
        SELECT COUNT(*) INTO viewcount FROM v_datacollection_dataclass;
        ASSERT sourcecount = viewcount, 'datacollection_dataclass view does not show the same data as the raw table';
    END;
$sql$;

-- issues
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM issues;
        SELECT COUNT(*) INTO viewcount FROM v_issues;
        ASSERT sourcecount = viewcount, 'issues view does not show the same data as the raw table';
    END;
$sql$;

-- Skipping asset violations as it's a composite view

-- scanner jobs
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM scannerjob;
        SELECT COUNT(*) INTO viewcount FROM v_scannerjob;
        ASSERT sourcecount = viewcount, 'scannerjob view does not show the same data as the raw table';
    END;
$sql$;

-- scanner job runs
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM scannerjobruns;
        SELECT COUNT(*) INTO viewcount FROM v_scannerjobruns;
        ASSERT sourcecount = viewcount, 'scannerjobruns view does not show the same data as the raw table';
    END;
$sql$;

-- scan findings
DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM scanfinding;
        SELECT COUNT(*) INTO viewcount FROM v_scanfinding;
        ASSERT sourcecount = viewcount, 'scanfinding view does not show the same data as the raw table';
    END;
$sql$;

ROLLBACK;
