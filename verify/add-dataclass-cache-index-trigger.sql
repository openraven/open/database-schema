-- Verify: add-dataclass-cache-index-trigger.sql

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.sequences
        WHERE  sequence_schema  = 'public'
          AND sequence_name = 'custom_dataclasses_global_seq';
        IF FOUND THEN
            RAISE EXCEPTION 'custom_dataclasses_global_seq was not removed';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'dataclass_cache_index_trg';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclass_cache_index_trg trigger does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'metadataclass_cache_index_trg';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadataclass_cache_index_trg trigger does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            information_schema.routines
        WHERE
            routine_type = 'FUNCTION'
        AND
            routine_schema = 'public'
        AND routine_name = 'populate_cache_index_func';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'populate_cache_index_func function does not exist';
END IF;
END;
$$;

ROLLBACK;