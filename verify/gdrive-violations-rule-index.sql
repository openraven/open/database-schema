-- Verify database-schema:gdrive-violations-rule-index on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'gdriveviolations'
          AND
            indexname = 'gdriveviolations_gdriveruleid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index gdriveviolations.gdriveviolations_gdriveruleid does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'gdriverules'
          AND
            indexname = 'gdriverules_name_lower';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index gdriverules.gdriverules_name_lower does not exist';
        END IF;
    END;
$$;

ROLLBACK;
