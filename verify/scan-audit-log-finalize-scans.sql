-- Verify database-schema:scan-audit-log-finalize-scans on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'finishedscans'
	  AND column_name = 'scanjobrunid';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'finishedscans.scanjobrunid does not exist';
    END IF;

	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'finishedscans'
	  AND column_name = 'scanjobid';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'finishedscans.scanjobid does not exist';
    END IF;

	PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'finishedscans'
          AND column_name = 'scancompletiontime';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'finishedscans.scancompletiontime does not exist';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'finishedscans'
          AND column_name = 'lastsendtime';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'finishedscans.lastsendtime does not exist';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'finishedscans'
          AND column_name = 'eventssent';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'finishedscans.eventssent does not exist';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'finishedscans'
          AND column_name = 'jvmid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'finishedscans.jvmid does not exist';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'finishedscans'
          AND column_name = 'lastprocessedeventid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'finishedscans.lastprocessedeventid does not exist';
    END IF;

END;
$$;

ROLLBACK;
