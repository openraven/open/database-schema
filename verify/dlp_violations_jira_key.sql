-- Verify database-schema:dlp_violations_jira_key on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriveviolations'
          AND column_name = 'external_key';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations.external_key does not exist';
        END IF;
    END;
$$;

ROLLBACK;
