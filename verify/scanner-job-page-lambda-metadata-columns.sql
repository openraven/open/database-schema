-- Verify database-schema:scanner-job-page-lambda-metadata-columns.sql in pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobpage'
	  AND column_name = 'lambdaregion';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobpage.lambdaregion does not exist';
    END IF;

    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobpage'
	  AND column_name = 'lambdaarchitecture';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobpage.lambdaarchitecture does not exist';
    END IF;

    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobpage'
	  AND column_name = 'lambdamemory';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobpage.lambdamemory does not exist';
    END IF;
END;
$$;
