-- Verify database-schema:alter-table-aggregate-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'aggregatedrelationaldatabasefindings';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedrelationaldatabasefindings table does not exist';
END IF;
END;
$$;

ROLLBACK;
