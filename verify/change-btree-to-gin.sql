-- Verify database-schema:change-btree-to-gin on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_dataclasses_gin';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_gdrive_file_aggregate_data_dataclasses_gin does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_rulesviolated_gin';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'mat_v_gdrive_file_aggregate_data_rulesviolated_gin does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_dataclasses_btree';
        IF FOUND THEN
            RAISE EXCEPTION 'mat_v_gdrive_file_aggregate_data_dataclasses_btree exists and should not';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_rulesviolated_btree';
        IF FOUND THEN
            RAISE EXCEPTION 'mat_v_gdrive_file_aggregate_data_rulesviolated_btree exists and should not';
END IF;
END;
$$;

ROLLBACK;
