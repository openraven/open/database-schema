BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriverules'
          AND column_name = 'ruletype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriverules.ruletype should exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.customFindingLimit') THEN
            RAISE EXCEPTION 'No records found in the table.';
        END IF;
    END;
$$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.personalFindingLimit') THEN
            RAISE EXCEPTION 'No records found in the table.';
        END IF;
    END;
$$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.healthFindingLimit') THEN
            RAISE EXCEPTION 'No records found in the table.';
        END IF;
    END;
$$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.financialFindingLimit') THEN
            RAISE EXCEPTION 'No records found in the table.';
        END IF;
    END;
$$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.devSecretFindingLimit') THEN
            RAISE EXCEPTION 'No records found in the table.';
        END IF;
    END;
$$;

DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'google-workspace.genericFindingLimit') THEN
            RAISE EXCEPTION 'No records found in the table.';
        END IF;
    END;
$$;

ROLLBACK;