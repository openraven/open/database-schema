-- Verify database-schema:add_schema_to_scanauditlogevent on pg

BEGIN;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'scanauditlogevent'
    AND column_name = 'schema';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'scanauditlogevent.schema does not exist';
END IF;
END;
$$;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'sdss_scanauditlogevent'
    AND column_name = 'schema';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'sdss_scanauditlogevent.schema does not exist';
END IF;
END;
$$;

ROLLBACK;
