-- Verify database-schema:issues-rule-index on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'issues'
          AND
            indexname = 'issues_ruleid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index issues.issues_ruleid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
