-- Verify database-schema:scanner-job-run-haserrors-column in pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'haserrors';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.haserrors does not exist';
    END IF;
END;
$$;
