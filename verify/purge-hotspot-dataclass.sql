-- Verify database-schema:purge-hotspot-dataclass on pg
DO $$
DECLARE var_hotspotid uuid;

BEGIN
var_hotspotid := '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';

PERFORM TRUE
FROM   issuedataclasses
WHERE  dataclassid = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table issuedataclasses - it should have been purged';
END IF;

PERFORM TRUE
FROM   scanfinding
WHERE  dataclassid = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table scanfinding - it should have been purged';
END IF;

PERFORM TRUE
FROM   aggregatedassetfindings
WHERE  dataclassid = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table aggregatedassetfindings - it should have been purged';
END IF;

PERFORM TRUE
FROM   aggregatedrelationaldatabasefindings
WHERE  dataclassid = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table aggregatedrelationaldatabasefindings - it should have been purged';
END IF;

PERFORM TRUE
FROM   aggregatedscanfindings
WHERE  dataclassid = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table aggregatedscanfindings - it should have been purged';
END IF;

PERFORM TRUE
FROM   datacollection_dataclass
WHERE  dataclassid = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table datacollection_dataclass - it should have been purged';
END IF;

PERFORM TRUE
FROM   dataclass
WHERE  id = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table dataclass - it should have been purged';
END IF;

PERFORM TRUE
FROM   dataclassv2
WHERE  id = var_hotspotid;
IF FOUND THEN
  RAISE EXCEPTION 'Hotspot data class was found in table dataclassv2 - it should have been purged';
END IF;

END $$;