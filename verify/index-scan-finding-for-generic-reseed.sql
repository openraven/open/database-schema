-- Verify database-schema:index-scan-finding-for-generic-reseed on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index scanfinding.scanfinding_assetid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index scanfinding.scanfinding_dataclassid does not exist';
END IF;
END;
$$;

ROLLBACK;
