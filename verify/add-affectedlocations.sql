-- Verify database-schema:add-affectedlocations on pg

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'issues'
          AND column_name = 'affectedlocationssummary';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'issues.affectedlocationssummary does not exist';
        END IF;
    END;
$$;
