-- Verify database-schema:add-managed-column on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'dataclassv2'
          AND column_name = 'managed';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclassv2.managed does not exist';
END IF;
END;
$$;

ROLLBACK;
