-- Verify database-schema:rename_sdss_scan_target_type on pg

BEGIN;

DO $sql$
    DECLARE
    oldTargetNameCount BIGINT;
BEGIN
    SELECT COUNT(*) INTO oldTargetNameCount FROM scannerjob WHERE scantargettype = 'AWS Structured SQL databases';
    ASSERT oldTargetNameCount = 0, 'AWS Structured SQL databases should be deprecated. Please use Structured Database';
END;
$sql$;

ROLLBACK;
