-- Verify database-schema:rule-severity-ordinals on pg

BEGIN;

SELECT severityordinal FROM rules LIMIT 1;

ROLLBACK;
