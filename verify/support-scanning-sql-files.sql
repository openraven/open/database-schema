-- Verify database-schema:support-scanning-sql-files on pgsql

BEGIN;

DO $$
    BEGIN

        PERFORM TRUE
        FROM scanfiletype
        WHERE mimetype = 'text/x-sql';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'text/x-sql mimetype should be supported';
        END IF;
        PERFORM TRUE
        FROM scanfiletype
        WHERE extensions::jsonb ? 'sql';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sql extension should be supported';
        END IF;

    END;
$$;

ROLLBACK;
