-- Verify database-schema:support-ignore-finding on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'ignored';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.ignored does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'auditlog'
          AND column_name = 'id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.id does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'auditlog'
          AND column_name = 'eventtarget';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.eventtarget does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'auditlog'
          AND column_name = 'eventtype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.eventtype does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'auditlog'
          AND column_name = 'userdescriptor';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.userdescriptor does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'auditlog'
          AND column_name = 'message';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.message does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'auditlog'
          AND column_name = 'eventtimestamp';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.eventtimestamp does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'auditlog'
          AND
                indexname = 'auditlog_eventtimestamp';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.auditlog_eventtimestamp does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'auditlog'
          AND
                indexname = 'auditlog_eventtype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.auditlog_eventtype does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'auditlog'
          AND
                indexname = 'auditlog_user';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'auditlog.auditlog_user does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'auditlog'
          AND
                indexname = 'auditlog_eventtarget';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.aggregatedscanmetrics_account does not exist';
END IF;
END;
$$;

ROLLBACK;
