-- Verify database-schema:add-scan-run-id-col on pg

BEGIN;

SELECT COUNT(scannerjobrunid) FROM googledriveactivitywatermark LIMIT 1;

ROLLBACK;
