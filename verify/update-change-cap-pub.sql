-- Verify database-schema:update-change-cap-pub on pg

BEGIN;

-- We cannot verify this because the base publication is created by debezium and is not present in CI.

ROLLBACK;
