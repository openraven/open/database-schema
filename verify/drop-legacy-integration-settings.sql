-- Verify database-schema:drop-legacy-integration-settings on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'public'
          AND table_name = 'slack_integration_settings';
        IF FOUND THEN
            RAISE EXCEPTION 'table slack_integration_settings still exists';
        END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'public'
          AND table_name = 'webhook_integration_settings';
        IF FOUND THEN
            RAISE EXCEPTION 'table webhook_integration_settings still exists';
        END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'public'
          AND table_name = 'jira_integration_settings';
        IF FOUND THEN
            RAISE EXCEPTION 'table jira_integration_settings still exists';
        END IF;
    END;
$$;

ROLLBACK;
