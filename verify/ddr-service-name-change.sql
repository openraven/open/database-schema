BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_state_store'
	  AND column_name = 'source_log';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_state_store.source_log should exist';
END IF;
END;
$$;
ROLLBACK;
