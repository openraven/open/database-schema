-- Verify database-schema:add_s3specializedscanfindings on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 's3specializeddatascanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 's3specializeddatascanfinding table does not exist';
        END IF;

        PERFORM TRUE
        FROM
            pg_indexes
        WHERE schemaname = 'public'
          AND tablename = 's3specializeddatascanfinding'
          AND indexname = 's3specfinding_assetid_scantarget_scantargetchild_scannerjobid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index s3specfinding_assetid_scantarget_scantargetchild_scannerjobid does not exist';
        END IF;

END;
$$;

ROLLBACK;
