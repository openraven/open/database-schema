-- Verify database-schema:add_scan_target_type_to_audit_event on pg

BEGIN;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'scanauditlogevent'
    AND column_name = 'scantargettype';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'scanauditlogevent.scantargettype does not exist';
  END IF;
END;
$$;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'sdss_scanauditlogevent'
    AND column_name = 'scantargettype';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'sdss_scanauditlogevent.scantargettype does not exist';
  END IF;
END;
$$;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'finishedscans'
    AND column_name = 'scantargettype';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'finishedscans.scantargettype does not exist';
  END IF;
END;
$$;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'sdss_finishedscans'
    AND column_name = 'scantargettype';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'sdss_finishedscans.scantargettype does not exist';
  END IF;
END;
$$;

ROLLBACK;
