BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'ddr_alerts'
	  AND column_name = 'event_parameters';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_alerts.event_parameters should exist';
END IF;
END;
$$;
ROLLBACK;