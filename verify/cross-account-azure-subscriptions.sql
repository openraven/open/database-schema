-- Verify database-schema:cross-account-azure-subscriptions on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'azure_subscriptions'
          AND column_name = 'id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'azure_subscriptions.id does not exist';
        END IF;
    END;
$$;

ROLLBACK;
