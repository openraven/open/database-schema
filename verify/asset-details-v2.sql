-- Verify database-schema:asset-details-v2 on pg

BEGIN;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetdetails'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetdetails.assetid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetdetails'
          AND column_name = 'cloudprovider';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetdetails.cloudprovider does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetdetails'
          AND column_name = 'details';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetdetails.details does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetdetails'
          AND column_name = 'updatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetdetails.updatedat does not exist';
END IF;
END;
$$;


ROLLBACK;
