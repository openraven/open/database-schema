-- Verify database-schema:add-compute-engine-info-tables on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'ec2priceinfo';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'ec2priceinfo table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'ebspriceinfo';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'ebspriceinfo table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'computeengineinfo';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'computeengineinfo table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'ebsvolumeinfo';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'ebsvolumeinfo table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'computeengineduration';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.computeengineduration does not exist';
END IF;

    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'computeenginecost';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.computeenginecost does not exist';
END IF;

END;
$$;

ROLLBACK;