-- Verify database-schema:rm-indexes-on-parent on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_assetid';
        IF FOUND THEN
            RAISE EXCEPTION 'scanfinding.scanfinding_assetid exists but should not be present.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_dataclassid';
        IF FOUND THEN
            RAISE EXCEPTION 'scanfinding.scanfinding_dataclassid exists but should not be present.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_scantarget';
        IF FOUND THEN
            RAISE EXCEPTION 'scanfinding.scanfinding_scantarget exists but should not be present.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_scantargetchild';
        IF FOUND THEN
            RAISE EXCEPTION 'scanfinding.scanfinding_scantargetchild exists but should not be present.';
END IF;
END;
$$;

ROLLBACK;
