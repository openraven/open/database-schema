-- Verify database-schema:remove-sdss-cloudsql-cred-model on pg

BEGIN;

DO $$
BEGIN

IF EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'GC_SECRETS_MANAGER_PASSWORD') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id GC_SECRETS_MANAGER_PASSWORD should not exist';
END IF;

IF EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id GC_IAM_CLOUD_SQL_ACCESS_TOKEN should not exist';
END IF;

END;
$$;

ROLLBACK;