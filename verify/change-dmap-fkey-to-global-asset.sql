-- Verify database-schema:change-dmap-fkey-to-global-asset on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'dmap_assetid_fkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dmap_assetid_fkey does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'dmap_arn_fkey';
        IF FOUND THEN
            RAISE EXCEPTION 'dmap_arn_fkey exists but should not.';
END IF;
END;
$$;

ROLLBACK;
