-- Verify database-schema:workflow-configuration-skeleton on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.id does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.name does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'description';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.description does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'awsaccountnumber';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.awsaccountnumber does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.dataclassid does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'stopprocessing';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.stopprocessing does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'previewmatch';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.previewmatch does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflowconfiguration'
          AND column_name = 'regionid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration.regionid does not exist';
        END IF;
    END;
$$;
ROLLBACK;
