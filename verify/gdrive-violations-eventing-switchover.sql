-- Verify database-schema:gdrive-violations-eventing-switchover on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriveviolations'
          AND column_name = 'reseedid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations.reseedid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
