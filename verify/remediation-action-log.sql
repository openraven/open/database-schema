-- Verify database-schema:remediation-action-log on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'remediationactionlog';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.remediationactionlog does not exist';
        END IF;
    END;
$$;

ROLLBACK;
