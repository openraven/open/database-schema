-- Verify database-schema:add-startedat-and-finishedat-to-finished-scans on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'finishedscans'
	  AND column_name = 'startedat';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'finishedscans.startedat does not exist';
END IF;
END;
$$;
DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'finishedscans'
	  AND column_name = 'finishedat';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'finishedscans.finishedat does not exist';
END IF;
END;
$$;