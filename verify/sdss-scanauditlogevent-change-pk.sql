-- Verify database-schema:sdss-scanauditlogevent-change-pk on pg

BEGIN;

DO $$
    DECLARE
primarykeycolumnscount BIGINT;
BEGIN
    SELECT COUNT(*) INTO primarykeycolumnscount FROM pg_index i
    JOIN   pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
    WHERE  i.indrelid = 'sdss_scanauditlogevent'::regclass AND i.indisprimary;
    ASSERT primarykeycolumnscount = 8, 'sdss_scanauditlogevent has incorrect primary key columns, should be ' ||
    'resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state, schemaname, tablename';
END;
$$;
ROLLBACK;
