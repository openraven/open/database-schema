-- Verify database-schema:add-mime-column on pg

BEGIN;

SELECT COUNT(mimetype) FROM googledrivefiles LIMIT 1;

ROLLBACK;
