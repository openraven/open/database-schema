-- Verify database-schema:add-scan-account-and-region on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'scanaccountandregion';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanaccountandregion table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
