-- Verify database-schema:create-kong-database on pg

BEGIN;
DO $$
BEGIN
    PERFORM TRUE
    FROM pg_catalog.pg_roles
    WHERE rolname = 'kong';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'pg_role "kong" does not exist';
    END IF;

    PERFORM TRUE
    FROM pg_catalog.pg_database
    WHERE datname = 'kong';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'pg_database "kong" does not exist';
    END IF;

    PERFORM TRUE
    FROM pg_catalog.pg_database d
    INNER JOIN pg_catalog.pg_roles r ON (d.datdba = r.oid)
    WHERE datname = 'kong' AND rolname = 'kong';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'pg_database "kong" has wrong owner oid';
    END IF;
END;
$$;
ROLLBACK;
