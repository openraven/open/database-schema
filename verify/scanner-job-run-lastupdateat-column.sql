-- Verify database-schema:scanner-job-run-lastupdateat-column in pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'lastupdateat';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.lastupdateat does not exist';
    END IF;
END;
$$;
