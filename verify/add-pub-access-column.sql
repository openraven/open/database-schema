-- Verify database-schema:add-pub-access-column on pg

BEGIN;

SELECT publiclyaccessible FROM suspiciousgoogledrivefilepermissions LIMIT 1;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'suspiciousgoogledrivefilepermissions'
          AND
                indexname = 'suspiciousgoogledrivefilepermissions_publiclyaccessible';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index suspiciousgoogledrivefilepermissions_publiclyaccessible does not exist';
END IF;
END;
$$;

ROLLBACK;
