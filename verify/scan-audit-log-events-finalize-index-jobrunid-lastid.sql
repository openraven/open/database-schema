-- Verify database-schema:scan-audit-log-events-finalize-index-jobrunid-lastid on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_index i
        JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
        WHERE  i.indrelid = 'scanauditlogevent'::regclass
        AND    i.indisprimary = false and a.attname = 'lastid';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.lastid should be of part of an index for scanauditlogevent';
    END IF;

        PERFORM TRUE
        FROM   pg_index i
        JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
        WHERE  i.indrelid = 'scanauditlogevent'::regclass
        AND    i.indisprimary = false and a.attname = 'scanjobrunid';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.scanjobrunid should be of part of an index for scanauditlogevent';
    END IF;
END;
$$;


ROLLBACK;
