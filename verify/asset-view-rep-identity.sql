-- Verify database-schema:asset-view-rep-identity on pg

BEGIN;

DO $sql$
DECLARE
replica_id TEXT;
BEGIN
SELECT CASE relreplident
           WHEN 'd' THEN 'default'
           WHEN 'n' THEN 'nothing'
           WHEN 'f' THEN 'full'
           WHEN 'i' THEN 'index'
           END AS replica_identity
FROM pg_class
    INTO replica_id
WHERE oid = 'awsassetsview'::regclass;
ASSERT replica_id = 'full', 'Replication identity for awsassetsview should be full';
END;
$sql$;

ROLLBACK;
