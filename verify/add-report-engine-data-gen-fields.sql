-- Verify database-schema:add-report-engine-data-gen-fields on pg

BEGIN;

SELECT operationkey, errorinfo, paused, currentstatestartcount, statechangeddate FROM reportjobrun LIMIT 1;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.schemata
        WHERE  schema_name  = 'reportengine_intermediate_tables';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'reportengine_intermediate_tables schema does not exist';
        END IF;
    END;
$$;

ROLLBACK;
