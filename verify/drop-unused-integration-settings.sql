-- Verify database-schema:drop-unused-integration-settings on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'public'
        AND table_name = 'fourme_integration_settings';
        IF FOUND THEN
            RAISE EXCEPTION 'table fourme_integration_settings still exists';
        END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_schema = 'public'
          AND table_name = 'email_integration_settings';
        IF FOUND THEN
            RAISE EXCEPTION 'table email_integration_settings still exists';
        END IF;
    END;
$$;

ROLLBACK;
