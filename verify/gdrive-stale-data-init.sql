-- Verify database-schema:gdrive-stale-data-init on pg

BEGIN;

SELECT * FROM googledrivestalefiles;
SELECT * FROM googledrivestalenesssettings WHERE id=1;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivestalefiles'
          AND
                indexname = 'googledrivestalefiles_fileid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivestalefiles.googledrivestalefiles_fileid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivestalefiles'
          AND
                indexname = 'googledrivestalefiles_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivestalefiles.googledrivestalefiles_assetid does not exist';
END IF;
END;
$$;

DO $sql$
    DECLARE
count_value INT;
BEGIN
SELECT count(*) FROM googledrivestalenesssettings
    INTO count_value where id = 1;
ASSERT count_value = 1, 'Expected 1 row in staleness settings.';
END;
$sql$;

ROLLBACK;
