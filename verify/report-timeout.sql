-- Verify database-schema:report-timeout on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'reportjobrun'
          AND column_name = 'timedoutstates';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'reportjobrun.timedoutstates does not exist';
        END IF;
    END;
$$;

ROLLBACK;
