-- Verify database-schema:remove-hotspot-dataclass on pg

BEGIN;

DO $$
BEGIN
    PERFORM TRUE
    FROM s3scanfinding
    WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
    IF FOUND THEN
        RAISE EXCEPTION 'removed dataclass id appears in s3scanfinding';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM dataclass
    WHERE id = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
    IF FOUND THEN
        RAISE EXCEPTION 'removed dataclass id appears in dataclass';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM datacollection
    WHERE id in ('02777843-e6ee-44bb-970a-d77cd5cdc1fa', 'ed920696-dccf-4946-9fa4-a2dd8fd97978');
    IF FOUND THEN
        RAISE EXCEPTION 'removed datacollection id appears in datacollection';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM datacollection_dataclass
    WHERE datacollectionid in ('02777843-e6ee-44bb-970a-d77cd5cdc1fa', 'ed920696-dccf-4946-9fa4-a2dd8fd97978');
    IF FOUND THEN
        RAISE EXCEPTION 'removed datacollection id appears in datacollection_dataclass';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM datacollection_dataclass
    WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
    IF FOUND THEN
        RAISE EXCEPTION 'removed dataclass id appears in datacollection_dataclass';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM aggregatedassetfindings
    WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
    IF FOUND THEN
        RAISE EXCEPTION 'removed dataclass id appears in aggregatedassetfindings';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM aggregatedscanfindings
    WHERE dataclassid = '9d7ff59d-25f4-400a-9e14-a0cd19ba79a8';
    IF FOUND THEN
        RAISE EXCEPTION 'removed dataclass id appears in aggregatedscanfindings';
    END IF;
END;
$$;

ROLLBACK;
