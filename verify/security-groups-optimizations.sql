-- Verify database-schema:security-groups-optimizations on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awssecuritygroupconnections'
          AND column_name = 'sgarn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awssecuritygroupconnections.sgarn does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awssecuritygrouppeering'
          AND column_name = 'parentsgarn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awssecuritygrouppeering.parentsgarn does not exist';
END IF;
END;
$$;

ROLLBACK;
