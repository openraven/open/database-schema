-- Verify database-schema:sdss-cloudsql-asset-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'cloudsqlasset';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cloudsqlasset table does not exist';
END IF;
END;
$$;

ROLLBACK;
