-- Verify database-schema:remove_max_lambda_duration_column on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjob'
          AND column_name = 'maxlambdaduration';
        IF FOUND THEN
            RAISE EXCEPTION 'scannerjob.maxlambdaduration exists';
END IF;
END;
$$;

ROLLBACK;
