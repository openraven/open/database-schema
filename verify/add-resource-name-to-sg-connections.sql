-- Verify database-schema:add-resource-name-to-sg-connections on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awssecuritygroupconnections'
          AND column_name = 'resourcename';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awssecuritygroupconnections.resourcename does not exist';
END IF;
END;
$$;

ROLLBACK;
