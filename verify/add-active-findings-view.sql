-- Verify database-schema:PROD-4382 on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_name  = 'v_active_scanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_active_scanfinding does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_name  = 'v_active_s3scanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_active_s3scanfinding does not exist';
END IF;
END;
$$;

ROLLBACK;