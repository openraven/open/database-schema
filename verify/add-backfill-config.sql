-- Verify database-schema:add-backfill-config on pg

BEGIN;

DO $$
BEGIN
        IF NOT EXISTS (SELECT 1 FROM cluster.global_configuration WHERE key = 'dlp.backfill-enable') THEN
            RAISE EXCEPTION 'No records found in the table.';
END IF;
END;
$$;

ROLLBACK;
