-- Verify database-schema:add-containingentity-column on pg

BEGIN;

SELECT containingentity FROM globalassets LIMIT 1;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_containingentity';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_containingentity does not exist';
END IF;
END;
$$;

ROLLBACK;
