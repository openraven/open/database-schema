-- Verify database-schema:add_gdrive_metadata_findings_table on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'gdrivemetadatascanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdrivemetadatascanfinding table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
