BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.table_privileges
    WHERE  grantee = 'orvn_ro'
    AND table_name = 'v_globalassets'
	AND privilege_type = 'SELECT';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'orvn_ro does not have SELECT access to v_globalassets';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.table_privileges
    WHERE  grantee = 'orvn_ro'
    AND table_name = 'v_globalassetscomplete'
	AND privilege_type = 'SELECT';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'orvn_ro does not have SELECT access to v_globalassetscomplete';
END IF;
END;
$$;

ROLLBACK;