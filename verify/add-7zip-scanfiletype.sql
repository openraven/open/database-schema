-- Verify database-schema:add-7zip-scanfiletype on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM scanfiletype
        WHERE mimetype = 'application/x-7z-compressed'
          AND extensions = '["7z"]'
          AND isarchive = true
          AND maxFileSize = 8000;
        IF NOT FOUND THEN
            RAISE EXCEPTION '7zip archives should be supported';
        END IF;
    END;
$$;

ROLLBACK;
