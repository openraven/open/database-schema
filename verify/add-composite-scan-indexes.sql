-- Verify database-schema:add-composite-scan-indexes on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM (SELECT *
              FROM (SELECT COUNT(*) AS cnt
                    FROM pg_indexes
                    WHERE schemaname = 'public'
                      AND indexname LIKE '%_composite_scan_idx') AS indexCount
              WHERE indexCount.cnt = 7) AS idxFound;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'at least one documentId index is missing. does not exist';
        END IF;
    END;
$$;

ROLLBACK;
