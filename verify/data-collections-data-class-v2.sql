-- Verify database-schema:data-collections-data-class-v2 on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datacollection_dataclass'
          AND column_name = 'dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datacollection_dataclass.dataclassid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datacollection_dataclass'
          AND column_name = 'datacollectionid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datacollection_dataclass.datacollectionid does not exist';
END IF;
END;
$$;

ROLLBACK;
