-- Verify database-schema:reseed-database-flag-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'scanfindingaggregatordatabaseversionflag';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfindingaggregatordatabaseversionflag table does not exist';
END IF;
END;
$$;

ROLLBACK;
