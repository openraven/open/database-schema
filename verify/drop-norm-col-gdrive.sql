-- Verify database-schema:drop-norm-col-gdrive on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'googledrivestalefiles'
          AND column_name = 'driveid';
        IF FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.driveid exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'googledrivestalefiles'
          AND column_name = 'filename';
        IF FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.filename exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'googledrivestalefiles'
          AND column_name = 'username';
        IF FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.username exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'googledrivestalefiles'
          AND column_name = 'fileurl';
        IF FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.fileurl exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'suspiciousgoogledrivefilepermissions'
          AND column_name = 'driveid';
        IF FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.driveid exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'suspiciousgoogledrivefilepermissions'
          AND column_name = 'filename';
        IF FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.filename exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'suspiciousgoogledrivefilepermissions'
          AND column_name = 'username';
        IF FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.username exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'suspiciousgoogledrivefilepermissions'
          AND column_name = 'fileurl';
        IF FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.fileurl exists and should not.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'suspiciousgoogledrivefilepermissions'
          AND column_name = 'permissions';
        IF FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.permissions exists and should not.';
END IF;
END;
$$;

ROLLBACK;
