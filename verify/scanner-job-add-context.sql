-- Verify database-schema:scanner-job-add-context.sql

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjob'
          AND column_name = 'context';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjob.context does not exist';
        END IF;
    END;
$$;
