-- Verify database-schema:index-assetid-mapping-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'datastoreresourceaccessmapping'
          AND
                indexname = 'datastoreresourceaccessmapping_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datastoreresourceaccessmapping.datastoreresourceaccessmapping_assetid does not exist.';
END IF;
END;
$$;

ROLLBACK;
