-- Verify database-schema:gdrive-mat-view-2024-08-06 on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_matviews
        WHERE
                pg_matviews.matviewname = 'mat_v_suspicious_user_aggregation';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Mat view mat_v_suspicious_user_aggregation does not exist.';
END IF;
END;
$$;

ROLLBACK;
