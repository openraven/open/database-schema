-- Verify database-schema:adding_documentid_indexes on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            (SELECT *
                FROM
                    (SELECT COUNT( * ) AS cnt
                        FROM
                            pg_indexes
                        WHERE
                            schemaname = 'public'
                            AND (indexname LIKE'idx_aws%_documentId'
                                OR indexname LIKE'idx_gcp%_documentId'
                                OR indexname LIKE'idx_gpc%_documentId' )
                     ) AS indexCount
                WHERE
                    indexCount.cnt = 148
            ) AS idxFound;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'at least one documentId index is missing. does not exist';
        END IF;
    END;
$$;

ROLLBACK;
