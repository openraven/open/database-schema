-- Verify database-schema:add-deriveddatablob-index on pg

BEGIN;

SELECT seq FROM globalassets LIMIT 1;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
            schemaname = 'public'
          AND
            tablename = 'globalassets'
          AND
            indexname = 'globalassets_deriveddatablob_severitycount';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_deriveddatablob_severitycount does not exist';
        END IF;
    END;
$$;

ROLLBACK;
