-- Verify database-schema:report-generation on pg

BEGIN;

SELECT * FROM reportjob LIMIT 1;
SELECT * FROM reportjobrun LIMIT 1;

ROLLBACK;
