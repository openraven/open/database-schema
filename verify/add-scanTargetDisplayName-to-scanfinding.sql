-- Verify database-schema:add-scanTargetDisplayName-to-scanfinding on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanfinding'
	  AND column_name = 'scantargetdisplayname';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanfinding.scantargetdisplayname does not exist';
    END IF;
END;
$$;

ROLLBACK;
