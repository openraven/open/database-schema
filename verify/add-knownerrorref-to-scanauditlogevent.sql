-- Verify database-schema:add-knownerrorref-to-scanauditlogevent on pg


BEGIN;

DO $$
BEGIN

PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanauditlogevent'
	  AND column_name = 'knownerrorref';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanauditlogevent.knownerrorref does not exist';
END IF;
END;
$$;

ROLLBACK;
