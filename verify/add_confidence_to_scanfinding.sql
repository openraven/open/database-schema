-- Verify database-schema:add_confidence_to_scanfinding on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanfinding'
	  AND column_name = 'confidencesum';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanfinding.confidencesum does not exist';
END IF;
END;
$$;
