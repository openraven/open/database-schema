-- Verify database-schema:add-table-aggregate-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'aggregateddatabasefindings';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregateddatabasefindings table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'assetid';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.assetid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'databasename';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.databasename does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'databaseschema';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.databaseschema does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'databasetable';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.databasetable does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'dataclassid';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.dataclassid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'findingscount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.findingscount does not exist';
END IF;
END;
$$;

    DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'updatedat';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.updatedat does not exist';
END IF;
END;
$$;


DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregateddatabasefindings'
	  AND column_name = 'reseededat';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregateddatabasefindings.reseededat does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregateddatabasefindings'
          AND
                indexname = 'aggregateddatabasefindings_pkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregateddatabasefindings.aggregateddatabasefindings_pkey does not exist';
END IF;
END;
$$;

ROLLBACK;
