-- Verify database-schema:drop-file-count-view on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_matviews
        WHERE
                pg_matviews.matviewname = 'mat_v_gdrive_asset_file_count';
        IF FOUND THEN
            RAISE EXCEPTION 'Mat view mat_v_gdrive_asset_file_count exists and should not.';
END IF;
END;
$$;

ROLLBACK;
