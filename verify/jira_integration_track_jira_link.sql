-- Verify database-schema:jira_integration_track_jira_link on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetviolation'
          AND column_name = 'external_link';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetviolation.external_link does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetviolation'
          AND column_name = 'external_key';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetviolation.external_key does not exist';
        END IF;
    END;
$$;

ROLLBACK;
