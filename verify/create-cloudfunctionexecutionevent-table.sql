-- Verify database-schema:create-cloudfunctionexecutionevent-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'cloudfunctionexecutionevent';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cloudfunctionexecutionevent table does not exist';
END IF;
END;
$$;

ROLLBACK;
