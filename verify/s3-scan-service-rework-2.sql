BEGIN; /*
ALTER TABLE awss3bucket ADD CONSTRAINT unique_s3_bucketname UNIQUE (resourcename);

CREATE TABLE scanfinding (
    scannerjobid            UUID NOT NULL,
    scannerjobrunid         UUID NOT NULL,
    assetid                 text references awss3bucket(resourcename),
    scantarget              text NOT NULL,
    scantargetversion       text,
    dataclassid             UUID NOT NULL,
    findingscount           int,
    createdat               timestamp with time zone,
    updatedat               timestamp with time zone,
    PRIMARY KEY (assetid, dataclassid, scantarget)
);
*/
COMMIT;
