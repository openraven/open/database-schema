-- Verify database-schema:add_azurefunctionexecutionevent_table on pg

BEGIN;

DO $$
BEGIN
    PERFORM TRUE
    FROM   information_schema.tables
    WHERE  table_schema  = 'public'
      AND table_name = 'azurefunctionexecutionevent';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'azurefunctionexecutionevent table does not exist';
    END IF;
END;
$$;

ROLLBACK;
