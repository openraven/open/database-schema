-- Verify database-schema:add_objectnamepattern_to_metadataclass on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'metadataclass'
          AND column_name = 'objectnamepattern'
          AND data_type   = 'text';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadataclass.objectnamepattern does not exist with type text';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'metadataclass'
	  AND column_name = 'fileformats';
	IF FOUND THEN
		RAISE EXCEPTION 'metadataclass.fileformats should not exist';
END IF;
END;
$$;

ROLLBACK;
