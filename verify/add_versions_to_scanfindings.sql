-- Verify database-schema:add_versions_to_scanfindings on pg

BEGIN;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'serviceversion'
          AND data_type   = 'bigint';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.serviceversion does not exist with type bigint';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'scannerversion'
          AND data_type   = 'bigint';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.scannerversion does not exist with type bigint';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'falsepositive'
          AND data_type   = 'boolean';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.falsepositive does not exist with type boolean';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'alwaysfalsepositive'
          AND data_type   = 'boolean';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.alwaysfalsepositive does not exist with type boolean';
        END IF;
    END;
$$;
ROLLBACK;
