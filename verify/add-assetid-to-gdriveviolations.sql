-- Verify database-schema:add-assetid-to-gdriveviolations on pg

BEGIN;

SELECT assetid FROM v_gdriveviolations LIMIT 1;

ROLLBACK;
