BEGIN;

DO $$
BEGIN
    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastorecredentialmodels'
          AND column_name = 'id'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastorecredentialmodels.id should exist, be of type text and not nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastorecredentialmodels'
          AND column_name = 'name'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastorecredentialmodels.name should exist, be of type text and not nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastorecredentialmodels'
          AND column_name = 'assettypes'
          AND data_type   = 'jsonb'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastorecredentialmodels.assettypes should exist, be of type jsonb and not nullable';
    END IF;
    

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastorecredentialmodels'
          AND column_name = 'credentialmodel'
          AND data_type   = 'jsonb'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastorecredentialmodels.credentialmodel should exist, be of type jsonb and not nullable';
    END IF;

    -------

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'credentialname'
          AND data_type   = 'text'
          AND is_nullable = 'YES';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.credentialname should exist, be of type text and nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'credentialmodeltype'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.credentialmodeltype should exist, be of type text and not nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'credentialmodel'
          AND data_type   = 'jsonb'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.credentialmodel should exist, be of type jsonb and not nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'secretlabel';
        IF FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.secretlabel should not exist';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'secretversion';
        IF FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.secretversion should not exist';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'passwordsecretlocation';
        IF FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.passwordsecretlocation should not exist';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreaccesscredentials'
          AND column_name = 'username';
        IF FOUND THEN
                RAISE EXCEPTION 'datastoreaccesscredentials.username should not exist';
    END IF;

END;
$$;


ROLLBACK;