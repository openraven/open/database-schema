-- Verify database-schema:fix-report-dates on pg

BEGIN;

-- An appropriate verify cannot be written because verifies are run every deploy
-- while the deploy that migrates the data happens once

ROLLBACK;
