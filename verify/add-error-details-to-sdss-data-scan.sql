
DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'sdss_scannerjobruns'
	  AND column_name = 'errorcode';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'sdss_scannerjobruns.errorcode does not exist';
END IF;
END;
$$;
DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'sdss_scannerjobruns'
	  AND column_name = 'errordetails';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'sdss_scannerjobruns.errordetails does not exist';
END IF;
END;
$$;
