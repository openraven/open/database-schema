-- Verify database-schema:add-gdrivefinding-table on pg

BEGIN;

DO $$
    BEGIN
            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 'gdrivescanfinding';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'gdrivescanfinding table does not exist';
            END IF;
    END;
$$;

ROLLBACK;