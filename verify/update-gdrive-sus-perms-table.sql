-- Verify database-schema:update-gdrive-sus-perms-table on pg

BEGIN;

SELECT assetid FROM suspiciousgoogledrivepermissions LIMIT 1;

ROLLBACK;
