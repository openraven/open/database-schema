-- Verify database-schema:add-data-classv2-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'dataclassv2';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclassv2 table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'dataclass_create';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclass_create trigger does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'metadataclass_create';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'metadataclass_create trigger does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'compositedataclass_create';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass_create trigger does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            information_schema.routines
        WHERE
            routine_type = 'FUNCTION'
        AND
            routine_schema = 'public'
        AND routine_name = 'dataclass_populate_dataclassv2';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclass_populate_dataclassv2 trigger does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            information_schema.routines
        WHERE
            routine_type = 'FUNCTION'
        AND
            routine_schema = 'public'
        AND routine_name = 'dataclassv2_update';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclassv2_update trigger does not exist';
END IF;
END;
$$;

ROLLBACK;