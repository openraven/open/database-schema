-- Verify database-schema:add-trashed-update-index on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_trashed';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivefiles.googledrivefiles_trashed does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_updatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivefiles.googledrivefiles_updatedat does not exist';
END IF;
END;
$$;

ROLLBACK;
