-- Verify database-schema:add-pre-aggregated-file-view on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_matviews
        WHERE
                pg_matviews.matviewname = 'mat_v_gdrive_file_aggregate_data';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Mat view mat_v_gdrive_file_aggregate_data does not exist.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_unique';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index mat_v_gdrive_file_aggregate_data.mat_v_gdrive_file_aggregate_data_unique does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_dataclasses_btree';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index mat_v_gdrive_file_aggregate_data.mat_v_gdrive_file_aggregate_data_dataclasses_btree does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'mat_v_gdrive_file_aggregate_data'
          AND
                indexname = 'mat_v_gdrive_file_aggregate_data_rulesviolated_btree';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index mat_v_gdrive_file_aggregate_data.mat_v_gdrive_file_aggregate_data_rulesviolated_btree does not exist';
END IF;
END;
$$;

ROLLBACK;
