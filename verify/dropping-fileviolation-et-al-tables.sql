-- Verify database-schema:dropping-fileviolation-et-al-tables on pg

BEGIN;

DO
$$
    BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'fileviolation';
        IF FOUND THEN
            RAISE EXCEPTION 'fileviolation still exists';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'fileviolation_opa';
        IF FOUND THEN
            RAISE EXCEPTION 'fileviolation_opa still exists';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'fileviolationdataclass';
        IF FOUND THEN
            RAISE EXCEPTION 'fileviolationdataclass still exists';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'fileviolationdataclass_opa';
        IF FOUND THEN
            RAISE EXCEPTION 'fileviolationdataclass_opa still exists';
        END IF;
    END;
$$;

ROLLBACK;
