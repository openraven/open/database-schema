-- Verify database-schema:add-meta-finding-gdrive on pg

BEGIN;

SELECT * FROM v_gdrive_scanfinding_enriched LIMIT 1;

ROLLBACK;
