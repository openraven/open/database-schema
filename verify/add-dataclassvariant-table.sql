-- Verify database-schema:add-dataclassvariant-table on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'dataclassvariant';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclassvariant table does not exist';
END IF;
END;
$$;

ROLLBACK;
