-- Verify database-schema:aws-ec2subnet on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsec2subnet'
          AND column_name = 'documentid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2subnet.documentid does not exist';
        END IF;

        PERFORM TRUE
        FROM   pg_index i
        JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
        WHERE  i.indrelid = 'awsec2subnet'::regclass
        AND    i.indisprimary = false and a.attname = 'documentid';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'awsec2subnet.documentid should be of part of an index for awsec2subnet';
        END IF;

    END;
$$;

ROLLBACK;
