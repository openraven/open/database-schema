-- Verify database-schema:add-lastsuccessfullyused-column on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'datastoreresourceaccessmapping'
	  AND column_name = 'lastsuccessfullyused';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'datastoreresourceaccessmapping.lastsuccessfullyused does not exist';
END IF;
END;
$$;
