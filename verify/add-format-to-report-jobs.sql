-- Verify database-schema:add-format-to-report-jobs on pg

BEGIN;

SELECT reportformat FROM reportjob LIMIT 1;
SELECT reportformat FROM reportjobrun LIMIT 1;

ROLLBACK;
