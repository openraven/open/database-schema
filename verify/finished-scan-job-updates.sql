-- Verify database-schema:finished-scan-job-updates on pg

BEGIN;
DO $$
    BEGIN
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobpage'
	  AND column_name = 'lambdaduration';
    IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobpage.lambdaduration does not exist';
    END IF;

    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'finishedscans'
	  AND column_name = 'duration';
    IF NOT FOUND THEN
		RAISE EXCEPTION 'finishedscans.duration does not exist';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'finishedscans'
          AND column_name = 'reason';
    IF NOT FOUND THEN
            RAISE EXCEPTION 'finishedscans.reason does not exist';
    END IF;
    END;
$$;
ROLLBACK;
