-- Verify database-schema:add-compositescanfinding on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'compositescanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositescanfinding table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositescanfinding'
          AND
                indexname = 'compositescanfinding_dataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositescanfinding_dataclass does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositescanfinding'
          AND
                indexname = 'compositescanfinding_scantarget_scantargetchild';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositescanfinding_scantarget_scantargetchild does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositescanfinding'
          AND
                indexname = 'compositescanfinding_assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositescanfinding_assetid does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositescanfinding'
          AND
                indexname = 'compositescanfinding_assetid_scantarget_scantargetch_scannerjob';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositescanfinding_assetid_scantarget_scantargetch_scannerjob does not exist';
        END IF;
    END;
$$;


ROLLBACK;
