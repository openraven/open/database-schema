BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'awsstaledatametrics'
	  AND column_name = 'arn';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'awsstaledatametrics.arn should exist';
END IF;
END;
$$;

ROLLBACK;