-- Verify database-schema:error-details-to-jsonb-type on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjobruns'
          AND column_name = 'errordetails'
          AND data_type   = 'jsonb';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjobruns.errordetails should exist and be of type jsonb';
END IF;

END;
$$;

ROLLBACK;
