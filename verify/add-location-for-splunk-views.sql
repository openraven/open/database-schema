-- Verify database-schema:add-location-for-splunk-views on pg

BEGIN;

DO $$
BEGIN
    PERFORM TRUE
    FROM   information_schema.tables
    WHERE  table_name  = 'v_scanfinding';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'v_scanfinding does not exist';
END IF;
END;
$$;

    DO $$
BEGIN
    PERFORM TRUE
    FROM   information_schema.tables
    WHERE  table_name  = 'v_assetviolation';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'v_assetviolation does not exist';
END IF;
END;
$$;

ROLLBACK;
