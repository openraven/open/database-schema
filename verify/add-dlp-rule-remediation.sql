-- Verify database-schema:add_dataclassversion_to_scanfindings on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'gdriverules'
          AND column_name = 'remediation'
          AND data_type   = 'text';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriverules.remediation does not exist with type text';
END IF;
END;
$$;

ROLLBACK;
