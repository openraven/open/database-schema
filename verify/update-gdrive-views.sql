-- Verify database-schema:update-gdrive-views on pg

BEGIN;

SELECT drivename,restrictions FROM v_suspiciousgoogledrivefilepermissions;
SELECT restrictions FROM v_suspiciousgoogleshareddrivepermissions;
SELECT drivename,restrictions FROM v_googledrivestalefiles;

ROLLBACK;
