-- Verify database-schema:add-finished-at-to-scanner-job-run on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobruns'
	  AND column_name = 'finishedat';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobruns.finishedat does not exist';
END IF;
END;
$$;
