-- Verify database-schema:gdrive-mat-view-2024-02-14 on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_attribute
        WHERE
                attrelid = (SELECT oid FROM pg_class WHERE relname = 'mat_v_suspicious_user_aggregation') AND attname = 'filescannedcount' AND NOT attisdropped;
        IF NOT FOUND THEN
            RAISE EXCEPTION 'New column on view does not exist: mat_v_suspicious_user_aggregation.filescannedcount';
END IF;
END;
$$;


ROLLBACK;
