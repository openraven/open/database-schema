-- Verify database-schema:add-implementation-type-column-scanfinding on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'implementationtype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.implementationtype does not exist';
END IF;
END;
$$;

ROLLBACK;
