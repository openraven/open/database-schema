-- Verify database-schema:redshift-credentialsmodel on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM datastorecredentialmodels
        WHERE (id = 'SECRETS_MANAGER_PASSWORD' OR
               id = 'SECRETS_MANAGER_PASSWORD_IN_JSON' OR
               id = 'PARAMETER_STORE_PASSWORD') AND
            (NOT assettypes ? 'AWS::Redshift::Cluster');

        IF FOUND THEN
            RAISE EXCEPTION 'AWS::Redshift::Cluster does not exist for all the needed credentialmodels';
        END IF;
    END;
$$;

ROLLBACK;
