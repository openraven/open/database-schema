-- Verify database-schema:add-findingscount-to-scan-audit-log-event on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanauditlogevent'
	  AND column_name = 'findingscount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanauditlogevent.findingscount does not exist';
END IF;
END;
$$;
