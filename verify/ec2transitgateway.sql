-- Verify database-schema:ec2transitgateway on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsec2transitgateway'
          AND column_name = 'documentid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2transitgateway.documentid does not exist';
        END IF;

        PERFORM TRUE
        FROM   pg_index i
        JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
        WHERE  i.indrelid = 'awsec2transitgateway'::regclass
        AND    i.indisprimary = false and a.attname = 'documentid';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'awsec2transitgateway.documentid should be of part of an index for awsec2transitgateway';
        END IF;

    END;
$$;

ROLLBACK;
