-- Verify database-schema:change-credential-report-and-ssm-table-inheritance on pg


BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'extendawsiamusercredentialreport';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'extendawsiamusercredentialreport table does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'extendawsssminstance';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'extendawsssminstance table does not exist';
END IF;
END;
$$;

ROLLBACK;

