-- Verify database-schema:add-audit-log-sequence on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.sequences
        WHERE  sequence_name = 'audit_log_event_sequence';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'audit_log_event_sequence does not exist';
        END IF;
    END;
$$;

ROLLBACK;
