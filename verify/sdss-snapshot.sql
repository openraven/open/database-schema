-- Verify database-schema:sdss-snapshot on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_snapshot';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_snapshot table does not exist';
END IF;
END;
$$;

ROLLBACK;
