-- Verify database-schema:add-invalid-files-to-scan-statistics on pg

BEGIN;

DO $$
BEGIN

PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'invalidfilecount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.invalidfilecount does not exist';
    END IF;
END;
$$;

ROLLBACK;

