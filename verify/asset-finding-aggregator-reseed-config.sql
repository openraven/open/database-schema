-- Verify database-schema:asset-finding-aggregator-reseed-config on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfindingaggregatorreseedconfig'
          AND column_name = 'reseedtimestamp';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfindingaggregatorreseedconfig.reseedtimestamp does not exist';
END IF;
END;
$$;

DO $sql$
    DECLARE
    count_value INT;
    BEGIN
    SELECT count(*) FROM scanfindingaggregatorreseedconfig
        INTO count_value where reseedtimestamp is NULL;
    ASSERT count_value = 1, 'Inserted timestamp should be NULL.';
    END;
$sql$;

ROLLBACK;
