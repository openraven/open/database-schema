-- Verify database-schema:fix-not-null-asset-violation on pg

BEGIN;

-- This is difficult to verify since NOT NULL does not show up in the pg_constraint table

ROLLBACK;
