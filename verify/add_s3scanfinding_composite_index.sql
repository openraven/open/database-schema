-- Verify database-schema:add_s3scanfinding_composite_index on pg
BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 's3scanfinding'
          AND
                indexname = 's3scanfinding_assetid_scantarget_scantargetchild_scannerjobid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 's3scanfinding.s3scanfinding_assetid_scantarget_scantargetchild_scannerjobid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
