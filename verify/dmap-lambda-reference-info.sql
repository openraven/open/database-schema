-- Verify database-schema:dmap-lambda-reference-info on pg

BEGIN;

INSERT INTO dmaplambdareferenceinfo(lambdaref, createdat, scantype, survivalcount, schedulerid, jobrunid, region, accountid, functionname)
VALUES ('testRef', current_timestamp, 'EC2', 0, 1, '1', 'region', 'accountid','my-function');

ROLLBACK;