-- Verify database-schema:file-list-indices on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_mimetype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivefiles.googledrivefiles_mimetype does not exist.';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_filename';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivefiles.googledrivefiles_filename does not exist.';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'gdrivescanfinding'
          AND
                indexname = 'gdrivescanfinding_dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdrivescanfinding.gdrivescanfinding_dataclassid does not exist.';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'suspiciousgoogledrivefilepermissions'
          AND
                indexname = 'suspiciousemails_fts';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.suspiciousemails_fts does not exist.';
END IF;
END;
$$;

ROLLBACK;
