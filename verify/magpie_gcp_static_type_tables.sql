-- Verify database-schema:magpie_gcp_static_type_tables on pg

BEGIN;


DO $$
    DECLARE
        table_names text[];
        table_count_expected int;
        table_count_found int;

    BEGIN
        PERFORM true;
        table_names := array[
            'gcpbigqueryreservation',
            'gcpbigqueryreservationcapacity',
            'gcpbigtableinstance',
            'extendawsiamusercredentialreport',
            'extendawsssminstance',
            'gcpaccess',
            'gcpasset',
            'gcpassetfeed',
            'gcpautomldataset',
            'gcpautomlmodel',
            'gcpbigquerydataset',
            'gcpbigquerydatatransfer',
            'gcpbillingaccount',
            'gcpcloudbuild',
            'gcpcloudbuildtrigger',
            'gcpcluster',
            'gcpcomputedisk',
            'gcpcomputeinstance',
            'gcpcontaineranalysisnote',
            'gcpcontaineranalysisoccurrence',
            'gcpdatacatalog',
            'gcpdatalabelig',
            'gcpdatalabelingannotations',
            'gcpdatalabelinginstruction',
            'gcpdialogflowconversation',
            'gcpdlpjob',
            'gcpdlpjobtrigger',
            'gcpdnszone',
            'gcperrorreporting',
            'gcpgameservice',
            'gcpiamrole',
            'gcpiamserviceaccount',
            'gcpiotdeviceregistry',
            'gcpkmskeyring',
            'gcploggingbucket',
            'gcploggingexclusion',
            'gcploggingmetric',
            'gcploggingsink',
            'gcpmemcacheinstance',
            'gcpmonitoringalertpolicy',
            'gcpmonitoringdashboard',
            'gcpmonitoringgroup',
            'gcpmonitoringservice',
            'gcposconfigpatchdeployment',
            'gcposconfigpatchjob',
            'gcpprojectinfo',
            'gcppubsublitesubscription',
            'gcppubsublitetopic',
            'gcppubsubschema',
            'gcppubsubsnapshots',
            'gcppubsubsubscription',
            'gcppubsubtopic',
            'gcprecaptchaenterprisekey',
            'gcpredisinstance',
            'gcpresourcemanagerfolder',
            'gcpresourcemanagerorganization',
            'gcpresourcemanagerproject',
            'gcpschedulerjob',
            'gcpsecretmanagersecret',
            'gcpsecurityscanconfig',
            'gcpservicediscoveryservice',
            'gcpspannerinstance',
            'gcpsqlinstance',
            'gcpstoragebucket',
            'gcptalenttenant',
            'gcptaskqueue',
            'gcptrace',
            'gcptranslateglossary',
            'gcpvisionproduct',
            'gcpvisionproductset',
            'gcpvpcfirewall',
            'gcpvpcnetwork',
            'gpcdataproccluster',
            'gpcdataprocjob',
            'gpcfunction',
            'orvnshadowaccount'
            ];
        table_count_expected := array_length(table_names, 1);
        SELECT INTO table_count_found COUNT(*)
        FROM   information_schema.tables
        WHERE  table_name = ANY(table_names);
        IF table_count_expected != table_count_found THEN
            RAISE EXCEPTION 'Not all tables created %:%' ,table_count_expected, table_count_found;
        END IF;
    END;
$$;

ROLLBACK;
