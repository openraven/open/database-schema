-- Verify database-schema:new_automations_schema_fixes on pg

BEGIN;

DO
$$
    BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'automation_rule';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.automation_rule does not exist';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_schema = 'public'
          AND table_name = 'automation_consequence';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'public.automation_consequence does not exist';
        END IF;
    END;
$$;

ROLLBACK;
