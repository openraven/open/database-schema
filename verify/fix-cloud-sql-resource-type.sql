-- Verify database-schema:fix-cloud-sql-resource-type on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM datastorecredentialmodels
        WHERE id='GC_SECRETS_MANAGER_PASSWORD' AND assettypes = '["GCP::SQL::Instance"]';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Cloud SQL type not updated for GC_SECRETS_MANAGER_PASSWORD';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM datastorecredentialmodels
        WHERE id='GC_IAM_CLOUD_SQL_ACCESS_TOKEN' AND assettypes = '["GCP::SQL::Instance"]' AND name = 'IAM access token for GCP Cloud SQL';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Cloud SQL type and name not updated for GC_IAM_CLOUD_SQL_ACCESS_TOKEN';
        END IF;
    END;
$$;

ROLLBACK;