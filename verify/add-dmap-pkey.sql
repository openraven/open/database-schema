-- Verify database-schema:add-dmap-pkey on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE contype = 'p'
          AND conname = 'dmap_pkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclassv2 table does not exist';
END IF;
END;
$$;

ROLLBACK;
