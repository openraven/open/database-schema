-- Verify database-schema:magpie_aws_static_type_tables on pg

BEGIN;


DO
$$
    DECLARE
        table_names          text[];
        table_count_expected int;
        table_count_found    int;

    BEGIN
        PERFORM true;
        table_names := array ['awsbackupbackupplan',
            'awscloudtrailtrail',
            'awsconfigurationrecorder',
            'awsec2snapshot',
            'awsguarddutydetector',
            'awsiamaccount',
            'awsiamcredentialsreport',
            'awslocationgeofencecollection',
            'awslocationmap',
            'awslocationplaceindex',
            'awslocationroutecalculator',
            'awslocationtracker',
            'awsrdsdbsnapshot',
            'awssecurityhubstandardsubscription',
            'awssnssubscription',
            'awssnstopic',
            'awswatchalarm',
            'awswatchdashboard',
            'awswatchlogsmetricfilter',
            'extendawsiamusercredentialreport',
            'extendawsssminstance',
            'awswatchloggroup'
            ];
        table_count_expected := array_length(table_names, 1);
        SELECT INTO table_count_found COUNT(*)
        FROM information_schema.tables
        WHERE table_name = ANY (table_names);
        IF table_count_expected != table_count_found THEN
            RAISE EXCEPTION 'Not all tables created %:%' ,table_count_expected, table_count_found;
        END IF;
    END;
$$;

ROLLBACK;
