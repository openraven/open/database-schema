-- Verify database-schema:scan-audit-log-events-varchar-text on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'statusreason'
	  AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.statusreason should be of type text';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'reportedmimetype'
          AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.reportedmimetype should be of type text';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'detectedmimetype'
          AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.detectedmimetype should be of type text';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'etag'
          AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.etag should be of type text';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'resourceid'
          AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.resourceid should be of type text';
    END IF;

END;
$$;

ROLLBACK;
