-- Verify database-schema:track-created-ec2-instances on pg

BEGIN;

do $$
begin
	perform true
	from   information_schema.columns
	where  table_name  = 'sdss_snapshot_vpc'
	  and column_name = 'has_created_db_resources';
	if not found then
		raise exception 'sdss_snapshot_vpc.has_created_db_resources does not exist';
end if;
end;
$$;

do $$
    begin
        perform true
        from   information_schema.tables
        where  table_schema  = 'public'
          and table_name = 'scannerjobruns_with_ec2';
        if not found then
            raise exception 'scannerjobruns_with_ec2 table does not exist';
        end if;
    end;
$$;


ROLLBACK;
