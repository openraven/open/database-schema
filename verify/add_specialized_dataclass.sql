-- Verify database-schema:add_specialized_dataclass on pg

BEGIN;

DO $$
BEGIN
    PERFORM TRUE
    FROM   information_schema.tables
    WHERE  table_schema  = 'public'
      AND table_name = 'specializeddataclass';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'specializeddataclass table does not exist';
    END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM   information_schema.triggers
    WHERE  trigger_schema  = 'public'
      AND trigger_name = 'specializeddataclass_create';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'specializeddataclass_create trigger does not exist';
    END IF;
END;
$$;

ROLLBACK;
