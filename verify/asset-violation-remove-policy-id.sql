-- Verify database-schema:asset-violation-remove-policy-id on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetviolations_removepolicyid'
          AND column_name = 'policyid';
        IF FOUND THEN
            RAISE EXCEPTION 'assetviolations_removepolicyid.policyid should not exist';
        END IF;
    END;
$$;

ROLLBACK;
