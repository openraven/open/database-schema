-- Verify database-schema:scan-audit-log-events-primary-key-update on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_index i
	JOIN   pg_attribute a ON a.attrelid = i.indrelid
        AND a.attnum = ANY(i.indkey)
	WHERE  i.indrelid = 'scanauditlogevent'::regclass
	AND    i.indisprimary and a.attname = 'dataclassid';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.dataclassid should be of part of the primary key for scanauditlogevent';
    END IF;

END;
$$;

ROLLBACK;
