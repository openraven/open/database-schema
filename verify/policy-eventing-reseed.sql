-- Verify database-schema:policy-eventing-reseed on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'reseed_job_run';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'reseed_job_run table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'reseed_work_unit';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'reseed_work_unit table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'gdriveviolations_eventing';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'gdriveviolations_eventing table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
