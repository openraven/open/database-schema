-- Verify database-schema:rework-reseed on pg

BEGIN;

    DO $$
    BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedassetfindings'
              AND column_name = 'reseededat';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'aggregatedassetfindings.reseededat does not exist';
    END IF;
    END;
    $$;

ROLLBACK;
