-- Verify database-schema:s3-scan-service-updatedat-remediation to pg

BEGIN;

DO $$
    DECLARE
        table_names text[];
        table_count_expected int;
        table_count_found int;

    BEGIN
        PERFORM true;
        table_names := array['metadataclass', 'datacollection_metadataclass'];
        table_count_expected := array_length(table_names, 1);
    SELECT INTO table_count_found COUNT(*)
    FROM   information_schema.tables
    WHERE   table_name = ANY(table_names);
    IF table_count_expected != table_count_found THEN
                RAISE EXCEPTION 'Not all tables created %:%' ,table_count_expected, table_count_found;
    END IF;
    END;
$$;

ROLLBACK;