-- Verify database-schema:integrations-status-fields on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'webhook_integration_settings'
          AND column_name = 'status';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'webhook_integration_settings.status does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'webhook_integration_settings'
          AND column_name = 'status_message';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'webhook_integration_settings.status_message does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'slack_integration_settings'
          AND column_name = 'status';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'slack_integration_settings.status does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'slack_integration_settings'
          AND column_name = 'status_message';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'slack_integration_settings.status_message does not exist';
        END IF;
    END;
$$;

ROLLBACK;
