-- Verify database-schema:aws-security-group-meta-processor-init on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsec2securitygroupconnectionmetadata'
          AND column_name = 'arn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2securitygroupconnectionmetadata.arn does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'awsec2securitygroupconnectionmetadata'
          AND
                indexname = 'awsec2securitygroupconnectionmetadata_region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2securitygroupconnectionmetadata.awsec2securitygroupconnectionmetadata_region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'awsec2securitygroupconnectionmetadata'
          AND
                indexname = 'awsec2securitygroupconnectionmetadata_lastupdated';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2securitygroupconnectionmetadata.awsec2securitygroupconnectionmetadata_lastupdated does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'awsec2securitygroupconnectionmetadata'
          AND
                indexname = 'awsec2securitygroupconnectionmetadata_haspeering';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2securitygroupconnectionmetadata.awsec2securitygroupconnectionmetadata_haspeering does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'awsec2securitygroupconnectionmetadata'
          AND
                indexname = 'awsec2securitygroupconnectionmetadata_hasexternalconnections';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsec2securitygroupconnectionmetadata.awsec2securitygroupconnectionmetadata_hasexternalconnections does not exist';
END IF;
END;
$$;

ROLLBACK;
