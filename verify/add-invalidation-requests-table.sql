-- Verify database-schema:add-invalidation-requests-table

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'cached'
          AND table_name = 'invalidationrequests';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'cached.invalidationrequests table does not exist';
END IF;
END;
$$;

ROLLBACK;
