-- Verify database-schema:workflow-configuration-schema on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflow_configuration'
          AND column_name = 'id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflow_configuration.id does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflow_configuration'
          AND column_name = 'name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflow_configuration.name does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'workflow_configuration'
          AND column_name = 'description';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'workflow_configuration.description does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.id does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'workflow_id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.workflow_id does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.name does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'description';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.description does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'salience';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.salience does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'constraints';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.constraints does not exist';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'consequences';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.consequences does not exist';
        END IF;
    END;
$$;

DO
$$
    BEGIN
        PERFORM TRUE
        FROM information_schema.table_constraints
        WHERE table_name = 'rule_configuration'
          AND constraint_name = 'fk_workflow';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'fk_workflow does not exist';
        END IF;
    END
$$;

ROLLBACK;
