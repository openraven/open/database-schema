-- Verify database-schema:add_last_scan_state_to_scanner_job on pg

BEGIN;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjob'
          AND column_name = 'lastscanjobrunid'
          AND data_type   = 'uuid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjob.lastScanJobRunId does not exists with type uuid';
        END IF;
    END;
$$;
DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.table_constraints
        WHERE  table_name  = 'scannerjob'
          AND constraint_name = 'lastscanjobrunid_fkey'
          AND constraint_type = 'FOREIGN KEY';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjob.lastScanJobRunId does not exists with type uuid';
        END IF;
    END;
$$;
ROLLBACK;
