-- Verify database-schema:automations-rule-enablement on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'enabled';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.enabled does not exist';
        END IF;
    END;
$$;

ROLLBACK;
