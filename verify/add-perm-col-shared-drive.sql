-- Verify database-schema:add-perm-col-shared-drive on pg

BEGIN;

SELECT COUNT(permissions) FROM suspiciousgoogleshareddrivepermissions LIMIT 1;

ROLLBACK;
