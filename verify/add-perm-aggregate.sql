-- Verify database-schema:add-perm-aggregate on pg

BEGIN;

SELECT COUNT(*) FROM assetfilepermissionaggregate;

ROLLBACK;
