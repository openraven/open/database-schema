-- Verify database-schema:rm-unused-columns on pg

BEGIN;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedassetfindings'
              AND column_name = 'region';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedassetfindings.region exists but should have been dropped.';
END IF;
END;
    $$;

        DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedassetfindings'
              AND column_name = 'account';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedassetfindings.account exists but should have been dropped.';
END IF;
END;
    $$;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedassetfindings'
              AND column_name = 'assettype';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedassetfindings.assettype exists but should have been dropped.';
END IF;
END;
    $$;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedassetfindings'
              AND column_name = 'source';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedassetfindings.source exists but should have been dropped.';
END IF;
END;
    $$;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedscanfindings'
              AND column_name = 'region';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedscanfindings.region exists but should have been dropped.';
END IF;
END;
    $$;

        DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedscanfindings'
              AND column_name = 'account';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedscanfindings.account exists but should have been dropped.';
END IF;
END;
    $$;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedscanfindings'
              AND column_name = 'assettype';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedscanfindings.assettype exists but should have been dropped.';
END IF;
END;
    $$;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'aggregatedscanfindings'
              AND column_name = 'source';
            IF FOUND THEN
                RAISE EXCEPTION 'aggregatedscanfindings.source exists but should have been dropped.';
END IF;
END;
    $$;
ROLLBACK;
