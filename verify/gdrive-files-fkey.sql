-- Verify database-schema:gdrive-files-fkey on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'fk_suspiciousgoogledrivefilepermissions_assetid_fileid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'fk_suspiciousgoogledrivefilepermissions_assetid_fileid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'fk_googledrivestalefiles_assetid_fileid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'fk_googledrivestalefiles_assetid_fileid does not exist';
END IF;
END;
$$;

ROLLBACK;
