-- Verify database-schema:update-gdrive-dash-dcoll on pg

BEGIN;

SELECT COUNT(1) FROM v_gdrive_dash LIMIT 1;

ROLLBACK;
