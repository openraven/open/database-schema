-- Verify database-schema:add-tsm-system-rows-extension on pg

BEGIN;

DO $$
BEGIN
        PERFORM true
        FROM pg_extension
		WHERE extname = 'tsm_system_rows';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'tsm_systems_rows extension does not exist';
END IF;
END;
$$;


ROLLBACK;