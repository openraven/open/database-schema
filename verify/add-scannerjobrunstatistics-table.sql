-- Verify database-schema:add-scannerjobrunstatistics-table on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'scannerjobrunstatistics';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjobrunstatistics table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
