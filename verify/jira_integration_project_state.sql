-- Verify database-schema:jira_integration_project_state on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'jira_label';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_label table does not exist';
        END IF;
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'jira_priority';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_priority table does not exist';
        END IF;
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'jira_project';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_project table does not exist';
        END IF;
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'jira_user';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_user table does not exist';
        END IF;
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'jira_field';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'jira_field table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
