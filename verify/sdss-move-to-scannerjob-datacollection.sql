-- Verify database-schema:sdss-move-to-scannerjob-datacollection on pg

BEGIN;

DO $$
BEGIN

IF EXISTS (SELECT * FROM scannerjob WHERE datacollections is null and scantype = 'STRUCTURED') THEN
    RAISE EXCEPTION 'There should not be null datacollection values';
END IF;


END;
$$;

ROLLBACK;
