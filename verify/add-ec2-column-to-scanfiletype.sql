-- Verify database-schema:add-ec2-column-to-scanfiletype on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfiletype'
          AND column_name = 'maxec2filesize';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfiletype.maxec2filesize does not exist';
        END IF;
    END;
$$;

ROLLBACK;
