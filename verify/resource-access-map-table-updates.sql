-- Verify database-schema:resource-access-map-table-updates on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'datastoreresourceaccessmapping'
          AND column_name = 'status';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'datastoreresourceaccessmapping.status does not exist';
END IF;
END;
$$;

DO $sql$
DECLARE
replica_id TEXT;
BEGIN
SELECT CASE relreplident
           WHEN 'd' THEN 'default'
           WHEN 'n' THEN 'nothing'
           WHEN 'f' THEN 'full'
           WHEN 'i' THEN 'index'
           END AS replica_identity
FROM pg_class
    INTO replica_id
WHERE oid = 'datastoreresourceaccessmapping'::regclass;
ASSERT replica_id = 'full', 'Replication identity for datastoreresourceaccessmapping should be full';
END;
$sql$;

ROLLBACK;
