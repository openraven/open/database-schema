-- Verify database-schema:drop-s3objects-cache to pg

BEGIN;

DO
$$
    BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 's3objects';
        IF FOUND THEN
            RAISE EXCEPTION 'cached.s3objects still exists';
        END IF;

        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'template_cached_s3objects';
        IF FOUND THEN
            RAISE EXCEPTION 'cached.template_cached_s3objects still exists';
        END IF;

        PERFORM TRUE
        FROM partman.part_config
        WHERE parent_table = 'cached.s3objects';
        IF FOUND THEN
            RAISE EXCEPTION 'cached.s3objects is not removed from partman.part_config';
        END IF;
    END;
$$;

ROLLBACK;