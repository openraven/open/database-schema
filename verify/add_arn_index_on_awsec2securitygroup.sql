-- Verify database-schema:add_arn_index_on_awsec2securitygroup on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'awsec2securitygroup'
          AND
                indexname = 'awsec2securitygroup_arn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index awsec2securitygroup.awsec2securitygroup_arn does not exist';
        END IF;
    END;
$$;

ROLLBACK;
