-- Verify database-schema:add-addt-info-col-gdrive on pg

BEGIN;

SELECT trashed,filesizebytes FROM googledrivefiles LIMIT 1;

ROLLBACK;
