BEGIN; /*
ALTER TABLE awss3bucket ADD CONSTRAINT awss3bucket_arn_unique UNIQUE (arn);

ALTER TABLE scanfinding DROP CONSTRAINT scanfinding_pkey;
ALTER TABLE scanfinding DROP CONSTRAINT scanfinding_assetid_fkey;

CREATE TABLE IF NOT EXISTS s3scanfinding () INHERITS (scanfinding);
ALTER TABLE s3scanfinding ADD CONSTRAINT scanfinding_assetid_fkey FOREIGN KEY(assetid) REFERENCES awss3bucket(arn);
ALTER TABLE s3scanfinding ADD PRIMARY KEY (assetid, dataclassid, scantarget);
*/
COMMIT;
