-- Verify database-schema:PROD-427 on pg

BEGIN;

DO $sql$
    DECLARE
        ct BIGINT;
    BEGIN
        SET ROLE = orvn_ro;
        SELECT COUNT(1) INTO ct FROM aws;
        ASSERT ct >= 0, 'should have allowed selection from aws';
    END;
$sql$;

DO $sql$
    BEGIN
        SET ROLE = orvn_dmap;
        SELECT COUNT(1) FROM slack_integration_settings;
        ASSERT TRUE, 'should NOT have allowed selection from slack_integration_settings';
    EXCEPTION
        WHEN OTHERS THEN NULL;
    END;
$sql$;

ROLLBACK;
