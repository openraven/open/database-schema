-- Verify database-schema:remove-info-from-v-assetviolation on pg

BEGIN;

-- v_assetviolation is a composit view, so not going to check the counts are the same
-- across tables

ROLLBACK;
