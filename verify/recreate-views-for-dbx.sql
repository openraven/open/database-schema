-- Verify database-schema:recreate-views-for-dbx on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.views
        WHERE  table_name = 'v_assetviolation';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_assetviolation does not exist';
END IF;
END
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.views
        WHERE  table_name = 'v_issues';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_issues does not exist';
END IF;
END
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.views
        WHERE  table_name = 'v_policies';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_policies does not exist';
END IF;
END
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.views
        WHERE  table_name = 'v_policy_rule';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_policy_rule does not exist';
END IF;
END
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.views
        WHERE  table_name = 'v_rules';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_rules does not exist';
END IF;
END
$$;



ROLLBACK;

