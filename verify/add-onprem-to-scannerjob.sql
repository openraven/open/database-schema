-- Verify database-schema:add-onprem-to-scannerjob on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scannerjob'
          AND column_name = 'onprem';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scannerjob.onprem does not exist';
END IF;
END;
$$;

ROLLBACK;