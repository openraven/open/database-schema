-- Verify database-schema:add-resourcetype-to-scanner-job on pg
BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjob'
	  AND column_name = 'resourcetype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjob.resourcetype does not exist';
END IF;

END;
$$;


ROLLBACK;
