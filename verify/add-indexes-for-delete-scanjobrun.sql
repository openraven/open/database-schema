-- Verify database-schema:add-indexes-for-delete-scanjobrun on pg

BEGIN;

DO $$
BEGIN
    PERFORM TRUE
    FROM
        pg_indexes
    WHERE
            schemaname = 'public'
      AND
            tablename = 'scannerjob'
      AND
            indexname = 'scannerjob_lastscannerjobrun_index';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Index scannerjob_lastscannerjobrun_index does not exist';
END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM
        pg_indexes
    WHERE
            schemaname = 'public'
      AND
            tablename = 'scannerjobpage'
      AND
            indexname = 'scannerjobpage_jobrunid_index';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Index scannerjobpage_jobrunid_index does not exist';
END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM
        pg_indexes
    WHERE
            schemaname = 'public'
      AND
            tablename = 'scanfinding'
      AND
            indexname = 'scanfinding_scannerjobrunid_index';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Index scanfinding_scannerjobrunid_index does not exist';
END IF;
END;
$$;

DO $$
BEGIN
    PERFORM TRUE
    FROM
        pg_indexes
    WHERE
            schemaname = 'public'
      AND
            tablename = 'scanfinding'
      AND
            indexname = 'scanfinding_scannerjobid_index';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Index scanfinding_scannerjobid_index does not exist';
END IF;
END;
$$;

ROLLBACK;
