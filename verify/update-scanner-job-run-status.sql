-- Verify database-schema:scan-audit-log-events-primary-key-update on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM scannerjob sj
        LEFT JOIN scannerjobruns sjr ON sj.lastscanjobrunid = sjr.id
        WHERE sj.status = 'ENABLED'
          AND sj.runoncejob = true
          AND sj.lastscanjobrunid = sjr.id
          AND sjr.status = 'COMPLETED';
        IF FOUND THEN
                RAISE EXCEPTION 'scannerjob.status should be DISABLED for scanjobrun.status COMPLETED';
    END IF;

END;
$$;

ROLLBACK;
