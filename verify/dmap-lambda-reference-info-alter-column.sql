-- Verify database-schema:dmap-lambda-reference-info-alter-column on pg

BEGIN;

INSERT INTO dmaplambdareferenceinfo(lambdaref, createdat, scantype, survivalcount, schedulerid, jobrunid, region, accountid, functionname)
VALUES ('testRef', current_timestamp, 'EC2', 0, 1, null, 'region', 'accountid','my-function');

ROLLBACK;
