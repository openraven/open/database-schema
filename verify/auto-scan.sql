-- Verify database-schema:auto-scan on pg

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'autoscan'
          AND column_name = 'id';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'autoscan.id does not exist';
        END IF;
    END;
$$;
