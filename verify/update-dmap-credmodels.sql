-- Verify database-schema:update-dmap-credmodels on pg

BEGIN;

DO $$
BEGIN
        PERFORM true
        FROM datastorecredentialmodels
        WHERE id='EC2_SECRETS_MANAGER_PASSWORD'
        AND credentialmodel='{"arn": "ARN to Secrets Manager where the password is held", "port": "Database port (required)", "user": "The username to connect to"}';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'EC2_SECRETS_MANAGER_PASSWORD does not exist with correct model.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM true
        FROM datastorecredentialmodels
        WHERE id='EC2_SECRETS_MANAGER_PASSWORD_IN_JSON'
        AND credentialmodel='{"arn": "ARN to Secrets Manager where the password is held", "port": "Database port (required)", "user": "The username to connect to", "secretKey": "Name of JSON field where password is stored"}';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'EC2_SECRETS_MANAGER_PASSWORD_IN_JSON does not exist with correct model.';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM true
        FROM datastorecredentialmodels
        WHERE id='EC2_PARAMETER_STORE_PASSWORD'
        AND credentialmodel='{"port": "Database port (required)", "user": "The username to connect to (required)", "region": "Region in which parameter is held", "parameterName": "Parameter name (required)"}';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'EC2_PARAMETER_STORE_PASSWORD does not exist with correct model.';
END IF;
END;
$$;

ROLLBACK;
