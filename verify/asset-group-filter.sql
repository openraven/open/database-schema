-- Verify database-schema:asset-group-filter on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetgrouptoassets'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetgrouptoassets.assetid does not exist';
END IF;
END;
$$;

ROLLBACK;
