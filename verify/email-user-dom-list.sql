-- Verify database-schema:email-user-dom-list on pg

BEGIN;

SELECT emaildomains FROM v_gdrive_dash LIMIT 1;
SELECT emailusers FROM v_gdrive_dash LIMIT 1;
SELECT emaildomains FROM v_suspiciousgoogleshareddrivepermissions LIMIT 1;
SELECT emailusers FROM v_suspiciousgoogleshareddrivepermissions LIMIT 1;
SELECT emaildomains FROM v_suspiciousgoogledrivefilepermissions LIMIT 1;
SELECT emailusers FROM v_suspiciousgoogledrivefilepermissions LIMIT 1;

ROLLBACK;
