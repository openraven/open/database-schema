-- Verify database-schema:add-object-create-update-time-to-scanfinding on pg

BEGIN;
DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'objectcreated'
          AND data_type   = 'timestamp with time zone';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.objectcreated does not exist with type timestamp with time zone';
END IF;
END;
$$;
DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanfinding'
          AND column_name = 'objectlastupdated'
          AND data_type   = 'timestamp with time zone';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanfinding.objectlastupdated does not exist with type timestamp with time zone';
END IF;
END;
$$;
ROLLBACK;