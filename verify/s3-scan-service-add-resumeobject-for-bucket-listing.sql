-- Verify database-schema:s3-scan-service-add-resumeobject-for-bucket-listing on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'resumeobject';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.resumeobject does not exist';
END IF;
END;
$$;