-- Verify database-schema:set-impl-type-on-findings on pg

BEGIN;

DO $sql$
    DECLARE
    nullcount BIGINT;
BEGIN
    SELECT COUNT(*) INTO nullcount FROM compositescanfinding WHERE implementationtype IS NULL;
    ASSERT nullcount = 0, 'All scanfindings in compositescanfinding should have implementationtype set.';
END;
$sql$;

DO $sql$
    DECLARE
nullcount BIGINT;
BEGIN
SELECT COUNT(*) INTO nullcount FROM sdss_scanfinding WHERE implementationtype IS NULL;
ASSERT nullcount = 0, 'All scanfindings in sdss_scanfinding should have implementationtype set.';
END;
$sql$;

DO $sql$
    DECLARE
nullcount BIGINT;
BEGIN
SELECT COUNT(*) INTO nullcount FROM s3scanfinding WHERE implementationtype IS NULL;
ASSERT nullcount = 0, 'All scanfindings in s3scanfinding should have implementationtype set.';
END;
$sql$;

DO $sql$
    DECLARE
nullcount BIGINT;
BEGIN
SELECT COUNT(*) INTO nullcount FROM cloudstoragescanfinding WHERE implementationtype IS NULL;
ASSERT nullcount = 0, 'All scanfindings in cloudstoragescanfinding should have implementationtype set.';
END;
$sql$;

DO $sql$
    DECLARE
nullcount BIGINT;
BEGIN
SELECT COUNT(*) INTO nullcount FROM s3metadatascanfinding WHERE implementationtype IS NULL;
ASSERT nullcount = 0, 'All scanfindings in s3metadatascanfinding should have implementationtype set.';
END;
$sql$;


DO $sql$
    DECLARE
nullcount BIGINT;
BEGIN
SELECT COUNT(*) INTO nullcount FROM cloudstoragemetadatascanfinding WHERE implementationtype IS NULL;
ASSERT nullcount = 0, 'All scanfindings in cloudstoragemetadatascanfinding should have implementationtype set.';
END;
$sql$;

ROLLBACK;
