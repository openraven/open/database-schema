-- Verify database-schema:add-share-col-gdrive on pg

BEGIN;

SELECT fileissharedtouser, sharinguser FROM googledrivefiles LIMIT 1;

ROLLBACK;
