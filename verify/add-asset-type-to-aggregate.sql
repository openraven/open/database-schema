-- Verify database-schema:add-asset-type-to-aggregate on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregatedassetfindings'
	  AND column_name = 'assettype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregatedassetfindings.assettype does not exist';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'aggregatedscanfindings'
	  AND column_name = 'assettype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'aggregatedscanfindings.assettype does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetfindings'
          AND
                indexname = 'aggregatedassetfindings_assettype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.aggregatedassetfindings_assettype does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanfindings'
          AND
                indexname = 'aggregatedscanfindings_assettype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.aggregatedscanfindings_assettype does not exist';
END IF;
END;
$$;

ROLLBACK;
