-- Verify database-schema:add-updated-col-dataclassv2 on pg

BEGIN;

    DO $$
BEGIN
            PERFORM TRUE
            FROM   information_schema.columns
            WHERE  table_name  = 'dataclassv2'
              AND column_name = 'updatedat';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'dataclassv2.updatedat does not exist';
END IF;
END;
    $$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'dataclassv2'
          AND
                indexname = 'dataclassv2_name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dataclassv2.dataclassv2_name does not exist.';
END IF;
END;
$$;


ROLLBACK;
