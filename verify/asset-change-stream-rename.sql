-- Verify database-schema:asset-change-stream-rename on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.assetid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.dataclassid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'findingscount';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.findingscount does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.account does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'updatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.updatedat does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.assetid does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.dataclassid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'findingscount';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.findingscount does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.account does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'updatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.updatedat does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'scannerjobid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.scannerjobid does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetfindings'
          AND
                indexname = 'aggregatedassetfindings_region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.aggregatedassetfindings_region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetfindings'
          AND
                indexname = 'aggregatedassetfindings_dataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.aggregatedassetfindings_dataclass does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetfindings'
          AND
                indexname = 'aggregatedassetfindings_account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetfindings.aggregatedassetfindings_account does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanfindings'
          AND
                indexname = 'aggregatedscanfindings_updateat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.aggregatedscanfindings_updateat does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanfindings'
          AND
                indexname = 'aggregatedscanfindings_region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.aggregatedscanfindings_region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanfindings'
          AND
                indexname = 'aggregatedscanfindings_dataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.aggregatedscanfindings_dataclass does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanfindings'
          AND
                indexname = 'aggregatedscanfindings_account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanfindings.aggregatedscanfindings_account does not exist';
END IF;
END;
$$;

ROLLBACK;
