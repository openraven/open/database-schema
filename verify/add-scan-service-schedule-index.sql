-- Verify.

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scannerjob'
          AND
                indexname = 'scan_type_target_type_status_idx';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index scannerjob.scan_type_target_type_status_idx does not exist';
END IF;
END;
$$;

ROLLBACK;

