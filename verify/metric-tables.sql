-- Verify database-schema:metric-tables on pg

BEGIN;

select * from violationaggregate limit 1;
select * from resourcetypeaggregate limit 1;
select * from s3aggregate limit 1;

ROLLBACK;
