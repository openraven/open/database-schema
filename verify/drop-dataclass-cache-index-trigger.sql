-- Verify: drop-dataclass-cache-index-trigger.sql

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'dataclass_cache_index_trg';
        IF FOUND THEN
            RAISE EXCEPTION 'dataclass_cache_index_trg trigger was not removed';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.triggers
        WHERE  trigger_schema  = 'public'
          AND trigger_name = 'metadataclass_cache_index_trg';
        IF FOUND THEN
            RAISE EXCEPTION 'metadataclass_cache_index_trg was not removed';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            information_schema.routines
        WHERE
            routine_type = 'FUNCTION'
        AND
            routine_schema = 'public'
        AND routine_name = 'populate_cache_index_func';
        IF FOUND THEN
            RAISE EXCEPTION 'populate_cache_index_func function was not removed';
END IF;
END;
$$;

ROLLBACK;