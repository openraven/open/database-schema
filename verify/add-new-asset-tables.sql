-- Verify database-schema:add-new-asset-tables on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'globalassets';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'globalassets table does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'globalassetslist';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'globalassetslist table does not exist';
END IF;
END;
$$;

DO $sql$
        DECLARE
replica_id TEXT;
BEGIN
SELECT CASE relreplident
           WHEN 'd' THEN 'default'
           WHEN 'n' THEN 'nothing'
           WHEN 'f' THEN 'full'
           WHEN 'i' THEN 'index'
           END AS replica_identity
FROM pg_class
    INTO replica_id
WHERE oid = 'globalassets'::regclass;
ASSERT replica_id = 'full', 'Replication identity should be full';
END;
$sql$;
    
    
DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_namelabel_gin_trgm_idx';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_namelabel_gin_trgm_idx does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_resourcetype';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_resourcetype does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_resourcetypelabel';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_resourcetypelabel does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_category';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_category does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_storageinbytes';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_storageinbytes does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_resourcetypesize';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_resourcetypesize does not exist';
END IF;
END;
$$;
    
    
DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_updatediso';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_updatediso does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_source';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_source does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_locationblob';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_locationblob does not exist';
END IF;
END;
$$;
    
    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_externalidentifiers';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_externalidentifiers does not exist';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_markedfordelete';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_markedfordelete does not exist';
END IF;
END;
$$;

        DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_dataclassmap';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_dataclassmap does not exist';
END IF;
END;
$$;

        DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_datacollectionmap';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_datacollectionmap does not exist';
END IF;
END;
$$;

    DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassetslist'
          AND
                indexname = 'globalassetslist_seq';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassetslist.seq does not exist';
END IF;
END;
$$; 

ROLLBACK;
