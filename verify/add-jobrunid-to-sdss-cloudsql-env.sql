-- Verify database-schema:add-jobrunid-to-sdss-cloudsql-env on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'sdss_cloudsql_env'
          AND column_name = 'jobrunid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_cloudsql_env.jobrunid does not exist';
END IF;
END;
$$;

ROLLBACK;