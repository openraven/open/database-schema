BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_name  = 'v_active_compositescanfinding';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'v_active_compositescanfinding does not exist';
        END IF;
    END;
$$;

ROLLBACK;