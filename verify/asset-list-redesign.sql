-- Verify database-schema:asset-list-redesign on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'assetsview'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'assetsview.assetid does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsassetsview'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsassetsview.assetid does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsbackupplantags'
          AND column_name = 'backupplanarn';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsbackupplantags.backupplanarn does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awsbackupjobsassetsview'
          AND column_name = 'backupjobid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awsbackupjobsassetsview.backupjobid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
