-- Verify database-schema:redshift_iam_auth_update on pg

BEGIN;

DO $$
BEGIN

IF NOT EXISTS (SELECT FROM datastorecredentialmodels d WHERE d.id = 'IAM_REDSHIFT_ACCESS_PASSWORD'
                AND credentialmodel = '{"user": "The username to connect to (required)"}') THEN
    RAISE EXCEPTION 'credentialsmodeltype row with id IAM_REDSHIFT_ACCESS_PASSWORD should exist and have user as a mandatory field';
END IF;

END;
$$;

ROLLBACK;
