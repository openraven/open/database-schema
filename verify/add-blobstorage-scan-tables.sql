-- Verify database-schema:add-blobstoragefinding-table on pg

BEGIN;

DO $$
    BEGIN
            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 'blobstoragescanfinding';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'blobstoragescanfinding table does not exist';
            END IF;

            PERFORM TRUE
            FROM   information_schema.tables
            WHERE  table_schema  = 'public'
              AND table_name = 'blobstoragemetadatascanfinding';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'blobstoragemetadatafinding table does not exist';
            END IF;
    END;
$$;

ROLLBACK;