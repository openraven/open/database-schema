-- Verify database-schema:add-col-last-scan-ext on pg

BEGIN;

SELECT fileisexternal,lastsuccessfulscan FROM googledrivefiles LIMIT 1;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_fileisexternal';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivefiles.googledrivefiles_fileisexternal does not exist.';
END IF;
END;
$$;

ROLLBACK;
