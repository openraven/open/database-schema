-- Verify database-schema:add_scan_target_type on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjob'
	  AND column_name = 'scantargettype';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjob.scantargettype does not exist';
END IF;

END;
$$;

ROLLBACK;
