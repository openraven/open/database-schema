BEGIN;

DO $$
BEGIN
    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'sdss_scanfinding'
          AND column_name = 'schemaname'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'sdss_scanfinding.schemaname should exist, be of type text and not nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'sdss_scanfinding'
          AND column_name = 'tablename'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'sdss_scanfinding.tablename should exist, be of type text and not nullable';
    END IF;

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'sdss_scanfinding'
          AND column_name = 'columnname'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'sdss_scanfinding.columnname should exist, be of type text and not nullable';
    END IF;
    

    PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'metadatatables'
          AND column_name = 'schemaname'
          AND data_type   = 'text'
          AND is_nullable = 'NO';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'metadatatables.schemaname should exist, be of type text and not nullable';
    END IF;

END;
$$;


ROLLBACK;