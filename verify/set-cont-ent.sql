-- Verify database-schema:set-cont-ent on pg

BEGIN;

DO $sql$
DECLARE
    mismatchCount BIGINT;
BEGIN
    SELECT COUNT(*) INTO mismatchCount FROM accounttoaccountalias ataa, globalassets WHERE containingentity != accountalias AND globalassets.locationblob->>'accountId' = ataa.accountid;
    ASSERT mismatchCount = 0, 'No mismatches should be detected.';
END;
$sql$;

ROLLBACK;
