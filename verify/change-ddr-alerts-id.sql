DO $$
BEGIN
    PERFORM TRUE
       FROM   information_schema.columns
      WHERE  table_name  = 'ddr_alerts'
        AND column_name = 'id'
        AND data_type   = 'uuid';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'ddr_alerts.id is not a UUID';
    END IF;
END;
$$;
