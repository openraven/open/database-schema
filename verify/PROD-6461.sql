-- Verify database-schema:remove-workflow-tables on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'event_type';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.event_type does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'workflowconfiguration';
        IF FOUND THEN
            RAISE EXCEPTION 'workflowconfiguration table was not dropped';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM information_schema.tables
        WHERE table_name = 'workflow_configuration';
        IF FOUND THEN
            RAISE EXCEPTION 'workflow_configuration table was not dropped';
        END IF;
    END;
$$;

ROLLBACK;
