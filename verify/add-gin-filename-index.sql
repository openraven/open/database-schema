-- Verify database-schema:add-gin-filename-index on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_filename_gin';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivefiles.googledrivefiles_filename_gin does not exist';
END IF;
END;
$$;

ROLLBACK;
