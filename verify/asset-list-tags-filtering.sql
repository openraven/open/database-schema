-- Verify database-schema:assest-list-tags-filtering on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'awstags'
          AND column_name = 'tagkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'awstags.tagkey does not exist';
        END IF;
    END;
$$;

ROLLBACK;
