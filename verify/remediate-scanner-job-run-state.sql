-- Verify database-schema:remediate-scanner-job-run-state on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM scannerjobruns sjr
        WHERE sjr.status = 'DISABLED';
        IF FOUND THEN
                RAISE EXCEPTION 'scannerjobruns.status should be CREATED instead of DISABLED';
    END IF;

END;
$$;