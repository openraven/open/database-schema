-- Verify database-schema:index-finding-for-flagging on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_scantarget';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index scanfinding.scanfinding_scantarget does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'scanfinding'
          AND
                indexname = 'scanfinding_scantargetchild';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index scanfinding.scanfinding_scantargetchild does not exist';
END IF;
END;
$$;

ROLLBACK;
