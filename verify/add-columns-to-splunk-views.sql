-- Verify database-schema:add-columns-to-splunk-views on pg

BEGIN;

DO $sql$
    DECLARE
        sourcecount BIGINT;
        viewcount BIGINT;
    BEGIN
        SELECT COUNT(*) INTO sourcecount FROM scanfinding;
        SELECT COUNT(*) INTO viewcount FROM v_scanfinding;
        ASSERT sourcecount = viewcount, 'scanfinding view does not show the same data as the raw table';
    END;
$sql$;

ROLLBACK;
