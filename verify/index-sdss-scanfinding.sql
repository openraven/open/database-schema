-- Verify database-schema:index-sdss-scanfinding on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'sdss_scanfinding'
          AND
                indexname = 'sdss_scanfinding_tablename';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index sdss_scanfinding.sdss_scanfinding_tablename does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'sdss_scanfinding'
          AND
                indexname = 'sdss_scanfinding_schemaname';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index sdss_scanfinding.sdss_scanfinding_schemaname does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'sdss_scanfinding'
          AND
                indexname = 'sdss_scanfinding_columnname';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index sdss_scanfinding.sdss_scanfinding_columnname does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'sdss_scanfinding'
          AND
                indexname = 'sdss_scanfinding_scannerjobrunid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index sdss_scanfinding.sdss_scanfinding_scannerjobrunid does not exist';
END IF;
END;
$$;

ROLLBACK;
