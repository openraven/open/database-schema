-- Verify database-schema:automations_forensics on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name = 'rule_configuration'
          AND column_name = 'created_at';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.created_at does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name = 'rule_configuration'
          AND column_name = 'updated_at';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.updated_at does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'created_by';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.created_by does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'rule_configuration'
          AND column_name = 'updated_by';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'rule_configuration.updated_by does not exist';
        END IF;
    END;
$$;

ROLLBACK;
