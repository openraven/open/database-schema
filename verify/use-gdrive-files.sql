-- Verify database-schema:use-gdrive-files on pg

BEGIN;

SELECT * FROM v_gdriveviolations LIMIT 1;
SELECT * FROM v_gdrive_scanfinding_enriched LIMIT 1;
SELECT * FROM v_gdrive_dash LIMIT 1;

ROLLBACK;
