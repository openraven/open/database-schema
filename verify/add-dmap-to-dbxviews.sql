-- Verify database-schema:add-dmap-to-dbxviews on pg

BEGIN;

DO $sql$
    DECLARE
    sourcecount BIGINT;
    viewcount BIGINT;
BEGIN
SELECT COUNT(*) INTO sourcecount FROM dmap;
SELECT COUNT(*) INTO viewcount FROM v_dmap;
ASSERT sourcecount = viewcount, 'dmap view does not show the same data as the raw table';
END;
$sql$;

ROLLBACK;
