BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name = 'ddr_alerts'
	  AND column_name = 'alert_rule_id'
	  AND   data_type = 'uuid';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_alerts.alert_rule_id should be of type uuid';
END IF;
END;
$$;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name = 'ddr_rules'
	  AND column_name = 'criteria';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ddr_rules.criteria should exist';
END IF;
END;
$$;
ROLLBACK;