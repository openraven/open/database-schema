-- Verify database-schema:add-policy-view-indices on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'suspiciousgoogledrivefilepermissions'
          AND
                indexname = 'suspiciousgoogledrivefilepermissions_sharedbylink';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'suspiciousgoogledrivefilepermissions.suspiciousgoogledrivefilepermissions_sharedbylink does not exist.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivestalefiles'
          AND
                indexname = 'googledrivestalefiles_fileaccessedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.googledrivestalefiles_fileaccessedat does not exist.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivestalefiles'
          AND
                indexname = 'googledrivestalefiles_filecreatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.googledrivestalefiles_filecreatedat does not exist.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivestalefiles'
          AND
                indexname = 'googledrivestalefiles_fileupdatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivestalefiles.googledrivestalefiles_fileupdatedat does not exist.';
END IF;
END;
$$;

ROLLBACK;
