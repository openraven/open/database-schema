-- Verify database-schema:add_scantargetid_to_scanauditlogevent_table on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'scantargetid'
          AND data_type   = 'text';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'scanauditlogevent.scantargetid does not exists with type text';
        END IF;
    END;
$$;

ROLLBACK;
