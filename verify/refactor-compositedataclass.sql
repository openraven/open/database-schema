-- Verify database-schema:refactor-compositedataclass on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'compositedataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass table does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositedataclass'
          AND
                indexname = 'compositedataclass_name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass_name does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositedataclass'
          AND
                indexname = 'compositedataclass_status';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass_status does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositedataclass'
          AND
                indexname = 'compositedataclass_dirty';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass_dirty does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'compositedataclass'
          AND
                indexname = 'compositedataclass_name';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass_managed does not exist';
        END IF;
    END;
$$;


ROLLBACK;
