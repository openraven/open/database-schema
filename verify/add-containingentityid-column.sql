-- Verify database-schema:add-containingentityid-column on pg

BEGIN;

SELECT containingentityid FROM globalassets LIMIT 1;

DO $$
    BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'globalassets'
          AND
                indexname = 'globalassets_containingentityid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index globalassets.globalassets_containingentityid does not exist';
        END IF;
    END;
$$;

ROLLBACK;
