-- Verify database-schema:PROD-2603 on pg

BEGIN;

DO $sql$
    DECLARE
        createRole BOOLEAN;
        createDb BOOLEAN;
    BEGIN
        SELECT rolcreaterole, rolcreatedb into createRole, createDb
        FROM pg_catalog.pg_roles
        WHERE rolcreaterole = true;
        ASSERT createRole, 'should have rolcreaterole permission';
        ASSERT createDb, 'shold have rolcreatedb permission';
    END;
$sql$;
ROLLBACK;
