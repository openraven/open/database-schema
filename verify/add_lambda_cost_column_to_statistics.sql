-- Verify database-schema:add_lambda_cost_column_to_statistics on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scannerjobrunstatistics'
	  AND column_name = 'functioncost';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scannerjobrunstatistics.functioncost does not exist';
END IF;
END;
$$;

ROLLBACK;
