-- Verify database-schema:add-auto-err-log on pg

BEGIN;

SELECT * FROM automations_error_audit LIMIT 1;

ROLLBACK;
