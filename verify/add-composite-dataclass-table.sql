-- Verify database-schema:add-composite-dataclass-table on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'compositedataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'compositedataclass table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
