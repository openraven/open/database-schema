-- Verify database-schema:snowflake_cred_models on pg

BEGIN;

    DO $$
    BEGIN
            PERFORM true
            FROM datastorecredentialmodels
            WHERE id='SECRETS_MANAGER_PASSWORD'
              AND assettypes='["AWS::RDS::DBInstance", "AWS::Redshift::Cluster", "SNOWFLAKE::Account"]';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'SECRETS_MANAGER_PASSWORD does not exist with correct model.';
    END IF;
    END;
    $$;

    DO $$
    BEGIN
            PERFORM true
            FROM datastorecredentialmodels
            WHERE id='SECRETS_MANAGER_PASSWORD_IN_JSON'
              AND assettypes='["AWS::RDS::DBInstance", "AWS::Redshift::Cluster", "SNOWFLAKE::Account"]';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'SECRETS_MANAGER_PASSWORD_IN_JSON does not exist with correct model.';
    END IF;
    END;
    $$;

    DO $$
    BEGIN
            PERFORM true
            FROM datastorecredentialmodels
            WHERE id='PARAMETER_STORE_PASSWORD'
              AND assettypes='["AWS::RDS::DBInstance", "AWS::Redshift::Cluster", "SNOWFLAKE::Account"]';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'PARAMETER_STORE_PASSWORD does not exist with correct model.';
    END IF;
    END;
    $$;

    DO $$
    BEGIN
            PERFORM true
            FROM datastorecredentialmodels
            WHERE id='SECRETS_MANAGER_PRIVATE_KEY'
              AND assettypes='["SNOWFLAKE::Account"]';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'SECRETS_MANAGER_PRIVATE_KEY does not exist with correct model.';
    END IF;
    END;
    $$;

ROLLBACK;
