-- Verify database-schema:set-columns-to-be-dropped-nullable on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'region'
          AND is_nullable = 'YES';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Nullable column aggregatedassetfindings.region not found.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetfindings'
          AND column_name = 'account'
          AND is_nullable = 'YES';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Nullable column aggregatedassetfindings.account not found.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'region'
          AND is_nullable = 'YES';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Nullable column aggregatedscanfindings.region not found.';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanfindings'
          AND column_name = 'account'
          AND is_nullable = 'YES';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Nullable column aggregatedscanfindings.account not found.';
END IF;
END;
$$;

ROLLBACK;
