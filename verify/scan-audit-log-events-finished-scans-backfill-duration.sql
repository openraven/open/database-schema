-- Verify database-schema:scan-audit-log-events-finished-scans-backfill-duration on pg
BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM finishedscans 
        WHERE duration is null;
        IF FOUND THEN
                RAISE EXCEPTION 'finishedscans.duration should not contain null values';
    END IF;

END;
$$;

ROLLBACK;
