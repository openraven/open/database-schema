-- Verify database-schema:remediation-action-log-fixes on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'remediationactionlog'
          AND column_name = 'exceptionstacktrace';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'remediationactionlog.exceptionstacktrace does not exist';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'remediationactionlog'
          AND column_name = 'exceptionstack_trace';
        IF FOUND THEN
            RAISE EXCEPTION 'remediationactionlog.exceptionstack_trace still exists';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'remediationactionlog'
          AND column_name = 'violationid';
        IF FOUND THEN
            RAISE EXCEPTION 'remediationactionlog.violationid still exists';
        END IF;
    END;
$$;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'remediationactionlog'
          AND column_name = 'violationtable';
        IF FOUND THEN
            RAISE EXCEPTION 'remediationactionlog.violationtable still exists';
        END IF;
    END;
$$;

ROLLBACK;
