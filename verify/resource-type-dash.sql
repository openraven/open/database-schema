-- Verify database-schema:resource-type-dash on pg

BEGIN;

SELECT resourcetype FROM v_gdrive_dash LIMIT 1;

ROLLBACK;
