-- Verify database-schema:asset-change-stream-init on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetmetrics'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.assetid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetmetrics'
          AND column_name = 'dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.dataclassid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetmetrics'
          AND column_name = 'findingscount';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.findingscount does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetmetrics'
          AND column_name = 'region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetmetrics'
          AND column_name = 'account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.account does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedassetmetrics'
          AND column_name = 'updatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.updatedat does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'assetid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.assetid does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'dataclassid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.dataclassid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'findingscount';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.findingscount does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.account does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'updatedat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.updatedat does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'aggregatedscanmetrics'
          AND column_name = 'scannerjobid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.scannerjobid does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetmetrics'
          AND
                indexname = 'aggregatedassetmetrics_region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.aggregatedassetmetrics_region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetmetrics'
          AND
                indexname = 'aggregatedassetmetrics_dataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.aggregatedassetmetrics_dataclass does not exist';
END IF;
END;
$$;


DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedassetmetrics'
          AND
                indexname = 'aggregatedassetmetrics_account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedassetmetrics.aggregatedassetmetrics_account does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanmetrics'
          AND
                indexname = 'aggregatedscanmetrics_updateat';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.aggregatedscanmetrics_updateat does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanmetrics'
          AND
                indexname = 'aggregatedscanmetrics_region';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.aggregatedscanmetrics_region does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanmetrics'
          AND
                indexname = 'aggregatedscanmetrics_dataclass';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.aggregatedscanmetrics_dataclass does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'aggregatedscanmetrics'
          AND
                indexname = 'aggregatedscanmetrics_account';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'aggregatedscanmetrics.aggregatedscanmetrics_account does not exist';
END IF;
END;
$$;

DO $sql$
        DECLARE
        replica_id TEXT;
        BEGIN
        SELECT CASE relreplident
                   WHEN 'd' THEN 'default'
                   WHEN 'n' THEN 'nothing'
                   WHEN 'f' THEN 'full'
                   WHEN 'i' THEN 'index'
                   END AS replica_identity
        FROM pg_class
        INTO replica_id
        WHERE oid = 's3scanfinding'::regclass;
        ASSERT replica_id = 'full', 'Replication identity should be full';
        END;
$sql$;

ROLLBACK;
