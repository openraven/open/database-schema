-- Verify database-schema:gdrive-activity-files on pg

BEGIN;

SELECT COUNT(*) FROM googledriveactivitywatermark LIMIT 1;
SELECT COUNT(*) FROM googledrivefiles LIMIT 1;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledriveactivitywatermark'
          AND
                indexname = 'googledriveactivitywatermark_enabled';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledriveactivitywatermark_enabled does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_parent';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivefiles_parent does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM
            pg_indexes
        WHERE
                schemaname = 'public'
          AND
                tablename = 'googledrivefiles'
          AND
                indexname = 'googledrivefiles_fileid';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'Index googledrivefiles_fileid does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'googledriveactivitywatermark_assetid_fkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledriveactivitywatermark_assetid_fkey does not exist';
END IF;
END;
$$;

DO $$
BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'googledrivefiles_assetid_fkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'googledrivefiles_assetid_fkey does not exist';
END IF;
END;
$$;

ROLLBACK;
