-- Verify database-schema:add-fields-for-sampling-to-scanfinding on pg

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanfinding'
	  AND column_name = 'samplesize';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanfinding.samplesize does not exist';
END IF;
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanfinding'
	  AND column_name = 'findingsfromsamplecount';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanfinding.findingsfromsamplecount does not exist';
END IF;
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanfinding'
	  AND column_name = 'matchpercentage';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanfinding.matchpercentage does not exist';
END IF;
    PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'scanfinding'
	  AND column_name = 'estimated';
	IF NOT FOUND THEN
		RAISE EXCEPTION 'scanfinding.estimated does not exist';
END IF;
END;
$$;
