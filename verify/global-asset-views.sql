-- Verify database-schema:global-asset-views on pg

BEGIN;

DO $$
BEGIN
	PERFORM TRUE
	FROM   information_schema.columns
	WHERE  table_name  = 'globalassets'
	  AND column_name = 'configurationblob';
	IF FOUND THEN
		RAISE EXCEPTION 'globalassets.configurationblob should not exist';
END IF;
END;
$$;

SELECT assetid FROM v_globalassetscomplete LIMIT 1;
SELECT assetid FROM v_globalassets LIMIT 1;

ROLLBACK;
