-- Verify database-schema:add_scanned_objects_stats on pg

BEGIN;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'scannerjobrunstatistics'
    AND column_name = 'totalobjects';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'scannerjobrunstatistics.totalobjects does not exist';
  END IF;
END;
$$;

DO $$
BEGIN
  PERFORM TRUE
  FROM   information_schema.columns
  WHERE  table_name  = 'scannerjobrunstatistics'
    AND column_name = 'scannedobjects';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'scannerjobrunstatistics.scannedobjects does not exist';
  END IF;
END;
$$;

ROLLBACK;
