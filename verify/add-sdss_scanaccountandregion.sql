BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   information_schema.tables
        WHERE  table_schema  = 'public'
          AND table_name = 'sdss_scanaccountandregion';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'sdss_scanaccountandregion table does not exist';
        END IF;
    END;
$$;

ROLLBACK;
