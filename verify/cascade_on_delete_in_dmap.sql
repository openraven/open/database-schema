-- Verify database-schema:cascade_on_delete_in_dmap on pg

BEGIN;

DO $$
    BEGIN
        PERFORM TRUE
        FROM   pg_constraint
        WHERE conname = 'dmap_assetid_fkey';
        IF NOT FOUND THEN
            RAISE EXCEPTION 'dmap_assetid_fkey does not exist';
        END IF;
    END;
$$;

ROLLBACK;
