-- Verify database-schema:scan-audit-log-events-target-length on pg

BEGIN;

DO $$
BEGIN
        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'scantarget'
	  AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.scantarget should be of type text';
    END IF;

        PERFORM TRUE
        FROM   information_schema.columns
        WHERE  table_name  = 'scanauditlogevent'
          AND column_name = 'childtarget'
          AND data_type   = 'text';
        IF NOT FOUND THEN
                RAISE EXCEPTION 'scanauditlogevent.childtarget should be of type text';
    END IF;

END;
$$;

ROLLBACK;
