-- Revert database-schema:add-object-create-update-time-to-scanfinding from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS objectcreated;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS objectlastupdated;

COMMIT;