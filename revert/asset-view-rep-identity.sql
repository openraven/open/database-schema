-- Revert database-schema:asset-view-rep-identity from pg

BEGIN;

ALTER TABLE awsassetsview REPLICA IDENTITY DEFAULT;

COMMIT;
