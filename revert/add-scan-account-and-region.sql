-- Revert database-schema:add-scan-account-and-region from pg

BEGIN;

DROP TABLE IF EXISTS scanaccountandregion;

DROP INDEX IF EXISTS scanaccountandregion_jobid;

COMMIT;
