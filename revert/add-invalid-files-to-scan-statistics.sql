-- Revert database-schema:add-invalid-files-to-scan-statistics from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS invalidfilecount;

COMMIT;
