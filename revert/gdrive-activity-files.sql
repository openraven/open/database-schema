-- Revert database-schema:gdrive-activity-files from pg

BEGIN;

DROP TABLE IF EXISTS googledriveactivitywatermark;
DROP TABLE IF EXISTS googledrivefiles;

COMMIT;
