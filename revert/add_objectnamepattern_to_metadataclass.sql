-- Revert database-schema:add_objectnamepattern_to_metadataclass from pg

BEGIN;


ALTER TABLE metadataclass DROP COLUMN IF EXISTS objectnamepattern;
ALTER TABLE metadataclass ADD COLUMN IF NOT EXISTS fileformats jsonb;

COMMIT;
