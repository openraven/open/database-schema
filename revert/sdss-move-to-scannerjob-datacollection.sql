-- Revert database-schema:sdss-move-to-scannerjob-datacollection from pg

BEGIN;

update scannerjob
    set datacollections = null
    where scantype = 'STRUCTURED';

COMMIT;
