-- Revert database-schema:update-gdrive-views from pg

BEGIN;

DROP VIEW IF EXISTS v_suspiciousgoogledrivefilepermissions;
DROP VIEW IF EXISTS v_suspiciousgoogleshareddrivepermissions;
DROP VIEW IF EXISTS v_googledrivestalefiles;

CREATE VIEW v_googledrivestalefiles (
    assetid,
    fileid,
    driveid,
    filename,
    username,
    fileurl,
    filecreatedat,
    fileupdatedat,
    fileaccessedat,
    recordupdatedat,
    ignored
) AS
SELECT
    s.assetid,
    s.fileid,
    s.driveid,
    s.filename,
    s.username,
    s.fileurl,
    s.filecreatedat,
    s.fileupdatedat,
    s.fileaccessedat,
    s.updatedat,
    s.ignored
FROM googledrivestalefiles s;

CREATE VIEW v_suspiciousgoogledrivefilepermissions (
    assetid,
    fileid,
    driveid,
    filename,
    username,
    fileurl,
    suspiciousemails,
    groupemails,
    anyonewithlink,
    updatedat
    ) AS
SELECT s.assetid,
       s.fileid,
       s.driveid,
       s.filename,
       s.username,
       s.fileurl,
       s.suspiciousemails,
       s.groupemails,
       s.sharedbylink,
       s.updatedat
FROM suspiciousgoogledrivefilepermissions s;

CREATE VIEW v_suspiciousgoogleshareddrivepermissions (
    assetid,
    driveid,
    drivename,
    suspiciousemails,
    groupemails,
    updatedat
) AS
SELECT s.assetid,
       s.driveid,
       s.drivename,
       s.suspiciousemails,
       s.groupemails,
       s.updatedat
FROM suspiciousgoogleshareddrivepermissions s;

COMMIT;
