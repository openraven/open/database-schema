-- Revert database-schema:finished-scan-job-updates from pg

BEGIN;

ALTER TABLE scannerjobpage DROP COLUMN IF EXISTS lambdaduration;

ALTER TABLE finishedscans DROP COLUMN IF EXISTS duration;

ALTER TABLE finishedscans DROP COLUMN IF EXISTS reason;

COMMIT;
