-- Revert database-schema:s3objectcache from pg

BEGIN;

DROP TABLE cached.s3objects CASCADE;

COMMIT;
