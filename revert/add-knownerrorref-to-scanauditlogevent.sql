-- Revert database-schema:add-knownerrorref-to-scanauditlogevent from pg

BEGIN;

ALTER TABLE scanauditlogevent DROP COLUMN IF EXISTS knownerrorref;

COMMIT;
