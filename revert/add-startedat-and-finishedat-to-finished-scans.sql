-- Revert database-schema:add-startedat-and-finishedat-to-finished-scans from pg

ALTER TABLE finishedscans DROP COLUMN IF EXISTS startedat;
ALTER TABLE finishedscans DROP COLUMN IF EXISTS finishedat;