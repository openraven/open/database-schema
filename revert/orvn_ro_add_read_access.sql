-- Revert database-schema:orvn_ro_add_read_access from pg

BEGIN;

-- We can't really revert / it is difficult to revert this to the previous state, so we won't.

COMMIT;
