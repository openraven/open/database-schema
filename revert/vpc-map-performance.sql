-- Revert database-schema:vpc-map-performance from pg

BEGIN;

DROP INDEX IF EXISTS awsassetsview_deriveddatablob_vpcid;

COMMIT;
