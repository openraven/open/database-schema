-- Revert database-schema:recreate-views-for-dbx from pg

BEGIN;

DROP VIEW IF EXISTS v_assetviolation;

DROP VIEW IF EXISTS v_issues;

DROP VIEW IF EXISTS v_policies

DROP VIEW IF EXISTS v_policy_rule

DROP VIEW IF EXISTS v_rules


    COMMIT;