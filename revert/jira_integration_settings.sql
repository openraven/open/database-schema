-- Revert database-schema:jira_integration_settings from pg

BEGIN;
DROP TABLE IF EXISTS jira_integration_settings;

COMMIT;
