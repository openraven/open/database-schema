-- Revert database-schema:PROD-4382 from pg

BEGIN;

DROP VIEW IF EXISTS v_active_scanfinding;
DROP VIEW IF EXISTS v_active_s3scanfinding;

COMMIT;