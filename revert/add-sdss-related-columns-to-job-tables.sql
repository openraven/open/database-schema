-- Revert database-schema:add-sdss-related-columns-to-job-tables from pg

BEGIN;

delete from scannerjobrunstatistics where scantype = 'STRUCTURED';

alter table scannerjobrunstatistics drop column if exists scantype;

---

delete from scannerjobpage where scantype = 'STRUCTURED';

alter table scannerjobpage drop column if exists scantype;

---

delete from scannerjob where scantype = 'STRUCTURED';

alter table scannerjob drop column if exists scantype,
                       drop column if exists scanrequest;

---

delete from scannerjobruns where scantype = 'STRUCTURED';

alter table scannerjobruns drop column if exists scantype,
                           drop column if exists errorcode,
                           drop column if exists errordetails;

---

alter table datastoreaccessjobs drop constraint if exists datastoreaccessjobs_jobid_fkey;
alter table datastoreaccessjobs add constraint datastoreaccessjobs_jobid_fkey foreign key (jobid) references sdss_scannerjob(id);

COMMIT;
