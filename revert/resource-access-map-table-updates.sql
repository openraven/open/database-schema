-- Revert database-schema:resource-access-map-table-updates from pg

BEGIN;

ALTER TABLE datastoreresourceaccessmapping DROP COLUMN IF EXISTS status;
ALTER TABLE datastoreresourceaccessmapping REPLICA IDENTITY DEFAULT;

COMMIT;
