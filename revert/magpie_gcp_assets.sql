-- Revert database-schema:magpie_gcp_assets from pg

BEGIN;

DROP table if exists  gcp cascade;

COMMIT;
