-- Revert database-schema:remove-workflow-tables from pg

BEGIN;

-- restores workflow_configuration table
CREATE TABLE IF NOT EXISTS workflow_configuration
(
    id            UUID NOT NULL PRIMARY KEY,
    name          TEXT NOT NULL,
    description   TEXT,
    workflow_type TEXT NOT NULL
);

-- rebuilds minimal necessary workflow records
INSERT INTO workflow_configuration
SELECT uuid_generate_v4() as id,
       'rebuilt ' || rc.event_type as name,
       'reconstructed via revert' as description,
       rc.event_type as workflow_type
FROM rule_configuration rc
GROUP BY rc.event_type;

-- reconstructs foreign key in rule_configuration
ALTER TABLE rule_configuration ADD COLUMN workflow_id UUID;

UPDATE rule_configuration rc SET workflow_id = wc.id
FROM workflow_configuration wc WHERE rc.event_type = wc.workflow_type;

ALTER TABLE rule_configuration ALTER COLUMN workflow_id SET NOT NULL;
ALTER TABLE orvn.public.rule_configuration
    ADD CONSTRAINT fk_workflow FOREIGN KEY (workflow_id)
        REFERENCES workflow_configuration (id)
        ON DELETE CASCADE;

-- removes event_type column from rule_configuration
ALTER TABLE rule_configuration DROP COLUMN event_type;

-- n.b. deliberately NOT recreating the prototype `workflowconfiguration` table
-- as it hasn't been used at all anywhere for a long time

COMMIT;
