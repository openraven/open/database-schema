-- Revert database-schema:gdrive_violation_remediation_fields from pg

BEGIN;

ALTER TABLE gdriveviolations DROP COLUMN IF EXISTS actionid;
ALTER TABLE gdriveviolations DROP COLUMN IF EXISTS remediationinfo;

COMMIT;
