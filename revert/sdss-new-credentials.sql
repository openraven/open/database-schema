-- Revert database-schema:sdss-new-credentials from pg

BEGIN;

alter table datastoreaccesscredentials
    add column secretlabel text,
    add column secretversion text,
    add column username text,
    add column passwordsecretlocation text;

update datastoreaccesscredentials
set
    username = credentialmodel->>'user',
    passwordsecretlocation = credentialmodel->>'arn',
    secretlabel = credentialmodel->>'secretKey';

alter table datastoreaccesscredentials
    alter column username set not null,
    alter column passwordsecretlocation set not null,
    drop column credentialname,
    drop column credentialmodeltype,
    drop column credentialmodel;

drop table if exists datastorecredentialmodels;

COMMIT;
