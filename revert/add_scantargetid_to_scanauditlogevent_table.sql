-- Revert database-schema:add_scantargetid_to_scanauditlogevent_table from pg

BEGIN;

ALTER TABLE scanauditlogevent DROP COLUMN IF EXISTS scantargetid;

COMMIT;
