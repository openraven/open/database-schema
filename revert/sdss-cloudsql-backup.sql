-- Revert database-schema:sdss-cloudsql-backup from pg

BEGIN;

drop table if exists sdss_cloudsql_env;

COMMIT;
