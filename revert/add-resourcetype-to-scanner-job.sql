-- Revert database-schema:add-resourcetype-to-scanner-job from pg

BEGIN;

alter table scannerjob drop column if exists resourcetype;

COMMIT;
