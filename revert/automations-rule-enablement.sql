-- Revert database-schema:automations-rule-enablement from pg

BEGIN;

ALTER TABLE rule_configuration DROP COLUMN IF EXISTS enabled;

COMMIT;
