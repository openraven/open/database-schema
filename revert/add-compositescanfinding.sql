-- Revert database-schema:add-compositescanfinding from pg

BEGIN;

DROP TABLE IF EXISTS compositescanfinding;

COMMIT;
