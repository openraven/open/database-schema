-- Revert database-schema:add-7zip-scanfiletype from pg

BEGIN;

DELETE FROM scanfiletype WHERE mimetype = 'application/x-7z-compressed';

COMMIT;
