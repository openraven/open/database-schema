BEGIN;
ALTER TABLE awsec2instance DROP CONSTRAINT awsec2instance_arn_unique;
ALTER TABLE awsec2instance ADD CONSTRAINT unique_resourcename UNIQUE (resourcename) ;
ALTER TABLE dmap DROP COLUMN arn text;
ALTER TABLE dmap ADD COLUMN resourcename;
COMMIT;



