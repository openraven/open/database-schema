-- Revert database-schema:asset-violation-remove-policy-id from pg

BEGIN;

DROP TABLE IF EXISTS assetviolations_removepolicyid;

COMMIT;
