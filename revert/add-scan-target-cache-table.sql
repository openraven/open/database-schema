-- Revert database-schema:add-scan-target-cache table

BEGIN;

DROP TABLE IF EXISTS cached.scantargetcache;

COMMIT;

