-- Revert database-schema:s3-scan-service-add-resumeobject-for-bucket-listing from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS resumeobject;

COMMIT;
