-- Revert database-schema:add-jobrunid-to-sdss-cloudsql-env from pg

BEGIN;

ALTER TABLE sdss_cloudsql_env DROP COLUMN IF EXISTS jobrunid;

COMMIT;