-- Revert database-schema:add-fingdingsdata-to-v_scanfinding from pg

BEGIN;

DROP VIEW v_scanfinding;

CREATE OR REPLACE VIEW v_scanfinding AS
SELECT scanfinding.scannerjobid,
       scanfinding.scannerjobrunid,
       scanfinding.assetid,
       scanfinding.scantarget,
       scanfinding.scantargetversion,
       scanfinding.dataclassid,
       scanfinding.findingscount,
       scanfinding.createdat,
       scanfinding.updatedat,
       scanfinding.scantargetchild,
       scanfinding.findingslocations
FROM scanfinding;

COMMIT;
