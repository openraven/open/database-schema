-- Revert database-schema:add-report-data-provider-datasource from pg

BEGIN;

DROP TABLE IF EXISTS report_operation_key_metadata;

COMMIT;
