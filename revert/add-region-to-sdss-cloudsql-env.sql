-- Revert database-schema:add-region-to-sdss-cloudsql-env from pg

BEGIN;

ALTER TABLE sdss_cloudsql_env DROP COLUMN IF EXISTS region;

COMMIT;