-- Revert database-schema:sdss-cred-model-ec2 from pg

BEGIN;

delete from datastorecredentialmodels where id = 'EC2_SECRETS_MANAGER_PASSWORD';
delete from datastorecredentialmodels where id = 'EC2_SECRETS_MANAGER_PASSWORD_IN_JSON';
delete from datastorecredentialmodels where id = 'EC2_PARAMETER_STORE_PASSWORD';

COMMIT;
