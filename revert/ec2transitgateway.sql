-- Revert database-schema:ec2transitgateway from pg

BEGIN;

DROP TABLE IF EXISTS awsec2transitgateway;

COMMIT;
