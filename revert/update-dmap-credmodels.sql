-- Revert database-schema:update-dmap-credmodels from pg

BEGIN;

UPDATE datastorecredentialmodels
SET credentialmodel=jsonb_build_object(
        'arn', 'ARN to Secrets Manager where the password is held',
        'port', 'Database port (required)',
        'user', 'The username to connect to',
        'databaseType', 'Database type (one of POSTGRESQL, MARIADB, MYSQL, MSSQL or ORACLE) (required)'
    )
WHERE id='EC2_SECRETS_MANAGER_PASSWORD';

UPDATE datastorecredentialmodels
SET credentialmodel=jsonb_build_object(
        'arn', 'ARN to Secrets Manager where the password is held',
        'port', 'Database port (required)',
        'user', 'The username to connect to',
        'secretKey', 'Name of JSON field where password is stored',
        'databaseType', 'Database type (one of POSTGRESQL, MARIADB, MYSQL, MSSQL or ORACLE) (required)'
    )
WHERE id='EC2_SECRETS_MANAGER_PASSWORD_IN_JSON';

UPDATE datastorecredentialmodels
SET credentialmodel=jsonb_build_object(
        'port', 'Database port (required)',
        'user', 'The username to connect to (required)',
        'region', 'Region in which parameter is held',
        'parameterName', 'Parameter name (required)',
        'databaseType', 'Database type (one of POSTGRESQL, MARIADB, MYSQL, MSSQL or ORACLE) (required)'
    )
WHERE id='EC2_PARAMETER_STORE_PASSWORD';

COMMIT;
