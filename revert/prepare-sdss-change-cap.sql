-- Revert database-schema:prepare-sdss-change-cap from pg

BEGIN;

ALTER TABLE sdss_scanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
