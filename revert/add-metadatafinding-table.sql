-- Revert database-schema:add-metadatafinding-table from pg

BEGIN;

DROP TABLE IF EXISTS metadatafinding;

COMMIT;