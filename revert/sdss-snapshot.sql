-- Revert database-schema:sdss-snapshot from pg

BEGIN;

DROP TABLE IF EXISTS sdss_snapshot;

COMMIT;
