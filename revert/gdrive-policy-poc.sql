-- Revert database-schema:gdrive-policy-poc from pg

BEGIN;

DROP VIEW IF EXISTS v_gdriveviolations;
DROP TABLE IF EXISTS gdriveviolationsconfig;
DROP TABLE IF EXISTS gdriverules;
DROP TABLE IF EXISTS gdriveviolations;

COMMIT;
