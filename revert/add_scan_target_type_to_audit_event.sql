-- Revert database-schema:add_scan_target_type_to_audit_event from pg

BEGIN;

ALTER TABLE scanauditlogevent DROP COLUMN IF EXISTS scantargettype;
ALTER TABLE sdss_scanauditlogevent DROP COLUMN IF EXISTS scantargettype;
ALTER TABLE finishedscans DROP COLUMN IF EXISTS scantargettype;
ALTER TABLE sdss_finishedscans DROP COLUMN IF EXISTS scantargettype;

COMMIT;
