DROP TABLE ddr_rules;
CREATE TABLE ddr_rules (
   id integer,
   rule_name text,
   description text,
   is_enabled boolean,
   alert_level varchar,
   PRIMARY KEY (id)
);


DROP TABLE ddr_alerts;
CREATE TABLE IF NOT EXISTS ddr_alerts (
    id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    source_log varchar not null default '',
    alert_rule_id integer not null,
    alert_rule_name varchar not null,
    alert_rule_desc varchar not null,
    alert_level varchar not null,
    alert_generation_ts timestamptz not null,
    alert_text varchar not null,
    events jsonb,
    alert_parameters jsonb
);
ALTER TABLE ddr_alerts REPLICA IDENTITY FULL;
