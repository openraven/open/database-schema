-- Revert database-schema:report-timeout from pg

BEGIN;

ALTER TABLE reportjobrun DROP COLUMN IF EXISTS timedoutstates;

COMMIT;
