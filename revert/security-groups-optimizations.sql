-- Revert database-schema:security-groups-optimizations from pg

BEGIN;

DROP TABLE IF EXISTS awssecuritygroupconnections;
DROP TABLE IF EXISTS awssecuritygrouppeering;

COMMIT;
