-- Revert database-schema:add-findingscount-to-scan-audit-log-event from pg

ALTER TABLE scanauditlogevent DROP COLUMN IF EXISTS findingscount;