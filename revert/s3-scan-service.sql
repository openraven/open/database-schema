BEGIN;
DROP TABLE IF EXISTS dataclass;
DROP TABLE IF EXISTS datacollection;
DROP TABLE IF EXISTS datacollection_dataclass;
COMMIT;
