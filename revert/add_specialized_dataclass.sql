-- Revert database-schema:add_specialized_dataclass from pg

BEGIN;

DROP TABLE IF EXISTS specializeddataclass;

DROP TRIGGER IF EXISTS specializeddataclass_create ON specializeddataclass;

CREATE
OR REPLACE FUNCTION dataclassv2_update() RETURNS TRIGGER AS
$BODY$
BEGIN
    UPDATE dataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    UPDATE metadataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    UPDATE compositedataclass
    SET name=new.name,
        description=new.description,
        category=new.category,
        refid=new.refid,
        status=new.status,
        managed=new.managed
    WHERE id = new.id;

    RETURN new;
END;
$BODY$
LANGUAGE plpgsql;

COMMIT;
