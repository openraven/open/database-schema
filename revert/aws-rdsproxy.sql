-- Revert database-schema:aws-rdsproxy from pg

BEGIN;

DROP TABLE IF EXISTS awsrdsproxy;

COMMIT;
