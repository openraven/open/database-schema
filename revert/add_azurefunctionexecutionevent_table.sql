-- Revert database-schema:add_azurefunctionexecutionevent_table from pg

BEGIN;

DROP TABLE IF EXISTS azurefunctionexecutionevent;

COMMIT;
