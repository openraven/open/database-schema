-- Revert database-schema:add-asset-fkey-drives from pg

BEGIN;

ALTER TABLE suspiciousgoogleshareddrivepermissions
    DROP CONSTRAINT fk_suspiciousgoogleshareddrivepermissions_asset_id;

COMMIT;
