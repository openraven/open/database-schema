-- Revert database-schema:scanner-job-run-progress-columns to pg

ALTER TABLE scannerjobruns DROP COLUMN IF EXISTS numerator;
ALTER TABLE scannerjobruns DROP COLUMN IF EXISTS denominator;
ALTER TABLE scannerjobruns DROP COLUMN IF EXISTS scannerjobid;
