-- Revert database-schema:set-columns-to-be-dropped-nullable from pg

BEGIN;

ALTER TABLE aggregatedassetfindings ALTER COLUMN region SET NOT NULL;
ALTER TABLE aggregatedassetfindings ALTER COLUMN account SET NOT NULL;

ALTER TABLE aggregatedscanfindings ALTER COLUMN region SET NOT NULL;
ALTER TABLE aggregatedscanfindings ALTER COLUMN account SET NOT NULL;

COMMIT;
