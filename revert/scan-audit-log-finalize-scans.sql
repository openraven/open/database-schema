-- Revert database-schema:scan-audit-log-finalize-scans from pg

BEGIN;

DROP TABLE IF EXISTS finishedscans;

COMMIT;
