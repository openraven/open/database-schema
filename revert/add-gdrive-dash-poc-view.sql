-- Revert database-schema:add-gdrive-dash-poc-view from pg

BEGIN;

DROP INDEX IF EXISTS gdriveviolations_nodeid;
DROP VIEW IF EXISTS v_gdrive_dash;

COMMIT;
