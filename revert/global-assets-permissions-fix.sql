-- Deploy correct permissions to v_global*

BEGIN;
    REVOKE SELECT ON v_globalassets FROM orvn_ro;
    REVOKE SELECT ON v_globalassetscomplete FROM orvn_ro;
COMMIT;