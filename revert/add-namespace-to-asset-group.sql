-- Revert database-schema:add-namespace-to-asset-group from pg

BEGIN;

ALTER TABLE assetworkspace DROP COLUMN IF EXISTS namespace;
-- WARNING: Unique Constraint on name cannot be safely added back.
-- This is because groups with non unique names may have been added under different
-- namespaces after the initial SQL deploy.
-- This is not truly reversible!

COMMIT;
