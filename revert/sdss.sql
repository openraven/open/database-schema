BEGIN;

drop table if exists datastoreaccessjobs;
drop table if exists datastoreresourceaccessmapping;
drop table if exists datastoreaccesscredentials;
drop table if exists sdss_scannerjobpage;
drop table if exists sdss_scannerjob;
drop table if exists sdss_scannerjobruns;
drop table if exists sdss_scannerjobrunstatistics;
drop index if exists sdss_lambdarefinfo_createdat;
drop table if exists sdss_lambdareferenceinfo;
drop table if exists metadatatables;
drop table if exists metadatadbs;
drop index if exists sdss_scanfinding_assetid_scantarget_scantargetchild_scannerjob;
drop table if exists sdss_scanfinding;
alter table awsrdsdbinstance drop constraint if exists awsrdsdbinstance_arn_unique;

COMMIT;
