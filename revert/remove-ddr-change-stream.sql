-- Revert database-schema:remove-ddr-change-stream from pg

BEGIN;

ALTER TABLE ddr_alerts REPLICA IDENTITY FULL;

COMMIT;
