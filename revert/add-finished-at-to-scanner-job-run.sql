-- Revert database-schema:add-finished-at-to-scanner-job-run from pg

ALTER TABLE scannerjobruns DROP COLUMN IF EXISTS finishedat;
