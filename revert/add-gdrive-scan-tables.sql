-- Revert database-schema:add-gdrive-scan-tables.sql from pg

BEGIN;

DROP TABLE IF EXISTS gdrivescanfinding;

COMMIT;