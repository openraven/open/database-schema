-- Revert database-schema:global-asset-views from pg

BEGIN;

ALTER TABLE globalassets ADD COLUMN IF NOT EXISTS configurationblob JSONB;
DROP VIEW IF EXISTS v_globalassets;
DROP VIEW IF EXISTS v_globalassetscomplete;

COMMIT;
