-- Revert database-schema:add-invalidation-requests table

BEGIN;

DROP TABLE IF EXISTS cached.invalidationrequests;

COMMIT;

