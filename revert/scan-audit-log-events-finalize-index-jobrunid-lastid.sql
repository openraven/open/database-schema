-- Revert database-schema:scan-audit-log-events-finalize-index-jobrunid-lastid from pg

BEGIN;

drop index if exists scanauditlogevent_jobrunid_lastid_idx;

COMMIT;
