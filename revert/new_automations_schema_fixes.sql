-- Revert database-schema:new_automations_schema_fixes from pg

BEGIN;

DROP TABLE IF EXISTS automation_rule, automation_consequence;

COMMIT;
