BEGIN;

DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.allowedDomains';
DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.deniedDomains';

COMMIT;
