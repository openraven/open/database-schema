-- Revert database-schema:policy_violation_perf_improvements from pg

BEGIN;

ALTER TABLE issues
    DROP COLUMN IF EXISTS violateddataclasses,
    DROP COLUMN IF EXISTS affectedaccounts,
    DROP COLUMN IF EXISTS affectedassetcount,
    DROP COLUMN IF EXISTS affectedregions;

DROP INDEX IF EXISTS dataclassname_idx;
DROP TABLE IF EXISTS issuedataclasses;


COMMIT;
