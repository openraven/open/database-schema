-- Revert database-schema:add-cloudstorage-scan-tables.sql from pg

BEGIN;

DROP TABLE IF EXISTS cloudstoragescanfinding;
DROP TABLE IF EXISTS cloudstoragemetadatascanfinding;

DROP TABLE IF EXISTS metadatafinding;
ALTER TABLE s3metadatascanfinding RENAME TO metadatafinding;

COMMIT;