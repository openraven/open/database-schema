-- Revert database-schema:policy-opa-removal from pg

BEGIN;
-- DROP newly created tables
DROP TABLE policies CASCADE;
DROP TABLE rules CASCADE;
DROP TABLE policy_rule CASCADE;
DROP TABLE issues CASCADE;
DROP TABLE issueremediation CASCADE;
DROP TABLE assetviolation CASCADE;
DROP TABLE fileviolation CASCADE;
DROP TABLE fileviolationdataclass CASCADE;
DROP TABLE issue_assetviolation CASCADE;

-- Restore old tables
ALTER TABLE policies_opa RENAME TO policies;
ALTER TABLE rules_opa RENAME TO rules;
ALTER TABLE policy_rule_opa RENAME TO policy_rule;
ALTER TABLE issues_opa RENAME TO issues;
ALTER TABLE issueremediation_opa RENAME TO issueremediation;
ALTER TABLE policyaudit_opa RENAME TO policyaudit;
ALTER TABLE assetviolation_opa RENAME TO assetviolation;
ALTER TABLE fileviolation_opa RENAME TO fileviolation;
ALTER TABLE fileviolationdataclass_opa RENAME TO fileviolationdataclass;
ALTER TABLE issue_assetviolation_opa RENAME TO issue_assetviolation;
COMMIT;
