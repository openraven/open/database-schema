-- Revert database-schema:dropping-fileviolation-et-al-tables from pg

BEGIN;

CREATE TABLE IF NOT EXISTS fileviolation
(
    id uuid PRIMARY KEY NOT NULL,
    assetviolationid uuid,
    CONSTRAINT  fk_assetviolation
        FOREIGN KEY (assetviolationid)
            REFERENCES assetviolation (id),
    filename text,
    sourcechild text
);

CREATE TABLE IF NOT EXISTS fileviolationdataclass
(
    id uuid PRIMARY KEY NOT NULL,
    -- this will one day sort of be a foreign key. We don't allow class name
    -- changes at the moment
    dataclassid uuid,
    dataclassname text,
    count bigint,
    fileviolationid uuid,
    CONSTRAINT fk_fileviolation
        FOREIGN KEY (fileviolationid)
            REFERENCES fileviolation (id),
    CONSTRAINT fk_dataclass
        FOREIGN KEY (dataclassid)
            REFERENCES dataclass (id)
);

DROP VIEW IF EXISTS v_assetviolation;
create or replace view v_assetviolation as
SELECT av.id           as id,
       av.datecreated  as datecreated,
       av.updatedat    as updatedat,
       av.resourcename as resourcename,
       av.policyid     as policyid,
       av.ruleid       as ruleid,
       av.status       as status,
       av.awsaccountid as awsaccountid,
       av.awsregion    as awsregion,
       av.objectcount  as objectcount,
       fv.filename     as filename,
       fv.sourcechild  as sourcechild,
       fvdc.dataclassname as dataclassname
FROM assetviolation av
         JOIN policies p on av.policyid = p.id
         JOIN rules r on av.ruleid = r.id
         LEFT JOIN fileviolation fv on av.id = fv.assetviolationid
         LEFT JOIN fileviolationdataclass fvdc on fv.id = fvdc.fileviolationid;

COMMIT;
