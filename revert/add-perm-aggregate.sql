-- Revert database-schema:add-perm-aggregate from pg

BEGIN;

DROP TABLE IF EXISTS assetfilepermissionaggregate;

COMMIT;
