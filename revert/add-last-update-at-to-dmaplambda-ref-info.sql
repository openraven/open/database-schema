-- Deploy database-schema:add-last-update-at-to-dmaplambda-ref-info to pg

BEGIN;

DROP INDEX IF EXISTS dmaplambdarefinfo_lastupdateat;
ALTER TABLE dmaplambdareferenceinfo DROP COLUMN IF EXISTS lastupdateat;
CREATE INDEX IF NOT EXISTS dmaplambdarefinfo_createdat ON dmaplambdareferenceinfo(createdat);

COMMIT;
