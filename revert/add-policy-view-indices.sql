-- Revert database-schema:add-policy-view-indices from pg

BEGIN;

DROP INDEX IF EXISTS suspiciousgoogledrivefilepermissions_sharedbylink;
DROP INDEX IF EXISTS googledrivestalefiles_fileaccessedat;
DROP INDEX IF EXISTS googledrivestalefiles_filecreatedat;
DROP INDEX IF EXISTS googledrivestalefiles_fileupdatedat;

COMMIT;
