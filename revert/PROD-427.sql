-- Revert database-schema:PROD-427 from pg

BEGIN;

REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, sqitch FROM orvn_ro;

DROP USER orvn_ro;
DROP USER orvn_superuser;

DROP USER orvn_account_management;
DROP USER orvn_asset_groups;
DROP USER orvn_aws_postgres_consumer;
DROP USER orvn_cluster_upgrade;
DROP USER orvn_cross_account;
DROP USER orvn_data_catalog;
DROP USER orvn_dmap_scheduler;
DROP USER orvn_dmap;
DROP USER orvn_integrations;
DROP USER orvn_policy_v2;
DROP USER orvn_s3_scan_service;
DROP USER orvn_user_notifications;

/*
we are choosing not to do this because those roles belong to RDS
however, any "deploy && revert && deploy" cycle is still
idempotent because of the try-catch in the "deploy" side

DROP ROLE rds_iam;
DROP ROLE rds_superuser;
*/

COMMIT;
