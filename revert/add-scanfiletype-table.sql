-- Revert database-schema:add-scanfiletype-table from pg

BEGIN;

DROP TABLE IF EXISTS scanfiletype;

COMMIT;
