-- Revert database-schema:add-addt-info-col-gdrive from pg

BEGIN;

ALTER TABLE googledrivefiles DROP COLUMN IF EXISTS trashed;
ALTER TABLE googledrivefiles DROP COLUMN IF EXISTS filesizebytes;

COMMIT;
