-- Revert database-schema:aws-inherits-1 from pg

BEGIN;

DROP TABLE IF EXISTS awsec2snapshot;
DROP TABLE IF EXISTS awscloudtrailtrail;
DROP TABLE IF EXISTS awsssminstance;

COMMIT;
