-- Revert database-schema:sdss-cloudsql-asset-table from pg

BEGIN;

DROP TABLE IF EXISTS cloudsqlasset;

COMMIT;
