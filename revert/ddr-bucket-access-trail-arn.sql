BEGIN;

ALTER TABLE ddr_bucket_access DROP COLUMN IF EXISTS trailarn;
ALTER TABLE ddr_bucket_access DROP CONSTRAINT IF EXISTS ddr_bucket_access_pkey;
ALTER TABLE ddr_bucket_access ADD CONSTRAINT ddr_bucket_access_pkey PRIMARY KEY (bucketname);

COMMIT;