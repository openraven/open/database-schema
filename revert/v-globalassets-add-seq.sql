-- Revert database-schema:v-globalassets-add-seq from pg

BEGIN;

-- no revert because v_globalassets is a dependency of other views
-- removing the added column would require us to drop v_globalassets, which in turn would require cascading
-- view deletions

COMMIT;
