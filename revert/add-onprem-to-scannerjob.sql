-- Revert database-schema:add-onprem-to-scannerjob from pg

BEGIN;

ALTER TABLE scannerjob DROP COLUMN IF EXISTS onprem;

COMMIT;