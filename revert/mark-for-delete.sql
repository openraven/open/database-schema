-- Revert database-schema:mark-for-delete from pg

BEGIN;

ALTER TABLE aws DROP COLUMN markedfordelete;

COMMIT;
