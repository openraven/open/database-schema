-- Revert database-schema:alter-table-aggregate-table from pg

BEGIN;

ALTER TABLE aggregatedrelationaldatabasefindings RENAME TO aggregateddatabasefindings;

COMMIT;
