-- Revert database-schema:add-fkey-perm-aggregate from pg

BEGIN;

ALTER TABLE assetfilepermissionaggregate
    DROP CONSTRAINT fk_assetfilepermissionaggregate_assetid;

COMMIT;
