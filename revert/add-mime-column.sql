-- Revert database-schema:add-mime-column from pg

BEGIN;

ALTER TABLE googledrivefiles DROP COLUMN mimetype;

COMMIT;
