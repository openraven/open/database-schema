-- Revert database-schema:add-ext-dom-column from pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS suspiciousdomains;

COMMIT;
