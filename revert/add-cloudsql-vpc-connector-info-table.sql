-- Revert database-schema:add-cloudsql-vpc-connector-info-table from pg

BEGIN;

DROP TABLE IF EXISTS cloudsqlvpcconnectorinfo;

COMMIT;
