-- Revert database-schema:snowflake_cred_models from pg

BEGIN;

UPDATE datastorecredentialmodels
SET assettypes=jsonb_build_array('AWS::RDS::DBInstance', 'AWS::Redshift::Cluster')
WHERE id='SECRETS_MANAGER_PASSWORD';

UPDATE datastorecredentialmodels
SET assettypes=jsonb_build_array('AWS::RDS::DBInstance', 'AWS::Redshift::Cluster')
WHERE id='SECRETS_MANAGER_PASSWORD_IN_JSON';

UPDATE datastorecredentialmodels
SET assettypes=jsonb_build_array('AWS::RDS::DBInstance', 'AWS::Redshift::Cluster')
WHERE id='PARAMETER_STORE_PASSWORD';

UPDATE datastorecredentialmodels
SET assettypes=jsonb_build_array('Snowflake')
WHERE id='SECRETS_MANAGER_PRIVATE_KEY';

COMMIT;
