-- Revert database-schema:rep-id-azure from pg

BEGIN;

ALTER TABLE blobstoragescanfinding REPLICA IDENTITY DEFAULT;
ALTER TABLE blobstoragemetadatascanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
