-- Revert database-schema:dmap-lambda-reference-info from pg

BEGIN;

DROP TABLE IF EXISTS dmaplambdareferenceinfo;
DROP INDEX IF EXISTS dmaplambdarefinfo_createdat;

COMMIT;