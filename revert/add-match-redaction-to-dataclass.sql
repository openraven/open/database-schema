-- Revert database-schema:add-match-redaction-to-dataclass from pg

BEGIN;

ALTER TABLE dataclass DROP COLUMN IF EXISTS redactiontype;
ALTER TABLE dataclass DROP COLUMN IF EXISTS redaction;

COMMIT;
