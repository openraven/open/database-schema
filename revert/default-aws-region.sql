-- Revert database-schema:default-aws-region from pg

BEGIN;

-- Here we only remove the DEFAULT's instead of restoring the bad data
-- since restoring a broken state would not help.
ALTER TABLE assetviolation ALTER COLUMN awsregion DROP DEFAULT;
ALTER TABLE assetviolation ALTER COLUMN awsregion DROP NOT NULL;

COMMIT;
