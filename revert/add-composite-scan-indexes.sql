-- Revert database-schema:add-composite-scan-indexes from pg

BEGIN;
DROP INDEX IF EXISTS scanfinding_composite_scan_idx;
DROP INDEX IF EXISTS cloudstoragemetadatascanfinding_composite_scan_idx;
DROP INDEX IF EXISTS cloudstoragescanfinding_composite_scan_idx;
DROP INDEX IF EXISTS compositescanfinding_composite_scan_idx;
DROP INDEX IF EXISTS s3metadatascanfinding_composite_scan_idx;
DROP INDEX IF EXISTS s3scanfinding_composite_scan_idx;
DROP INDEX IF EXISTS sdss_scanfinding_composite_scan_idx;
DROP INDEX IF EXISTS scanfinding_copy_composite_scan_idx;

COMMIT;
