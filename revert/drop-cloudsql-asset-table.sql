-- Revert database-schema:drop-cloudsql-asset-table from pg

BEGIN;

CREATE TABLE IF NOT EXISTS cloudsqlasset
(
    id                  TEXT NOT NULL,
    host                TEXT NOT NULL,
    port                INTEGER NOT NULL,
    databasetype        TEXT NOT NULL,
    dbname              TEXT NOT NULL,
    vpcnetwork          TEXT NOT NULL,
    PRIMARY KEY (id)
    );

COMMIT;
