-- Revert database-schema:policy-eventing-reseed from pg

BEGIN;

DROP TABLE IF EXISTS reseed_work_unit;
DROP TABLE IF EXISTS reseed_job_run;
DROP TABLE IF EXISTS gdriveviolations_eventing;

COMMIT;
