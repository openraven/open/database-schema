-- Revert database-schema:metadataclass_create_tables to pg

BEGIN;

    DROP TABLE IF EXISTS datacollection_metadataclass;
    DROP TABLE IF EXISTS metadataclass;

COMMIT;
