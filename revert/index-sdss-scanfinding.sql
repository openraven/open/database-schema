-- Revert database-schema:index-sdss-scanfinding from pg

BEGIN;

DROP INDEX IF EXISTS sdss_scanfinding_tablename;
DROP INDEX IF EXISTS sdss_scanfinding_schemaname;
DROP INDEX IF EXISTS sdss_scanfinding_columnname;
DROP INDEX IF EXISTS sdss_scanfinding_scannerjobrunid;

COMMIT;
