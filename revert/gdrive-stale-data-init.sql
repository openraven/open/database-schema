-- Revert database-schema:gdrive-stale-data-init from pg

BEGIN;

DROP TABLE IF EXISTS googledrivestalefiles;
DROP TABLE IF EXISTS googledrivestalenesssettings;

COMMIT;
