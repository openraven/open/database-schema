-- Revert database-schema:rename_sdss_scan_target_type from pg

BEGIN;

UPDATE scannerjob SET scantargettype = 'AWS Structured SQL databases' WHERE scantargettype = 'Structured Database';

COMMIT;
