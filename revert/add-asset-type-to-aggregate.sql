-- Revert database-schema:add-asset-type-to-aggregate from pg

BEGIN;

DROP INDEX IF EXISTS aggregatedassetfindings_assettype;
DROP INDEX IF EXISTS aggregatedscanfindings_assettype;
ALTER TABLE aggregatedassetfindings DROP COLUMN IF EXISTS assettype;
ALTER TABLE aggregatedscanfindings DROP COLUMN IF EXISTS assettype;

COMMIT;
