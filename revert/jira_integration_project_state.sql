-- Revert database-schema:jira_integration_project_state from pg

BEGIN;

DROP TABLE IF EXISTS jira_label;
DROP TABLE IF EXISTS jira_priority;
DROP TABLE IF EXISTS jira_project;
DROP TABLE IF EXISTS jira_user;
DROP TABLE IF EXISTS jira_field;

COMMIT;
