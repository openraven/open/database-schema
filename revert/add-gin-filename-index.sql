-- Revert database-schema:add-gin-filename-index from pg

BEGIN;

DROP INDEX IF EXISTS googledrivefiles_filename_gin;

COMMIT;
