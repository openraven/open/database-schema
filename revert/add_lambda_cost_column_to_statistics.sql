-- Revert database-schema:add_lambda_cost_column_to_statistics from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS functioncost;

COMMIT;
