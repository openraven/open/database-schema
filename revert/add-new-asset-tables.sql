-- Revert database-schema:add-new-asset-tables from pg

BEGIN;

DROP TABLE IF EXISTS globalassets;
DROP TABLE IF EXISTS globalassetslist;

COMMIT;
