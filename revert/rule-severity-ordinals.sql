-- Revert database-schema:rule-severity-ordinals from pg

BEGIN;

ALTER TABLE rules DROP COLUMN IF EXISTS severityordinal;

COMMIT;
