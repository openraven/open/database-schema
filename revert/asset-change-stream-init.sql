-- Revert database-schema:asset-change-stream-init from pg

BEGIN;

DROP TABLE IF EXISTS aggregatedassetmetrics;
DROP TABLE IF EXISTS aggregatedscanmetrics;
ALTER TABLE s3scanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
