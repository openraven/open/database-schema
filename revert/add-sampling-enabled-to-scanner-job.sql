-- Revert database-schema:add-sampling-enabled-to-scanner-job to pg

ALTER TABLE scannerjob DROP COLUMN IF EXISTS samplingenabled;
