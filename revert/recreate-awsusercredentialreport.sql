-- Revert database-schema:recreate-awsusercredentialreport from pg

BEGIN;

DROP TABLE IF EXISTS awsusercredentialreport;

COMMIT;
