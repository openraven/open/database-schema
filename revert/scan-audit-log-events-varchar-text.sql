-- Revert database-schema:scan-audit-log-events-varchar-text from pg

BEGIN;

alter table scanauditlogevent alter column statusreason type varchar(255);
alter table scanauditlogevent alter column reportedmimetype type varchar(255);
alter table scanauditlogevent alter column detectedmimetype type varchar(255);
alter table scanauditlogevent alter column etag type varchar(255);
alter table scanauditlogevent alter column resourceid type varchar(255);

COMMIT;
