-- Revert database-schema:reseed-database-flag-table from pg

BEGIN;

DROP TABLE IF EXISTS scanfindingaggregatordatabaseversionflag;

COMMIT;
