-- Revert database-schema:metadataclass-fileformats-column from pg

BEGIN;

ALTER TABLE metadataclass DROP COLUMN IF EXISTS fileformats;

COMMIT;
