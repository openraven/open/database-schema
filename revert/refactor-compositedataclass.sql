-- Revert database-schema:refactor-compositedataclass from pg

BEGIN;

DROP TABLE IF EXISTS compositedataclass;

CREATE TABLE IF NOT EXISTS compositedataclass (
                                                  id UUID NOT NULL,
                                                  refid TEXT,
                                                  name TEXT,
                                                  status TEXT,
                                                  owner TEXT,
                                                  description TEXT,
                                                  version TEXT,
                                                  managed boolean,
                                                  criterion JSONB NOT NULL,
                                                  dirty boolean DEFAULT 't',
                                                  createdat timestamptz,
                                                  updatedat timestamptz);

ALTER TABLE compositedataclass ADD PRIMARY KEY (id);

CREATE INDEX IF NOT EXISTS compositedataclass_name ON compositedataclass(name);
CREATE INDEX IF NOT EXISTS compositedataclass_status ON compositedataclass(status);
CREATE INDEX IF NOT EXISTS compositedataclass_dirty ON compositedataclass(dirty);
CREATE INDEX IF NOT EXISTS compositedataclass_managed ON compositedataclass(managed);

COMMIT;
