-- Revert database-schema:stale-data-view from pg

BEGIN;

DROP VIEW IF EXISTS v_googledrivestalefiles;

COMMIT;
