-- Revert database-schema:asset-violation-audit-columns from pg

BEGIN;

ALTER TABLE assetviolation DROP COLUMN IF EXISTS statuschangedby;
ALTER TABLE assetviolation DROP COLUMN IF EXISTS statuschangedbytimestamp;

COMMIT;
