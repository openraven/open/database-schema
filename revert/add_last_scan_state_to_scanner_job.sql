-- Revert database-schema:add_last_scan_state_to_scanner_job from pg

BEGIN;
ALTER TABLE scannerjob DROP CONSTRAINT lastscanjobrunid_fkey;
ALTER TABLE scannerjob DROP COLUMN lastscanjobrunid;
DROP TABLE IF EXISTS scannerjobruns;
COMMIT;
