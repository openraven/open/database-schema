-- Revert change the id in ddr_alerts from a serial int to a random UUID

BEGIN;

ALTER TABLE ddr_alerts DROP CONSTRAINT ddr_alerts_pkey;
ALTER TABLE ddr_alerts DROP COLUMN id;
ALTER TABLE ddr_alerts ADD COLUMN id SERIAL PRIMARY KEY NOT NULL;

COMMIT;
