-- Revert database-schema:add_versions_to_scanfindings from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN serviceversion;

ALTER TABLE scanfinding DROP COLUMN scannerversion;

ALTER TABLE scanfinding DROP COLUMN falsepositive;

ALTER TABLE scanfinding DROP COLUMN alwaysfalsepositive;

COMMIT;
