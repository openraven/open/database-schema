-- Revert database-schema:rm-unused-columns from pg

BEGIN;

ALTER TABLE aggregatedassetfindings ADD COLUMN IF NOT EXISTS region TEXT;
ALTER TABLE aggregatedassetfindings ADD COLUMN IF NOT EXISTS account TEXT;
ALTER TABLE aggregatedassetfindings ADD COLUMN IF NOT EXISTS assettype TEXT;
ALTER TABLE aggregatedassetfindings ADD COLUMN IF NOT EXISTS source TEXT DEFAULT 'aws';

ALTER TABLE aggregatedscanfindings ADD COLUMN IF NOT EXISTS region TEXT;
ALTER TABLE aggregatedscanfindings ADD COLUMN IF NOT EXISTS account TEXT;
ALTER TABLE aggregatedscanfindings ADD COLUMN IF NOT EXISTS assettype TEXT;
ALTER TABLE aggregatedscanfindings ADD COLUMN IF NOT EXISTS source TEXT DEFAULT 'aws';

CREATE INDEX IF NOT EXISTS aggregatedassetfindings_assettype ON aggregatedassetfindings(assettype);
CREATE INDEX IF NOT EXISTS aggregatedassetfindings_region ON aggregatedassetfindings(region);
CREATE INDEX IF NOT EXISTS aggregatedassetfindings_account ON aggregatedassetfindings(account);
CREATE INDEX IF NOT EXISTS aggregatedassetfindings_source ON aggregatedassetfindings(source);

CREATE INDEX IF NOT EXISTS aggregatedscanfindings_region ON aggregatedscanfindings(region);
CREATE INDEX IF NOT EXISTS aggregatedscanfindings_account ON aggregatedscanfindings(account);
CREATE INDEX IF NOT EXISTS aggregatedscanfindings_assettype ON aggregatedscanfindings(assettype);
CREATE INDEX IF NOT EXISTS aggregatedscanfindings_source ON aggregatedscanfindings(source);

COMMIT;
