-- Revert database-schema:add_confidence_to_scanfinding from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS confidencesum;

COMMIT;
