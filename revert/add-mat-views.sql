-- Revert database-schema:add-mat-views from pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS mat_v_external_files_by_asset;
DROP MATERIALIZED VIEW IF EXISTS mat_v_suspicious_user_aggregation;

COMMIT;
