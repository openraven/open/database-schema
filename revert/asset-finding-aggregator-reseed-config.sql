-- Revert database-schema:asset-finding-aggregator-reseed-config from pg

BEGIN;

DROP TABLE IF EXISTS scanfindingaggregatorreseedconfig;

COMMIT;
