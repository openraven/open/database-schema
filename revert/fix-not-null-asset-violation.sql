-- Revert database-schema:fix-not-null-asset-violation from pg

BEGIN;

ALTER TABLE assetviolation ALTER COLUMN awsregion SET NOT NULL;

COMMIT;
