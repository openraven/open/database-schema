-- Revert database-schema:add-share-col-gdrive from pg

BEGIN;

ALTER TABLE googledrivefiles DROP COLUMN IF EXISTS fileissharedtouser;
ALTER TABLE googledrivefiles DROP COLUMN IF EXISTS sharinguser;

COMMIT;
