-- Revert database-schema:sdss-drop-scanfinding-assetid-constraint from pg

BEGIN;

alter table sdss_scanfinding 
  add constraint sdss_scanfinding_assetid_fkey 
  foreign key (assetid) 
  references awsrdsdbinstance (arn);

COMMIT;
