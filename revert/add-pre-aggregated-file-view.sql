-- Revert database-schema:add-pre-aggregated-file-view from pg

BEGIN;

DROP MATERIALIZED VIEW mat_v_gdrive_file_aggregate_data;

COMMIT;
