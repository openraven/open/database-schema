-- Revert database-schema:agg-updates from pg

BEGIN;

ALTER TABLE s3aggregate DROP COLUMN IF EXISTS assetid;
ALTER TABLE s3aggregate RENAME COLUMN accountid TO acccountid;

COMMIT;
