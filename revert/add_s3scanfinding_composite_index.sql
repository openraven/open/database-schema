-- Revert database-schema:add_s3scanfinding_composite_index from pg

BEGIN;

DROP INDEX IF EXISTS s3scanfinding_assetid_scantarget_scantargetchild_scannerjobid;

COMMIT;
