-- Revert database-schema:integrations-status-fields from pg

BEGIN;

ALTER TABLE webhook_integration_settings DROP COLUMN IF EXISTS status;
ALTER TABLE webhook_integration_settings DROP COLUMN IF EXISTS status_message;

ALTER TABLE slack_integration_settings DROP COLUMN IF EXISTS status ;
ALTER TABLE slack_integration_settings DROP COLUMN IF EXISTS status_message;

COMMIT;
