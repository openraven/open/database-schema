-- Revert database-schema:s3-scan-service-updatedat-remediation to pg
BEGIN;

    -- We do not need a rollback for the updated createdat data.

ROLLBACK;
