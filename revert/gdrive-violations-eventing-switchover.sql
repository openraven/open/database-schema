-- Revert database-schema:gdrive-violations-eventing-switchover from pg

BEGIN;

ALTER TABLE gdriveviolations DROP COLUMN IF EXISTS reseedid;

COMMIT;
