-- Revert database-schema:create_lambdaexecutionevent_table from pg

BEGIN;

DROP TABLE IF EXISTS lambdaexecutionevent;

COMMIT;
