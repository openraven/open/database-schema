-- Revert database-schema:index-scan-finding-for-generic-reseed from pg

BEGIN;

DROP INDEX IF EXISTS scanfinding_assetid;
DROP INDEX IF EXISTS scanfinding_dataclassid;

COMMIT;
