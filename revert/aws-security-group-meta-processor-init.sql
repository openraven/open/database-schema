-- Revert database-schema:aws-security-group-meta-processor-init from pg

BEGIN;

DROP TABLE IF EXISTS awsec2securitygroupconnectionmetadata;

COMMIT;
