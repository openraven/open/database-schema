-- Revert database-schema:add-ec2-column-to-scanfiletype from pg

BEGIN;

ALTER TABLE scanfiletype DROP COLUMN IF EXISTS maxec2filesize;

COMMIT;
