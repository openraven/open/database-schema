-- Revert database-schema:add-asset-to-file-count from pg

BEGIN;

DROP MATERIALIZED VIEW mat_v_gdrive_asset_file_count;
DROP INDEX mat_v_gdrive_file_aggregate_data_assetid;

COMMIT;
