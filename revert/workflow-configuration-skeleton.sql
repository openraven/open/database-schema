-- Revert database-schema:workflow-configuration-skeleton from pg

BEGIN;

DROP TABLE IF EXISTS workflowconfiguration;

COMMIT;
