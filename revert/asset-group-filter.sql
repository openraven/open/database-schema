-- Revert database-schema:asset-group-filter from pg

BEGIN;

DROP TABLE IF EXISTS assetgrouptoassets;

COMMIT;
