-- Revert database-schema:dbx-views from pg

BEGIN;
DROP VIEW IF EXISTS v_assetgroups;

DROP VIEW IF EXISTS v_policies;

DROP VIEW IF EXISTS v_rules;

DROP VIEW IF EXISTS v_policy_rule;

DROP VIEW IF EXISTS v_dataclass;

DROP VIEW IF EXISTS v_datacollection;

DROP VIEW IF EXISTS v_datacollection_dataclass;

DROP VIEW IF EXISTS v_issues;

DROP VIEW IF EXISTS v_assetviolation;

DROP VIEW IF EXISTS v_scannerjob;

DROP VIEW IF EXISTS v_scannerjobruns;

DROP VIEW IF EXISTS v_scanfinding;


COMMIT;
