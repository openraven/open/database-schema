-- Revert database-schema:violation-status-migration from pg

BEGIN;

DROP TABLE IF EXISTS rule_dataclasses;

ALTER TABLE assetviolations_removepolicyid DROP COLUMN IF EXISTS statuschangedbytype;
ALTER TABLE assetviolations_removepolicyid DROP CONSTRAINT IF EXISTS av_status_types;

WITH subquery AS (SELECT id,
                         status
                  FROM assetviolations_removepolicyid_status_copy
                  WHERE status IN (
                                   'CLOSED_AS_VERIFIED',
                                   'CLOSED_PENDING_VERIFICATION',
                                   'CLOSED_BY_USER',
                                   'CLOSED_AS_ASSET_DELETED',
                                   'IGNORED'
                      ))
UPDATE assetviolations_removepolicyid
SET status = subquery.status
FROM subquery
WHERE assetviolations_removepolicyid.id = subquery.id;

DROP TABLE IF EXISTS assetviolations_removepolicyid_status_copy;

-- Run same thing for legacy table
ALTER TABLE assetviolation DROP COLUMN IF EXISTS statuschangedbytype;

WITH subquery AS (SELECT id,
                         status
                  FROM assetviolation_status_copy
                  WHERE status IN (
                                   'CLOSED_AS_VERIFIED',
                                   'CLOSED_PENDING_VERIFICATION',
                                   'CLOSED_BY_USER',
                                   'CLOSED_AS_ASSET_DELETED',
                                   'IGNORED'
                      ))
UPDATE assetviolation
SET status = subquery.status
FROM subquery
WHERE assetviolation.id = subquery.id;

DROP TABLE IF EXISTS assetviolation_status_copy;

COMMIT;
