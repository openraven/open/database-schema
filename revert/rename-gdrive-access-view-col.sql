-- Revert database-schema:rename-gdrive-access-view-col from pg

BEGIN;

ALTER VIEW v_suspiciousgoogledrivefilepermissions
RENAME COLUMN anyonewithlink TO sharedbylink;

COMMIT;
