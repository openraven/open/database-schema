-- Revert database-schema:add-sdss-audit-log from pg

BEGIN;

DROP TABLE IF EXISTS sdss_finishedscans;
DROP INDEX IF EXISTS sdss_scanauditlogevent_jobrunid_lastid_idx;
DROP TABLE IF EXISTS sdss_scanauditlogevent;

COMMIT;
