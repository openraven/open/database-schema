-- Revert database-schema:change-credential-report-and-ssm-table-inheritance from pg

BEGIN;

DROP TABLE IF EXISTS extendawsiamusercredentialreport;
DROP TABLE IF EXISTS extendawsssminstance;

COMMIT;
