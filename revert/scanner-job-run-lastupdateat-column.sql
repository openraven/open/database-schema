-- Revert database-schema:scanner-job-run-lastupdateat-column to pg

ALTER TABLE scannerjobruns DROP COLUMN IF EXISTS lastupdateat;
