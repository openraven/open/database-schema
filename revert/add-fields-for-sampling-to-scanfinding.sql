-- Revert database-schema:add-fields-for-sampling-to-scanfinding from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS samplesize;
ALTER TABLE scanfinding DROP COLUMN IF EXISTS findingsfromsamplecount;
ALTER TABLE scanfinding DROP COLUMN IF EXISTS matchpercentage;
ALTER TABLE scanfinding DROP COLUMN IF EXISTS estimated;

COMMIT;
