-- Revert database-schema:change-btree-to-gin from pg

BEGIN;

DROP INDEX IF EXISTS mat_v_gdrive_file_aggregate_data_dataclasses_gin;
DROP INDEX IF EXISTS mat_v_gdrive_file_aggregate_data_rulesviolated_gin;
-- Do not recreate btree indices. They encounter errors during creation on certain customers.

COMMIT;
