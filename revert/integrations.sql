BEGIN;
DROP TABLE IF EXISTS email_integration_settings;
DROP TABLE IF EXISTS fourme_integration_settings;
DROP TABLE IF EXISTS slack_integration_settings;
DROP TABLE IF EXISTS webhook_integration_settings;
COMMIT;
