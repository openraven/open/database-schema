-- Revert database-schema:add-dmap-pkey from pg

BEGIN;

ALTER TABLE dmap DROP CONSTRAINT dmap_pkey;

COMMIT;
