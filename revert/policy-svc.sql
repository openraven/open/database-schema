BEGIN;
DROP TABLE IF EXISTS policies;

DROP TABLE IF EXISTS rules;

DROP TABLE IF EXISTS policy_rule;

DROP TABLE IF EXISTS issues;

DROP TABLE IF EXISTS issueremediation;

DROP TABLE IF EXISTS policyaudit;
DROP TABLE IF EXISTS assetviolation;
DROP TABLE IF EXISTS fileviolation;
DROP TABLE IF EXISTS fileviolationdataclass;
DROP TABLE IF EXISTS issue_assetviolation;
COMMIT;
