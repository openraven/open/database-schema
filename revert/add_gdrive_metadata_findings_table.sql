-- Revert database-schema:add_gdrive_metadata_findings_table from pg

BEGIN;

DROP TABLE IF EXISTS gdrivemetadatascanfinding;

COMMIT;
