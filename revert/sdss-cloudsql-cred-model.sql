-- Revert database-schema:sdss-cloudsql-cred-model from pg

BEGIN;

delete from datastorecredentialmodels where id = 'GC_SECRETS_MANAGER_PASSWORD';

COMMIT;