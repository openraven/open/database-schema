-- Revert database-schema:add-cluster-config-table from pg

BEGIN;

DROP TABLE IF EXISTS cluster.global_configuration;

DROP SCHEMA IF EXISTS cluster;

COMMIT;
