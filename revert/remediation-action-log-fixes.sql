-- Revert database-schema:remediation-action-log-fixes from pg

BEGIN;

ALTER TABLE remediationactionlog RENAME COLUMN exceptionstacktrace TO exceptionstack_trace;
ALTER TABLE remediationactionlog ADD COLUMN IF NOT EXISTS violationid UUID;
ALTER TABLE remediationactionlog ADD COLUMN IF NOT EXISTS violationtable TEXT;

COMMIT;
