-- Revert database-schema:rep-id-full-composite-metadata from pg

BEGIN;

ALTER TABLE metadatafinding REPLICA IDENTITY DEFAULT;
ALTER TABLE compositescanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
