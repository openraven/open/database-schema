-- Revert database-schema:sdss-snapshot-vpc from pg

BEGIN;

DROP TABLE IF EXISTS sdss_snapshot_vpc;

COMMIT;
