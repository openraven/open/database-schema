-- Revert database-schema:sdss-privatekey-cred-model from pg

BEGIN;

delete from datastorecredentialmodels where id = 'SECRETS_MANAGER_PRIVATE_KEY';
delete from datastorecredentialmodels where id = 'PARAMETER_STORE_PASSWORD';

COMMIT;
