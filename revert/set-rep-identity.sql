-- Revert database-schema:set-rep-identity from pg

BEGIN;

ALTER TABLE orvn.public.gdrivemetadatascanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
