-- Revert database-schema:add_schema_to_scanauditlogevent from pg

BEGIN;

ALTER TABLE scanauditlogevent DROP COLUMN IF EXISTS schema;
ALTER TABLE sdss_scanauditlogevent DROP COLUMN IF EXISTS schema;

COMMIT;
