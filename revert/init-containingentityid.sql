-- Revert database-schema:init-containingentityid from pg

BEGIN;

UPDATE globalassets SET containingentityid=null;

COMMIT;
