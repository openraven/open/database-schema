-- Revert database-schema:add-format-to-report-jobs from pg

BEGIN;

ALTER TABLE reportjob DROP COLUMN IF EXISTS reportformat;
ALTER TABLE reportjobrun DROP COLUMN IF EXISTS reportformat;

COMMIT;
