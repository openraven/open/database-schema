-- Revert database-schema:add-scanfinding-view-gdrive from pg

BEGIN;

DROP VIEW IF EXISTS v_gdrive_scanfinding_enriched;
DROP INDEX IF EXISTS suspiciousgoogledrivefilepermissions_filename;

COMMIT;
