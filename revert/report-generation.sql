-- Revert database-schema:report-generation from pg

BEGIN;

DROP TABLE IF EXISTS reportjobrun;
DROP TABLE IF EXISTS reportjob;

COMMIT;
