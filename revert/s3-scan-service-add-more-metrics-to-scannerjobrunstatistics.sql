-- Revert database-schema:s3-scan-service-add-more-metrics-to-scannerjobrunstatistics from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS numberofbuckets;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS currentbucketnumber;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS currentbucketname;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS skippedfilecount;

COMMIT;
