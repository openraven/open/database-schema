-- Revert database-schema:add-last-update-at-to-dmaplambda-ref-info from pg

BEGIN;

DROP INDEX IF EXISTS dmaplambdarefinfo_lastupdateat;
ALTER TABLE dmaplambdareferenceinfo DROP COLUMN IF EXISTS lastupdateat;
CREATE INDEX IF NOT EXISTS dmaplambdarefinfo_createdat ON dmaplambdareferenceinfo(createdat);

COMMIT;
