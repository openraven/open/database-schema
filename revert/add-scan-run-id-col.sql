-- Revert database-schema:add-scan-run-id-col from pg

BEGIN;

ALTER TABLE googledriveactivitywatermark DROP COLUMN IF EXISTS scannerjobrunid;

COMMIT;
