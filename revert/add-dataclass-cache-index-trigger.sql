-- Revert: add-dataclass-cache-index-trigger.sql

CREATE SEQUENCE IF NOT EXISTS custom_dataclasses_global_seq START 512 MAXVALUE 1024;

select setval('custom_dataclasses_global_seq',
              (SELECT max(val) + 1 FROM (
                                            SELECT COALESCE(MAX(cacheindex), 0) as val FROM dataclass
                                            UNION
                                            SELECT COALESCE(MAX(cacheindex), 0) as val FROM metadataclass
                                        ) a));

DROP TRIGGER IF EXISTS dataclass_cache_index_trg ON dataclass;
DROP TRIGGER IF EXISTS metadataclass_cache_index_trg ON metadataclass;
DROP FUNCTION IF EXISTS populate_cache_index_func;
