-- Revert database-schema:add-col-last-scan-ext from pg

BEGIN;

ALTER TABLE googledrivefiles DROP COLUMN IF EXISTS lastsuccessfulscan;
ALTER TABLE googledrivefiles DROP COLUMN IF EXISTS fileisexternal;
DROP INDEX IF EXISTS googledrivefiles_fileisexternal;

COMMIT;
