-- Revert database-schema:gdrive-files-fkey from pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions
    DROP CONSTRAINT fk_suspiciousgoogledrivefilepermissions_assetid_fileid;

ALTER TABLE googledrivestalefiles
    DROP CONSTRAINT fk_googledrivestalefiles_assetid_fileid;

COMMIT;
