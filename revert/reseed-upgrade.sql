-- Revert database-schema:reseed-upgrade from pg

BEGIN;

DROP TABLE IF EXISTS reseed_provider_upgrade_version;

COMMIT;
