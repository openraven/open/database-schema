-- Revert database-schema:add-dlp-rule-remediation from pg

BEGIN;

ALTER TABLE gdriverules DROP COLUMN IF EXISTS remediation;

COMMIT;
