BEGIN;

alter table public.sdss_scanfinding
    drop if exists schemaname,
    drop if exists tablename,
    drop if exists columnname;


alter table public.metadatatables
    drop if exists schemaname;

COMMIT;
