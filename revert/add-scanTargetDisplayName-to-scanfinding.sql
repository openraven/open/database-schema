-- Revert database-schema:add-scanTargetDisplayName-to-scanfinding from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS scantargetdisplayname;

COMMIT;
