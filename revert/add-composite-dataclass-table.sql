-- Revert database-schema:add-composite-dataclass-table from pg

BEGIN;

DROP TABLE IF EXISTS compositedataclass;

COMMIT;
