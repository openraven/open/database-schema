-- Revert database-schema:policy-indexes from pg

BEGIN;

DROP INDEX IF EXISTS assetviolation_assetid;
DROP INDEX IF EXISTS assetviolation_policyid;
DROP INDEX IF EXISTS assetviolation_ruleid;
DROP INDEX IF EXISTS assetviolation_status;

COMMIT;
