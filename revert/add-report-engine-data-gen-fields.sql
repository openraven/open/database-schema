-- Revert database-schema:add-report-engine-data-gen-fields from pg

BEGIN;

ALTER TABLE reportjobrun DROP COLUMN IF EXISTS operationkey;
ALTER TABLE reportjobrun DROP COLUMN IF EXISTS errorinfo;
ALTER TABLE reportjobrun DROP COLUMN IF EXISTS paused;
ALTER TABLE reportjobrun DROP COLUMN IF EXISTS currentstatestartcount;
ALTER TABLE reportjobrun DROP COLUMN IF EXISTS statechangeddate;
DROP SCHEMA IF EXISTS reportengine_intermediate_tables;

COMMIT;
