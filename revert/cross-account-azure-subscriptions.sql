-- Revert database-schema:cross-account-azure-subscriptions from pg

BEGIN;

DROP TABLE IF EXISTS azure_subscriptions;

COMMIT;
