-- Revert database-schema:add-data-classv2-table from pg

BEGIN;

DROP TRIGGER IF EXISTS dataclass_create ON dataclass;
DROP TRIGGER IF EXISTS metadataclass_create ON metadataclass;
DROP TRIGGER IF EXISTS compositedataclass_create ON compositedataclass;
DROP TRIGGER IF EXISTS dataclassv2_update ON dataclassv2;

DROP FUNCTION IF EXISTS dataclass_populate_dataclassv2;
DROP FUNCTION IF EXISTS dataclassv2_update;

DROP TABLE IF EXISTS dataclassv2;

ALTER TABLE compositedataclass INHERIT dataclass;

COMMIT;