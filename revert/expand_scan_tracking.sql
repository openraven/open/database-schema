-- Revert database-schema:expand_scan_tracking from pg

BEGIN;

DROP TABLE IF EXISTS scannerjobpage;

COMMIT;
