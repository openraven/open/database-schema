-- Revert database-schema:add-dmap-to-dbxviews from pg

BEGIN;

DROP VIEW IF EXISTS v_dmap;

COMMIT;
