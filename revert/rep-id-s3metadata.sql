-- Revert database-schema:rep-id-s3metadata from pg

BEGIN;

ALTER TABLE s3metadatascanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
