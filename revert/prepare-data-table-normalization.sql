-- Revert database-schema:prepare-data-table-normalization from pg

BEGIN;

-- We will not revert view drops. They are unused.
ALTER TABLE suspiciousgoogledrivefilepermissions ALTER COLUMN filename SET NOT NULL;
ALTER TABLE suspiciousgoogledrivefilepermissions ALTER COLUMN fileurl SET NOT NULL;
ALTER TABLE googledrivestalefiles ALTER COLUMN filename SET NOT NULL;
ALTER TABLE googledrivestalefiles ALTER COLUMN fileurl SET NOT NULL;

COMMIT;
