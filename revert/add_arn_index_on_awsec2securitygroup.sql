-- Revert database-schema:add_arn_index_on_awsec2securitygroup from pg

BEGIN;

DROP INDEX IF EXISTS awsec2securitygroup_arn;

COMMIT;
