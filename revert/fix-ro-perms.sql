-- Revert database-schema:fix-ro-perms from pg

BEGIN;

-- Read-only access to future new tables
ALTER DEFAULT PRIVILEGES IN SCHEMA cluster
    REVOKE SELECT ON TABLES FROM orvn_ro;
-- Read-only access to all existing tables
REVOKE SELECT ON ALL TABLES IN SCHEMA cluster FROM orvn_ro;
-- Basic usage privilege
REVOKE USAGE ON SCHEMA cluster FROM orvn_ro;

COMMIT;
