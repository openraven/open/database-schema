-- Revert database-schema:asset-workspace from pg

BEGIN;

DROP TABLE IF EXISTS assetworkspace;

COMMIT;
