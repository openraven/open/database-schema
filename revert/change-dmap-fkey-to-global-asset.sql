-- Revert database-schema:change-dmap-fkey-to-global-asset from pg

BEGIN;

ALTER TABLE dmap DROP CONSTRAINT dmap_assetid_fkey;

ALTER TABLE dmap ADD CONSTRAINT dmap_arn_fkey FOREIGN KEY(arn) REFERENCES awsec2instance(arn);

COMMIT;
