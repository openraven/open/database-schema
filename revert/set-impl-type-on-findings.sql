-- Revert database-schema:set-impl-type-on-findings from pg

BEGIN;

-- This migration is an UPDATE. We do not want to revert.

COMMIT;
