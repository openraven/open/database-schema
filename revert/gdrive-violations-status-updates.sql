-- Revert database-schema:gdrive-violations-status-updates from pg

BEGIN;

ALTER TABLE gdriveviolations DROP CONSTRAINT IF EXISTS gdrive_violations_status_types;

WITH subquery AS (SELECT id,
                         status
                  FROM gdriveviolations_status_copy
                  WHERE status IN (
                                   'CLOSED_AS_VERIFIED',
                                   'IGNORED'
                      ))
UPDATE gdriveviolations
SET status = subquery.status
FROM subquery
WHERE gdriveviolations.id = subquery.id;

DROP TABLE IF EXISTS gdriveviolations_status_copy;

COMMIT;
