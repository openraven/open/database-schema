-- Revert database-schema:gdrive-access-analysis-splunk from pg

BEGIN;

DROP VIEW IF EXISTS v_suspiciousgoogledrivefilepermissions;
DROP VIEW IF EXISTS v_suspiciousgoogleshareddrivepermissions;

COMMIT;
