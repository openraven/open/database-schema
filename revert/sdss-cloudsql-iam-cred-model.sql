-- Revert database-schema:sdss-cloudsql-iam-cred-model from pg

BEGIN;

delete from datastorecredentialmodels where id = 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN';

COMMIT;
