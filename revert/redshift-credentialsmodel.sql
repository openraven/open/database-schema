-- Revert database-schema:redshift-credentialsmodel from pg

BEGIN;

UPDATE datastorecredentialmodels
    SET assettypes = assettypes - 'AWS::Redshift::Cluster'
    WHERE id = 'SECRETS_MANAGER_PASSWORD' OR
          id = 'SECRETS_MANAGER_PASSWORD_IN_JSON' OR
          id = 'PARAMETER_STORE_PASSWORD';

COMMIT;
