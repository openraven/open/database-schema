-- Revert database-schema:add-auto-err-log from pg

BEGIN;

DROP TABLE IF EXISTS automations_error_audit;

COMMIT;
