-- Revert database-schema:remove-sdss-cloudsql-cred-model from pg

BEGIN;

INSERT INTO datastorecredentialmodels
(id, name, assettypes, credentialmodel)
VALUES ('GC_SECRETS_MANAGER_PASSWORD', 'Google Cloud Secrets Manager password',
        '["GCP::SQL::DBInstance"]',
        '{"resourceId" : "Path to Secrets Manager where the password is held (required)",
        "user": "The username to connect to (required)"}') ON CONFLICT DO NOTHING;

INSERT INTO datastorecredentialmodels
(id, name, assettypes, credentialmodel)
VALUES ('GC_IAM_CLOUD_SQL_ACCESS_TOKEN', 'IAM access token for Cloud Sql',
        '["GCP::SQL::DBInstance"]',
        '{"user": "The username to connect to (required)"}') ON CONFLICT DO NOTHING;

COMMIT;