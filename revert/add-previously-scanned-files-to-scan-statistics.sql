-- Revert database-schema:add-previously-scanned-files-to-scan-statistics.sql from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS previouslyscannedfilescount;

COMMIT;
