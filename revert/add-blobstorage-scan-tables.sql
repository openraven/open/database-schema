-- Revert database-schema:add-blobstorage-scan-tables.sql from pg

BEGIN;

DROP TABLE IF EXISTS blobstoragescanfinding;
DROP TABLE IF EXISTS blobstoragemetadatascanfinding;

COMMIT;