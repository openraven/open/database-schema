-- Revert database-schema:workflow-type-field from pg

BEGIN;

ALTER TABLE workflow_configuration DROP COLUMN workflow_type;

COMMIT;
