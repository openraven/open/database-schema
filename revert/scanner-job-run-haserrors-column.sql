-- Revert database-schema:scanner-job-run-haserrors-column to pg

ALTER TABLE scannerjobruns DROP COLUMN IF EXISTS haserrors;
