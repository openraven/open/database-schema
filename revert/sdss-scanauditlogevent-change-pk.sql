-- Revert database-schema:sdss-scanauditlogevent-change-pk from pg

BEGIN;

ALTER TABLE sdss_scanauditlogevent DROP CONSTRAINT IF EXISTS sdss_scanauditlogevent_pkey;

ALTER TABLE sdss_scanauditlogevent ALTER COLUMN schemaname DROP NOT null;

ALTER TABLE sdss_scanauditlogevent ALTER COLUMN tablename DROP NOT null;

UPDATE sdss_scanauditlogevent SET schemaname = null WHERE schemaname = '';

UPDATE sdss_scanauditlogevent SET tablename = null WHERE tablename = '';

ALTER TABLE sdss_scanauditlogevent ADD CONSTRAINT sdss_scanauditlogevent_pkey PRIMARY KEY (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state);

COMMIT;
