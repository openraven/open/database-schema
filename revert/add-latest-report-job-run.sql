-- Revert database-schema:add-latest-report-job-run from pg

BEGIN;

ALTER TABLE reportjob DROP COLUMN IF EXISTS latestrunid;

COMMIT;
