-- Revert database-schema:add-account-to-alias-table from pg

BEGIN;

DROP TABLE IF EXISTS accounttoaccountalias;

COMMIT;
