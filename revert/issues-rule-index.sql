-- Revert database-schema:issues-rule-index from pg

BEGIN;

DROP INDEX IF EXISTS issues_ruleid;

COMMIT;
