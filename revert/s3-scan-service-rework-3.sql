BEGIN;
ALTER TABLE awss3bucket DROP CONSTRAINT awss3bucket_arn_unique;
ALTER TABLE awss3bucket ADD CONSTRAINT unique_s3_bucketname UNIQUE (resourcename);

ALTER TABLE scanfinding DROP CONSTRAINT scanfinding_pkey;
ALTER TABLE scanfinding DROP CONSTRAINT scanfinding_assetid_fkey;

ALTER TABLE scanfinding ADD CONSTRAINT scanfinding_assetid_fkey FOREIGN KEY (assetid) REFERENCES awss3bucket(resourcename);
ALTER TABLE scanfinding ADD CONSTRAINT scanfinding_pkey PRIMARY KEY (assetid, dataclassid, scantarget);

DROP TABLE IF EXISTS s3scanfinding;
COMMIT;
