-- Revert database-schema:add-affectedlocations from pg

BEGIN;

ALTER TABLE issues DROP COLUMN affectedlocationssummary;

COMMIT;
