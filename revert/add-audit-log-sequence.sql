-- Revert database-schema:add-audit-log-sequence from pg

BEGIN;

DROP SEQUENCE IF EXISTS audit_log_event_sequence;

COMMIT;
