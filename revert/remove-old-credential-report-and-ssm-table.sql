-- Revert database-schema:remove-old-credential-report-and-ssm-table from pg

BEGIN;


CREATE TABLE IF NOT EXISTS awsusercredentialreport () INHERITS (aws);
CREATE TABLE IF NOT EXISTS awsssminstance () INHERITS (aws);

DROP TABLE IF EXISTS extendawsiamusercredentialreport;
DROP TABLE IF EXISTS extendawsssminstancessm;

COMMIT;
