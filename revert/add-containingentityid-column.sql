-- Revert database-schema:add-containingentityid-column from pg

BEGIN;

ALTER TABLE globalassets DROP COLUMN IF EXISTS containingentityid;

COMMIT;
