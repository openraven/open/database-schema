-- Revert database-schema:add-scannerjobrunstatistics-table from pg

BEGIN;

DROP TABLE IF EXISTS scannerjobrunstatistics;

COMMIT;
