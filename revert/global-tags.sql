-- Revert database-schema:global-tags from pg

BEGIN;

ALTER TABLE IF EXISTS globalassettags RENAME TO awstags;

COMMIT;
