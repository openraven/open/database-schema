BEGIN;

alter table sdss_scannerjobruns drop column if exists errorcode;
alter table sdss_scannerjobruns drop column if exists errordetails;

COMMIT;