-- Revert database-schema:index-finding-for-flagging from pg

BEGIN;

DROP INDEX IF EXISTS scanfinding_scantarget;
DROP INDEX IF EXISTS scanfinding_scantargetchild;

COMMIT;
