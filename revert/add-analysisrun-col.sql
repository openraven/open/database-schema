-- Revert database-schema:add-analysisrun-col from pg

BEGIN;

ALTER TABLE googledriveactivitywatermark DROP COLUMN IF EXISTS analysisscannerjobrunid;

COMMIT;
