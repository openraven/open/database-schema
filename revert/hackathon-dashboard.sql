-- Revert database-schema:hackathon-dashboard from pg

BEGIN;

DROP TABLE IF EXISTS dashboardwidgets;
DROP TABLE IF EXISTS discoveredassets;

COMMIT;
