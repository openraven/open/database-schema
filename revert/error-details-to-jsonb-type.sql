-- Revert database-schema:error-details-to-jsonb-type from pg

BEGIN;

ALTER TABLE scannerjobruns ALTER COLUMN errordetails TYPE text;

UPDATE scannerjobruns SET errordetails = cast(errordetails as jsonb)->0->>'errorMessage' WHERE errordetails <>'[]';
UPDATE scannerjobruns SET errordetails = NULL WHERE errordetails = '[]';

COMMIT;
