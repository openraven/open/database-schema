-- Revert database-schema:dlp_violations_jira_key from pg

BEGIN;

ALTER TABLE gdriveviolations DROP COLUMN IF EXISTS external_key;

COMMIT;
