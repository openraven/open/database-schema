-- Revert database-schema:fix-cloud-sql-resource-type from pg

BEGIN;

UPDATE datastorecredentialmodels
SET assettypes = '["GCP::SQL::DBInstance"]'
WHERE id IN ('GC_SECRETS_MANAGER_PASSWORD', 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN');

UPDATE datastorecredentialmodels
SET name = 'IAM access token for Cloud Sql'
WHERE id = 'GC_IAM_CLOUD_SQL_ACCESS_TOKEN';

COMMIT;
