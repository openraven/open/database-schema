-- Revert database-schema:gdrive-policy-event-debezium-change-streams from pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions REPLICA IDENTITY DEFAULT;
ALTER TABLE googledrivefiles REPLICA IDENTITY DEFAULT;
ALTER TABLE googledrivestalefiles REPLICA IDENTITY DEFAULT;

COMMIT;
