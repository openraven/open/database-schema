-- Revert database-schema:skip_cache_column_in_job from pg

BEGIN;

alter table scannerjob drop column if exists skipcache;

COMMIT;
