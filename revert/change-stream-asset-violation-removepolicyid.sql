-- Revert database-schema:change-stream-asset-violation-removepolicyid from pg

BEGIN;

ALTER TABLE assetviolations_removepolicyid REPLICA IDENTITY DEFAULT;

COMMIT;
