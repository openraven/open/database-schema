-- Revert database-schema:gdrive-violations-rule-index from pg

BEGIN;

DROP INDEX IF EXISTS gdriveviolations_gdriveruleid;
DROP INDEX IF EXISTS gdriverules_name_lower;

COMMIT;
