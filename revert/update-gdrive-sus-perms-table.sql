-- Revert database-schema:update-gdrive-sus-perms-table from pg

BEGIN;

DROP TABLE IF EXISTS suspiciousgoogledrivepermissions;
CREATE TABLE IF NOT EXISTS suspiciousgoogledrivepermissions (
    driveid TEXT NOT NULL,
    fileid TEXT NOT NULL,
    filename TEXT NOT NULL,
    username TEXT,
    fileurl TEXT NOT NULL,
    suspiciousemails JSONB DEFAULT '[]'::jsonb,
    groupemails JSONB DEFAULT '[]'::jsonb,
    sharedbylink BOOLEAN DEFAULT FALSE,
    updatedat TIMESTAMPTZ,
    PRIMARY KEY (driveid, fileid)
);

COMMIT;
