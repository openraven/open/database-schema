-- Revert database-schema:suspicious-file-perms-file-index from pg

BEGIN;

DROP INDEX IF EXISTS suspiciousgoogledrivefilepermissions_fileid;

COMMIT;
