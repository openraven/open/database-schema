-- Revert database-schema:sdss-cred-model-iam from pg

BEGIN;

delete from datastorecredentialmodels where id = 'IAM_REDSHIFT_ACCESS_PASSWORD';

COMMIT;
