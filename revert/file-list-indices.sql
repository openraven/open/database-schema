-- Revert database-schema:file-list-indices from pg

BEGIN;

DROP INDEX IF EXISTS googledrivefiles_mimetype;
DROP INDEX IF EXISTS gdrivescanfinding_dataclassid;
DROP INDEX IF EXISTS suspiciousemails_fts;
DROP INDEX IF EXISTS googledrivefiles_filename;

COMMIT;
