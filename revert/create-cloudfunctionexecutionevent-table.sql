-- Revert database-schema:create-cloudfunctionexecutionevent-table from pg

BEGIN;

DROP TABLE IF EXISTS cloudfunctionexecutionevent;

COMMIT;
