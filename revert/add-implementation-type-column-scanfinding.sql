-- Revert database-schema:add-implementation-type-column-scanfinding from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS implementationtype;

COMMIT;
