-- Revert database-schema:workflow-configuration-schema from pg

BEGIN;

DROP TABLE IF EXISTS rule_configuration;

DROP TABLE IF EXISTS workflow_configuration;

COMMIT;
