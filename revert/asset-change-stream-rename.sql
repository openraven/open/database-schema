-- Revert database-schema:asset-change-stream-rename from pg

BEGIN;

ALTER TABLE aggregatedassetfindings RENAME TO aggregatedassetmetrics;
ALTER TABLE aggregatedscanfindings RENAME TO aggregatedscanmetrics;

ALTER INDEX IF EXISTS aggregatedassetfindings_region RENAME TO aggregatedassetmetrics_region;
ALTER INDEX IF EXISTS aggregatedassetfindings_dataclass RENAME TO aggregatedassetmetrics_dataclass;
ALTER INDEX IF EXISTS aggregatedassetfindings_account RENAME TO aggregatedassetmetrics_account;

ALTER INDEX IF EXISTS aggregatedscanfindings_updateat RENAME TO aggregatedscanmetrics_updateat;
ALTER INDEX IF EXISTS aggregatedscanfindings_region RENAME TO aggregatedscanmetrics_region;
ALTER INDEX IF EXISTS aggregatedscanfindings_dataclass RENAME TO aggregatedscanmetrics_dataclass;
ALTER INDEX IF EXISTS aggregatedscanfindings_account RENAME TO aggregatedscanmetrics_account;

COMMIT;
