-- Revert database-schema:add-location-for-splunk-views from pg

BEGIN;
DROP VIEW IF EXISTS v_scanfinding;
create or replace view v_scanfinding
            (scannerjobid, scannerjobrunid, assetid, scantarget, scantargetversion, dataclassid, findingscount,
             createdat, updatedat, scantargetchild, findingslocations, findingsdata)
as
SELECT s.scannerjobid,
       s.scannerjobrunid,
       s.assetid,
       s.scantarget,
       s.scantargetversion,
       s.dataclassid,
       s.findingscount,
       s.createdat,
       s.updatedat,
       s.scantargetchild,
       s.findingslocations,
       cs.findingsdata
FROM scanfinding s
         LEFT JOIN compositescanfinding cs ON s.assetid = cs.assetid AND s.scantarget = cs.scantarget AND
                                              s.scantargetchild = cs.scantargetchild AND s.dataclassid = cs.dataclassid;



DROP VIEW IF EXISTS v_assetviolation;
create or replace view v_assetviolation
            (id, datecreated, updatedat, resourcename, policyid, ruleid, status, awsaccountid, awsregion,
             objectcount) as
SELECT av.id,
       av.datecreated,
       av.updatedat,
       av.resourcename,
       av.policyid,
       av.ruleid,
       av.status,
       av.awsaccountid,
       av.awsregion,
       av.objectcount
FROM assetviolation av
         JOIN policies p ON av.policyid = p.id
         JOIN rules r ON av.ruleid = r.id;

COMMIT;