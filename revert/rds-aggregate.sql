-- Revert database-schema:rds-aggregate from pg

BEGIN;

DROP TABLE IF EXISTS rdsaggregate;

COMMIT;
