-- Revert database-schema:aws-assets-view-pkey-fix from pg

BEGIN;

    ALTER TABLE awsassetsview DROP CONSTRAINT IF EXISTS awsassetsview_pkey;

COMMIT;
