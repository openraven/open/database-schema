-- Revert database-schema:ddr-change-stream from pg

BEGIN;

ALTER TABLE ddr_alerts REPLICA IDENTITY DEFAULT;

COMMIT;
