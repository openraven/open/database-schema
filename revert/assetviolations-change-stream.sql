-- Revert database-schema:violations-change-stream from pg

BEGIN;

ALTER TABLE assetviolation REPLICA IDENTITY DEFAULT;

COMMIT;
