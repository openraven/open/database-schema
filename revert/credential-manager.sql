-- Revert database-schema:credential-manager from pg

BEGIN;

DROP TABLE IF EXISTS apikeyextension;

COMMIT;
