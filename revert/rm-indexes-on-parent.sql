-- Revert database-schema:rm-indexes-on-parent from pg

BEGIN;

-- Indices on parent tables are useless. We do not want to revert this.

COMMIT;
