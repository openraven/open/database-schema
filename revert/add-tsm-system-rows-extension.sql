-- Revert database-schema:add-tsm-system-rows-extension from pg

BEGIN;

DROP EXTENSION IF EXISTS "tsm_system_rows";

COMMIT;
