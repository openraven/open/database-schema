-- Revert database-schema:jira_integration_track_jira_link from pg

BEGIN;

ALTER TABLE assetviolation DROP COLUMN IF EXISTS external_link;
ALTER TABLE assetviolation DROP COLUMN IF EXISTS external_key;

COMMIT;
