-- Revert database-schema:reaper from pg

BEGIN;

ALTER TABLE s3scanfinding DROP CONSTRAINT IF EXISTS s3scanfinding_assetid_fkey;
ALTER TABLE s3scanfinding ADD CONSTRAINT s3scanfinding_assetid_fkey FOREIGN KEY(assetid) REFERENCES awss3bucket(arn);

ALTER TABLE dmap DROP CONSTRAINT IF EXISTS dmap_arn_fkey;
ALTER TABLE dmap ADD CONSTRAINT dmap_arn_fkey FOREIGN KEY(arn) REFERENCES awsec2instance(arn);
COMMIT;
