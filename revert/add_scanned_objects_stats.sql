-- Revert database-schema:add_scanned_objects_stats from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS totalobjects;
ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS scannedobjects;

COMMIT;
