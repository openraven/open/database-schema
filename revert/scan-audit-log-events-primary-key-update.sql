-- Revert database-schema:scan-audit-log-events-primary-key-update from pg

BEGIN;

alter table scanauditlogevent drop constraint pk_scanauditlogevent ;
update scanauditlogevent set dataclassid = null where dataclassid = '00000000-0000-0000-0000-000000000000';
alter table scanauditlogevent add primary key (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state);

COMMIT;
