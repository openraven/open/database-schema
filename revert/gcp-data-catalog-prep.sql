-- Revert database-schema:gcp-data-catalog-prep from pg

BEGIN;

ALTER TABLE cloudstoragescanfinding REPLICA IDENTITY DEFAULT;
ALTER TABLE aggregatedassetfindings DROP COLUMN IF EXISTS source;
ALTER TABLE aggregatedscanfindings DROP COLUMN IF EXISTS source;

COMMIT;
