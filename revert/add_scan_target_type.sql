-- Revert database-schema:add_scan_target_type from pg

BEGIN;

alter table scannerjob drop column if exists scantargettype;

COMMIT;
