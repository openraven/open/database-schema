-- Revert database-schema:auto-scan from pg

BEGIN;

DROP TABLE IF EXISTS autoscan;

COMMIT;
