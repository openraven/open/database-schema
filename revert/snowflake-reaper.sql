-- Revert database-schema:snowflake-reaper from pg

BEGIN;

ALTER TABLE globalassets DROP COLUMN IF EXISTS manuallydiscovered;

COMMIT;
