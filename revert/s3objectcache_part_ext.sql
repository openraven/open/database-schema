-- Revert database-schema:s3objectcache_part_ext from pg

BEGIN;

DROP EXTENSION pg_partman CASCADE;
DROP SCHEMA partmam CASCADE;

COMMIT;
