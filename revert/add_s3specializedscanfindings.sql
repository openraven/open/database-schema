-- Revert database-schema:add_s3specializedscanfindings from pg

BEGIN;

DROP INDEX IF EXISTS s3specfinding_assetid_scantarget_scantargetchild_scannerjobid;
DROP TABLE IF EXISTS s3specializeddatascanfinding;

COMMIT;
