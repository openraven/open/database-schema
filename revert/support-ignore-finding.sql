-- Revert database-schema:support-ignore-finding from pg

BEGIN;

DROP TABLE IF EXISTS auditlog;
ALTER TABLE scanfinding DROP COLUMN IF EXISTS ignored;

COMMIT;
