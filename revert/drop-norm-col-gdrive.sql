-- Revert database-schema:drop-norm-col-gdrive from pg

BEGIN;

ALTER TABLE googledrivestalefiles ADD COLUMN IF NOT EXISTS driveid TEXT;
ALTER TABLE googledrivestalefiles ADD COLUMN IF NOT EXISTS filename TEXT;
ALTER TABLE googledrivestalefiles ADD COLUMN IF NOT EXISTS username TEXT;
ALTER TABLE googledrivestalefiles ADD COLUMN IF NOT EXISTS fileurl TEXT;
ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS driveid TEXT;
ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS filename TEXT;
ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS username TEXT;
ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS fileurl TEXT;
ALTER TABLE suspiciousgoogledrivefilepermissions ADD COLUMN IF NOT EXISTS permissions JSONB;

COMMIT;
