-- Revert database-schema:support-scanning-sql-files from pg

BEGIN;

UPDATE scanfiletype SET extensions = extensions - 'sql' WHERE mimetype='text/plain';

DELETE FROM scanfiletype WHERE mimetype='text/x-sql';

COMMIT;
