-- Revert database-schema:assest-list-tags-filtering from pg

BEGIN;

DROP TABLE IF EXISTS awstags;

COMMIT;
