-- Revert database-schema:add-dataclassvariant-table from pg

BEGIN;

DROP TABLE IF EXISTS dataclassvariant;

COMMIT;