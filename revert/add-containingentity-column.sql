-- Revert database-schema:add-containingentity-column from pg

BEGIN;

ALTER TABLE globalassets DROP COLUMN IF EXISTS containingentity;

COMMIT;
