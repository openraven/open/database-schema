-- Revert database-schema:add-deriveddatablob-index from pg

BEGIN;

DROP INDEX IF EXISTS globalassets_deriveddatablob_severitycount;
ALTER TABLE globalassets DROP COLUMN IF EXISTS seq;

COMMIT;
