-- Revert database-schema:remediation-action-log from pg

BEGIN;

DROP TABLE IF EXISTS remediationactionlog;

COMMIT;
