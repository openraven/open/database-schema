-- Revert database-schema:remediate-scanner-job-run-state from pg

BEGIN;

UPDATE scannerjobruns
SET status = 'DISABLED'
WHERE status = 'CREATED';

COMMIT;
