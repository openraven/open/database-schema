-- Revert database-schema:recreate_caching_table from pg

BEGIN;

DROP TABLE IF EXISTS cached.s3objects CASCADE;
DELETE FROM partman.part_config WHERE parent_table = 'cached.s3objects';
DROP TABLE IF EXISTS partman.template_cached_s3objects;

COMMIT;
