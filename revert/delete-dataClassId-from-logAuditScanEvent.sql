-- Revert database-schema:delete-dataClassId-from-logAuditScanEvent from pg

BEGIN;

ALTER TABLE scanauditlogevent ADD COLUMN IF NOT EXISTS dataclassid UUID DEFAULT '00000000-0000-0000-0000-000000000000';
ALTER TABLE scanauditlogevent DROP CONSTRAINT IF EXISTS scanauditlogevent_pkey;
ALTER TABLE scanauditlogevent ADD PRIMARY KEY (resourceid, scantarget, childtarget, scanjobrunid, scanjobid, state, dataclassid);

COMMIT;
