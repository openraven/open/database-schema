-- Revert database-schema:scanner-job-page-lambda-metadata-columns.sql to pg

ALTER TABLE scannerjobpage DROP COLUMN IF EXISTS lambdaregion;
ALTER TABLE scannerjobpage DROP COLUMN IF EXISTS lambdaarchitecture;
ALTER TABLE scannerjobpage DROP COLUMN IF EXISTS lambdamemory;