-- Revert database-schema:asset-list-redesign from pg

BEGIN;
DROP EXTENSION IF EXISTS "pg_trgm";
DROP TABLE IF EXISTS awsassetsview;
DROP TABLE IF EXISTS assetsview;
DROP TABLE IF EXISTS awsbackupplantags;
DROP TABLE IF EXISTS awsbackupjobsassetsview;

COMMIT;
