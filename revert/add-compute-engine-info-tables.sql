-- Revert database-schema:add-compute-engine-info-tables from pg

BEGIN;

DROP TABLE IF EXISTS ec2priceinfo;
DROP TABLE IF EXISTS ebspriceinfo;
DROP TABLE IF EXISTS ebsvolumeinfo;
DROP TABLE IF EXISTS computeengineinfo;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS computeengineduration;
ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS computeenginecost;

COMMIT;
