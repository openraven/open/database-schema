-- Revert database-schema:add-index-column-to-dataclasses.sql to pg

BEGIN;

ALTER TABLE dataclass DROP COLUMN IF EXISTS cacheindex;
ALTER TABLE metadataclass DROP COLUMN IF EXISTS cacheindex;

DROP SEQUENCE IF EXISTS custom_dataclasses_global_seq;

COMMIT;