-- Revert database-schema:add-indexes-for-delete-scanjobrun from pg

BEGIN;

DROP INDEX IF EXISTS scannerjob_lastscannerjobrun_index;

DROP INDEX IF EXISTS scannerjobpage_jobrunid_index;

DROP INDEX IF EXISTS scanfinding_scannerjobrunid_index;

DROP INDEX IF EXISTS scanfinding_scannerjobid_index;

COMMIT;
