-- Revert database-schema:gcp-reaper from pg

BEGIN;

ALTER TABLE globalassets DROP COLUMN IF EXISTS markedfordelete;
ALTER TABLE globalassets ADD COLUMN IF NOT EXISTS markedfordelete boolean DEFAULT false;
CREATE INDEX globalassets_markedfordelete ON globalassets(markedfordelete);

COMMIT;
