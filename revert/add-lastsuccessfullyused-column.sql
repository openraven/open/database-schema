-- Revert database-schema:add-lastsuccessfullyused-column from pg

BEGIN;

ALTER TABLE datastoreresourceaccessmapping DROP COLUMN IF EXISTS lastsuccessfullyused;

COMMIT;
