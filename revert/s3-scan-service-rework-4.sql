-- Revert database-schema:s3-scan-service-rework-4 from pg

BEGIN;

ALTER TABLE s3scanfinding DROP CONSTRAINT s3scanfinding_pkey;

ALTER TABLE scanfinding DROP COLUMN scantargetchild;

ALTER TABLE scanfinding DROP COLUMN findingslocations;

ALTER TABLE s3scanfinding ADD PRIMARY KEY (assetid, dataclassid, scantarget);

COMMIT;
