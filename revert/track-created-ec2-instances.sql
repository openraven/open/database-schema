-- Revert database-schema:track-created-ec2-instances from pg

BEGIN;

alter table if exists sdss_snapshot_vpc
    drop column if exists has_created_db_resources;

drop table if exists scannerjobruns_with_ec2;

COMMIT;
