-- Revert database-schema:rework-reseed from pg

BEGIN;

    ALTER TABLE aggregatedassetfindings DROP COLUMN reseededat;

    CREATE TABLE IF NOT EXISTS scanfindingaggregatorreseedconfig (
         id SERIAL PRIMARY KEY NOT NULL,
         reseedtimestamp timestamp with time zone
    );

    INSERT INTO scanfindingaggregatorreseedconfig (id, reseedtimestamp) VALUES (1, NULL);

COMMIT;
