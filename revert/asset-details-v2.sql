-- Revert database-schema:asset-details-v2 from pg

BEGIN;

DROP TABLE IF EXISTS assetdetails;

COMMIT;
