-- Revert database-schema:add-perm-col-shared-drive from pg

BEGIN;

ALTER TABLE suspiciousgoogleshareddrivepermissions DROP COLUMN IF EXISTS permissions;

COMMIT;
