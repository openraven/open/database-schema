-- Revert database-schema:splunk-data-class-v2-view from pg

BEGIN;

DROP VIEW IF EXISTS v_dataclass;
CREATE OR replace VIEW v_dataclass AS
SELECT id,
       name,
       version,
       owner,
       managed,
       refid,
       description,
       status,
       category,
       createdat,
       updatedat
FROM dataclass;

COMMIT;
