-- Revert database-schema:add-resource-name-to-sg-connections from pg

BEGIN;

DROP TABLE IF EXISTS awssecuritygroupconnections;

-- Create old version of table
CREATE TABLE IF NOT EXISTS awssecuritygroupconnections
(
    seq               bigserial NOT NULL,
    sgarn             text NOT NULL,
    awsregion         text NOT NULL,
    cidr              text NOT NULL,
    cidrowner         text,
    cidrownernetrange text,
    cidrlabel         text,
    isbadcidr         boolean NOT NULL,
    description       text,
    fromport          bigint,
    toport            bigint,
    ipprotocol        text,
    egress            boolean NOT NULL,
    isexternal        boolean NOT NULL,
    updatediso        timestamptz
);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_seq ON awssecuritygroupconnections(seq);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_sgarn ON awssecuritygroupconnections(sgarn);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_awsregion ON awssecuritygroupconnections(awsregion);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidr ON awssecuritygroupconnections(cidr);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrowner ON awssecuritygroupconnections(cidrowner);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrlabel ON awssecuritygroupconnections(cidrlabel);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_cidrownernetrange ON awssecuritygroupconnections(cidrownernetrange);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_egress ON awssecuritygroupconnections(egress);
CREATE INDEX IF NOT EXISTS awssecuritygroupconnections_updatediso ON awssecuritygroupconnections(updatediso);



COMMIT;
