-- Revert database-schema:add-sus-drive-file-perms-table from pg

BEGIN;

-- Dev tables, no data
DROP TABLE IF EXISTS suspiciousgoogledrivefilepermissions;
DROP TABLE IF EXISTS suspiciousgoogleshareddrivepermissions;
CREATE TABLE IF NOT EXISTS suspiciousgoogledrivepermissions (
    assetid TEXT NOT NULL,
    fileid TEXT NOT NULL,
    driveid TEXT,
    filename TEXT NOT NULL,
    username TEXT,
    fileurl TEXT NOT NULL,
    suspiciousemails JSONB DEFAULT '[]'::jsonb,
    groupemails JSONB DEFAULT '[]'::jsonb,
    sharedbylink BOOLEAN DEFAULT FALSE,
    updatedat TIMESTAMPTZ,
    PRIMARY KEY(assetid, fileid)
);

COMMIT;
