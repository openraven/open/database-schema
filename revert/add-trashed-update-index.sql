-- Revert database-schema:add-trashed-update-index from pg

BEGIN;

DROP INDEX IF EXISTS googledrivefiles_updatedat;
DROP INDEX IF EXISTS googledrivefiles_trashed;

COMMIT;
