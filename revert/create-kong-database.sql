-- Revert database-schema:create-kong-database from pg

DROP DATABASE kong;
BEGIN;
    REVOKE kong FROM orvn_superuser;
    DROP ROLE kong;
COMMIT;
