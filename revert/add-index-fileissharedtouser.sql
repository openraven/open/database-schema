-- Revert database-schema:add-index-fileissharedtouser from pg

BEGIN;

DROP INDEX IF EXISTS googledrivefiles_fileissharedtouser;

COMMIT;
