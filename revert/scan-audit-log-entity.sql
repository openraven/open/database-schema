-- Revert database-schema:scan-audit-log-entity from pg

BEGIN;

DROP TABLE IF EXISTS scanauditlogevent;

COMMIT;
