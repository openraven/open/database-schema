-- Revert database-schema:lambda-duration-field-per-job-run from pg

BEGIN;

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS lambdaduration;
ALTER TABLE scannerjob DROP COLUMN IF EXISTS maxscancost;
ALTER TABLE scannerjob DROP COLUMN IF EXISTS maxlambdaduration;


COMMIT;
