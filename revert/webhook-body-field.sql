-- Revert database-schema:webhook-body-field from pg

BEGIN;

ALTER TABLE orvn.public.webhook_integration_settings DROP COLUMN IF EXISTS body;

COMMIT;
