-- Revert database-schema:duplicate-issue-assetviolation from pg

BEGIN;

DROP TABLE IF EXISTS issue_assetviolation;
ALTER TABLE issue_assetviolation_dupe_copy RENAME TO issue_assetviolation;

COMMIT;
