-- Revert database-schema:redshift_iam_auth_update from pg

BEGIN;

delete from datastorecredentialmodels where id = 'IAM_REDSHIFT_ACCESS_PASSWORD';

COMMIT;
