-- Revert database-schema:add-backfill-config from pg

BEGIN;

DELETE FROM cluster.global_configuration WHERE key = 'dlp.backfill-enable';

COMMIT;
