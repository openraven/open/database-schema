BEGIN;

ALTER TABLE ddr_state_store RENAME COLUMN source_log TO event_source;

COMMIT;