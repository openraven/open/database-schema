-- Revert database-schema:unify-integration-settings from pg

BEGIN;

DROP TABLE IF EXISTS eventbridge_integration_settings_v2, jira_integration_settings_v2,
    webhook_integration_settings_v2, slack_integration_settings_v2, integrations;

COMMIT;
