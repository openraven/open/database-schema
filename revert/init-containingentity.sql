-- Revert database-schema:init-containingentity from pg

BEGIN;

UPDATE globalassets SET containingentity=null;

COMMIT;
