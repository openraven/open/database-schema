-- Revert database-schema:add_dataclassversion_to_scanfindings from pg

BEGIN;

ALTER TABLE scanfinding DROP COLUMN IF EXISTS dataclassversion;

COMMIT;
