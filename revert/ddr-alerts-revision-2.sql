BEGIN;

ALTER TABLE ddr_alerts DROP COLUMN alert_parameters;
ALTER TABLE ddr_alerts ADD COLUMN event_parameters jsonb;

COMMIT;