-- Revert database-schema:add-key-ext-scan from pg

BEGIN;

DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.scan-externally-owned-enabled';

COMMIT;
