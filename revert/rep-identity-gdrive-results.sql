-- Revert database-schema:rep-identity-gdrive-results from pg

BEGIN;

ALTER TABLE gdrivescanfinding REPLICA IDENTITY DEFAULT;

COMMIT;
