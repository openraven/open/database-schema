-- Revert database-schema:PROD-2603 from pg

BEGIN;


ALTER ROLE orvn_superuser WITH NOCREATEROLE NOCREATEDB;

COMMIT;
