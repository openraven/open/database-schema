-- Revert database-schema:remove-awss3bucket-foreign-keys from pg

BEGIN;

ALTER TABLE s3scanfinding ADD CONSTRAINT s3scanfinding_assetid_fkey
    FOREIGN KEY(assetid) REFERENCES awss3bucket(arn)
        ON DELETE CASCADE;
ALTER TABLE s3metadatascanfinding ADD CONSTRAINT metadatafinding_assetid_fkey FOREIGN KEY(assetid) REFERENCES awss3bucket(arn);

COMMIT;
