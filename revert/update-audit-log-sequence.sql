-- Revert database-schema:update-audit-log-sequence from pg

BEGIN;

ALTER SEQUENCE IF EXISTS audit_log_event_sequence INCREMENT BY 1;

COMMIT;
