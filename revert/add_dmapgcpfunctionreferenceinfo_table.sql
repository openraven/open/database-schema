-- Revert database-schema:add_dmapgcpfunctionreferenceinfo_table from pg

BEGIN;

DROP INDEX IF EXISTS gcpfunctionreferenceinfo_lastupdateat;
DROP TABLE IF EXISTS gcpfunctionreferenceinfo;

COMMIT;
