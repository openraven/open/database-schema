-- Revert database-schema:dlp_violations_jira_link from pg

BEGIN;

ALTER TABLE gdriveviolations DROP COLUMN IF EXISTS external_link;

COMMIT;
