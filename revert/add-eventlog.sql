-- Revert database-schema:add-eventslog from pg

BEGIN;

DROP table if exists eventlog;

COMMIT;
