-- Revert database-schema:add-updated-col-dataclassv2 from pg

BEGIN;

ALTER TABLE dataclassv2 DROP COLUMN IF EXISTS updatedat;
DROP INDEX IF EXISTS dataclassv2_name;

COMMIT;
