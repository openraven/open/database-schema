-- Revert database-schema:bucket-resume-info from pg

BEGIN;

DROP TABLE IF EXISTS bucketresumeinfo;

COMMIT;
