BEGIN;

ALTER TABLE gdriverules DROP COLUMN IF EXISTS ruletype;

DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.customFindingLimit' and value = '10';
DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.personalFindingLimit' and value = '100';
DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.healthFindingLimit' and value = '10';
DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.financialFindingLimit' and value = '10';
DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.devSecretFindingLimit' and value = '10';
DELETE FROM cluster.global_configuration WHERE key = 'google-workspace.genericFindingLimit' and value = '10';

COMMIT;