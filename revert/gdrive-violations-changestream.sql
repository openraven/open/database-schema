-- Revert database-schema:gdrive-violations-changestream from pg

BEGIN;

ALTER TABLE orvn.public.gdriveviolations REPLICA IDENTITY DEFAULT;

COMMIT;
