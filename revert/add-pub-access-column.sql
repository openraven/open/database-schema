-- Revert database-schema:add-pub-access-column from pg

BEGIN;

ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS publiclyaccessible;

COMMIT;
