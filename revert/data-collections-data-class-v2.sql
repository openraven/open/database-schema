-- Revert database-schema:data-collections-data-class-v2 from pg

BEGIN;

ALTER TABLE IF EXISTS datacollection_dataclass RENAME TO datacollection_dataclass_old;
CREATE TABLE IF NOT EXISTS datacollection_dataclass
(
    datacollectionid   UUID NOT NULL REFERENCES datacollection(id) ON DELETE CASCADE ON UPDATE CASCADE,
    dataclassid        UUID NOT NULL REFERENCES dataclass(id) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO datacollection_dataclass(
    datacollectionid,
    dataclassid
) SELECT datacollection_dataclass_old.datacollectionid, datacollection_dataclass_old.dataclassid
  FROM datacollection_dataclass_old
  INNER JOIN dataclass ON dataclass.id = datacollection_dataclass_old.dataclassid;

CREATE OR replace VIEW v_datacollection_dataclass AS
SELECT datacollectionid, dataclassid
FROM datacollection_dataclass;

DROP TABLE IF EXISTS datacollection_dataclass_old;

COMMIT;
