-- Revert database-schema:scanner-job-run-statistics-errorscount-and-findingscount-columns from pg

ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS errorscount;
ALTER TABLE scannerjobrunstatistics DROP COLUMN IF EXISTS findingscount;