-- Revert database-schema:remove_max_lambda_duration_column from pg

BEGIN;

ALTER TABLE scannerjob ADD COLUMN IF NOT EXISTS maxlambdaduration BIGINT DEFAULT 0 NOT NULL;

COMMIT;
