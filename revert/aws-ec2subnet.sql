-- Revert database-schema:aws-ec2subnet from pg

BEGIN;

DROP TABLE IF EXISTS awsec2subnet;

COMMIT;
