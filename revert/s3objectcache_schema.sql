-- Revert database-schema:s3objectcache_schema from pg

BEGIN;

DROP schema cached CASCADE;

COMMIT;
