-- Revert database-schema:index-assetid-mapping-table from pg

BEGIN;

DROP INDEX IF EXISTS datastoreresourceaccessmapping_assetid;

COMMIT;
