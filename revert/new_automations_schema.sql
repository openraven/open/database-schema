-- Revert database-schema:new_automations_schema from pg

BEGIN;

DROP TABLE IF EXISTS automation_rule, automation_consequence;

COMMIT;
