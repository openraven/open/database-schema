-- Revert database-schema:add-assetid-to-gdriveviolations from pg

BEGIN;

DROP VIEW v_gdriveviolations;

-- Reverts back to what's defined in fix-vio-view-col-name.sql
CREATE VIEW v_gdriveviolations AS SELECT v.id,
     v.nodeid,
     v.nodename,
     v.nodetype,
     v.updatedat,
     v.datecreated,
     v.gdriveruleid,
     r.name AS rulename,
     r.severity,
     v.status,
     v.previousstatus,
     v.supplementalinformation,
     gf.mimetype
FROM gdriveviolations v
       INNER JOIN gdriverules r ON v.gdriveruleid = r.id
       INNER JOIN googledrivefiles gf ON gf.fileid=v.nodeid;

DROP INDEX IF EXISTS gdriveviolations_assetid;
DROP INDEX IF EXISTS gdriveviolations_status;
DROP INDEX IF EXISTS gdriveviolations_assetid_nodeid;

ALTER TABLE gdriveviolations DROP COLUMN assetid;

COMMIT;