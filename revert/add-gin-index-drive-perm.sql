-- Revert database-schema:add-gin-index-drive-perm from pg

BEGIN;

DROP INDEX IF EXISTS suspiciousgoogleshareddrivepermissions_permissions_gin;

COMMIT;
