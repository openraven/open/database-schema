-- Revert database-schema:add-gdrive-sus-perms-table from pg

BEGIN;

DROP TABLE IF EXISTS suspiciousgoogledrivepermissions;

COMMIT;
