-- Revert database-schema:rds_iam_cred_model from pg

BEGIN;

delete from datastorecredentialmodels where id = 'IAM_RDS_ACCESS_TOKEN';

COMMIT;
