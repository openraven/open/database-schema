-- Revert database-schema:add-function-for-dcollect from pg

BEGIN;

DROP FUNCTION IF EXISTS calculate_word_frequencies(text[]);

COMMIT;