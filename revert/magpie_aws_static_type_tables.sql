-- Revert database-schema:magpie_aws_static_type_tables from pg

BEGIN;

drop table IF EXISTS awsconfigurationrecorder ;
drop table IF EXISTS awsguarddutydetector ;
drop table IF EXISTS awsiamaccount ;
drop table IF EXISTS awsiamcredentialsreport ;
drop table IF EXISTS awslocationgeofencecollection ;
drop table IF EXISTS awslocationmap ;
drop table IF EXISTS awslocationplaceindex ;
drop table IF EXISTS awslocationroutecalculator ;
drop table IF EXISTS awslocationtracker ;
drop table IF EXISTS awsrdsdbsnapshot ;
drop table IF EXISTS awssecurityhubstandardsubscription ;
drop table IF EXISTS awssnssubscription ;
drop table IF EXISTS awssnstopic ;
drop table IF EXISTS awswatchalarm ;
drop table IF EXISTS awswatchdashboard ;
drop table IF EXISTS awswatchlogsmetricfilter ;
drop table IF EXISTS awswatchloggroup ;
drop table IF EXISTS awsssminstance;

COMMIT;
