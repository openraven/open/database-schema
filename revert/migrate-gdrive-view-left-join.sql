-- Revert database-schema:migrate-gdrive-view-left-join from pg

BEGIN;

DROP VIEW v_gdrive_dash;

CREATE VIEW v_gdrive_dash AS SELECT gs.filename, ga.namelabel AS drivename, gp.username, gp.sharedbylink AS anyonewithlink,
                                    gp.suspiciousemails, gs.filecreatedat, gs.fileupdatedat, gs.fileaccessedat,
                                    json_agg(gr.name) AS rulesviolated, gb.configuration->'restrictions' AS restrictions
                             FROM googledrivestalefiles gs
                                      INNER JOIN globalassets ga ON ga.assetid=gs.assetid
                                      INNER JOIN globalassetsblob gb ON gb.assetid=gs.assetid
                                      INNER JOIN suspiciousgoogledrivefilepermissions gp ON gp.fileid=gs.fileid AND gp.username=gs.username
                                      INNER JOIN gdriveviolations gv ON gv.nodeid=gp.fileid
                                      INNER JOIN gdriverules gr ON gv.gdriveruleid=gr.id
                             GROUP BY gs.filename, ga.namelabel, gp.username, gp.sharedbylink, gp.suspiciousemails, gs.filecreatedat,
                                      gs.fileupdatedat, gs.fileaccessedat, gb.configuration;

ALTER TABLE suspiciousgoogledrivefilepermissions DROP COLUMN IF EXISTS permissions;

COMMIT;
