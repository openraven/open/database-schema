-- Revert database-schema:metric-tables from pg

BEGIN;

drop table resourcetypeaggregate;
drop table violationaggregate;
drop table s3aggregate;

COMMIT;
