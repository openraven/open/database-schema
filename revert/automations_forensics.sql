-- Revert database-schema:automations_forensics from pg

BEGIN;

ALTER TABLE rule_configuration DROP COLUMN IF EXISTS created_at;
ALTER TABLE rule_configuration DROP COLUMN IF EXISTS updated_at;
ALTER TABLE rule_configuration DROP COLUMN IF EXISTS created_by;
ALTER TABLE rule_configuration DROP COLUMN IF EXISTS updated_by;

COMMIT;
