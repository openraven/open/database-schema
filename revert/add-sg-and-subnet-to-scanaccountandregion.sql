-- Revert database-schema:add-sg-and-subnet-to-scanaccountandregion from pg

BEGIN;

alter table scanaccountandregion 
drop column if exists securitygroupids,
drop column if exists subnetids;

COMMIT;
