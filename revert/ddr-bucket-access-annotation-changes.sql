BEGIN;

ALTER TABLE ddr_bucket_access DROP COLUMN noaccessreason;
ALTER TABLE ddr_cloudtrail_coverage DROP COLUMN noaccessreason;

COMMIT;
