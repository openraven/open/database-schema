-- Revert database-schema:scan-audit-log-events-target-length from pg

BEGIN;

alter table scanauditlogevent alter COLUMN scantarget type VARCHAR(255);
alter table scanauditlogevent alter COLUMN childtarget type VARCHAR(255);

COMMIT;
