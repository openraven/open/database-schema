#!/bin/bash

if [ $# -ne 2 ];then
	echo "Usage: "
	echo "./db-schema.sh <CHANGE_NAME> <NOTE>"
	echo "./db-schema.sh add-new-table 'Adds Table: Example'"
	exit 0
fi

./sqitch add "${1}" --note "${2}"
